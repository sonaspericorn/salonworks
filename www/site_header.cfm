<!doctype html>
<html class="no-js" lang="en">
    <cfset strPath = "">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
        <meta name="google-signin-client_id" content="426650001236-lmr4di68vdbsvpauh9e4n7b0oglvbcij.apps.googleusercontent.com">
        <meta name="format-detection" content="telephone=no">
        <meta name="SalonWorks" content="Salonworks ,online Salon appointment booking website,  Customised  online salon booking, Manage salon appointments online, online appointment availability checking,  Email notification of online booking, hair and spa appt booking,Salon website">
        <meta name="keywords" content="Salonworks ,online Salon appointment booking website,  Customised  online salon booking, Manage salon appointments online, online appointment availability checking,  Email notification of online booking, hair and spa appt booking,Salon website">
        <meta name="description" content="Salonworks ,online Salon appointment booking website,  Customised  online salon booking, Manage salon appointments online, online appointment availability checking,  Email notification of online booking, hair and spa appt booking,Salon website">
        <meta name="robots" content="index,nofollow">
        <title>SalonWorks</title>
        <link rel="icon" type="image/x-icon" href="salonnewhome/img/favicon.png" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="salonnewhome/css/bootstrap/bootstrap.css">
        <link rel="stylesheet" href="salonnewhome/css/main.css">
        <link rel="stylesheet" href="salonnewhome/css/cutom.css">
        <link rel="stylesheet" href="css/blog.css">    
        <link rel="stylesheet" href="salonnewhome/css/slick.css">
        <link rel="stylesheet" href="salonnewhome/css/fontello.css">
        <link rel="stylesheet" href="salonnewhome/css/magnific-popup.css">
        <link href="https://fonts.googleapis.com/css?family=Hind+Vadodara:300,400,500,600|Poppins:100,200,300,400,500,600,700,900" rel="stylesheet">


        <!--- old  --->
       
        <!--- <link rel="stylesheet" href="<cfoutput>#strPath#</cfoutput>css/bootstrap.css"> --->
        <link rel="stylesheet" href="<cfoutput>#strPath#</cfoutput>css/main.css">
        <link rel="stylesheet" href="<cfoutput>#strPath#</cfoutput>css/animations.css">
        <link rel="stylesheet" href="<cfoutput>#strPath#</cfoutput>css/animate.min.css">
        <link rel="stylesheet" href="css/editor.css">
    
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <script async type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
       <!---  <script src="<cfoutput>#strPath#</cfoutput>js/vendor/modernizr-2.8.3.min.js"></script> --->
        <script defer="true" src="salonnewhome/js/vendor/modernizr-2.8.3.min.js"></script>
        <script defer="true" src="https://www.google.com/recaptcha/api.js" ></script>
       <!---  <script type="text/javascript" src="js/common_validations.js"></script>  --->
       <!---  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script> --->
        
        <script   src="js/jquery.mask.js"></script> 
        <!--- <script type="text/javascript" src="<cfoutput>#strPath#</cfoutput>js/jquery.validate.additional-methods.min.js"></script> ---> 
        <script async language="javascript" src="js/indexscript.js"></script>
        <!--- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script> --->
        <!--- <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script> --->
        <!--- SVG sprites --->
        <script src="js/svg4everybody.min.js"></script>
        <script> svg4everybody(); </script>
        <script >

          $('.phone_us').mask('(000) 000-0000');
        </script>
        <!--- Install google tag manager --->
     <!-- Google Tag Manager -->
        <script >
          (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-K86HR56');
      </script>
          <!--- end --->

          <style type="text/css">
      .col-sm-2 {width:200px;}
      textarea.error, input.error {
      border: 1px solid #FF0000;
      }
      .erroron_form label.error {
      color: #FF0000 !important;
      margin-left: 0px;
      width: auto;
      display: inline;
      /* margin-top: -8px;*/
      display: inline-block;
      float: left;
      width: 100%;
      }
      a.anchor{margin-top: -100px;
      padding-top: 125px;}
      .address, .city  {
      text-transform:capitalize;
      }
      .eml {display:none;}
      }
      /*Stle for email demo*/
       .form-button {
         position: fixed;
         right: 0px;
         bottom: 10px;
         width: 73px;
         height: 400px;
         z-index: 1400;
         cursor: pointer;
       }
         
      .form-button .box .textWrapper {
          width: 0px;
          height: 0px;
          position: relative;
          top: 96px;
          left: -38px;
      }
      .form-button .box {
          float: left;
          width: 73px;
          height: 223px;
          background: #e31e25;
      }
      .form-button .box .text {
          font-family: bebas-neue,Helvetica Neue,helvetica,arial,sans-serif;
          letter-spacing: 1px;
          width: 150px;
          text-align: center;
          color: #ffffff;
          -webkit-transform: rotate(270deg) translate(0px,0px);
          -moz-transform: rotate(270deg) translate(0px,0px);
          -o-transform: rotate(270deg) translate(0px,0px);
          -ms-transform: rotate(270deg) translate(0px,0px);
          transform: rotate(270deg) translate(0px,0px);
          font-size: 22px;
          display: block;
      }

      .abcRioButton {
        height: 29px !important;
        width: 190px !important;
        border: 1px solid #bbb !important;
        border-radius: 5px !important;
      }
      .abcRioButtonSvgImageWithFallback.abcRioButtonIconImage.abcRioButtonIconImage18 {
            margin-top: -3px !important;
      }
      span.abcRioButtonContents{
            line-height: 27px !important;
      }
      
   </style>
          
    </head>
    <cfset variables.objMailgun =  createObject("component","cfc.mailgun") />
    <body>
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <header>
            <div class="min-hd">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="top-cnt-scnt">
                                <ul>
                                    <li><svg class="icon"><use xlink:href="img/svg-sprite.svg#icon_clock"/></svg>Mon - Fri: 8:00AM-5:00PM CST</li>
                                    <li><svg class="icon"><use xlink:href="img/svg-sprite.svg#icon_call"/></svg><a href="tel:(978) 352-0235">(978) 352-0235</a></li>
                                    <li><svg class="icon"><use xlink:href="img/svg-sprite.svg#icon_mail"/></svg><a href="mailto:salonworks@salonworks.com">salonworks@salonworks.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-md-3">
                        <div class="logo"><a href=""><img src="salonnewhome/img/logo.png" alt=""></a></div>
                    </div>
                    <div class="col-xl-9 col-md-9">
           <div class="flx-algin">
                                <div class="menu-wrp">
                                    <!--  Nav start here -->
                                        <nav class="main-nav">
                                            <ul>
                                                <li>
                                                    <a class="nav-link<cfif cgi.SCRIPT_NAME eq '/index.cfm'> active</cfif>" href="index.cfm" id="home">Home <span class="sr-only">(current)</span></a>
                                                </li>
                                                <li>
                                                    <a class="nav-link<cfif cgi.SCRIPT_NAME eq '/features.cfm'> active</cfif>" href="features.cfm" id="featureslink">Features</a>
                                                </li>
                                                <li>
                                                    <a class="nav-link <cfif cgi.SCRIPT_NAME eq '/pricing.cfm'> active</cfif>" href="pricing.cfm">Pricing</a>
                                                </li>  
                                                <li>
                                                    <a class="nav-link <cfif cgi.SCRIPT_NAME eq '/blogData.cfm'> active</cfif>" href="blogData.cfm">Blog</a>
                                                </li>                                   
                                                <li>
                                                  <a class="nav-link" href="" data-toggle="modal" data-target="#modalSupport">Support</a>
                                                </li>
                                                <li>
                                                    <a class="nav-link" href="http://salonworks.com/demo" target="new">Demo Site </a>
                                                </li>
                                          
                                                <li>
                                                    <a class="nav-link clr-rd line" onclick="$('html').removeClass('show-menu');" href="index.cfm#signupform" >SIGN-UP</a>
                                                </li>
                                                <li>
                                                    <a class="nav-link clr-rd" href="" data-toggle="modal" data-target="#modalLogin">LOGIN</a>
                                                </li>
                                       
                                              </ul>
                                        </nav>
                                    <div class="mob-btn">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                    <div class="overlay"></div>
                                    <!-- Nav End here -->
                                </div>

                    <div class="search-otr">
                        <form class="searchbox">
                            <input type="search" placeholder="Search..." name="search" class="searchbox-input" onkeyup="buttonUp();" required>
                            
                            <span class="searchbox-icon"></span>
                        </form>
                    </div>
 </div>
                </div>

            </div>
        </div>
    </header>