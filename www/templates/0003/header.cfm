<cfoutput>
	<div class="row-fluid top-banner-content">
		<div class="container outer-container-layout">
			<div class="row top-banner-row">
				<div class="col-10">					
					<h1>Welcome to #qCompany.COMPANY_NAME#</h1>					
					<p class="banner-label">#qCompany.COMPANY_DESCRIPTION#</p>					
					<div id="galleryDiv"></div>
					<div id="blogDiv"></div>
					<div id="contactDiv"></div>	
					<div id="appointmentDiv"></div>	
					<div id="bookingDiv"></div>				
				</div>
			</div>
		</div>
	</div>
</cfoutput>