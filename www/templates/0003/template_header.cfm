<!doctype html>
<html class="no-js" lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="google-signin-client_id" content="426650001236-lmr4di68vdbsvpauh9e4n7b0oglvbcij.apps.googleusercontent.com">
		<title>Index</title>
		
		<link rel="icon" type="image/x-icon" href="<cfoutput>#templatePath#img/favicon.ico</cfoutput>" />
		<link rel="stylesheet" href="<cfoutput>#templatePath#css/bootstrap.css</cfoutput>">
		<link rel="stylesheet" href="<cfoutput>#templatePath#css/main.css</cfoutput>">
		<link rel="stylesheet" href="<cfoutput>#templatePath#css/font-awesome.css</cfoutput>">
		<link rel="stylesheet" href="<cfoutput>#templatePath#css/owl.theme.default.min.css</cfoutput>">
		<link rel="stylesheet" href="<cfoutput>#templatePath#css/owl.carousel.min.css</cfoutput>">
		<link rel="stylesheet" href="<cfoutput>#templatePath#fonts/font.css</cfoutput>">
		<link rel="stylesheet" href="<cfoutput>#templatePath#css/custom.css</cfoutput>">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="/jquery-ui-1.8.21/themes/smoothness/jquery.ui.all.css">	
		<link rel="stylesheet" type="text/css" href="/jquery-ui-1.8.21/css/smoothness/jquery-ui-1.8.21.custom.css"/>
		<!--- <link rel="stylesheet" type="text/css" href="/css/jquery.ui.datepicker.css"/> --->
		<script src="<cfoutput>#templatePath#js/vendor/jquery-3.3.1.slim.min.js</cfoutput>"></script>
		<script src="<cfoutput>#templatePath#js/jquery-migrate-1.2.1.min.js</cfoutput>"></script> 
		<script src="<cfoutput>#templatePath#js/jquery.validate.js</cfoutput>"></script>


		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		
		<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    	<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.min.js"></script>
		<script src="<cfoutput>#templatePath#js/vendor/bootstrap.min.js</cfoutput>"></script>
		<style>
			.sticky {
			  position: fixed;
			  top: 0;
			  width: 100%;
		      background-color: #000000;
			}
			.hideDiv{			
			    display: none;
			}
			.profileLink{
				background-color:#252525;
			}
			.profileMenu {
				    position: absolute;
				    top: 57px;
				    left: -73%;
			}
			.customGoogleBtn{
	            border: 1px solid #eee;
			    height: 45px;
			    width: 350px;
			    border-radius: 4px;
			    padding:4px 26px 0px 20px;
			    font-size: 15px;
			    color: white;
			    box-shadow: 0 2px 4px 0 rgba(0,0,0,.25);
			    background-color: #fff;
			    cursor: pointer;
			    font-family: Helvetica, Arial, sans-serif;
			    letter-spacing: .25px;
			    overflow: hidden;
			    white-space: nowrap;
			    background: #dd4b39;
			}
			#loggingButton{
			  	border: 1px solid #eee;
			    height: 45px;
			    width: 350px;
			    border-radius: 4px;
			    padding:4px 26px 0px 20px;
			    font-size: 15px;
			    color: #fff;
			    box-shadow: 0 2px 4px 0 rgba(0,0,0,.25);
			    background-color: #3B5998;
			    cursor: pointer;
			    font-family: Helvetica, Arial, sans-serif;
			    letter-spacing: .25px;
			    overflow: hidden;
			    white-space: nowrap;
			    margin-bottom: 20px;
			}
			  .clearfix {
			  	clear: both;
			  }

		   	.fbLoginDiv{
		  		text-align: center;
			}

		  	.customGoogleBtnDiv{
				text-align: center;
		  	}
			  .socialMedia {
			  	text-align: center;
			  }
			  .sendme{
			  	width: 125px;
			    text-transform: uppercase;
			    padding: 10px 15px;
			    border: 1px solid #ddd;
			    background-color: #000;
			    border-color: #e2caa1;
			    color: #e2caa1;
			    position: relative;
			    z-index: 1;
			    font-weight: 600;
			    overflow: hidden;
			  }

		  	.orDiv{
				margin: 32px 0 10px;
			}

			.orDiv-line{
				height: 1px;
				background-color: #ccc;
			}

			.orDiv-text{
				width: 20%;
			    font-size: 16px;
			    background-color: #fff;
			    position: relative;
			    top: -11px;
			    left: 40%;
			    text-align: center;
			}

			.fa-facebook {
			  background: #3B5998;
			  color: white;
			  font-size: 20px;
			  float: left;
			}

			.fa-google{
		 		background: #dd4b39;
				color: white;
				font-size: 20px;
			  	float: left;
			}
			.forgotPassLink{
				text-align: center;
		  		margin-top: 20px;
			}
			.btnSignin{
			   margin-top: 20px;
			}
		  	@media (max-width: 460px) {
				.customGoogleBtnDiv, .fbLoginDiv {
				    width:80% !important;
				    margin: 15px auto;
				    float: none !important; 
				    text-align: center !important;
			    } 
			    #loggingButton, .customGoogleBtn {
					width: 100% !important;
					padding: 0 !important;
					height: 35px !important;
			    }
			}
		</style>
	</head>
	<body>
		<cfinclude template="menu.cfm">
		<!-- Sigin Modal -->
		<div class="modal sigin-modal fade" id="signinModal" tabindex="-1" role="dialog" aria-labelledby="signInModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<ul class="nav nav-tabs" role="tablist">
					  <li class="nav-item" style="width: 50%;text-align: center;font-size:17px;">
					    <a class="nav-link active" href="#login" role="tab" data-toggle="tab">Log In</a>
					  </li>
					  <li class="nav-item" style="width: 50%;text-align: center;font-size:17px;">
					    <a class="nav-link" href="#signup" role="tab" data-toggle="tab">Sign Up<span class="close" data-dismiss="modal" aria-hidden="true">&times;</span></a>
					  </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
					  <div role="tabpanel" class="tab-pane active" id="login">
					  	<div class="signin-content">
							<form name="frmSignin" id="frmSignin" role="form" class="form-horizontal">
								<cfoutput>
									<input type="hidden" id="qCompanyId" name="companyid" value="#session.company_id#" />
								</cfoutput>
								 <div class="fbLoginDiv">
			                       <button id="loggingButton" onclick="return facebooklogin();">
			                       	<a class="fa fa-facebook"></a>
			                       	Log in with Facebook</button>
			                    </div>
				                <div class="customGoogleBtnDiv">
			                      <button class="customGoogleBtn">
								  <a class="fa fa-google"></a>
			                      Log in with Google</button>
			                      	<div class="hide">
				                      	<div id="customBtn" class="customGPlusSignIn">
									    </div>
								    </div>
			                    </div>
			                    <div class="orDiv">
						          <div class="orDiv-line"></div>
						          <div class="orDiv-text">OR</div>
						        </div>
								<div class="form-group">
									<input type="email" id="emailAddress" name="emailAddress" class="signin-field" placeholder="Your email address" required maxlength="50" />		
								</div>
								<div class="form-group">
									<input class="signin-field" id="pw" name="pw" type="password" required maxlength="50" placeholder="Your password">
								</div>
								<div id="signin-msg"></div>
								<div class="forgotPassLink"><a href="" data-toggle="modal" data-target="#resetPassModal" data-dismiss="modal">Forgot my password</a></div>
								<div class="form-group">
									<button type="button" class="signin btnSignin">Log in</button>	
								</div>
							</form>
						</div>
					  </div>
					  <div role="tabpanel" class="tab-pane" id="signup">
						<div class="signin-content">
							<form name="frmRegister" id="frmRegister" role="form" class="form-horizontal">
								<input type="hidden" id="companyId" value="<cfoutput>#session.company_id#</cfoutput>"/>
								<div class="fbLoginDiv">
			                       <button id="loggingButton" onclick="return facebooklogin();">
			                       <a class="fa fa-facebook"></a>
			                   	   Sign up with Facebook</button>
			                    </div>
				                <div class="customGoogleBtnDiv">
			                      <button class="customGoogleBtn">
								  <a class="fa fa-google"></a>
			                      Sign up with Google</button>
			                      	<div class="hide">
				                      	<div id="customBtn" class="customGPlusSignIn">
									    </div>
								    </div>
			                    </div>
			                    <div class="orDiv">
						          <div class="orDiv-line"></div>
						          <div class="orDiv-text">OR</div>
						        </div>
								<div class="form-group">
									<input type="text" id="firstName" name="firstName" class="signin-field required" placeholder="Your first Name" required maxlength="50"/>		
								</div>
								<div class="form-group">
									<input type="text" id="lastName" name="lastName" class="signin-field required" placeholder="Your last Name" required maxlength="50"/>		
								</div>
								<div class="form-group">
									<input type="email" name="emailAddress" class="signin-field emailAddress required" placeholder="Your email address" required maxlength="100"/>		
								</div>
								<div class="form-group">
									<input class="signin-field required" id="ph" name="ph" type="text" placeholder="Your mobile Number"required maxlength="12"/><br>
									<span style="padding-left: 2px;">Example: 512-753-0000</span>
								</div>
								<div class="form-group">
									<input  class="signin-field required" type="password" id="pw" placeholder="choose a password" name="pw"  required maxlength="20" />
								</div>
								<div id="register-msg" class="form-group"></div>
								<div class="modal-footer">
									<button type="button" class="signin" id="btnRegister">Continue</button>		
								</div>
							</form>
						</div>
					  </div>
					</div>

					<!--- <div class="modal-header">
						<h5 class="modal-title" id="signInModalLabel">Sign In</h5>
						<button type="button" class="close signin-close-btn" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="signin-content">
							<form name="frmSignin" id="frmSignin" role="form" class="form-horizontal">
								<cfoutput>
									<input type="hidden" id="qCompanyId" name="companyid" value="#session.company_id#" />
								</cfoutput>
								<div class="form-group">
									<input type="email" id="emailAddress" name="emailAddress" class="signin-field" placeholder="Your email address" required maxlength="50" />		
								</div>
								<div class="form-group">
									<input class="signin-field" id="pw" name="pw" type="password" required maxlength="50" placeholder="Your password">
								</div>
								<div id="signin-msg"></div>
								<div class="form-group">
									<button type="button" class="signin btnSignin">Log in</button>										
								</div>
							</form>
						</div>
						
						<div class="clearfix" ></div>
						<div class="socialMedia">
		                    <div class="fbLoginDiv">
		                       <button id="loggingButton" onclick="return facebooklogin();">Log in with Facebook</button>
		                    </div>
		                    <div class="customGoogleBtnDiv">
		                      <button class="customGoogleBtn">Log in with Google</button>
		                      	<div class="hide">
			                      	<div id="customBtn" class="customGPlusSignIn">
								    </div>
							    </div>
		                    </div>
		                </div>
					</div>
					<div class="modal-footer">
						<style>
						.forgotPassLink{margin-right: 388px;text-decoration: underline;}
						</style>
						<div class="forgotPassLink"><a href="" data-toggle="modal" data-target="#resetPassModal" data-dismiss="modal">Forgot my password</a></div>
						<p>New member?</p>
						<button type="button"  href="#" class="btn signin-footer-btn btn-secondary" data-target="#registerModal" data-toggle="modal" data-dismiss="modal">Register</button>

					</div> --->
				</div>
			</div>
		</div>
		<!-- Signin Modal end -->


		<cfset resetPassKey = "" />
		<cfif isDefined('url.reGenPass') >
			<cfset resetPassKey=url.reGenPass>
		</cfif>

		<cfif structKeyExists(url, "reGenPass")>
			<input type="hidden" value="1" id="reGenHid">
		</cfif>
		  <!-- Modal -->
		  <div class="modal fade" id="resetPassModal" role="dialog">
		    <div class="modal-dialog">
		    	<style>
		    	.hg{
		    		height: 380px;
		    	}
		    	.ht{height:200px;} .btnForPass{margin: 40px;}
		    	</style>
		    
		      <!-- Modal content-->
		      <div class="modal-content hg">
		        <div class="modal-header">
		          
		          <h4 class="modal-title">Retrieve Password</h4>
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		        </div>
		        <div class="modal-body ht">
		            <p>Enter your email to receive instructions</p>
		            <form action="#" method="POST" id="forgot_form">
			            <div class="">
			          		<input type="email" class="form-control" name="Email_Address_forgot" id="forgotPassEmail" placeholder="Email">
			            </div>
			            <div class="btnForPass">
				            <button type="button" class="width-35 pull-right sendme" id="forgotPassSubmit">
								<i class="icon-lightbulb"></i>
									Send Me!
							</button>
							<p class="error_box_forgot" style="color:red;"></p>
						</div>
					</form>
		        </div>
		        <div class="modal-footer"> 	
		         <!---  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --->
		        </div>
		      </div>
		      
		    </div>
		  </div>

		 <!--- modat reset password --->
		  <!-- Modal -->
		<cfif structKeyExists(url, "reGenPass")>
			<cfquery name="getPasswordReset" datasource="#request.dsn#" result="rPasswordResetkey">
				SELECT pwdReset FROM customers 
				WHERE resetPassKey = <cfqueryparam value="#url.reGenPass#" cfsqltype="cf_sql_varchar">
			</cfquery>
			<div class="modal fade" id="resetPasswordModal" role="dialog">
				<div class="modal-dialog">
					<style>
					.hg{
					height: 380px;
					}
					.ht{height:200px;} .btnForPass{margin: 40px;} .pwd{width:300px;} .fieldsReset{margin-left: 100px;} .fields{padding: 5px;} .btn-reset{margin-right: 120px;}
					</style>

					<!-- Modal content-->
					<div class="modal-content hg">
						<div class="modal-header">
							
							<h4 class="modal-title">Reset Password</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<cfif getPasswordReset.RecordCount and getPasswordReset.pwdReset eq 1>
							<style>.linkExpired{position: absolute;left: 50px;top: 108px;}</style>
							<div class="linkExpired">The current url is no longer valid. Kindly contact web admin for any support</div>
						<cfelse>
							<div class="modal-body ht">
								<form action="#" method="POST" id="forgot_form">
								<div class="fieldsReset">
									<div class="fields">
										<input type="password" class="form-control pwd" name="password" id="password" placeholder="Password">
									</div>
									<div class="fields">
										<input type="password" class="form-control pwd" name="confirmPassword" id="confirmPassword" placeholder="Confirm Password">
										<input type="hidden" id="resetPassKey" name="resetPassKey" value="<cfoutput>#resetPassKey#</cfoutput>">
										<i class="icon-password"></i>
										<div id="passwordError"></div>
									</div>
									<div class="btnForPass">
										<button type="button" class="width-35 pull-right btn btn-sm sendme btn-reset" id="resetPasswordBtn" onclick="checkPassword()">
										<i class="icon-lightbulb"></i>
										Reset!
										</button>
										<style>.successMsg{display:none;}.errorMsg{display:none;color:red;}</style>
										<div class="successMsg" id="successMsg">Password reset successfully</div>
										<div class="errorMsg" id="errorMsg">Please fill all the fields</div>
										<p class="error_box_forgot" style="color:red;"></p>
									</div>
									<!--- <div><a href="index.cfm">Back to login</div> --->
									<!--- <a data-target="#signinModal" data-toggle="modal" class="MainNavText" id="MainNavHelp" 
									href="#signinModal">Back to login</a> --->
									</div>
								</form>
							</div>
						</cfif>
						<div class="modal-footer"> 	
						<!---  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --->
						</div>
					</div>
				</div>
			</div>
		</cfif>
		<!--- /modal reset password --->

		<!-- registration Modal -->
		<div class="modal sigin-modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="signInModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="signInModalLabel">Sign up for an account</h5>
						<button type="button" class="close signin-close-btn" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="signin-content">
							<form name="frmRegister" id="frmRegister" role="form" class="form-horizontal">
								<input type="hidden" id="companyId" value="<cfoutput>#session.company_id#</cfoutput>"/>

								<div class="form-group">
									<input type="text" id="firstName" name="firstName" class="signin-field required" placeholder="Your first Name" required maxlength="50"/>		
								<!--- </div>
								<div class="form-group"> --->
									<input type="text" id="lastName" name="lastName" class="signin-field required" placeholder="Your last Name" required maxlength="50"/>		
								<!--- </div>
								<div class="form-group"> --->
									<input type="email" name="emailAddress" class="signin-field emailAddress required" placeholder="Your email address" required maxlength="100"/>		
								<!--- </div>
								<div class="form-group"> --->
									<input class="signin-field required" id="ph" name="ph" type="text" placeholder="Your mobile Number"required maxlength="12"/>
									Example: 512-753-0000
								<!--- </div>
								<div class="form-group"> --->
									<input  class="signin-field required" type="password" id="pw" placeholder="choose a password" name="pw"  required maxlength="20" />
								</div>
								<div id="register-msg" class="form-group"></div>
								<div class="modal-footer">
									<button type="button" class="signin" id="btnRegister">Continue</button>
									<button type="button" class="signin" data-dismiss="modal">Close</button>		
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- registration Modal end -->
		<script>
		function checkPassword() {
	      var password = $('#password').val();
	      var confirmPassword = $('#confirmPassword').val(); 
	      if(password != "" && confirmPassword != "") {    
		      if(password == confirmPassword) {
					$.ajax({
						// console.log(1);
						type: "POST",
						url: "/cfc/customers.cfc",
						data: {
							method: "resetPassword",
							password: $('#password').val(),
							confirmPassword : $('#confirmPassword').val(),
							resetPassKey : $('#resetPassKey').val(),
							noCache: new Date().getTime()
							},
						dataType: "json",

						// Define request handlers.
						success: function( objResponse ){		

							// $("#successMsg").css("display","block");
							$("#signinModal").modal('show');
							$("#resetPasswordModal").modal('hide');
							// location.href = "login.cfm";
						}
					

						});
							
		      }
		      else {
		          $("#passwordStatus").val(1);    
		          $('#passwordError').empty();    
		          $('#password').css('border-color', 'red');
		          $('#confirmPassword').css('border-color', 'red');
		          $("#errorMsg").css("display","none");
		          $("#passwordError").append("Password does not match with confirm password ");
		      }
	  		}else{
	  			//alert('please enter password and confirm password.');
	  			$("#errorMsg").css("display","block");
		  		return false;   
	  		}
	  }



		$('#forgotPassSubmit').on('click', function(){
			var email =  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
				$('.error_box_forgot').html("");
				$('.error_box_forgot').hide();
				var subdomain = "<cfoutput>#variables.subdomain#</cfoutput>";
		         var email_user=$.trim($('#forgotPassEmail').val());
		         console.log(email);
		          if(email_user.length ==0) {
		          	 $('.error_box_forgot').html("Email is required");
		          	 $('.error_box_forgot').show();
		          }else if(!email.test(email_user)) {
		          	$('.error_box_forgot').html("Email is not valid");
		          	$('.error_box_forgot').show();
		          }
		         else{
		         	$('.error_box_forgot').html("");
		         	$('.error_box_forgot').hide();
		            $.ajax({
		               url: "/cfc/customers.cfc?method=forgotPasswordCust&showtemplate=false",
		               type: 'POST',
		               data:{email:email_user,subdomain:subdomain},
		               success: function(data){
		                  console.log(data);
		                  if(data==1){
		                  	alert("Your password reset link has been send to your email address");
		                  	$('#forgot_form')[0].reset();
		                  	$('#resetPassModal').modal('hide');
		                  }
		                  else{

		                     alert("Invalid email address.Make sure this is your registered email address");
		                  }
		               },
		            });
		         }
			    });
			$(document).ready(function(){
				var restVal = $("#reGenHid").val();
				if(restVal == 1){
					$("#resetPasswordModal").modal('show');
				}

			});
			function onLoad() {
			    gapi.load('auth2', function() {
			        gapi.auth2.init();
			    });
			}
		</script>
		<script type="text/javascript">
	    	var subDomain = "<cfoutput>#application.subdomain#</cfoutput>";
	  	</script>

		<script src="/js/socialLogin.js"></script>
    	<script src="https://apis.google.com/js/api:client.js"></script>


			
		
		

	

		