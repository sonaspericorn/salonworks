<footer>
	<div class="container">
		<div class="row">
		
			<!--- <div class="col-sm-4">
				<div class="headline"><h3>Subscribe</h3></div>
				<div class="content">
					<p>Enter your e-mail below to subscribe to our free newsletter.<br />We promise not to bother you often!</p>
					<form class="form" role="form">
					<div class="row">
					<div class="col-sm-8">
					<div class="input-group">
					<label class="sr-only" for="subscribe-email">Email address</label>
					<input type="email" class="form-control" id="subscribe-email" placeholder="Enter email">
					<span class="input-group-btn">
					<button type="submit" class="btn btn-default">OK</button>
					</span>
					</div>
					</div>
					</div>
					</form>
				</div>
			</div> --->
			
			<div class="col-sm-6">
				<div class="headline"><h3>Contact us</h3></div>
				<div class="content">
					<cfinclude template="/customer_sites/include_contact_info.cfm">
					<div class="clearfix"></div>
				</div>
			</div>

			<div class="col-sm-6">
				<div class="headline"><h3>Go Social</h3></div>
				<div class="content social">
					<cfinclude template="/customer_sites/include_social_media.cfm">
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</footer>