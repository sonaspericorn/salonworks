
<!doctype html>
<html class="no-js" lang="en">
    <cfset strPath = "">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
        <meta name="format-detection" content="telephone=no">
        <title>Home</title>
        <link rel="icon" type="image/x-icon" href="salonnewhome/img/favicon.png" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="salonnewhome/css/bootstrap/bootstrap.css">
        <link rel="stylesheet" href="salonnewhome/css/main.css">
        <link rel="stylesheet" href="salonnewhome/css/cutom.css">
        <link rel="stylesheet" href="salonnewhome/css/slick.css">
        <link rel="stylesheet" href="salonnewhome/css/fontello.css">
        <link rel="stylesheet" href="salonnewhome/css/magnific-popup.css">
        <link href="https://fonts.googleapis.com/css?family=Hind+Vadodara:300,400,500,600|Poppins:100,200,300,400,500,600,700,900" rel="stylesheet">
        <script src="salonnewhome/js/vendor/modernizr-2.8.3.min.js"></script>

        <!--- old  --->
       
        <!--- <link rel="stylesheet" href="<cfoutput>#strPath#</cfoutput>css/bootstrap.css"> --->
        <link rel="stylesheet" href="<cfoutput>#strPath#</cfoutput>css/main.css">
        <link rel="stylesheet" href="<cfoutput>#strPath#</cfoutput>css/animations.css">
        <link rel="stylesheet" href="<cfoutput>#strPath#</cfoutput>css/animate.min.css">
        <link rel="stylesheet" href="css/editor.css">
    
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="<cfoutput>#strPath#</cfoutput>js/vendor/modernizr-2.8.3.min.js"></script>
        <script type="text/javascript" src="js/common_validations.js"></script> 
         <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
          <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.min.js"></script>
         
          <script type="text/javascript" src="<cfoutput>#strPath#</cfoutput>js/jquery.validate.additional-methods.min.js"></script> 
           <script language="javascript" src="js/indexscript.js"></script>
          <!--- end --->

          <style type="text/css">
      .col-sm-2 {width:200px;}
      textarea.error, input.error {
      border: 1px solid #FF0000;
      }
      .erroron_form label.error {
      color: #FF0000 !important;
      margin-left: 0px;
      width: auto;
      display: inline;
      /* margin-top: -8px;*/
      display: inline-block;
      float: left;
      width: 100%;
      }
      a.anchor{margin-top: -100px;
      padding-top: 125px;}
      .address, .city  {
      text-transform:capitalize;
      }
      .eml {display:none;}
      }
      /*Stle for email demo*/
       .form-button {
         position: fixed;
         right: 0px;
         bottom: 10px;
         width: 73px;
         height: 400px;
         z-index: 1400;
         cursor: pointer;
       }
         
      .form-button .box .textWrapper {
          width: 0px;
          height: 0px;
          position: relative;
          top: 96px;
          left: -38px;
      }
      .form-button .box {
          float: left;
          width: 73px;
          height: 223px;
          background: #e31e25;
      }
      .form-button .box .text {
          font-family: bebas-neue,Helvetica Neue,helvetica,arial,sans-serif;
          letter-spacing: 1px;
          width: 150px;
          text-align: center;
          color: #ffffff;
          -webkit-transform: rotate(270deg) translate(0px,0px);
          -moz-transform: rotate(270deg) translate(0px,0px);
          -o-transform: rotate(270deg) translate(0px,0px);
          -ms-transform: rotate(270deg) translate(0px,0px);
          transform: rotate(270deg) translate(0px,0px);
          font-size: 22px;
          display: block;
      }
   </style>
          
    </head>
    <body>
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <header>
            <div class="min-hd">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="top-cnt-scnt">
                                <ul>
                                    <li><i><img src="salonnewhome/img/icon-clock.png" alt=""></i>Mon - Fri: 8:00AM-5:00PM CST</li>
                                    <li><i><img src="salonnewhome/img/icon-call.png" alt=""></i><a href="tel:(978) 352-0235">(978) 352-0235</a></li>
                                    <li><i><img src="salonnewhome/img/icon-mail.png" alt=""></i><a href="mailto:salonworks@salonworks.com">salonworks@salonworks.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-md-3">
                        <div class="logo"><a href=""><img src="salonnewhome/img/logo.png" alt=""></a></div>
                    </div>
                    <div class="col-xl-9 col-md-9">
           <div class="flx-algin">
                                <div class="menu-wrp">
                                    <!--  Nav start here -->
                                        <nav class="main-nav">
                                            <ul>
                                                <li>
                                                    <a class="nav-link active" href="#" id="home">Home <span class="sr-only">(current)</span></a>
                                                </li>
                                                <li>
                                                    <a class="nav-link" href="features.cfm" id="featureslink">Features</a>
                                                </li>
                                                <li>
                                                    <a class="nav-link" href="#">Pricing</a>
                                                </li>                                    <li>
                                                <a class="nav-link" href="" data-toggle="modal" data-target="#modalSupport">Support</a>
                                            </li>
                                            <li>
                                                <a class="nav-link" href="http://demo.salonworks.com">Demo Site </a>
                                            </li>
                                      
                                            <li>
                                                <a class="nav-link clr-rd line" href="index.cfm#signupform" >SIGN-UP</a>
                                            </li>
                                            <li>
                                                <a class="nav-link clr-rd" href="" data-toggle="modal" data-target="#modalLogin">LOGIN</a>
                                            </li>
                                       
                                        </ul>
                                        </nav>
                                    <div class="mob-btn">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                    <div class="overlay"></div>
                                    <!-- Nav End here -->
                                </div>

                    <div class="search-otr">
                        <form class="searchbox">
                            <input type="search" placeholder="Search..." name="search" class="searchbox-input" onkeyup="buttonUp();" required>
                            
                            <span class="searchbox-icon"></span>
                        </form>
                    </div>
 </div>
                </div>

            </div>
        </div>
    </header>