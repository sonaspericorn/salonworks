<cfset variables.objMailgun =  createObject("component","cfc.mailgun") />
<cfif not ListContains('99.103.70.43,107.194.73.65,75.103.109.211,220.227.196.145,13.58.245.95,127.0.0.1,202.88.234.192',cgi.REMOTE_ADDR)><cfabort></cfif> 
<cfset local.utcTime = DateConvert('local2Utc', now()) >
<cfset dateh = DateAdd('h',24,local.utcTime )>
<cfset date = DateAdd('n',59,dateh) >
<!--- Function for calendar attachment --->
<cfscript>
	function iCalUS(stEvent) {
	    var vCal = "";
	    var CRLF=chr(13)&chr(10);
	    var date_now = Now();
	        
	    if (NOT IsDefined("stEvent.organizerName"))
	        stEvent.organizerName = "Organizer Name";
	        
	    if (NOT IsDefined("stEvent.organizerEmail"))
	        stEvent.organizerEmail = "Organizer_Name@CFLIB.ORG";
	                
	    if (NOT IsDefined("stEvent.subject"))
	        stEvent.subject = "Event subject goes here";
	        
	    if (NOT IsDefined("stEvent.location"))
	        stEvent.location = "Event location goes here";
	    
	    if (NOT IsDefined("stEvent.description"))
	        stEvent.description = "Event description goes here\n---------------------------\nProvide the complete event details\n\nUse backslash+n sequences for newlines.";
	        
	    if (NOT IsDefined("stEvent.startTime"))  // This value must be in Eastern time!!!
	        stEvent.startTime = ParseDateTime("3/21/2008 14:30");  // Example start time is 21-March-2008 2:30 PM Eastern
	    
	    if (NOT IsDefined("stEvent.endTime"))
	        stEvent.endTime = ParseDateTime("3/21/2008 15:30");  // Example end time is 21-March-2008 3:30 PM Eastern
	        
	    if (NOT IsDefined("stEvent.priority"))
	        stEvent.priority = "1";
	        
	    vCal = "BEGIN:VCALENDAR" & CRLF;
	    vCal = vCal & "PRODID: -//CFLIB.ORG//iCalUS()//EN" & CRLF;
	    vCal = vCal & "VERSION:2.0" & CRLF;
	    vCal = vCal & "METHOD:REQUEST" & CRLF;
	    vCal = vCal & "BEGIN:VTIMEZONE" & CRLF;
	    vCal = vCal & "TZID:Eastern Time" & CRLF;
	    vCal = vCal & "BEGIN:STANDARD" & CRLF;
	    vCal = vCal & "DTSTART:20061101T020000" & CRLF;
	    vCal = vCal & "RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=1SU;BYMONTH=11" & CRLF;
	    vCal = vCal & "TZOFFSETFROM:-0400" & CRLF;
	    vCal = vCal & "TZOFFSETTO:-0500" & CRLF;
	    vCal = vCal & "TZNAME:Standard Time" & CRLF;
	    vCal = vCal & "END:STANDARD" & CRLF;
	    vCal = vCal & "BEGIN:DAYLIGHT" & CRLF;
	    vCal = vCal & "DTSTART:20060301T020000" & CRLF;
	    vCal = vCal & "RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=2SU;BYMONTH=3" & CRLF;
	    vCal = vCal & "TZOFFSETFROM:-0500" & CRLF;
	    vCal = vCal & "TZOFFSETTO:-0400" & CRLF;
	    vCal = vCal & "TZNAME:Daylight Savings Time" & CRLF;
	    vCal = vCal & "END:DAYLIGHT" & CRLF;
	    vCal = vCal & "END:VTIMEZONE" & CRLF;
	    vCal = vCal & "BEGIN:VEVENT" & CRLF;
	    vCal = vCal & "UID:#date_now.getTime()#.CFLIB.ORG" & CRLF;  // creates a unique identifier
	    vCal = vCal & "ORGANIZER;CN=#stEvent.organizerName#:MAILTO:#stEvent.organizerEmail#" & CRLF;
	    vCal = vCal & "DTSTAMP:" & 
	            DateFormat(date_now,"yyyymmdd") & "T" & 
	            TimeFormat(date_now, "HHmmss") & CRLF;
	    vCal = vCal & "DTSTART;TZID=Eastern Time:" & 
	            DateFormat(stEvent.startTime,"yyyymmdd") & "T" & 
	            TimeFormat(stEvent.startTime, "HHmmss") & CRLF;
	    vCal = vCal & "DTEND;TZID=Eastern Time:" & 
	            DateFormat(stEvent.endTime,"yyyymmdd") & "T" & 
	            TimeFormat(stEvent.endTime, "HHmmss") & CRLF;
	    vCal = vCal & "SUMMARY:#stEvent.subject#" & CRLF;
	    vCal = vCal & "LOCATION:#stEvent.location#" & CRLF;
	    vCal = vCal & "DESCRIPTION:#stEvent.description#" & CRLF;
	    vCal = vCal & "PRIORITY:#stEvent.priority#" & CRLF;
	    vCal = vCal & "TRANSP:OPAQUE" & CRLF;
	    vCal = vCal & "CLASS:PUBLIC" & CRLF;
	    vCal = vCal & "BEGIN:VALARM" & CRLF;
	    vCal = vCal & "TRIGGER:-PT30M" & CRLF;  // alert user 30 minutes before meeting begins
	    vCal = vCal & "ACTION:DISPLAY" & CRLF;
	    vCal = vCal & "DESCRIPTION:Reminder" & CRLF;
	    vCal = vCal & "END:VALARM" & CRLF;
	    vCal = vCal & "END:VEVENT" & CRLF;
	    vCal = vCal & "END:VCALENDAR";
	    return Trim(vCal);
	}
</cfscript>
<cfquery name="getAppointments" datasource="#request.dsn#">
	SELECT 
		Appointments.Appointment_ID    
		,Professionals.First_Name as Professional_First
		,Professionals.Last_Name as Professional_Last
		,Professionals.Professional_ID 
		,Professionals.Email_Address as Professional_Email
		,Appointments.Start_Time 
		,Appointments.End_Time 
		,Predefined_Services.Service_Name	
		,Companies.Web_Address
		,Companies.Company_Name 
		,Companies.Company_Address 
		,Companies.Company_City 
		,Companies.Company_State 
		,Companies.Company_Postal 
		,Locations.Location_Name
		,Locations.Location_Address
		,Locations.Location_Address2
		,Locations.Location_City
		,Locations.Location_State
		,Locations.Location_Postal
		,Locations.Location_Phone
		,Locations.Directions
		,Locations.Time_Zone_ID
		,Locations.Payment_Methods_List
		,Locations.Cancellation_Policy
		,Locations.Parking_Fees
		,Professionals_Services.Price
		,Professionals_Services.Service_Time
		,Customers.First_Name AS Customer_First
		,Customers.Last_Name AS Customer_Last
		,Customers.Mobile_Phone AS Customer_Mobile
		,Customers.Email_Address AS Customer_Email
	FROM         
		Appointments INNER JOIN
	    Professionals ON Appointments.Professional_ID = Professionals.Professional_ID INNER JOIN
	    Predefined_Services ON Appointments.Service_ID =  Predefined_Services.Service_ID INNER JOIN
	    Customers ON Appointments.Customer_ID = Customers.Customer_ID INNER JOIN
	    Locations ON Professionals.Location_ID = Locations.Location_ID INNER JOIN
	    Companies ON Locations.Company_ID = Companies.Company_ID INNER JOIN
	    Professionals_Services ON  Predefined_Services.Service_ID = Professionals_Services.Service_ID
	WHERE
		utc_time BETWEEN '#DateFormat(DateAdd('h',25,local.utcTime),'dd-mmm-yyyy')# #TimeFormat(DateAdd('h',24,local.utcTime),'HH:mm')#'
	AND '#DateFormat(DateAdd('h',25,local.utcTime),'dd-mmm-yyyy')# #TimeFormat(date,'HH:mm')#'
	AND 
		utc_time > '#DateFormat(local.utcTime,'dd-mmm-yyyy')# #TimeFormat(local.utcTime,'HH:mm')#'
		
		 <!--- Prevents duplicate alerts within 24 hours --->
	AND Appointments.Appointment_ID NOT IN
		(SELECT Appointment_ID FROM Appointment_Alerts WHERE Alert_Date_Time > '#DateFormat(DateAdd('h',-24,local.utcTime),'dd-mmm-yyyy')# #TimeFormat(DateAdd('h',-24,local.utcTime),'HH:mm')#')
</cfquery>
<cfdump var="#getAppointments#">
	<!--- Assigned values for calendar attachment --->
<cfset eventStr = {} >
<cfset eventStr.organizerName = '#getAppointments.Professional_First#
' & '#getAppointments.Professional_Last#'>
<cfset eventStr.organizerEmail = "#getAppointments.Professional_Email#">
<cfset eventStr.location = "#getAppointments.Location_Address#">
<cfset eventStr.subject = "Reminder of Your Appointment with #getAppointments.Professional_First# #getAppointments.Professional_Last#">	
<cfset eventStr.startTime = "#getAppointments.Start_Time#">
<cfset eventStr.endTime= "#getAppointments.End_Time#">
<cfset eventStr.description = "Reminder of Your Appointment with #getAppointments.Professional_First# #getAppointments.Professional_Last#" >
<cfloop query="getAppointments" group="APPOINTMENT_ID">
	<cfoutput>
		<cfif Len(getAppointments.Customer_Email)>
			<!---<cfsavecontent variable="mailBody">
	            <cfoutput>
			        <!doctype html>
						<html xmlns="http://www.w3.org/1999/xhtml">
						<head>
							<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
							<meta name="viewport" content="initial-scale=1.0" />
							<meta name="format-detection" content="telephone=no" />
							<title></title>
							<style type="text/css">
								body {
									width: 100%;
									margin: 0;
									padding: 0;
									-webkit-font-smoothing: antialiased;
								}
								@media only screen and (max-width: 600px) {
									table[class="table-row"] {
										float: none !important;
										width: 98% !important;
										padding-left: 20px !important;
										padding-right: 20px !important;
									}
									table[class="table-row-fixed"] {
										float: none !important;
										width: 98% !important;
									}
									table[class="table-col"], table[class="table-col-border"] {
										float: none !important;
										width: 100% !important;
										padding-left: 0 !important;
										padding-right: 0 !important;
										table-layout: fixed;
									}
									td[class="table-col-td"] {
										width: 100% !important;
									}
									table[class="table-col-border"] + table[class="table-col-border"] {
										padding-top: 12px;
										margin-top: 12px;
										border-top: 1px solid ##E8E8E8;
									}
									table[class="table-col"] + table[class="table-col"] {
										margin-top: 15px;
									}
									td[class="table-row-td"] {
										padding-left: 0 !important;
										padding-right: 0 !important;
									}
									table[class="navbar-row"] , td[class="navbar-row-td"] {
										width: 100% !important;
									}
									img {
										max-width: 100% !important;
										display: inline !important;
									}
									img[class="pull-right"] {
										float: right;
										margin-left: 11px;
										max-width: 125px !important;
										padding-bottom: 0 !important;
									}
									img[class="pull-left"] {
										float: left;
										margin-right: 11px;
										max-width: 125px !important;
										padding-bottom: 0 !important;
									}
									table[class="table-space"], table[class="header-row"] {
										float: none !important;
										width: 98% !important;
									}
									td[class="header-row-td"] {
										width: 100% !important;
									}
								}
								@media only screen and (max-width: 480px) {
										table[class="table-row"] {
											padding-left: 16px !important;
											padding-right: 16px !important;
										}
								}
								@media only screen and (max-width: 320px) {
										table[class="table-row"] {
											padding-left: 12px !important;
											padding-right: 12px !important;
										}
								}
								@media only screen and (max-width: 608px) {
										td[class="table-td-wrap"] {
											width: 100% !important;
										}
								}
							</style>
						</head>
						<body style="font-family: Arial, sans-serif; font-size:13px; color: ##444444; min-height: 200px;" bgcolor="##E4E6E9" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
							<table width="100%" height="100%" bgcolor="##E4E6E9" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td width="100%" align="center" valign="top" bgcolor="##E4E6E9" style="background-color:##E4E6E9; min-height: 200px;">
									<table style="table-layout: auto; width: 100%; background-color: ##438eb9;" width="100%" bgcolor="##438eb9" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td width="100%" align="center" style="width: 100%; background-color: ##438eb9;" bgcolor="##438eb9" valign="top">
													<table class="table-row-fixed" height="50" width="600" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
														<tbody>
															<tr>
																<td class="navbar-row-td" valign="middle" height="50" width="600" data-skipstyle="true" align="left">
																	<table class="table-row" style="table-layout: auto; padding-right: 16px; padding-left: 16px; width: 600px;" width="600" cellspacing="0" cellpadding="0" border="0">
																		<tbody>
																			<tr style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px;">
																				<td class="table-row-td" style="padding-right: 16px; font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; vertical-align: middle;" valign="middle" align="left">
																					<a href="##" style="color: ##ffffff; text-decoration: none; padding: 10px 0px; font-size: 18px; line-height: 20px; height: auto; background-color: transparent;">
																						Reminder of Your Appointment with  #getAppointments.Professional_First# #getAppointments.Professional_Last# #DateFormat(getAppointments.Start_Time,"dddd")#  #DateFormat(getAppointments.Start_Time,"long")# #TimeFormat(getAppointments.Start_Time,"short")#
																					</a>
																				</td>

																		
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>


									<table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" align="left">&nbsp;</td>
											</tr>
										</tbody>
									</table>

									<table class="table-row-fixed" width="600" bgcolor="##FFFFFF" style="table-layout: fixed; background-color: ##ffffff;" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td class="table-row-fixed-td" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; padding-left: 24px; padding-right: 24px;" valign="top" align="left">
													<table class="table-col" align="left" width="285" style="padding-right: 18px; table-layout: fixed;" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr>
																<td class="table-col-td" width="267" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
																	<table class="header-row" width="267" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																		<tbody>
																			<tr>
																				<td class="header-row-td" width="267" style="font-size: 28px; margin: 0px; font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: ##478fca; padding-bottom: 10px; padding-top: 15px;" valign="top" align="left"><img src="http://www.salonworks.com/images/staff/#getAppointments.Professional_ID#.jpg" alt="#getAppointments.Customer_First#" id="personImg" width="220px"></td>
																			</tr>
																			<tr>
																				<td class="header-row-td" width="267" style="font-size: 25px;margin: 0px;font-family: Arial,sans-serif;font-weight: normal;line-height: 26px;color: ##478fca;padding-bottom: 10px;padding-top: 15px;">Dear #getAppointments.Customer_First# #getAppointments.Customer_Last#
																				</td>

																			</tr>
																		</tbody>
																	</table>
																	<p style="margin: 0px; font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px;">
																		Your #getAppointments.Service_Name# appointment is scheduled for #DateFormat(getAppointments.Start_Time,'dddd, mmm dd, yyyy')# at #TimeFormat(getAppointments.Start_Time,'hh:mm:tt')#  with #getAppointments.Professional_First# #getAppointments.Professional_Last#
																		. We look forward to serving you! If you have any questions, please contact #getAppointments.Professional_First# #getAppointments.Professional_Last# at #getAppointments.Professional_Email#.<br>
																			Click <a href="http://salonworks.com/#getAppointments.Web_Address#/customer_profile.cfm?eflgsignup=1">here</a> to view your appointments, or update your profile.<br>
																			<cfif structKeyExists(getAppointments, 'Location_Phone') and len(getAppointments.Location_Phone)>
																				<p>If you can not make this appointment, please call immediately to reschedule #getAppointments.Location_Phone#.</p>
																				</cfif>
																			

																			<p> Cancellation Policy : #getAppointments.Cancellation_Policy#
																		    </p>
																	</p>											
																	<br>
																	
																</td>
															</tr>
														</tbody>
													</table>
													<table class="table-col" align="left" width="267" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
														<tbody>
															<tr>
																<td class="table-col-td" width="267" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
																	<table class="table-space" height="6" style="height: 6px; font-size: 0px; line-height: 0; width: 267px; background-color: ##ffffff;" width="267" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0">
																		<tbody>
																			<tr>
																				<td class="table-space-td" valign="middle" height="6" style="height: 6px; width: 267px; background-color: ##ffffff;" width="267" bgcolor="##FFFFFF" align="left">&nbsp;</td>
																			</tr>
																		</tbody>
																	</table>
																	<br>
																	<table width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																		<tbody>
																			<tr>
																				<td width="100%" bgcolor="##f5f5f5" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; padding: 19px; border: 1px solid ##e3e3e3; background-color: ##f5f5f5;" valign="top" align="left">
																					
																					<table class="header-row" width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																						<tbody>
																							<tr>
																								<td class="header-row-td" width="100%" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: ##478fca; margin: 0px; font-size: 18px; padding-bottom: 8px; padding-top: 10px;" valign="top" align="left">Contact Info</td>
																							</tr>
																						</tbody>
																					</table>
																					<span style="font-family: Arial, sans-serif; line-height: 19px; color: ##31708f; font-size: 13px;">#getAppointments.Company_Name#</span>
																					<br>
																					<span style="font-family: Arial, sans-serif; line-height: 19px; color: ##31708f; font-size: 13px;">#getAppointments.Company_Address# #getAppointments.Company_City# #getAppointments.Company_State# #getAppointments.Company_Postal#</span>
																					<a href="https://www.google.com/maps/place/#getAppointments.Company_Address#,+#getAppointments.Company_City#,+#getAppointments.Company_State#,+#getAppointments.Company_Postal#">Map</a>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>

									<table class="table-space" height="32" style="height: 32px; font-size: 0px; line-height: 0; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="32" style="height: 32px; width: 600px; padding-left: 18px; padding-right: 18px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" align="center">&nbsp;<table bgcolor="##E8E8E8" height="0" width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td bgcolor="##E8E8E8" height="1" width="100%" style="height: 1px; font-size:0;" valign="top" align="left">&nbsp;</td></tr></tbody></table></td></tr></tbody></table>

									<table class="table-row" width="600" bgcolor="##FFFFFF" style="table-layout: fixed; background-color: ##ffffff;" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
													<table class="table-col" align="left" width="528" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
														<tbody>
															<tr>
																<td class="table-col-td" width="528" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
																	<div style="font-family: Arial, sans-serif; line-height: 19px; color: ##777777; font-size: 14px; text-align: center;">PLEASE DO NOT REPLY TO THIS EMAIL.  THIS EMAIL ACCOUNT IS NOT MONITORED
																	</div>
																	<table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 528px; background-color: ##ffffff;" width="528" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 528px; background-color: ##ffffff;" width="528" bgcolor="##FFFFFF" align="left">&nbsp;</td></tr></tbody></table>

																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<table class="table-space" height="14" style="height: 14px; font-size: 0px; line-height: 0; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td class="table-space-td" valign="middle" height="14" style="height: 14px; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" align="left">&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						 </table>
						</body>
					</html>  
		        </cfoutput>
         	</cfsavecontent>
			<cfset inviteIcs = iCalUS(eventStr)  >--->

			<!---<cffile action="write"
				file="C:\Sites\Salonworks\www\inviteics\invite.ics"
				output="#inviteIcs#">
			
        	<cfset local.mailCustomer = variables.objMailgun.sendMailViaMailgun(mailTo="#getAppointments.Customer_Email#",mailFrom="salonworks@salonworks.com",mailSubject="Reminder of Your Appointment with #getAppointments.Professional_First# #getAppointments.Professional_Last# #DateFormat(getAppointments.Start_Time,"dddd")# #DateFormat(getAppointments.Start_Time,"long")# #TimeFormat(getAppointments.Start_Time,"short")#",mailHtml = "#mailBody#" ,mailAttachment="C:\Sites\Salonworks\www\inviteics\invite.ics") />
			<cffile action="delete"
			file="C:\Sites\Salonworks\www\inviteics\invite.ics">--->
        	<!--- Commented on 25/04/2019 (Remaining )--->
			 <cfmail from="salonworks@salonworks.com" To="#getAppointments.Customer_Email#"  Subject="Reminder of Your Appointment with #getAppointments.Professional_First# #getAppointments.Professional_Last# #DateFormat(getAppointments.Start_Time,"dddd")# #DateFormat(getAppointments.Start_Time,"long")# #TimeFormat(getAppointments.Start_Time,"short")#" server="smtp-relay.sendinblue.com" port="587" type="HTML" username="ciredrofdarb@gmail.com" 
			password="2xf5ZLbMdyDr0VSv" usetls="true">
				<cfoutput>
					<cfmailpart type="html"> 
						<!doctype html>
						<html xmlns="http://www.w3.org/1999/xhtml">
						<head>
							<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
							<meta name="viewport" content="initial-scale=1.0" />
							<meta name="format-detection" content="telephone=no" />
							<title></title>
							<style type="text/css">
								body {
									width: 100%;
									margin: 0;
									padding: 0;
									-webkit-font-smoothing: antialiased;
								}
								@media only screen and (max-width: 600px) {
									table[class="table-row"] {
										float: none !important;
										width: 98% !important;
										padding-left: 20px !important;
										padding-right: 20px !important;
									}
									table[class="table-row-fixed"] {
										float: none !important;
										width: 98% !important;
									}
									table[class="table-col"], table[class="table-col-border"] {
										float: none !important;
										width: 100% !important;
										padding-left: 0 !important;
										padding-right: 0 !important;
										table-layout: fixed;
									}
									td[class="table-col-td"] {
										width: 100% !important;
									}
									table[class="table-col-border"] + table[class="table-col-border"] {
										padding-top: 12px;
										margin-top: 12px;
										border-top: 1px solid ##E8E8E8;
									}
									table[class="table-col"] + table[class="table-col"] {
										margin-top: 15px;
									}
									td[class="table-row-td"] {
										padding-left: 0 !important;
										padding-right: 0 !important;
									}
									table[class="navbar-row"] , td[class="navbar-row-td"] {
										width: 100% !important;
									}
									img {
										max-width: 100% !important;
										display: inline !important;
									}
									img[class="pull-right"] {
										float: right;
										margin-left: 11px;
										max-width: 125px !important;
										padding-bottom: 0 !important;
									}
									img[class="pull-left"] {
										float: left;
										margin-right: 11px;
										max-width: 125px !important;
										padding-bottom: 0 !important;
									}
									table[class="table-space"], table[class="header-row"] {
										float: none !important;
										width: 98% !important;
									}
									td[class="header-row-td"] {
										width: 100% !important;
									}
								}
								@media only screen and (max-width: 480px) {
										table[class="table-row"] {
											padding-left: 16px !important;
											padding-right: 16px !important;
										}
								}
								@media only screen and (max-width: 320px) {
										table[class="table-row"] {
											padding-left: 12px !important;
											padding-right: 12px !important;
										}
								}
								@media only screen and (max-width: 608px) {
										td[class="table-td-wrap"] {
											width: 100% !important;
										}
								}
							</style>
						</head>
						<body style="font-family: Arial, sans-serif; font-size:13px; color: ##444444; min-height: 200px;" bgcolor="##E4E6E9" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
							<table width="100%" height="100%" bgcolor="##E4E6E9" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td width="100%" align="center" valign="top" bgcolor="##E4E6E9" style="background-color:##E4E6E9; min-height: 200px;">
									<table style="table-layout: auto; width: 100%; background-color: ##438eb9;" width="100%" bgcolor="##438eb9" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td width="100%" align="center" style="width: 100%; background-color: ##438eb9;" bgcolor="##438eb9" valign="top">
													<table class="table-row-fixed" height="50" width="600" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
														<tbody>
															<tr>
																<td class="navbar-row-td" valign="middle" height="50" width="600" data-skipstyle="true" align="left">
																	<table class="table-row" style="table-layout: auto; padding-right: 16px; padding-left: 16px; width: 600px;" width="600" cellspacing="0" cellpadding="0" border="0">
																		<tbody>
																			<tr style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px;">
																				<td class="table-row-td" style="padding-right: 16px; font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; vertical-align: middle;" valign="middle" align="left">
																					<a href="##" style="color: ##ffffff; text-decoration: none; padding: 10px 0px; font-size: 18px; line-height: 20px; height: auto; background-color: transparent;">
																						Reminder of Your Appointment with  #getAppointments.Professional_First# #getAppointments.Professional_Last# #DateFormat(getAppointments.Start_Time,"dddd")#  #DateFormat(getAppointments.Start_Time,"long")# #TimeFormat(getAppointments.Start_Time,"short")#
																					</a>
																				</td>

																		
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>


									<table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" align="left">&nbsp;</td>
											</tr>
										</tbody>
									</table>

									<table class="table-row-fixed" width="600" bgcolor="##FFFFFF" style="table-layout: fixed; background-color: ##ffffff;" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td class="table-row-fixed-td" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; padding-left: 24px; padding-right: 24px;" valign="top" align="left">
													<table class="table-col" align="left" width="285" style="padding-right: 18px; table-layout: fixed;" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr>
																<td class="table-col-td" width="267" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
																	<table class="header-row" width="267" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																		<tbody>
																			<tr>
																				<td class="header-row-td" width="267" style="font-size: 28px; margin: 0px; font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: ##478fca; padding-bottom: 10px; padding-top: 15px;" valign="top" align="left"><img src="http://www.salonworks.com/images/staff/#getAppointments.Professional_ID#.jpg" alt="#getAppointments.Customer_First#" id="personImg" width="220px"></td>
																			</tr>
																			<tr>
																				<td class="header-row-td" width="267" style="font-size: 25px;margin: 0px;font-family: Arial,sans-serif;font-weight: normal;line-height: 26px;color: ##478fca;padding-bottom: 10px;padding-top: 15px;">Dear #getAppointments.Customer_First# #getAppointments.Customer_Last#
																				</td>

																			</tr>
																		</tbody>
																	</table>
																	<p style="margin: 0px; font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px;">
																		Your #getAppointments.Service_Name# appointment is scheduled for #DateFormat(getAppointments.Start_Time,'dddd, mmm dd, yyyy')# at #TimeFormat(getAppointments.Start_Time,'hh:mm:tt')#  with #getAppointments.Professional_First# #getAppointments.Professional_Last#
																		. We look forward to serving you! If you have any questions, please contact #getAppointments.Professional_First# #getAppointments.Professional_Last# at #getAppointments.Professional_Email#.<br>
																			Click <a href="http://salonworks.com/#getAppointments.Web_Address#/customer_profile.cfm?eflgsignup=1">here</a> to view your appointments, or update your profile.<br>
																			<cfif structKeyExists(getAppointments, 'Location_Phone') and len(getAppointments.Location_Phone)>
																				<p>If you can not make this appointment, please call immediately to reschedule #getAppointments.Location_Phone#.</p>
																				</cfif>
																			

																			<p> Cancellation Policy : #getAppointments.Cancellation_Policy#
																		    </p>
																	</p>											
																	<br>
																	
																</td>
															</tr>
														</tbody>
													</table>
													<table class="table-col" align="left" width="267" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
														<tbody>
															<tr>
																<td class="table-col-td" width="267" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
																	<table class="table-space" height="6" style="height: 6px; font-size: 0px; line-height: 0; width: 267px; background-color: ##ffffff;" width="267" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0">
																		<tbody>
																			<tr>
																				<td class="table-space-td" valign="middle" height="6" style="height: 6px; width: 267px; background-color: ##ffffff;" width="267" bgcolor="##FFFFFF" align="left">&nbsp;</td>
																			</tr>
																		</tbody>
																	</table>
																	<br>
																	<table width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																		<tbody>
																			<tr>
																				<td width="100%" bgcolor="##f5f5f5" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; padding: 19px; border: 1px solid ##e3e3e3; background-color: ##f5f5f5;" valign="top" align="left">
																					
																					<table class="header-row" width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																						<tbody>
																							<tr>
																								<td class="header-row-td" width="100%" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: ##478fca; margin: 0px; font-size: 18px; padding-bottom: 8px; padding-top: 10px;" valign="top" align="left">Contact Info</td>
																							</tr>
																						</tbody>
																					</table>
																					<span style="font-family: Arial, sans-serif; line-height: 19px; color: ##31708f; font-size: 13px;">#getAppointments.Company_Name#</span>
																					<br>
																					<span style="font-family: Arial, sans-serif; line-height: 19px; color: ##31708f; font-size: 13px;">#getAppointments.Company_Address# #getAppointments.Company_City# #getAppointments.Company_State# #getAppointments.Company_Postal#</span>
																					<a href="https://www.google.com/maps/place/#getAppointments.Company_Address#,+#getAppointments.Company_City#,+#getAppointments.Company_State#,+#getAppointments.Company_Postal#">Map</a>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>

									<table class="table-space" height="32" style="height: 32px; font-size: 0px; line-height: 0; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="32" style="height: 32px; width: 600px; padding-left: 18px; padding-right: 18px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" align="center">&nbsp;<table bgcolor="##E8E8E8" height="0" width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td bgcolor="##E8E8E8" height="1" width="100%" style="height: 1px; font-size:0;" valign="top" align="left">&nbsp;</td></tr></tbody></table></td></tr></tbody></table>

									<table class="table-row" width="600" bgcolor="##FFFFFF" style="table-layout: fixed; background-color: ##ffffff;" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
													<table class="table-col" align="left" width="528" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
														<tbody>
															<tr>
																<td class="table-col-td" width="528" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
																	<div style="font-family: Arial, sans-serif; line-height: 19px; color: ##777777; font-size: 14px; text-align: center;">PLEASE DO NOT REPLY TO THIS EMAIL.  THIS EMAIL ACCOUNT IS NOT MONITORED
																	</div>
																	<table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 528px; background-color: ##ffffff;" width="528" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 528px; background-color: ##ffffff;" width="528" bgcolor="##FFFFFF" align="left">&nbsp;</td></tr></tbody></table>

																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<table class="table-space" height="14" style="height: 14px; font-size: 0px; line-height: 0; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td class="table-space-td" valign="middle" height="14" style="height: 14px; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" align="left">&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						 </table>
						</body>
						</html>
					</cfmailpart>
					<cfmailpart type="text/calendar" ><cfset inviteIcs = iCalUS(eventStr)  ><cfoutput>>#inviteIcs#</cfoutput></cfmailpart>
				</cfoutput>
			</cfmail> 
			<cfoutput>Successfully sent mail!</cfoutput>
		</cfif>
		<cfset variables.Customer_Mobile = Replace(getAppointments.Customer_Mobile,'-','','all')>
		<cfset variables.TextMessage="This is a reminder of your upcoming appointment with #getAppointments.Professional_First# #getAppointments.Professional_Last#, on #DateFormat(getAppointments.Start_Time,'dddd, mmm dd, yyyy')# at #TimeFormat(getAppointments.Start_Time,'hh:mm')#">
		<!--- Text reminder section start --->
		<cfif Len(variables.Customer_Mobile)>
			<!--- <cfhttp 
			url="http://api.clickatell.com/http/sendmsg?user=salonworks2&password=AGHdFPDYcHYRIM&api_id=3455257&to=1#variables.Customer_Mobile#,15124974494&text=#URLEncodedFormat(variables.TextMessage)#&from=15128616364&mo=1" 
			method="get">
			</cfhttp> --->
			<cfset variables.Customer_Mobile = Replace(variables.Customer_Mobile, ' ', '', 'all') />
			<cfset variables.Customer_Mobile =  Replace(Replace(variables.Customer_Mobile, ')', '', 'all'), '(', '', 'all')>
			Customer_Mobile = <cfoutput>1#variables.Customer_Mobile#</cfoutput>
			<!--- Send text reminder to 15124974494 --->
			<cfhttp 
			url="https://platform.clickatell.com/messages/http/send?apiKey=o6x8dYxORO2PRF2c7CtgBw==&to=15124974494&content=#URLEncodedFormat(variables.TextMessage)#&from=12134585101" 
			method="get">
			</cfhttp>
			<!--- Send text reminder to customer --->
			<cfhttp 
			url="https://platform.clickatell.com/messages/http/send?apiKey=o6x8dYxORO2PRF2c7CtgBw==&to=1#variables.Customer_Mobile#&content=#URLEncodedFormat(variables.TextMessage)#&from=12134585101" 
			method="get">
			</cfhttp>

		</cfif>
		 <!---Text reminder section end --->
	<cfif Len(getAppointments.Customer_Email) OR Len(variables.Customer_Mobile)>
			<cfquery name="insertAppointmentAlert" datasource="#request.dsn#">
				INSERT
				INTO 
					Appointment_Alerts
					(Appointment_ID
					,Alert_Date_Time
					,Confirmation_ID
					)
				VALUES
					(#getAppointments.Appointment_ID#
					,#now()#
					<cfif Len(variables.Customer_Mobile) >,'#Replace(cfhttp.filecontent,'ID: ','')#'<cfelse>,null</cfif>
					)
			</cfquery>
		</cfif>
	</cfoutput>
</cfloop>




