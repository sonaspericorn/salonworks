<!--- Daily : Scheduler to get transaction details based on customerPaymentprofileID --->
<cfoutput>
	
	<!--- Query to get list off all comapnies whose subscriptions is active --->
	<cfset qActiveCompanies = getActiveSubscribedCompanies() />
	<cfdump var="#qActiveCompanies#" >
	<cfif arrayLen(qActiveCompanies)>
		<cfscript>
				for( company in qActiveCompanies ) {

					// Do API call to get transaction details and update transactionHistory in DB
					doAPItransaction(
						customerProfileId = company.customerProfileId,
						customerPaymentprofileID = company.customerPaymentprofileID,
						subscriptionID = company.subscriptionID,
						companyID = company.company_ID
					);
				}
		</cfscript>
	</cfif>

<!--- Helper Functions --->

	<cffunction name="doAPItransaction" access="private" output="false" returntype="void">
		<cfscript>
			param name="arguments.customerProfileId" required="true";
			param name="arguments.customerPaymentprofileID" required="true";
			param name="arguments.subscriptionID" required="true";
			param name="arguments.companyID" required="true";
			var apiRequest = '{
			    "getTransactionListForCustomerRequest": {
			        "merchantAuthentication": {
			            "name": "7xG5qVRm65k",
			            "transactionKey": "97p5B2MD2v4v7tA3"
			        },
			        "customerProfileId": "#arguments.customerProfileId#",
			        "customerPaymentProfileId": "#arguments.customerPaymentprofileID#",
			        "sorting": {
			            "orderBy": "submitTimeUTC",
			            "orderDescending": false
			        },
			        "paging": {
			            "limit": "100",
			            "offset": "1"
			        }
			    }
			}';

			var httpService = new http();
    		httpService.setMethod("POST");
   			httpService.setUrl("https://apitest.authorize.net/xml/v1/request.api");
   			// Set headers.
			httpService.addParam(type="header", name="Authorization", value="Basic N3hHNXFWUm02NWs6OTdwNUIyTUQydjR2N3RBMw==");
		    httpService.addParam(type="header", name="Content-Type", value="application/json");
		    httpService.addParam(type="body", value="#apiRequest#");

		    var response  =  httpService.send().getPrefix();
			writedump(response);abort;
		    if(response.statuscode == 200 ) {
		    	var fileContentJSON = response.fileContent;
		    	var fileContent = deserializeJSON(fileContentJSON);
		    	if( structKeyExists(fileContent, "totalNumInResultSet") && isNumeric(fileContent.totalNumInResultSet) && fileContent.totalNumInResultSet > 0 ) {
		    		var transactions = fileContent.transactions;
		    		for( var transaction in transactions ) {
		    			updateTransactionHistory(
		    				transactionID = transaction.transId,
		    				amount = transaction.settleAmount,
		    				subscriptionID = arguments.subscriptionID,
		    				companyID = arguments.companyID
		    			);
		    		}
		    	}

		    }
		</cfscript>
	</cffunction>

	<cffunction name="updateTransactionHistory" access="private" output="false" >
		<cfparam name="arguments.transactionID" required="true">
		<cfparam name="arguments.amount" required="true">
		<cfparam name="arguments.subscriptionID" required="true">
		<cfparam name="arguments.companyID" required="true">
		<cfquery name="qUpdateTransactionHistory" datasource="#request.dsn#">
			IF NOT EXISTS (
				SELECT 
					Transaction_ID
				FROM
					Transactions
				WHERE
					Transaction_ID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.transactionID#">
			)
			BEGIN
				INSERT INTO
					Transactions
					(
						Transaction_ID,
						subscription_ID,
						company_ID,
						Amount_Paid	
					)
					VALUES (
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.transactionID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.subscriptionID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.companyID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.amount#">
					)
			END	
		</cfquery>
	</cffunction>
		
	<cffunction name="getActiveSubscribedCompanies" access="private" output="false"  returntype="array">
		
		<!--- Get query --->
		<cfquery name="qGetActiveSubscribedCompanies" datasource="#request.dsn#">
		 	SELECT 
					CustomerProfileID,
					customerPaymentProfileId,
					subscriptionId,
					company_ID
			FROM
					Companies
			WHERE 
					subscription_status = 1
					--AND
					--subscriptionEnddate < getdate()
		</cfquery>
		<cfif qGetActiveSubscribedCompanies.RecordCount >
			<cfreturn QueryToArray( qGetActiveSubscribedCompanies ) />
		</cfif>	

		<!--- Else return empty array --->
		<cfreturn [] />
	</cffunction>

	<cffunction name="QueryToArray" access="private" returntype="array" output="false" hint="This turns a query into an array of structures.">

        <!--- Define arguments. --->
        <cfargument name="Data" type="query" required="yes" />

	    <cfscript>
	        // Define the local scope.
	        var LOCAL = StructNew();
	        // Get the column names as an array.
	        LOCAL.Columns = ListToArray( ARGUMENTS.Data.ColumnList );
	        // Create an array that will hold the query equivalent.
	        LOCAL.QueryArray = ArrayNew( 1 );
	        // Loop over the query.
	        for (LOCAL.RowIndex = 1 ; LOCAL.RowIndex LTE ARGUMENTS.Data.RecordCount ; LOCAL.RowIndex = (LOCAL.RowIndex + 1)){
	            // Create a row structure.
	            LOCAL.Row = StructNew();
	            // Loop over the columns in this row.
	            for (LOCAL.ColumnIndex = 1 ; LOCAL.ColumnIndex LTE ArrayLen( LOCAL.Columns ) ; LOCAL.ColumnIndex = (LOCAL.ColumnIndex + 1)){
	                // Get a reference to the query column.
	                LOCAL.ColumnName = LOCAL.Columns[ LOCAL.ColumnIndex ];
	                // Store the query cell value into the struct by key.
	                LOCAL.Row[ LOCAL.ColumnName ] = ARGUMENTS.Data[ LOCAL.ColumnName ][ LOCAL.RowIndex ];
	            }
	            // Add the structure to the query array.
	            ArrayAppend( LOCAL.QueryArray, LOCAL.Row );
	        }
	        // Return the array equivalent.
	        return( LOCAL.QueryArray );
	    </cfscript>
	</cffunction>

</cfoutput>