var company_id = 0;
var isSubDomain = false;
var auth2;
var googleUser = {};
if( subDomain != "www" ) {
    isSubDomain = true;
    company_id = $('#qCompanyId').val();
}

var onLoad = function(){
    gapi.load('auth2', function () {
        auth2 = gapi.auth2.init({
            client_id: '426650001236-lmr4di68vdbsvpauh9e4n7b0oglvbcij.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
            // Request scopes in addition to 'profile' and 'email'
            scope: 'profile email'
        });
        attachSignin(document.getElementById('customBtn'));
    })
}
$(document).ready(function(){
    onLoad();
});

$(".customGoogleBtn").click(function() {
    document.getElementById('customBtn').click();
    return false;
});

var attachSignin = function(evnt) {
    element = evnt;
    auth2.attachClickHandler(element, {},
        function(googleUser) {
            var profile = googleUser.getBasicProfile();
            var googleEmail = profile.getEmail();
            var googleId = profile.getId();
            var googleName = profile.getName().split(' ');
            var dataset = { "EmailAddress": googleEmail,"firstName" : googleName[0],"lastname": googleName[1],"socialId":googleId,"companyId":company_id,"loginType":"G" };
            var url = "/cfc/socialSignin.cfc?method=iscustomerEmailExisting";
            if( isSubDomain == false ) {
                dataset = { "EmailAddress": googleEmail,"firstName" : googleName[0],"lastname": googleName[1],"socialId":googleId,"loginType":"G"};
                url = "/cfc/socialSignin.cfc?method=isEmailExisting";
            }
            $.ajax({
                type: "get",
                url: url,
                //data: ,
                data: dataset,
                dataType: "json",
                // Define request handlers.
                success: function(objResponse) {
                    if(objResponse == 1 || objResponse == 0){
                        if($.trim(subDomain) == "www" ) {
                            window.location.href = "/admin/login.cfm?socialId="+googleId;
                        } else {
                            window.location.href = "/"+subDomain+"/appointments.cfm";
                        }
                    }
                },
                error: function(objRequest, strError) {
                    alert('Error: There is a problem with the system.  Please try again.  If problem presist, contact site help. ' + strError);
                }
            });

        }, function(error) {
            // alert(JSON.stringify(error, undefined, 2));
        }
    );
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=355505225284774&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
window.fbAsyncInit = function() {
    FB.init({
        appId: '355505225284774',
        cookie: true, 
        xfbml: true, 
        version: 'v2.2' 
    });
};

//facebook
function facebooklogin(){
    FB.getLoginStatus(function(response) {
        console.log(response);
        if (!response.authResponse) {
            FB.login(function(response) {
                testAPI();
            }, {scope: 'public_profile,email'});
        }
        testAPI();
    });
    return false;
}

function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', {"fields":"id,name,email,first_name,last_name"}, function(response) {
        if(response.error == undefined && response.id.length){
            var fbId = response.id;
            var fbEmail = response.email;
            var fbfirstname = response.first_name;
            var fbLastname = response.last_name;
            var dataset = { "EmailAddress": fbEmail,"firstName" : fbfirstname,"lastname": fbLastname,"socialId":fbId,"companyId":company_id,"loginType":"F" };
            var url = "/cfc/socialSignin.cfc?method=iscustomerEmailExisting";
            if( isSubDomain == false ) {
                dataset = { "EmailAddress": fbEmail,"firstName" : fbfirstname,"lastname": fbLastname,"socialId":fbId,"loginType":"F"};
                url = "/cfc/socialSignin.cfc?method=isEmailExisting";
            }
            $.ajax({
                type: "get",
                url: url,
                data: dataset,
                dataType: "json",
                // Define request handlers.
                success: function(objResponse) {
                    if(objResponse == 1 || objResponse == 0){
                        if($.trim(subDomain) == "www" ) {
                            window.location.href = "/admin/login.cfm?socialId="+fbId;
                        } else {
                            window.location.href = "/"+subDomain+"/appointments.cfm";
                        }
                    }
                },
                error: function(objRequest, strError) {
                    alert('Error: There is a problem with the system.  Please try again.  If problem presist, contact site help. ' + strError);
                }
            });
        } else {
        }
    });
}

// facebook logout
function logout(id) {
    var template_id = id;
    FB.getLoginStatus(function(response) {
        if (response.authResponse) {
            FB.logout(function (response) {     
                if(isSubDomain == true){
                    $.ajax({
                        type: "get",
                        url: "/cfc/customers.cfc?method=logoutCustomer",
                        dataType: "html",
                        success: function (data){
                            $('#sign-in').removeClass('hidden');
                            $('#profile-menu').addClass('hidden');
                            $('.user-email').html('');
                            $('.user-name').html('');
                            window.location.href='index.cfm?template_id='+template_id+'';
                        }
                    });
                }else{
                    window.location="logout.cfm";
                }
            });
        } 
    });
}

//Google logout
function signOut(id) {
    var template_id = id;
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
        if(isSubDomain == true){
            $.ajax({
                type: "get",
                url: "/cfc/customers.cfc?method=logoutCustomer",
                dataType: "html",
                success: function (data){
                    $('#sign-in').removeClass('hidden');
                    $('#profile-menu').addClass('hidden');
                    $('.user-email').html('');
                    $('.user-name').html('');
                    window.location.href='index.cfm?template_id='+template_id+'';
                }
            });
        }else{
            window.location="logout.cfm";
        }
    });
}




