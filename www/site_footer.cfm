        <cfparam name="url.modalShow" default="0">
        <style type="text/css">
          .customGoogleBtn{
            border: 1px solid #eee;
            height: 35px;
            width: 80%;
            border-radius: 5px;
            /*padding: 3px 0px 0px 30px;*/
            text-align: center;
            padding: 7px;
            font-size: 13px;
            color: #757575;
            box-shadow: 0 2px 4px 0 rgba(0,0,0,.25);
            background-color: #fff;
            cursor: pointer;
          }
          #loggingButton{
            border: 1px solid #eee;
            height: 35px;
            width: 80%;
            border-radius: 5px;
            /*padding: 3px 0px 0px 30px;*/
            text-align: center;
            padding: 7px;
            font-size: 13px;
            color: #fff;
            box-shadow: 0 2px 4px 0 rgba(0,0,0,.25);
            background-color: #3B5998;
            cursor: pointer;
          }
        </style>
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="ftr-logo-cnt">
                                <a href="#"><img src="salonnewhome/img/footer-logo.png" alt=""></a>
                                <p>Salon and spa management tool designed
                                to bring simplicity to growing your client 
                                and operating your business in the digital age.
                                </p>

                                <!--- <div class="scl-ftr">
                                    <ul>
                                        <li ><a class="icon-facebook" href="https://www.facebook.com/Salonworks-1434509316766493"></a></li>
                                        <li ><a class="icon-twitter" href=""></a></li>
                                        <li ><a class="icon-linkedin" href=""></a></li>
                                        <li ><a class="icon-instagram" href=""></a></li>
                                        <li ><a class="icon-youtube-play" href=""></a></li>
                                    </ul>
                                </div> --->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="ftr-menu">
                                <strong>FEATURES</strong>
                                <ul>
                                    <li><a href="#features">Custom Website</a></li>
                                    <li><a href="#features">Calendar</a></li>
                                    <li><a href="#features">Online Booking</a></li>
                                    <li><a href="">Photo Gallery</a></li>
                                    <li><a href="#features">E-Mail & Text</a></li>
                                </ul>
                            </div>
                        </div>
                        <!--    <div class="col-md-3">
                         <div class="ftr-menu">
                             <strong>BLOG POSTS</strong>
                               <ul>
                                   <li><a href="">ARTICLE 1</a></li>
                                   <li><a href="">ARTICLE 2</a></li>
                                   <li><a href="">ARTICLE 3</a></li>
                                   <li><a href="">ARTICLE 4</a></li>
                                   <li><a href="">ARTICLE 5</a></li>
                               </ul>
                           </div>
                        </div> -->

                        <div class="col-md-3">
                            <div class="ftr-menu adrs">
                                <address>
                                    <strong>Address Information</strong>
                                    <h6>Call us<a href="tel:(978) 352-0235"><span> (978) 352-0235</span></a> </h6>
                                    <div class="ftr-contact">
                                        <ul>
                                            <li><svg class="icon"><use xlink:href="img/svg-sprite.svg#icon_mail"/></svg><a href="mailto:salonworks@salonworks.com">salonworks@salonworks.com</a></li>
                                            <li> <svg class="icon"><use xlink:href="img/svg-sprite.svg#icon_clock"/></svg>Mon - Fri: 8:00AM-5:00PM CST</li>
                                        </ul>   
                                    </div>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ftr-strip">
                    <p>Copyrights SALONWORKS  All rights reserved. <a href="">Privacy Policy</a> <a href="">Terms & Conditions</a> </p>
                </div>
            </footer>
            
            <!-- The Modal login >> -->
            <div class="modal fade loginModal" id="modalLogin">
               <div class="modal-dialog">
                  <div class="modal-content">
                     <!-- Modal Header -->
                     <!-- Modal body -->
                     <div class="modal-body">
                        <button type="button" class="close" style="margin-right: -20px;
                        margin-top: -2px;"data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Log In</h4>
                        <form action="admin/login.cfm" id="loginFrm" method="POST" class="erroron_form">
                           <div class="form-group">
                              <input type="email" name="Email_Address_log" id="login-email" class="form-control" placeholder="Email">
                           </div>
                           <div class="form-group">
                              <input type="password" class="form-control" name="password" id="login-password" placeholder="Password">
                           </div>
                           <!-- <div class="form-group form-check">
                              <label class="form-check-label">
                                <input class="form-check-input" type="checkbox"> Remember me
                              </label>
                              </div> -->
                           <button type="button" id="loginBtn" class="btn btn-block" style="background-color:#8a171a;color:white;">Log In</button>
                           <div id="msgLogin" style="color:red; display:none;margin-top:10px;text-align: center;">&nbsp;</div>
                        </form>
                        <a class="float-right link-default" data-dismiss="modal" data-toggle="modal" href="#modalForgotPassword">Forgot password?</a>
                     </div>
                     <!--- Google and facebook signin --->
                     <div class="socialMedia" style="padding-left:30px;margin-bottom:20px;">
                        <div style="width:45%;float:left;">
                            <!--- <div class="fb-login-button" data-onlogin="testAPI" data-size="medium" data-button-type="login_with" data-auto-logout-link="false" data-use-continue-as="false"></div> --->
                            <button id="loggingButton" onclick="facebooklogin();">Log in with Facebook</button>
                        </div>
                        <div class="customGoogleBtnDiv">
                          <button class="customGoogleBtn">Log in with Google</button>
                        </div>
                    </div>
                        <div class="hide">
                            <div id="customBtn" class="customGPlusSignIn">
                            </div>
                        </div>
                     <!-- Modal footer -->
                     <div class="modal-footer">
                        <p>
                           Don't have an account? <br>

                           <a data-dismiss="modal" class="signup-trial" href="">Sign up for your FREE 30 Day Trial now!</a>
                          
                        </p>
                     </div>
                  </div>
               </div>
            </div>
        <script>
            
            $(document).ready(function(){
              $(".signup-trial").click(function(){
                  $(".modal-backdrop").removeClass("modal-backdrop fade");
              });
          });
        </script>
            <!-- The Modal login << -->
            <!-- The Modal Forgot Password >> -->
            <div class="modal fade loginModal" id="modalForgotPassword">
               <div class="modal-dialog">
                  <div class="modal-content">
                     <!-- Modal Header -->
                     <!-- Modal body -->
                     <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Forgot Password</h4>
                        <form action="#" id="forgotPassForm" method="POST" class="erroron_form">
                           <div class="form-group">
                              <input type="email" class="form-control" name="Email_Address_forgot" id="forgotPassEmail" placeholder="Email" required="required">
                           </div>
                           <button type="button" class="btn btn-block" id="forgotPassSubmit" style="background-color:#8a171a;color:white;">Send Mail</button>
                        </form>
                     </div>
                     <!-- Modal footer -->
                     <div class="modal-footer">
                        <p>
                           We are ready to help you <br>
                           <a href="#">Keep in touch with us and enjoy our services!</a>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal fade modalsucces" id="emailsuccess" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                     </div>
                     <div class="modal-body">
                        Your password has been<br> sent to your email address
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnEmailOk">Ok</button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- The Modal  Forgot Password << -->
            <!-- The Modal Support >> -->
            <div class="modal fade loginModal modalSupport" id="modalSupport">
               <div class="modal-dialog">
                  <div class="modal-content">
                     <!-- Modal Header -->
                     <!-- Modal body -->
                     <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Need help? We're here to assist you!</h4>
                        <form id="modalSupportform" method="POST" class="erroron_form">
                           <div class="form-group">
                              <input type="text" class="form-control" placeholder="Name" name="Name" id="support-name" required>
                           </div>
                           <div class="form-group">
                              <input type="email" class="form-control" placeholder="Email" name="Email" id="support-email">
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" placeholder="Subject" name="Subject" id="support-support">
                           </div>
                           <div class="form-group">
                              <textarea class="form-control" placeholder="Enter Your Question or Message" name="Message" id="support-message"></textarea>
                           </div>
                           <button type="submit" id="submitButton" class="btn btn-block" name="submit" style="background-color:#8a171a;color:white;">Submit</button>
                        </form>
                     </div>
                     <!-- Modal footer -->
                     <div class="modal-footer">
                        <p>
                           We are ready to help you <br>
                           <a href="#">Keep in touch and enjoy our services!</a>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <!-- The Modal  Support -->

                <script src="salonnewhome/js/vendor/jquery-3.3.1.slim.min.js"></script>
                <script defer="true" src="salonnewhome/js/vendor/popper.min.js"></script>
                <script  src="salonnewhome/js/vendor/bootstrap.min.js"></script>
                <script src="salonnewhome/js/vendor/slick.min.js"></script>
                <script defer="true" src="salonnewhome/js/vendor/jquery.magnific-popup.js"></script>
                <script  src="salonnewhome/js/main.js"></script>  



                <!--- old scripts --->
               <!---  <script src="<cfoutput>#strPath#</cfoutput>js/vendor/jquery-3.3.1.slim.min.js"></script> --->
                <script defer="true" src="<cfoutput>#strPath#</cfoutput>js/vendor/wow.min.js"></script>
                <script  src="<cfoutput>#strPath#</cfoutput>js/main.js"></script>
                <script defer="true" src="js/editor.js"></script>
                <script defer="true" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
                <script defer="true" type="text/javascript" src="<cfoutput>#strPath#</cfoutput>js/css3-animate-it.js"></script>
               <!---  <script defer="true" src="js/jquery.mask.js"></script> --->
                <script defer="true" src="js/summernote.js"></script>
                <script defer="true" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
                <script defer="true" type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.min.js"></script>
                <script defer="true" type="text/javascript" src="js/error_messages.js"></script>
                <!--- end --->
                <script type="text/javascript" >
                   $(document).ready(function(){        
                        $('#modalLoad').modal('show');
                    }); 
                </script>

                <script >
                   var inputemail = document.getElementById("login-email");
                   var inputpw = document.getElementById("login-password");
                   
                   inputemail.addEventListener("keyup", function(event) {
                       event.preventDefault();
                       if (event.keyCode === 13) {
                           document.getElementById("loginBtn").click();
                       }
                   });
                   inputpw.addEventListener("keyup", function(event) {
                       event.preventDefault();
                       if (event.keyCode === 13) {
                           document.getElementById("loginBtn").click();
                       }
                   });
                </script>

                <script >
                   $(document).ready(function() {
                       $("#loginFrm").validate({
                           rules: {
                               Email_Address_log: {
                                   required: true,
                                   email: true
                               },
                               password: {
                                   required: true
                               }
                   
                           },
                           messages: {
                               Email_Address_log: {
                                   required: "Email is required"
                               },
                               password: "Password is required"
                           }
                       });
                       $("#forgotPassForm").validate({
                           rules: {
                               Email_Address_forgot: {
                                   required: true,
                                   email: true
                               },
                           },
                           messages: {
                               Email_Address_forgot: {
                                   required: "Email is required"
                               },
                           }
                       });
                       $("#modalSupportform").validate({
                           rules: {
                               Email: {
                                   required: true,
                                   email: true
                               },
                               Name: {
                                   required: true
                               },
                               Subject: {
                                   required: true
                               },
                               Message: {
                                   required: true
                               }
                           },
                           messages: {
                               Email: {
                                   required: "Email is required"
                               },
                               Name: "Name is required",
                               Subject: "Subject is required",
                               Message: "Message is required"
                           }
                       });
                       $('#modalSupportform').validate({});
                       if ($("#modalSupportform").valid()) {
                           $('#modalSupportform').submit();
                       } else {
                           return false;
                       }
                   
                   });
                </script>

                <script type="text/javascript" >
                  var loginModalFlag = '<cfoutput>#url.modalShow#</cfoutput>';
                  if(loginModalFlag == 1 ){
                    $('#modalLogin').modal('show');
                  }
                   $(".signup-trial").click(function() {
                       $('#modalLogin').modal('hide');
                       $('#modalLoad').modal('hide');
                       $('html,body').stop(true, false).animate({
                   
                           scrollTop: $(".form-web-otr").offset().top - 80
                       }, 1000);
                       return false;
                       $('html,body').animate({
                   
                           scrollTop: $(window.location.hash).offset().top - 90
                       }, 500);
                   });
                </script>

                <script type="text/javascript" >
                   $(".video-but").click(function() {
                       $('#modalLoad').modal('hide');
                       $('html,body').stop(true, false).animate({
                   
                           scrollTop: $(".video-otr").offset().top - 80
                       }, 1000);
                       return false;
                       $('html,body').animate({
                   
                           scrollTop: $(window.location.hash).offset().top - 90
                       }, 500);
                   });
                </script>

                <script>
                   $(document).ready(function() {
                       $('#featureslink').click(function(){
                          $('#featureslink').addClass('active');
                          
                        });
                       $('[data-toggle="popover"]').popover();
                   });
                </script>

                <script type="text/javascript" >
                   $(function() {
                       fnCheckEmailAddress = function(isSignup) {
                            
                           if ($('#Email_Address').val().length) {
                   
                               $.ajax({
                                   type: "get",
                                   url: "admin/company.cfc",
                                   data: {
                                       method: "isExistingEmailAddress",
                                       EmailAddress: $('#Email_Address').val(),
                                       noCache: new Date().getTime()
                                   },
                                   dataType: "json",
                   
                                   // Define request handlers.
                                   success: function(objResponse) {
                                       // Check to see if request was successful.
                                       if (objResponse.SUCCESS) {
                                           if (objResponse.DATA) {
                                               $('#Email_Address').val('');
                                               $('#Email_Address').focus();
                                               alert('The Email address, ' + $('#Email_Address').val() + ', entered already exist.  Please enter a different address.');
                                               return false;
                                           }else if(isSignup == 1){
                                              $('#finishBtn').click();
                                           }
                   
                                       } else {
                                           alert('Error: There is a problem with the system.  Please try again.  If problem presist, contact site help.');
                                       }
                                   },
                   
                                   error: function(objRequest, strError) {
                                       alert('Error: There is a problem with the system.  Please try again.  If problem presist, contact site help. ' + strError);
                                   }
                               });
                           } else if(isSignup == 1){
                              $('#finishBtn').click();
                           }
                       }

                       fnCheckWebAddress = function(){
                        $('.finishBtn').prop('disabled', false);
                           var checkaddress = /^[a-zA-Z0-9]+(?:--?[a-zA-Z0-9]+)*$/;
                           if($('#Web_Address').val().length){
                               console.log($('#Web_Address').val());
                               if (!checkaddress.test($('#Web_Address').val())) {
                                       alert("Please enter a valid web address");
                                       $('#Web_Address').val('');
                                       $('#Web_Address').focus();
                                   return false;
                                   }
                               if($('#Web_Address').val().toLowerCase() == 'www'){
                                   alert('Web Address can not be "www".');
                                   $('#Web_Address').val('');
                                   $('#Web_Address').focus();
                                   return false;
                               }
                                    
                               $.ajax({
                                       type: "get",
                                       url: "admin/company.cfc",
                                       data: {
                                           method: "isExistingWebAddress",
                                           WebAddress: $('#Web_Address').val(),
                                           noCache: new Date().getTime()
                                           },
                                       dataType: "json",
                       
                                       // Define request handlers.
                                       success: function( objResponse ){
                                           // Check to see if request was successful.
                                           if (objResponse.SUCCESS){
                                               if(objResponse.DATA){
                                                   alert('The web address, ' + $('#Web_Address').val() + ', entered already exist.  Please enter a different address.');
                                                   $('#Web_Address').val('');
                                                   $('#Web_Address').focus();
                                                   $('.finishBtn').prop('disabled', true);
                                                   return false;
                                               }
                       
                                           } else {
                                               alert('Error: There is a problem with the system.  Please try again.  If problem presist, contact site help.');
                                           }
                                       },
                       
                                       error: function( objRequest, strError){
                                           alert('Error: There is a problem with the system.  Please try again.  If problem presist, contact site help. ' + strError);
                                       }
                               });
                           }
                       }
                   
                       $('.close').click(function() {
                           console.log(1);
                           // $('#video-learn iframe').attr("src", jQuery("#video-learn iframe").attr("src"));
                       });
                   });
                    // var frm = frames['frame'].document;
                    // var otherhead = frm.getElementsByTagName("head")[0];
                    // otherhead.appendChild("<style type='text/css'>  #u_0_1{height: 36px;}  </style>");
                    // $('iframe').load( function() {
                    //     $('iframe').contents().find("head").append($("<style type='text/css'>  #u_0_1{height: 36px;}  </style>"));
                    // });

                    $('#submitButton').click(function(){
                        var supportName = $('#support-name').val();
                        var supportEmail = $('#support-email').val();
                        var supportSubject = $('#support-support').val();
                        var supportMsg = $('#support-message').val();
                        $.ajax({
                               type: "get",
                               url: "admin/login.cfc?method=sendSupportEmail&returnFormat=JSON",
                               data: { supportName : supportName, supportEmail:supportEmail,supportSubject:supportSubject,supportMsg:supportMsg},
                               dataType: "json",
                               success: function( data ){
                                if(data === true){
                                  location.reload();
                                }
                               },
               
                               error: function( data, strError){
                                   
                               }
                       });
                    });

                </script>
                <!--- Script for google analytics --->
                <script >
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                
                  ga('create', 'UA-50779113-1', 'salonworks.com');
                  ga('send', 'pageview');
                
                </script>   
                <!--- Script end --->        
                <script type="text/javascript" >
                 var subDomain = "<cfoutput>#application.subdomain#</cfoutput>";
                </script>

    </body>
      <script defer="true" src="js/socialLogin.js"></script>
      <script defer="true" src="https://apis.google.com/js/api:client.js"></script>
     

</html>
       