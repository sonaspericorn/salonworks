<cfcomponent displayname="android api">

	<!--- function for validate api call --->
	<cffunction name="validateAPICall" access="private" returntype="struct">
		<cfargument name="httpData" type="struct">
		<cfset local.returnVal = structNew()>
		<cfset local.returnVal.status = 0 >
		<cfif IsJSON(arguments.httpData.content)>
			<cfset local.returnVal.status = 1 >
			<cfset local.returnVal.data = deserializeJSON(arguments.httpData.content) >
			<cfreturn local.returnVal>
		</cfif>
		<cfset local.returnVal.errorMsg = "Invalid API call, Please check the arguments">
		<cfreturn local.returnVal>
	</cffunction>

	<!--- function for validate arguments --->
	<cffunction name="argumentsValidation" access="public" returntype="Struct">
		<cfargument name="args" type="struct">
		<cfargument name="argsList" type="String">

		<cfset local.returnVal = structNew()>
		<cfset local.returnVal.status = 0>

		<cfset local.myKeyList = StructKeyList(arguments.args)>
		<cfset local.args = listToArray(local.myKeyList)>
		<cfset local.argsList= listToArray(arguments.argsList)>

		<cfset local.args.retainAll(local.argsList) />
		<cfif arrayLen(local.args) eq arrayLen(local.argsList)>
			<cfloop array="#local.argsList#" index="key">
				<cfif structKeyExists(arguments.args, "#key#") and not len(trim(evaluate("arguments.args.#key#"))) >
					<cfset local.returnVal.errorMsg = "Please check the key #key#">
					<cfreturn local.returnVal>
				</cfif>
			</cfloop>
			<cfset local.returnVal.status = 1 >
			<cfreturn local.returnVal>
		</cfif>
		<cfset local.returnVal.errorMsg = "Invalid API call, Please check the arguments">
		<cfreturn local.returnVal>
	</cffunction>

	<!--- function for login --->
	<cffunction name="login" access="remote" returnformat="plain" returntype="String">
		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "emailAddress,password">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal =structNew()>
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="local.qryResultsExist" datasource="#request.dsn#">
		            SELECT 	Password 
		            FROM 	Professionals
					WHERE 	Email_Address = <cfqueryparam value="#local.validateAPICall.data.emailAddress#" cfsqltype="cf_sql_varchar" />
		        </cfquery>
				<cfif local.qryResultsExist.RecordCount>
					<cfquery name="local.qryResultsLogin" datasource="#request.dsn#">
						SELECT 	Professionals.Professional_ID
								, Professionals.First_Name
								, Professionals.Last_Name
								, Companies.Company_ID AS Company_Admin
								, Locations.Location_ID
								, Companies_1.Company_ID AS Company_ID
						FROM        Locations 
						INNER JOIN 	Professionals ON Locations.Location_ID = Professionals.Location_ID 
						INNER JOIN 	Companies AS Companies_1 ON Locations.Company_ID = Companies_1.Company_ID 
						LEFT OUTER JOIN 	Companies ON Professionals.Professional_ID = Companies.Professional_Admin_ID
						WHERE Professionals.Email_Address=<cfqueryparam value="#local.validateAPICall.data.emailAddress#" cfsqltype="cf_sql_varchar" /> 
						AND Professionals.Password=<cfqueryparam value="#hash(local.validateAPICall.data.password, "SHA")#" cfsqltype="cf_sql_varchar" />
		        	</cfquery>
					<cfif local.qryResultsLogin.RecordCount>
						<cfset local.returnVal.status = 1 >
						<cfset local.returnVal.CustomerID = local.qryResultsLogin.Professional_ID />
						<cfset local.returnVal.First_Name = local.qryResultsLogin.First_Name />
						<cfset local.returnVal.Last_Name = local.qryResultsLogin.Last_Name />
						<cfset local.returnVal.Company_ID = local.qryResultsLogin.Company_ID />
						<cfset local.returnVal.Location_ID = local.qryResultsLogin.Location_ID />
						<cfset local.returnVal.Email_Address = local.validateAPICall.data.emailAddress />
					<cfelse>
						<cfset local.returnVal.errorMsg	 = "The password submitted did not match." />
					</cfif>
				<cfelse>
					<cfset local.returnVal.errorMsg = "The email address submitted does not have account.Please try another email address, or register." />
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<!--- function for register --->
	 <cffunction name="register" access="remote" returnformat="plain" returntype="String">
		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "firstName,lastName,phoneNumber,emailAddress,password">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal =structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >

			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction> 

</cfcomponent>