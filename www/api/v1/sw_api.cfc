<cfcomponent displayname="salonworks api">
	<cfset variables.paymentObj = createObject("component",".cfc.paymentMobile") />
	<cfset variables.objMailgun =  createObject("component",".cfc.mailgun") />
	<!--- <cfset variables.dsn  = "salonworksqa" /> --->
	<cfset variables.dsn  = "salonworks" />
	<cfset request.dsn  = variables.dsn />
	<!--- function for validate api call --->
	<cffunction name="validateAPICall" access="private" returntype="struct">
		<cfargument name="httpData" type="struct">
		<cfset local.returnVal = structNew()>
		<cfset local.returnVal.status = 0 >
		<cfif IsJSON(arguments.httpData.content)>
			<cfset local.returnVal.status = 1 >
			<cfset local.returnVal.data = deserializeJSON(arguments.httpData.content) >
			<cfreturn local.returnVal>
		</cfif>
		<cfset local.returnVal.errorMsg = "Invalid API call, Please check the arguments">
		<cfreturn local.returnVal>
	</cffunction>

	<!--- function for validate arguments --->
	<cffunction name="argumentsValidation" access="public" returntype="Struct">
		<cfargument name="args" type="struct">
		<cfargument name="argsList" type="String">
		<cfset local.returnVal = structNew()>
		<cfset local.returnVal.status = 0>

		<cfset local.myKeyList = StructKeyList(arguments.args)>
		<cfset local.args = listToArray(local.myKeyList)>
		<cfset local.argsList= listToArray(arguments.argsList)>

		<!--- <cfset local.args.retainAll(local.argsList) /> 
--->
		
		<cfif arrayLen(local.args) eq arrayLen(local.argsList)>
			<cfloop array="#local.argsList#" index="key">
				<cfif structKeyExists(arguments.args, "#key#") and not len(trim(evaluate("arguments.args.#key#"))) >
					<cfset local.returnVal.errorMsg = "Please enter #key#">
					<cfif #key# eq 'emailSubject'>
						<cfset local.returnVal.errorMsg = "Please enter Email subject">
					<cfelseif (#key# eq 'Customer_ID') >
						<cfset local.returnVal.errorMsg = "Please select a customer">
					<cfelseif (#key# eq 'Service_ID')>
						<cfset local.returnVal.errorMsg = "Please select a service">
					</cfif>
					<cfreturn local.returnVal>
				<cfelseif  structKeyExists(arguments.args, "Professional_ID")  and isNull(arguments.args.Professional_ID) >
					<cfset local.returnVal.errorMsg = "Professional id is empty">
					<cfreturn local.returnVal>
				<cfelseif  (structKeyExists(arguments.args, "first_name")  and len(trim(arguments.args.first_name)) LT 2) OR  (structKeyExists(arguments.args, "first_name")  and len(trim(arguments.args.first_name)) GE 50)>
					<cfset local.returnVal.errorMsg = "Please enter a valid First Name">
					<cfreturn local.returnVal>
				<cfelseif   (structKeyExists(arguments.args, "Last_Name")  and len(trim(arguments.args.Last_Name)) LT 1) OR  (structKeyExists(arguments.args, "Last_Name")  and len(trim(arguments.args.Last_Name)) GE 50)>
					<cfset local.returnVal.errorMsg = "Please enter a valid Last Name">
					<cfreturn local.returnVal>
				<cfelseif  ((structKeyExists(arguments.args, "emailAddress")  and not isValid("email", arguments.args.emailAddress))) OR ((structKeyExists(arguments.args, "email_address")  and  not isValid("email", arguments.args.email_address))) OR ((structKeyExists(arguments.args, "Company_Email")  and  not isValid("email", arguments.args.Company_Email)))>
					<cfset local.returnVal.errorMsg = "Please Enter a valid email address">
					<cfreturn local.returnVal>
				<cfelseif  structKeyExists(arguments.args, "phoneNumber")  and len(trim(evaluate("arguments.args.phoneNumber"))) LE 12>
					<cfset local.returnVal.errorMsg = "Please enter a valid Phone number">
					<cfreturn local.returnVal>
				<cfelseif  structKeyExists(arguments.args, "Mobile_Phone")  and len(trim(evaluate("arguments.args.Mobile_Phone"))) LE 12>
					<cfset local.returnVal.errorMsg = "Please enter a valid mobile number">
					<cfreturn local.returnVal>
				<cfelseif  structKeyExists(arguments.args, "Location_Phone")  and len(trim(evaluate("arguments.args.Location_Phone"))) LE 12>
					<cfset local.returnVal.errorMsg = "Please enter a valid location Phone number">
					<cfreturn local.returnVal>
				<cfelseif  (structKeyExists(arguments.args, "Password")  and len(trim(arguments.args.Password)) LT 8) OR  (structKeyExists(arguments.args, "Password")  and len(trim(arguments.args.Password)) GE 15)>
					<cfset local.returnVal.errorMsg = "Password length between 8 to 15 characters">
					<cfreturn local.returnVal>
				<cfelseif  structKeyExists(arguments.args, "Service_Time") and  not isValid("integer" , arguments.args.Service_Time) >
					<cfset local.returnVal.errorMsg = "Please enter a valid service time">
					<cfreturn local.returnVal>
				<cfelseif  structKeyExists(arguments.args, "Service_Price")  and arguments.args.Service_Price eq 0>
					<cfif not isValid("numeric" , arguments.args.Service_Price) >
						<cfset local.returnVal.errorMsg = "Please enter a valid price">
						<cfreturn local.returnVal>
					<cfelse>
						<cfset local.returnVal.errorMsg = "Please enter a valid price">
						<cfreturn local.returnVal>
					</cfif>
				<cfelseif structKeyExists(arguments.args, "Service_Price")  and not isValid("numeric" , arguments.args.Service_Price) >
				 	<cfset local.returnVal.errorMsg = "Please enter a valid price">
					<cfreturn local.returnVal>
				<cfelseif  structKeyExists(arguments.args, "amount")  and arguments.args.amount eq 0>
					<cfif not isValid("numeric" , arguments.args.amount) >
						<cfset local.returnVal.errorMsg = "Please enter a valid amount">
						<cfreturn local.returnVal>
					<cfelse>
						<cfset local.returnVal.errorMsg = "Please enter a valid amount">
						<cfreturn local.returnVal>
					</cfif>
				<cfelseif structKeyExists(arguments.args, "amount")  and not isValid("numeric" , arguments.args.amount) >
				 	<cfset local.returnVal.errorMsg = "Please enter a valid amount">
					<cfreturn local.returnVal>
				<cfelseif  (structKeyExists(arguments.args, "gallery_description")  and len(trim(arguments.args.gallery_description)) GT 250) >
					<cfset local.returnVal.errorMsg = "Maximum 250 characters allowed in description">
					<cfreturn local.returnVal>
				<cfelseif  (structKeyExists(arguments.args, "Description")  and len(trim(arguments.args.Description)) GT 1500) >
					<cfset local.returnVal.errorMsg = "Maximum 1500 characters allowed in description">
					<cfreturn local.returnVal>	
				</cfif>
			</cfloop>
			
			<cfset local.returnVal.status = 1 >
			<cfreturn local.returnVal>
		</cfif>
		<cfset local.returnVal.errorMsg = "Invalid API call, Please check the arguments">
		<cfreturn local.returnVal>
	</cffunction>

	<!--- function for login --->
	<cffunction name="login" access="remote" returnformat="plain" returntype="String">
		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "emailAddress,logPassword">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal =structNew()>
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="local.qryResultsExist" datasource="#request.dsn#">
		            SELECT 	Password 
		            FROM 	Professionals
					WHERE 	Email_Address = <cfqueryparam value="#local.validateAPICall.data.emailAddress#" cfsqltype="cf_sql_varchar" />
		        </cfquery>
				<cfif local.qryResultsExist.RecordCount>
					<cfquery name="local.qryResultsLogin" datasource="#request.dsn#">
						SELECT 	Professionals.Professional_ID
								, Professionals.First_Name
								, Professionals.Last_Name
								, Companies.Company_ID AS Company_Admin
								, Locations.Location_ID
								, Companies_1.Company_ID AS Company_ID
								,Professionals_Services.Service_ID
						FROM        Locations 
						INNER JOIN 	Professionals ON Locations.Location_ID = Professionals.Location_ID 
						INNER JOIN 	Companies AS Companies_1 ON Locations.Company_ID = Companies_1.Company_ID 
						LEFT JOIN Professionals_Services AS Professionals_Services ON Professionals.Professional_ID = Professionals_Services.Professional_ID
						LEFT OUTER JOIN 	Companies ON Professionals.Professional_ID = Companies.Professional_Admin_ID
						WHERE Professionals.Email_Address=<cfqueryparam value="#local.validateAPICall.data.emailAddress#" cfsqltype="cf_sql_varchar" /> 
						AND Professionals.Password=<cfqueryparam value="#hash(local.validateAPICall.data.logPassword, "SHA")#" cfsqltype="cf_sql_varchar" />
		        	</cfquery>
					<cfif local.qryResultsLogin.RecordCount>
						<cfset local.returnVal.status = 1 >
						<cfset local.returnVal.Professional_ID = local.qryResultsLogin.Professional_ID />
						<cfset local.returnVal.Name = local.qryResultsLogin.First_Name&' '&local.qryResultsLogin.Last_Name />
						<cfset local.returnVal.First_Name = local.qryResultsLogin.First_Name />
						<cfset local.returnVal.Last_Name = local.qryResultsLogin.Last_Name />
						<cfset local.returnVal.Company_ID = local.qryResultsLogin.Company_ID />
						<cfset local.returnVal.Location_ID = local.qryResultsLogin.Location_ID />
						<cfset local.returnVal.Email_Address = local.validateAPICall.data.emailAddress />
						<cfset local.returnVal.Service_ID = local.qryResultsLogin.Service_ID />
					<cfelse>
						<cfset local.returnVal.status = 0>
						<cfset local.returnVal.errorMsg	 = "The password submitted did not match." />
					</cfif>
				<cfelse>
					<cfset local.returnVal.status = 0>
					<cfset local.returnVal.errorMsg = "The email address submitted does not have an account.Please try another email address, or register." />
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<!--- function for Register--->
	<cffunction name="register" access="remote" returnformat="plain" returntype="String">
		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "first_name,Last_Name,phoneNumber,emailAddress,password">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal =structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.responseStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.responseStatus >
			<cfif local.responseStatus.status >
				<cfquery name="local.qryResultsExist" datasource="#request.dsn#">
		            SELECT 	Email_Address 
		            FROM 	Professionals
					WHERE 	Email_Address = <cfqueryparam value="#local.validateAPICall.data.emailAddress#" cfsqltype="cf_sql_varchar" />
		        </cfquery>
		        <cfif  local.qryResultsExist.RecordCount>
		        	<cfset local.returnVal.status = 0 >
					<cfset local.returnVal.errorMsg = "Email address already exist." />
				<cfelse>
					<cfset local.returnVal.Company_ID = InsertCompany()>
					<cfset local.returnVal.Location_ID = InsertLocation( Company_ID = local.returnVal.Company_ID)>
					<cfset local.returnVal.Professional_ID = InsertProfessional( Location_ID = local.returnVal.Location_ID,First_Name =local.validateAPICall.data.first_name,Last_Name = local.validateAPICall.data.Last_Name,
						Password = local.validateAPICall.data.password,
						Email_Address = local.validateAPICall.data.emailAddress,
						Mobile_Phone = local.validateAPICall.data.phoneNumber
						)>
					<!---<cfsavecontent variable="mailBody">
						<cfoutput>
							<div style="background: ##f0f3f6; min-height: 100vh;">
					      <div class="container" style="margin: auto; max-width: 100%; width: 840px; border:1px solid ##ddd; border-bottom: 0; background: ##fff;">
					        <table class="main-width" style="border-spacing: 0; width:100%;" border="0" cellspacing="0" cellpadding="0"><!--logo-space-->
					          <tbody>
					            <tr>
					              <td>
					                <table style="width: 840px; margin: auto; border-spacing: 0px;"><!--text-content-->
					                  <tbody>
					                    <tr class="" style="width: 100%; background: ##fff; text-align: center;">
					                      <td style="padding: 20px 0;"> <a href=""><img src="https://salonworks.com/salonnewhome/img/logo.png" alt=""></a></td>
					                    </tr>
					                    <table border="0" cellspacing="0" cellpadding="0" style=" height:265px; width: 100%; text-align: left;">
					                      <tr>
					                        <td style="padding: 10px 50px;">

					                          <h2 style="font-family: Roboto, sans-serif, arial;" >Hello #local.validateAPICall.data.first_name#!</h2>

					                          <p style="font: 17px/23px 'Roboto', sans-serif, arial; color:##646464; margin-bottom:40px;">
					                        
					                            Congratulations on your new account with SalonWorks!<br><br>
					                            
					                           

					                            You are on your way to filling your appointment book and streamlining the way you do business. <br><br>

					                            An Onboarding Specialist will contact you shortly to help you make the absolute most of your account. However, if you need to speak to someone immediately, please feel free to call us at (978)352-0235.<br><br>

					                            *Add <strong><a style="color: ##8a171a;">salonworks@salonworks.com</a><!--- salonworks@salonworks.com ---></strong> to your address book to make sure you don't miss out on our emails.
					                          </p>
					                        </td>
					                      </tr>
					                      <tr>
					                        <td style="padding: 10px 50px;">
					                          <p style="font: 17px/23px 'Roboto', sans-serif, arial; color:##646464; padding-top: 25px; border-top: 1px solid ##eee;">
					                            Regards,<br>
					                            SalonWorks Customer Support<br>
					                            <!--- salonworks@salonworks.com --->
					                            <a style="color: ##8a171a;">salonworks@salonworks.com</a>
					                          </p>
					                        </td>
					                      </tr>
					                    </table>
					                    <table border="0" cellspacing="0" cellpadding="0" style=" height:95px; width: 100%; text-align: center; background: ##8a171a; padding: 34px 5px;">
					                      <tr>
					                        <td style="width: 33%; float: left; ">
					                          <a href="https://www.facebook.com/pages/Salonworks/1434509316766493" style="margin-right: 10px;"><img src="http://salonworks.com/images/facebook_round.png" alt=""></a>
					                        </td>
					                        <td style="width: 33%; float: left; font:14px/23px 'Roboto', sans-serif, arial;">
					                          <img src="http://salonworks.com/images/call.png" alt="" style="vertical-align: middle; margin-right: 5px;">
					                          <label>
					                           <a href="tel:+ (978) 352-0235" style="color: ##fff; text-decoration:  none;">+ (978) 352-0235</a>
					                          </label>
					                        </td>
					                        <td style="width: 33%; float: left; font:14px/23px 'Roboto', sans-serif, arial;">
					                          <img src="http://salonworks.com/images/mail.png" alt="" style="vertical-align: middle; margin-right: 5px;">
					                          <label>
					                           <a href="mailto:salonworks@salonworks.com" style="color: ##fff; text-decoration:  none;">salonworks@salonworks.com </a>
					                          </label>
					                        </td>
					                      </tr>
					                    </table>
					                  </tbody>
					                </table>
					              </td>
					            </tr>
					          </tbody>
					        </table>
					      </div>
					    </div> 
						</cfoutput>
					</cfsavecontent>--->
					<!---<cfset local.mailCustomer = variables.objMailgun.sendMailViaMailgun(mailTo="#local.validateAPICall.data.emailAddress#",mailFrom="salonworks@salonworks.com",mailSubject="Welcome To SalonWorks",mailHtml = "#mailBody#" ) />--->
					<cfmail from="salonworks@salonworks.com" to="#local.validateAPICall.data.emailAddress#"  subject="Welcome To SalonWorks" server="smtp-relay.sendinblue.com" port="587"  username="ciredrofdarb@gmail.com" password="2xf5ZLbMdyDr0VSv" type="HTML" usetls="true">
					  <cfmailpart type="text/html">
					    <div style="background: ##f0f3f6; min-height: 100vh;">
					      <div class="container" style="margin: auto; max-width: 100%; width: 840px; border:1px solid ##ddd; border-bottom: 0; background: ##fff;">
					        <table class="main-width" style="border-spacing: 0; width:100%;" border="0" cellspacing="0" cellpadding="0"><!--logo-space-->
					          <tbody>
					            <tr>
					              <td>
					                <table style="width: 840px; margin: auto; border-spacing: 0px;"><!--text-content-->
					                  <tbody>
					                    <tr class="" style="width: 100%; background: ##fff; text-align: center;">
					                      <td style="padding: 20px 0;"> <a href=""><img src="https://salonworks.com/salonnewhome/img/logo.png" alt=""></a></td>
					                    </tr>
					                    <table border="0" cellspacing="0" cellpadding="0" style=" height:265px; width: 100%; text-align: left;">
					                      <tr>
					                        <td style="padding: 10px 50px;">

					                          <h2 style="font-family: Roboto, sans-serif, arial;" >Hello #local.validateAPICall.data.first_name#!</h2>

					                          <p style="font: 17px/23px 'Roboto', sans-serif, arial; color:##646464; margin-bottom:40px;">
					                        
					                            Congratulations on your new account with SalonWorks!<br><br>
					                            
					                           

					                            You are on your way to filling your appointment book and streamlining the way you do business. <br><br>

					                            An Onboarding Specialist will contact you shortly to help you make the absolute most of your account. However, if you need to speak to someone immediately, please feel free to call us at (978)352-0235.<br><br>

					                            *Add <strong><a style="color: ##8a171a;">salonworks@salonworks.com</a><!--- salonworks@salonworks.com ---></strong> to your address book to make sure you don't miss out on our emails.
					                          </p>
					                        </td>
					                      </tr>
					                      <tr>
					                        <td style="padding: 10px 50px;">
					                          <p style="font: 17px/23px 'Roboto', sans-serif, arial; color:##646464; padding-top: 25px; border-top: 1px solid ##eee;">
					                            Regards,<br>
					                            SalonWorks Customer Support<br>
					                            <!--- salonworks@salonworks.com --->
					                            <a style="color: ##8a171a;">salonworks@salonworks.com</a>
					                          </p>
					                        </td>
					                      </tr>
					                    </table>
					                    <table border="0" cellspacing="0" cellpadding="0" style=" height:95px; width: 100%; text-align: center; background: ##8a171a; padding: 34px 5px;">
					                      <tr>
					                        <td style="width: 33%; float: left; ">
					                          <a href="https://www.facebook.com/pages/Salonworks/1434509316766493" style="margin-right: 10px;"><img src="http://salonworks.com/images/facebook_round.png" alt=""></a>
					                        </td>
					                        <td style="width: 33%; float: left; font:14px/23px 'Roboto', sans-serif, arial;">
					                          <img src="http://salonworks.com/images/call.png" alt="" style="vertical-align: middle; margin-right: 5px;">
					                          <label>
					                           <a href="tel:+ (978) 352-0235" style="color: ##fff; text-decoration:  none;">+ (978) 352-0235</a>
					                          </label>
					                        </td>
					                        <td style="width: 33%; float: left; font:14px/23px 'Roboto', sans-serif, arial;">
					                          <img src="http://salonworks.com/images/mail.png" alt="" style="vertical-align: middle; margin-right: 5px;">
					                          <label>
					                           <a href="mailto:salonworks@salonworks.com" style="color: ##fff; text-decoration:  none;">salonworks@salonworks.com </a>
					                          </label>
					                        </td>
					                      </tr>
					                    </table>
					                  </tbody>
					                </table>
					              </td>
					            </tr>
					          </tbody>
					        </table>
					      </div>
					    </div>
					  </cfmailpart>
					</cfmail>
					<cfset local.returnVal.First_Name = local.validateAPICall.data.first_name />
					<cfset local.returnVal.Last_Name = local.validateAPICall.data.Last_Name />
					<cfset local.returnVal.Email_Address = local.validateAPICall.data.emailAddress />
					<cfset local.returnVal.phoneNumber = local.validateAPICall.data.phoneNumber />
					<cfset local.returnVal.status = 1 >
		        </cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<!--- function for forgot password --->
	 <cffunction name="forgotPassword" access="remote" returnformat="plain" returntype="String">
		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "emailAddress">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal =structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="local.qryResultsExist" datasource="#request.dsn#">
		            SELECT 	Email_Address,Professional_ID,First_Name,Last_Name,Mobile_Phone,Location_ID
		            FROM 	Professionals
					WHERE 	Email_Address = <cfqueryparam value="#local.validateAPICall.data.emailAddress#" cfsqltype="cf_sql_varchar" />
		        </cfquery>
		        <cfif  local.qryResultsExist.RecordCount>
		        	<cfset local.verificationCode = GetTickCount()>
					<cfset local.verificationCode = Right(local.verificationCode, 6)>
					<cfset local.expirationTime = CreateTime(0, 05, 00) />
					<cfquery name="local.updTblResetKey" datasource="#request.dsn#" result="rupdTblResetKey">
						UPDATE Professionals
		   				SET otp = <cfqueryparam value="#local.verificationCode#" cfsqltype="cf_sql_integer">,	pwdReset = <cfqueryparam value="0" cfsqltype="cf_sql_bit">
		   					,otpTime = <cfqueryparam value="#now()#" cfsqltype="cf_sql_timestamp">
		   				WHERE Professional_ID = <cfqueryparam value="#local.qryResultsExist.Professional_ID#" cfsqltype="cf_sql_integer">
					</cfquery>
					<!---<cfsavecontent variable="mailBody">
						<cfoutput>
							<div style="background: ##f0f3f6; min-height: 100vh;">
								<div class="container" style="margin: auto; max-width: 100%; width: 840px; border:1px solid ##ddd; border-bottom: 0; background: ##fff;">
									<table class="main-width" style="border-spacing: 0; width:100%;" border="0" cellspacing="0" cellpadding="0"><!--logo-space-->
										<tbody>
											<tr>
												<td>
													<table style="width: 840px; margin: auto; border-spacing: 0px;"><!--text-content-->
														<tbody>
															<tr class="" style="width: 100%; background: ##fff; text-align: center;">
																<td style="padding: 20px 0;"> <a href=""><img src="http://salonworks.com/img/logo.png" alt=""></a></td>
															</tr>
															<table border="0" cellspacing="0" cellpadding="0" style=" height:265px; width: 100%; text-align: left;">
																<tr>
																	<td style="padding: 10px 50px;">

																	<h2 style="font-family: Roboto, sans-serif, arial;" >Hello #local.qryResultsExist.First_Name#' '#local.qryResultsExist.Last_Name#</h2>

																		<p style="font: 17px/23px 'Roboto', sans-serif, arial; color:##646464; margin-bottom:40px;">
																	
																			This is your one time password for resetting the password :<br>
																			<!--- <a href="#cgi.server_name#/admin/login.cfm?reGenPass=#local.uuid#">Click here to change the password</a> --->
																			<b>#local.verificationCode#</b>
																			<br><i>(NB:Please don't share it with any one.)<i>			

																		</p>
																	</td>
																</tr>
																<tr>
																	<td style="padding: 10px 50px;">
																		<p style="font: 17px/23px 'Roboto', sans-serif, arial; color:##646464; padding-top: 25px; border-top: 1px solid ##eee;">
																			Regards,<br>
																			SalonWorks Customer Support<br>
																			salonworks@salonworks.com
																 		</p>
																	</td>
																</tr>
															</table>
														<!--##1f2937-->

															<table border="0" cellspacing="0" cellpadding="0" style=" height:95px; width: 100%; text-align: center; background: ##2995d3; padding: 34px 5px;">
																<tr>
																<td style="width: 33%; float: left; ">
																	<a href="https://www.facebook.com/pages/Salonworks/1434509316766493" style="margin-right: 10px;"><img src="http://salonworks.com/images/facebook_round.png" alt=""></a>
																</td>
																<td style="width: 33%; float: left; font:14px/23px 'Roboto', sans-serif, arial;">
																<img src="http://salonworks.com/images/call.png" alt="" style="vertical-align: middle; margin-right: 5px;">
																	<label>
																	 <a href="tel:+ (978) 352-0235" style="color: ##fff; text-decoration:  none;">+ (978) 352-0235</a>
																	</label>
																</td>

																<td style="width: 33%; float: left; font:14px/23px 'Roboto', sans-serif, arial;">
																<img src="http://salonworks.com/images/mail.png" alt="" style="vertical-align: middle; margin-right: 5px;">
																	<label>
																	 <a href="mailto:salonworks@salonworks.com" style="color: ##fff; text-decoration:  none;">salonworks@salonworks.com </a>
																	</label>

																</td>
																</tr>
															</table>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div> 
						</cfoutput>
					</cfsavecontent>
					<cfset local.mailCustomer = variables.objMailgun.sendMailViaMailgun(mailTo="#local.qryResultsExist.Email_Address#",mailFrom="salonworks@salonworks.com",mailSubject="New Password for Login",mailHtml = "#mailBody#" ) />--->
					<cfmail from="salonworks@salonworks.com" to="#local.qryResultsExist.Email_Address#"  subject="New Password for Login" server="smtp-relay.sendinblue.com" port="587" 
					username="ciredrofdarb@gmail.com" password="2xf5ZLbMdyDr0VSv"  useTLS ="yes" type="html">
						<cfmailpart type="text/html">
							<div style="background: ##f0f3f6; min-height: 100vh;">
								<div class="container" style="margin: auto; max-width: 100%; width: 840px; border:1px solid ##ddd; border-bottom: 0; background: ##fff;">
									<table class="main-width" style="border-spacing: 0; width:100%;" border="0" cellspacing="0" cellpadding="0"><!--logo-space-->
										<tbody>
											<tr>
												<td>
													<table style="width: 840px; margin: auto; border-spacing: 0px;"><!--text-content-->
														<tbody>
															<tr class="" style="width: 100%; background: ##fff; text-align: center;">
																<td style="padding: 20px 0;"> <a href=""><img src="http://salonworks.com/img/logo.png" alt=""></a></td>
															</tr>
															<table border="0" cellspacing="0" cellpadding="0" style=" height:265px; width: 100%; text-align: left;">
																<tr>
																	<td style="padding: 10px 50px;">

																	<h2 style="font-family: Roboto, sans-serif, arial;" >Hello #local.qryResultsExist.First_Name#' '#local.qryResultsExist.Last_Name#</h2>

																		<p style="font: 17px/23px 'Roboto', sans-serif, arial; color:##646464; margin-bottom:40px;">
																	
																			This is your one time password for resetting the password :<br>
																			<!--- <a href="#cgi.server_name#/admin/login.cfm?reGenPass=#local.uuid#">Click here to change the password</a> --->
																			<b>#local.verificationCode#</b>
																			<br><i>(NB:Please don't share it with any one.)<i>			

																		</p>
																	</td>
																</tr>
																<tr>
																	<td style="padding: 10px 50px;">
																		<p style="font: 17px/23px 'Roboto', sans-serif, arial; color:##646464; padding-top: 25px; border-top: 1px solid ##eee;">
																			Regards,<br>
																			SalonWorks Customer Support<br>
																			salonworks@salonworks.com
																 		</p>
																	</td>
																</tr>
															</table>
														<!--##1f2937-->

															<table border="0" cellspacing="0" cellpadding="0" style=" height:95px; width: 100%; text-align: center; background: ##2995d3; padding: 34px 5px;">
																<tr>
																<td style="width: 33%; float: left; ">
																	<a href="https://www.facebook.com/pages/Salonworks/1434509316766493" style="margin-right: 10px;"><img src="http://salonworks.com/images/facebook_round.png" alt=""></a>
																</td>
																<td style="width: 33%; float: left; font:14px/23px 'Roboto', sans-serif, arial;">
																<img src="http://salonworks.com/images/call.png" alt="" style="vertical-align: middle; margin-right: 5px;">
																	<label>
																	 <a href="tel:+ (978) 352-0235" style="color: ##fff; text-decoration:  none;">+ (978) 352-0235</a>
																	</label>
																</td>

																<td style="width: 33%; float: left; font:14px/23px 'Roboto', sans-serif, arial;">
																<img src="http://salonworks.com/images/mail.png" alt="" style="vertical-align: middle; margin-right: 5px;">
																	<label>
																	 <a href="mailto:salonworks@salonworks.com" style="color: ##fff; text-decoration:  none;">salonworks@salonworks.com </a>
																	</label>

																</td>
																</tr>
															</table>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</cfmailpart>
					</cfmail>
					<cfquery name="local.getCompanyData" datasource="#request.dsn#">
						select c.Company_ID
						from Locations as l
						inner join Companies as c
						on l.Company_ID = c.Company_ID
						where Location_ID = <cfqueryparam value="#local.qryResultsExist.Location_ID#" cfsqltype="cf_sql_integer" />
					</cfquery>
					<cfset local.returnVal.status = 1 >
					<cfset local.returnVal.Professional_ID = local.qryResultsExist.Professional_ID />
					<cfset local.returnVal.Company_ID = local.getCompanyData.Company_ID />
					<cfset local.returnVal.Location_ID = local.qryResultsExist.Location_ID />
					<cfset local.returnVal.Email_Address = local.validateAPICall.data.emailAddress />
					<cfset local.returnVal.otp = local.verificationCode />
				<cfelse>
					<cfset local.returnVal.status = 0 >
					<cfset local.returnVal.errorMsg = "The email address not exists." />
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction> 

	<!--- function for add company details  --->
	<cffunction name="InsertCompany" access="public" output="false" returntype="numeric">
		<cfset Trial_Expiration = DateFormat(DateAdd("m",1,Now()),'dd-mmm-yyyy')>
		<cfset local.appointment_count=GetTickCount()>
		<cfset local.appointment_code=Right(local.appointment_count, 8)>
		<cftransaction isolation="READ_COMMITTED">
			<cfquery name="InsertCompany" datasource="#request.dsn#" result="rInsertCompany">
				INSERT INTO Companies(
					Trial_Expiration,
					appointment_code
				) VALUES (
					'#Trial_Expiration#',
					'#local.appointment_code#'
				)
			</cfquery>
			<!--- Configure company as a trial account ---> 
			<cfquery name="getTrial" datasource="#request.dsn#">
				INSERT INTO Company_Prices
				(Company_ID, Company_Service_Plan_ID, Price)
					VALUES
				(#rInsertCompany.generatedkey#,2,49.99)
			</cfquery>
			<cfset newCompany_ID =  rInsertCompany.generatedkey>
		</cftransaction>
		<cfreturn newCompany_ID>
	</cffunction>	

	<!--- function for add location details  --->
	<cffunction name="InsertLocation" access="public" output="false" returntype="numeric" hint="">
		<cfargument name="Company_ID" type="string" required="false" default="">
		<cftransaction isolation="READ_COMMITTED">
			<cfquery name="InsertLocation" datasource="#request.dsn#" result="rInsertLocation">
				INSERT INTO Locations
				(Company_ID) VALUES ('#arguments.Company_ID#')
			</cfquery>
			<cfset newLocation_ID =  rInsertLocation.generatedkey>
		</cftransaction>
		<cfreturn newLocation_ID>
	</cffunction> 

	<!--- function for add professional details  --->
	<cffunction name="InsertProfessional" access="public" output="false" returntype="numeric" hint="">
		<cfargument name="Location_ID" type="string" required="false" default="">
		<cfargument name="First_Name" type="string" required="false" default="">
		<cfargument name="Last_Name" type="string" required="false" default="">
		<cfargument name="Email_Address" type="string" required="false" default="">
		<cfargument name="socialId" type="string" required="false" default="">
		<cfargument name="loginType" type="string" required="false" default="">
		<cfargument name="password" type="string" required="false" default="">
		<cftransaction isolation="READ_COMMITTED">
			<cfquery name="InsertProfessional" datasource="#request.dsn#" result="rInsertProfessional">
				INSERT INTO Professionals(
					Location_ID,First_Name,Last_Name,Email_Address,socialId,loginType,password,Active_Flag
				) VALUES (
					'#arguments.Location_ID#'
					,'#arguments.First_Name#'
					,'#arguments.Last_Name#'
					,'#arguments.Email_Address#'
					,'#arguments.socialId#'
					,'#arguments.loginType#'
					,'#hash(arguments.password,"SHA")#'
					,1
				)
			</cfquery>
			<cfset newProfessional_ID =  rInsertProfessional.generatedkey>
		</cftransaction>
		<cfreturn newProfessional_ID>
	</cffunction> 

	<!--- function to login through Facebook and Gmail  --->
	<cffunction name="socialLogin" access="remote" returnformat="plain" returntype="String">
		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "socialId,loginType,firstName,lastName">
		<cfif local.validateAPICall.data.loginType eq 'G'>
			<cfset local.argsList = "socialId,loginType,firstName,lastName,emailAddress">
		</cfif>

		<cfif local.validateAPICall.status >
			<cfset local.returnVal =structNew()>
			<cfset local.returnVal.status = 0 >
			<cfset local.responseStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.responseStatus >
			<cfif local.responseStatus.status >
				<cfquery name="local.qryResultsExist" datasource="#request.dsn#">
		           SELECT TOP 1 * FROM Professionals
					WHERE socialId = <cfqueryparam value="#local.validateAPICall.data.socialId#" cfsqltype="cf_sql_varchar" />
					AND loginType = <cfqueryparam value="#local.validateAPICall.data.loginType#" cfsqltype="cf_sql_varchar" />
		        </cfquery>
		        
                <!---  Begin qryResultsExist  ---> 
				<cfif local.qryResultsExist.RecordCount>

					<cfquery name="local.getCompanyData" datasource="#request.dsn#">
						select c.Company_ID
						from Locations as l
						inner join Companies as c
						on l.Company_ID = c.Company_ID
						where Location_ID = <cfqueryparam value="#local.qryResultsExist.Location_ID#" cfsqltype="cf_sql_integer" />
					</cfquery>
					
					<cfset local.returnVal.status = 1 >
					<cfset local.returnVal.CustomerID = local.qryResultsExist.Professional_ID />
					<cfset local.returnVal.First_Name = local.qryResultsExist.First_Name />
					<cfset local.returnVal.Last_Name = local.qryResultsExist.Last_Name />
					<cfset local.returnVal.name = '#local.validateAPICall.data.firstName#'&' '&'#local.validateAPICall.data.lastName#' />
					<cfset local.returnVal.Company_ID = local.getCompanyData.Company_ID />
					<cfset local.returnVal.Location_ID = local.qryResultsExist.Location_ID />
					<cfset local.returnVal.Email_Address = local.qryResultsExist.Email_Address />
					<cfset local.returnVal.Professional_ID = local.qryResultsExist.Professional_ID />
					
				<cfelse>
					<cfset local.email = '' >
					<cfset local.returnVal.status = 1/>
					<cfset local.returnVal.Company_ID = InsertCompany()>
					<cfset local.returnVal.Location_ID = InsertLocation( Company_ID = local.returnVal.Company_ID)>
					<cfif local.validateAPICall.data.loginType eq 'G' >
						<cfset local.email = local.validateAPICall.data.emailAddress />
					</cfif>
					<cfset local.returnVal.Professional_ID = InsertProfessional( Location_ID = local.returnVal.Location_ID,First_Name =local.validateAPICall.data.firstName,Last_Name = local.validateAPICall.data.lastName,
						Email_Address =  local.email,
						socialId = local.validateAPICall.data.socialId,loginType = local.validateAPICall.data.loginType )>
					<cfset local.returnVal.First_Name = local.validateAPICall.data.firstName />
					<cfset local.returnVal.Last_Name = local.validateAPICall.data.lastName />
					<cfset local.returnVal.name = '#local.validateAPICall.data.firstName#'&' '&'#local.validateAPICall.data.lastName#' />
					<cfset local.returnVal.Email_Address = local.email />
					
				</cfif> <!---  End qryResultsExist  ---> 
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
	
	<!--- function to check otp for reset password  --->
	<cffunction name="checkOtp" access="remote" returnformat="plain" returntype="String">
		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "otp,Professional_ID,password,confirmPassword">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name = "local.qCheckOtp" datasource="#request.dsn#">
					SELECT pwdReset,otp,otpTime,Professional_ID,Location_ID,First_Name,Last_Name,Mobile_Phone
					FROM Professionals
					WHERE
					Professional_ID = <cfqueryparam value = "#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer" >
					AND otp = <cfqueryparam value = "#local.validateAPICall.data.otp#" cfsqltype="cf_sql_integer" >
				</cfquery>

				<cfif local.qCheckOtp.RecordCount>
					<cfset local.timeDiff = DateDiff('n',dateTimeFormat(local.qCheckOtp.otpTime,'yyyy-mm-dd hh:nn') , dateTimeFormat(now(),'yyyy-mm-dd hh:nn')) >
					<cfif local.timeDiff LE 10>
						<cfquery name="local.getCompanyData" datasource="#request.dsn#">
							select c.Company_ID
							from Locations as l
							inner join Companies as c
							on l.Company_ID = c.Company_ID
							where Location_ID = <cfqueryparam value="#local.qCheckOtp.Location_ID#" cfsqltype="cf_sql_integer" />
						</cfquery>
						<cfif trim(#hash(local.validateAPICall.data.password, "SHA")#) eq trim(#hash(local.validateAPICall.data.confirmPassword, "SHA")#) >
							<cfset local.changePwStatus = resetPassword(password = local.validateAPICall.data.password, Professional_ID =  local.qCheckOtp.Professional_ID) >
							<cfset local.returnVal.Professional_ID = local.qCheckOtp.Professional_ID />
							<cfset local.returnVal.Company_ID = local.getCompanyData.Company_ID />
							<cfset local.returnVal.Location_ID = local.qCheckOtp.Location_ID />
							<cfset local.returnVal.status = 1 >
						<cfelse>
							<cfset local.returnVal.status = 0 >
							<cfset local.returnVal.errorMsg = "Password mismatch ." />
						</cfif>
					<cfelse>
						<cfset local.returnVal.status = 0 >
						<cfset local.returnVal.errorMsg = "The entered otp is incorrect or expired." />
					</cfif>

				<cfelse>
					<cfset local.returnVal.status = 0 >
					<cfset local.returnVal.errorMsg = "The entered otp is incorrect or expired." />
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction> 

	<!--- function for reset password  --->
	<cffunction name="resetPassword" access="remote" returnformat="plain" returntype="String">
		<cfargument name="password" type="string" >
		<cfargument name="Professional_ID" type="numeric" >
		<cfquery name="updNewPass" datasource="#request.dsn#" result="upd">
			UPDATE Professionals 
			SET Password = <cfqueryparam value="#hash(arguments.password, "SHA")#" cfsqltype="cf_sql_varchar">,
				pwdReset = <cfqueryparam value="1" cfsqltype="cf_sql_bit">
			WHERE Professional_ID = <cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer">
		</cfquery>	
		<cfreturn 1 >
	</cffunction>
	
	<!--- Function for book/add appointments Calendar --->
    <cffunction name="appointments" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Customer_ID,Professional_ID,Service_ID,Start_Time,start_Date">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfif len(local.validateAPICall.data.Professional_ID) and len(local.validateAPICall.data.Service_ID) >
					<cfquery name="local.qGetServiceDetails" datasource="#request.dsn#" >
						SELECT Service_Time
						FROM Professionals_Services
						WHERE Service_ID = <cfqueryparam value="#local.validateAPICall.data.Service_ID#" cfsqltype="cf_sql_integer">
						and Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer">
					</cfquery>
					<cfif local.qGetServiceDetails.recordcount > 
						<cfset start_Date = local.validateAPICall.data.start_Date />
						<cfset end_Date =  local.validateAPICall.data.start_Date />
						<cfset start_tym =  local.validateAPICall.data.Start_Time />
						<cfset end_tym =  local.validateAPICall.data.Start_Time />
						<cfset isAppointment =  1 />
						<cfset service_Time =  local.qGetServiceDetails.Service_Time />						
						<cfset local.data = createTimeformatCalender(start_Date=start_Date,end_Date=end_Date,Start_Time=start_tym,End_Time=end_tym,isAppointment = isAppointment,service_Time = service_Time) >	
						<cfset dateTimeStart = local.data.dateTimeStart />
						<cfset dateTimeEnd = local.data.dateTimeEnd />
						<cfset local.addAppointment = addAppointment(CustomerID = local.validateAPICall.data.Customer_ID,Professional_ID = local.validateAPICall.data.Professional_ID,ServiceID=local.validateAPICall.data.Service_ID,apptStartTime=dateTimeStart,apptEndTime =dateTimeEnd) >
						<cfset local.returnVal.Appointment_ID = local.addAppointment.id >
						<cfset local.returnVal.status = 1 />
					<cfelse>
						<cfset local.returnVal.status = 0 />
						<cfset local.returnVal.errorMsg = "Please check your arguments" />
					</cfif>
				<cfelse>
					<cfset local.returnVal.status = 0 />
					<cfset local.returnVal.errorMsg = "Please check your arguments" />
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction> 
	<!--- <cffunction name="ISOToDateTime" access="public" returntype="string" output="false" hint="Converts an ISO 8601 date/time stamp with optional dashes to a ColdFusion date/time stamp.">
		<cfargument name="Date" type="string" required="true" hint="ISO 8601 date/time stamp." >
        <cfreturn arguments.Date.ReplaceFirst("^.*?(\d{4})-?(\d{2})-?(\d{2})T([\d:]+).*$","$1-$2-$3 $4") >
	</cffunction> --->
	<!--- This function extends bookAppointments --->
	<cffunction name="addAppointment" access="remote" output="false" returntype="struct" >
		<cfargument name="CustomerID" type="numeric" required="true" />
		<cfargument name="Professional_ID" type="numeric" required="true" />
		<cfargument name="ServiceID" type="numeric" required="true" />		
		<cfargument name="apptStartTime" type="any" required="true" />
		<cfargument name="apptEndTime" type="any" required="true" />
		<cfset var local = StructNew() />
		
		<cfquery name="local.qryInsert" datasource="#request.dsn#" result="qryResult">
			INSERT INTO Appointments(Customer_ID, Professional_ID, Service_ID, Start_Time, End_Time, Date_Booked)
			VALUES(
					<cfqueryparam value="#arguments.CustomerID#" cfsqltype="cf_sql_integer" />,
					<cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer" />,
					<cfqueryparam value="#arguments.ServiceID#" cfsqltype="cf_sql_integer" />,
					<cfqueryparam value="#arguments.apptStartTime#" cfsqltype="cf_sql_timestamp" />,
					<cfqueryparam value="#arguments.apptEndTime#" cfsqltype="cf_sql_timestamp" />,
					<cfqueryparam cfsqltype="cf_sql_date" value="#DateFormat(Now(),'dd-mm-yyyy')#">
				)
		</cfquery>
	
		<cfquery name="local.qryAppointmentResults" datasource="#request.dsn#">
			SELECT      Appointments.Appointment_ID, Customers.First_Name + ' ' + Customers.Last_Name as CustomerName, 
						Customers.Email_Address as CustomerEmail, Customers.Mobile_Phone as CustomerPhone, 
						Locations.Location_Address + ' ' + Locations.Location_Address2 + ' ' + Locations.Location_City + ', ' + Locations.Location_State + ' ' + Locations.Location_Postal as LocationDesc, 
						Locations.Location_Phone, Locations.Location_Name, Professionals.First_Name + ' ' + Professionals.Last_Name AS ProfessionalName, 
						Professionals.Email_Address as ProfessionalEmail
			FROM            Appointments INNER JOIN
									 Customers ON Appointments.Customer_ID = Customers.Customer_ID INNER JOIN
									 Professionals ON Appointments.Professional_ID = Professionals.Professional_ID INNER JOIN
									 Locations ON Professionals.Location_ID = Locations.Location_ID
			WHERE Appointments.Appointment_ID = <cfqueryparam value="#qryResult.generatedkey#" cfsqltype="cf_sql_integer">
		</cfquery>
		
		<cfquery name="local.qGetCompanId" datasource="#request.dsn#" result="getCompanId">
			SELECT Company_ID,Customer_ID
			FROM Customers
			WHERE Customer_ID = <cfqueryparam value="#arguments.CustomerID#" cfsqltype="cf_sql_integer">
		</cfquery>
		
		<cfquery name="local.qGetCompanyDetails" datasource="#request.dsn#" result="getCompanyDetails">
			SELECT Company_Name,Company_Address ,Company_Address2,Company_City,Company_State,Company_Postal,Company_Phone,Company_Email,Company_ID,Web_Address
			FROM Companies
			WHERE Company_ID = <cfqueryparam value="#local.qGetCompanId.Company_ID#" cfsqltype="cf_sql_integer">
		</cfquery>
		
		<cfquery name="local.qGetSocialDetails" datasource="#request.dsn#" result="getSocialDetails">
			SELECT Social_Media_ID,Company_ID ,URL 
			FROM Companies_Social_Media
			WHERE Company_ID = <cfqueryparam value="#local.qGetCompanId.Company_ID#" cfsqltype="cf_sql_integer">
		</cfquery>
			
		<cftry>
				<!---<cfsavecontent variable="mailBody">
	            <cfoutput>
			        <!doctype html>
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
						<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
						<meta name="viewport" content="initial-scale=1.0" />
						<meta name="format-detection" content="telephone=no" />
						<title></title>
						<style type="text/css">
						body {
							width: 100%;
							margin: 0;
							padding: 0;
							-webkit-font-smoothing: antialiased;
						}
						@media only screen and (max-width: 600px) {
							table[class="table-row"] {
								float: none !important;
								width: 98% !important;
								padding-left: 20px !important;
								padding-right: 20px !important;
							}
							table[class="table-row-fixed"] {
								float: none !important;
								width: 98% !important;
							}
							table[class="table-col"], table[class="table-col-border"] {
								float: none !important;
								width: 100% !important;
								padding-left: 0 !important;
								padding-right: 0 !important;
								table-layout: fixed;
							}
							td[class="table-col-td"] {
								width: 100% !important;
							}
							table[class="table-col-border"] + table[class="table-col-border"] {
								padding-top: 12px;
								margin-top: 12px;
								border-top: 1px solid ##E8E8E8;
							}
							table[class="table-col"] + table[class="table-col"] {
								margin-top: 15px;
							}
							td[class="table-row-td"] {
								padding-left: 0 !important;
								padding-right: 0 !important;
							}
							table[class="navbar-row"] , td[class="navbar-row-td"] {
								width: 100% !important;
							}
							img {
								max-width: 100% !important;
								display: inline !important;
							}
							img[class="pull-right"] {
								float: right;
								margin-left: 11px;
								max-width: 125px !important;
								padding-bottom: 0 !important;
							}
							img[class="pull-left"] {
								float: left;
								margin-right: 11px;
								max-width: 125px !important;
								padding-bottom: 0 !important;
							}
							table[class="table-space"], table[class="header-row"] {
								float: none !important;
								width: 98% !important;
							}
							td[class="header-row-td"] {
								width: 100% !important;
							}
						}
						@media only screen and (max-width: 480px) {
							table[class="table-row"] {
								padding-left: 16px !important;
								padding-right: 16px !important;
							}
						}
						@media only screen and (max-width: 320px) {
							table[class="table-row"] {
								padding-left: 12px !important;
								padding-right: 12px !important;
							}
						}
						@media only screen and (max-width: 608px) {
							td[class="table-td-wrap"] {
								width: 100% !important;
							}
						}
						</style>
					</head>
					<body style="font-family: Arial, sans-serif; font-size:13px; color: ##444444; min-height: 200px;" bgcolor="##E4E6E9" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
						<table width="100%" height="100%" bgcolor="##E4E6E9" cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td width="100%" align="center" valign="top" bgcolor="##E4E6E9" style="background-color:##E4E6E9; min-height: 200px;">
								<table style="table-layout: auto; width: 100%; background-color: ##438eb9;" width="100%" bgcolor="##438eb9" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<td width="100%" align="center" style="width: 100%; background-color: ##438eb9;" bgcolor="##438eb9" valign="top">
												<table class="table-row-fixed" height="50" width="600" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
													<tbody>
														<tr>
															<td class="navbar-row-td" valign="middle" height="50" width="600" data-skipstyle="true" align="left">
																<table class="table-row" style="table-layout: auto; padding-right: 16px; padding-left: 16px; width: 600px;" width="600" cellspacing="0" cellpadding="0" border="0">
																	<tbody>
																		<tr style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px;">
																			<td class="table-row-td" style="padding-right: 16px; font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; vertical-align: middle;" valign="middle" align="left">
																				<a href="##" style="color: ##ffffff; text-decoration: none; padding: 10px 0px; font-size: 18px; line-height: 20px; height: auto; background-color: transparent;">
																					Your Appointment with #local.qryAppointmentResults.CustomerName# #DateFormat(ISOToDateTime(arguments.StartDateTime),"long")# #timeformat(ISOToDateTime(arguments.StartDateTime),"short")#
																				</a>
																			</td>

																	
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>


								<table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" align="left">&nbsp;</td>
										</tr>
									</tbody>
								</table>

								<table class="table-row-fixed" width="600" bgcolor="##FFFFFF" style="table-layout: fixed; background-color: ##ffffff;" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<td class="table-row-fixed-td" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; padding-left: 24px; padding-right: 24px;" valign="top" align="left">
												<table class="table-col" align="left" width="285" style="padding-right: 18px; table-layout: fixed;" cellspacing="0" cellpadding="0" border="0">
													<tbody>
														<tr>
															<td class="table-col-td" width="267" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
																<table class="header-row" width="267" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																	<tbody>
																		<tr>
																			<td class="header-row-td" width="267" style="font-size: 28px; margin: 0px; font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: ##478fca; padding-bottom: 10px; padding-top: 15px;" valign="top" align="left">Dear, #local.qryAppointmentResults.CustomerName#,</td>
																		</tr>
																	</tbody>
																</table>
																<p style="margin: 0px; font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px;">
																You are booked appointment with us.  We look forward to serving you soon!
																Click <a href="http://<cfoutput>#local.qGetCompanyDetails.Web_Address#</cfoutput>.salonworks.com/customer_profile.cfm?eflgsignup=1">here</a> to view your appointments, or update your profile.
																</p>											
																<br>
																
															</td>
														</tr>
													</tbody>
												</table>
												<table class="table-col" align="left" width="267" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
													<tbody>
														<tr>
															<td class="table-col-td" width="267" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
																<table class="table-space" height="6" style="height: 6px; font-size: 0px; line-height: 0; width: 267px; background-color: ##ffffff;" width="267" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0">
																	<tbody>
																		<tr>
																			<td class="table-space-td" valign="middle" height="6" style="height: 6px; width: 267px; background-color: ##ffffff;" width="267" bgcolor="##FFFFFF" align="left">&nbsp;</td>
																		</tr>
																	</tbody>
																</table>
																<br>
																<table width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																	<tbody>
																		<tr>
																			<td width="100%" bgcolor="##f5f5f5" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; padding: 19px; border: 1px solid ##e3e3e3; background-color: ##f5f5f5;" valign="top" align="left">
																			
																				<cfif not local.qGetSocialDetails.recordcount >
																					<table class="header-row" width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																						<tbody>
																							<tr>
																								<td class="header-row-td" width="100%" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: ##478fca; margin: 0px; font-size: 18px; padding-bottom: 8px; padding-top: 0px;" valign="top" align="left">Connect With Us</td>
																							</tr>
																						</tbody>
																					</table>
																		
																					<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 100px; background-color: transparent;" width="100" bgcolor="transparent" cellspacing="0" cellpadding="0" border="0">
																						<tbody>
																							<tr>
																								<td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 100px; background-color: transparent;" width="100" bgcolor="transparent" align="left">&nbsp;</td>
																							</tr>
																						</tbody>
																					</table>
																					<div style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; text-align: center;">
																						<a href="##" style="color: ##ffffff; text-decoration: none; margin: 0px; text-align: center; vertical-align: baseline; border: 4px solid ##428bca; padding: 4px 9px; font-size: 14px; line-height: 19px; background-color: ##428bca;"> &nbsp;&nbsp; &nbsp; Facebook &nbsp; &nbsp;&nbsp; </a>
																						<br>
																						<br>
																						<a href="##" style="color: ##ffffff; text-decoration: none; margin: 0px; text-align: center; vertical-align: baseline; border: 4px solid ##6fb3e0; padding: 4px 9px; font-size: 14px; line-height: 19px; background-color: ##6fb3e0;"> &nbsp;&nbsp; &nbsp; &nbsp; Twitter &nbsp; &nbsp; &nbsp; &nbsp; </a>
																						<br>
																						<br>
																						<a href="##" style="color: ##ffffff; text-decoration: none; margin: 0px; text-align: center; vertical-align: baseline; border: 4px solid ##d15b47; padding: 4px 9px; font-size: 14px; line-height: 19px; background-color: ##d15b47;"> &nbsp; &nbsp; &nbsp; Google + &nbsp; &nbsp; &nbsp; </a>
																					</div>
																				</cfif>
																				<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 100px; background-color: transparent;" width="100" bgcolor="transparent" cellspacing="0" cellpadding="0" border="0">
																					<tbody>
																						<tr>
																							<td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 100px; background-color: transparent;" width="100" bgcolor="transparent" align="left">&nbsp;</td>
																						</tr>
																					</tbody>
																				</table>
																				<hr data-skipstyle="true" style="border-width: 0px; height: 1px; background-color: ##e8e8e8;">

																				<table class="header-row" width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																					<tbody>
																						<tr>
																							<td class="header-row-td" width="100%" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: ##478fca; margin: 0px; font-size: 18px; padding-bottom: 8px; padding-top: 10px;" valign="top" align="left">Contact Info</td>
																						</tr>
																					</tbody>
																				</table>
																				<span style="font-family: Arial, sans-serif; line-height: 19px; color: ##31708f; font-size: 13px;">#local.qGetCompanyDetails.Company_Name#</span>
																					<br>
																					<span style="font-family: Arial, sans-serif; line-height: 19px; color: ##31708f; font-size: 13px;">#local.qGetCompanyDetails.Company_Address# #local.qGetCompanyDetails.Company_City# #local.qGetCompanyDetails.Company_State# #local.qGetCompanyDetails.Company_Postal#</span>
																					<a href="https://www.google.com/maps/place/#local.qGetCompanyDetails.Company_Address#,+#local.qGetCompanyDetails.Company_City#,+#local.qGetCompanyDetails.Company_State#,+#local.qGetCompanyDetails.Company_Postal#">Map</a>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>

								<table class="table-space" height="32" style="height: 32px; font-size: 0px; line-height: 0; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="32" style="height: 32px; width: 600px; padding-left: 18px; padding-right: 18px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" align="center">&nbsp;<table bgcolor="##E8E8E8" height="0" width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td bgcolor="##E8E8E8" height="1" width="100%" style="height: 1px; font-size:0;" valign="top" align="left">&nbsp;</td></tr></tbody></table></td></tr></tbody></table>

								<table class="table-row" width="600" bgcolor="##FFFFFF" style="table-layout: fixed; background-color: ##ffffff;" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
												<table class="table-col" align="left" width="528" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
													<tbody>
														<tr>
															<td class="table-col-td" width="528" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
																<div style="font-family: Arial, sans-serif; line-height: 19px; color: ##777777; font-size: 14px; text-align: center;">PLEASE DO NOT REPLY TO THIS EMAIL.  THIS EMAIL ACCOUNT IT NOT MONITORED
																</div>
																<table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 528px; background-color: ##ffffff;" width="528" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 528px; background-color: ##ffffff;" width="528" bgcolor="##FFFFFF" align="left">&nbsp;</td></tr></tbody></table>

															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
								<table class="table-space" height="14" style="height: 14px; font-size: 0px; line-height: 0; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<td class="table-space-td" valign="middle" height="14" style="height: 14px; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" align="left">&nbsp;</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					 </table>
					</body>
					</html>	
		        </cfoutput>
         	</cfsavecontent>
         	<cfset local.mailCustomer = variables.objMailgun.sendMailViaMailgun(mailTo="#local.qryAppointmentResults.CustomerEmail#",mailFrom="salonworks@salonworks.com",mailSubject="Your Appointment with #local.qryAppointmentResults.ProfessionalName# #DateFormat(arguments.StartDateTime,"long")# #timeformat(arguments.StartDateTime,"short")#",mailHtml = "#mailBody#" ) />--->
			<cfmail from="salonworks@salonworks.com" To="#local.qryAppointmentResults.CustomerEmail#" replyto="no-reply@salonworks.com" port="587" server="smtp-relay.sendinblue.com" username="ciredrofdarb@gmail.com" password="2xf5ZLbMdyDr0VSv" usetls="true" Subject="Your Appointment with #local.qryAppointmentResults.ProfessionalName# #DateFormat(arguments.StartDateTime,"long")# #timeformat(arguments.StartDateTime,"short")#" type="html">
				<cfoutput>
					<!doctype html>
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
						<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
						<meta name="viewport" content="initial-scale=1.0" />
						<meta name="format-detection" content="telephone=no" />
						<title></title>
						<style type="text/css">
						body {
							width: 100%;
							margin: 0;
							padding: 0;
							-webkit-font-smoothing: antialiased;
						}
						@media only screen and (max-width: 600px) {
							table[class="table-row"] {
								float: none !important;
								width: 98% !important;
								padding-left: 20px !important;
								padding-right: 20px !important;
							}
							table[class="table-row-fixed"] {
								float: none !important;
								width: 98% !important;
							}
							table[class="table-col"], table[class="table-col-border"] {
								float: none !important;
								width: 100% !important;
								padding-left: 0 !important;
								padding-right: 0 !important;
								table-layout: fixed;
							}
							td[class="table-col-td"] {
								width: 100% !important;
							}
							table[class="table-col-border"] + table[class="table-col-border"] {
								padding-top: 12px;
								margin-top: 12px;
								border-top: 1px solid ##E8E8E8;
							}
							table[class="table-col"] + table[class="table-col"] {
								margin-top: 15px;
							}
							td[class="table-row-td"] {
								padding-left: 0 !important;
								padding-right: 0 !important;
							}
							table[class="navbar-row"] , td[class="navbar-row-td"] {
								width: 100% !important;
							}
							img {
								max-width: 100% !important;
								display: inline !important;
							}
							img[class="pull-right"] {
								float: right;
								margin-left: 11px;
								max-width: 125px !important;
								padding-bottom: 0 !important;
							}
							img[class="pull-left"] {
								float: left;
								margin-right: 11px;
								max-width: 125px !important;
								padding-bottom: 0 !important;
							}
							table[class="table-space"], table[class="header-row"] {
								float: none !important;
								width: 98% !important;
							}
							td[class="header-row-td"] {
								width: 100% !important;
							}
						}
						@media only screen and (max-width: 480px) {
							table[class="table-row"] {
								padding-left: 16px !important;
								padding-right: 16px !important;
							}
						}
						@media only screen and (max-width: 320px) {
							table[class="table-row"] {
								padding-left: 12px !important;
								padding-right: 12px !important;
							}
						}
						@media only screen and (max-width: 608px) {
							td[class="table-td-wrap"] {
								width: 100% !important;
							}
						}
						</style>
					</head>
					<body style="font-family: Arial, sans-serif; font-size:13px; color: ##444444; min-height: 200px;" bgcolor="##E4E6E9" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
						<table width="100%" height="100%" bgcolor="##E4E6E9" cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td width="100%" align="center" valign="top" bgcolor="##E4E6E9" style="background-color:##E4E6E9; min-height: 200px;">
								<table style="table-layout: auto; width: 100%; background-color: ##438eb9;" width="100%" bgcolor="##438eb9" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<td width="100%" align="center" style="width: 100%; background-color: ##438eb9;" bgcolor="##438eb9" valign="top">
												<table class="table-row-fixed" height="50" width="600" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
													<tbody>
														<tr>
															<td class="navbar-row-td" valign="middle" height="50" width="600" data-skipstyle="true" align="left">
																<table class="table-row" style="table-layout: auto; padding-right: 16px; padding-left: 16px; width: 600px;" width="600" cellspacing="0" cellpadding="0" border="0">
																	<tbody>
																		<tr style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px;">
																			<td class="table-row-td" style="padding-right: 16px; font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; vertical-align: middle;" valign="middle" align="left">
																				<a href="##" style="color: ##ffffff; text-decoration: none; padding: 10px 0px; font-size: 18px; line-height: 20px; height: auto; background-color: transparent;">
																					Your Appointment with #local.qryAppointmentResults.CustomerName# #DateFormat(ISOToDateTime(arguments.StartDateTime),"long")# #timeformat(ISOToDateTime(arguments.StartDateTime),"short")#
																				</a>
																			</td>

																	
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>


								<table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" align="left">&nbsp;</td>
										</tr>
									</tbody>
								</table>

								<table class="table-row-fixed" width="600" bgcolor="##FFFFFF" style="table-layout: fixed; background-color: ##ffffff;" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<td class="table-row-fixed-td" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; padding-left: 24px; padding-right: 24px;" valign="top" align="left">
												<table class="table-col" align="left" width="285" style="padding-right: 18px; table-layout: fixed;" cellspacing="0" cellpadding="0" border="0">
													<tbody>
														<tr>
															<td class="table-col-td" width="267" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
																<table class="header-row" width="267" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																	<tbody>
																		<tr>
																			<td class="header-row-td" width="267" style="font-size: 28px; margin: 0px; font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: ##478fca; padding-bottom: 10px; padding-top: 15px;" valign="top" align="left">Dear, #local.qryAppointmentResults.CustomerName#,</td>
																		</tr>
																	</tbody>
																</table>
																<p style="margin: 0px; font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px;">
																You are booked appointment with us.  We look forward to serving you soon!
																Click <a href="http://<cfoutput>#local.qGetCompanyDetails.Web_Address#</cfoutput>.salonworks.com/customer_profile.cfm?eflgsignup=1">here</a> to view your appointments, or update your profile.
																</p>											
																<br>
																
															</td>
														</tr>
													</tbody>
												</table>
												<table class="table-col" align="left" width="267" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
													<tbody>
														<tr>
															<td class="table-col-td" width="267" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
																<table class="table-space" height="6" style="height: 6px; font-size: 0px; line-height: 0; width: 267px; background-color: ##ffffff;" width="267" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0">
																	<tbody>
																		<tr>
																			<td class="table-space-td" valign="middle" height="6" style="height: 6px; width: 267px; background-color: ##ffffff;" width="267" bgcolor="##FFFFFF" align="left">&nbsp;</td>
																		</tr>
																	</tbody>
																</table>
																<br>
																<table width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																	<tbody>
																		<tr>
																			<td width="100%" bgcolor="##f5f5f5" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; padding: 19px; border: 1px solid ##e3e3e3; background-color: ##f5f5f5;" valign="top" align="left">
																			
																				<cfif not local.qGetSocialDetails.recordcount >
																					<table class="header-row" width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																						<tbody>
																							<tr>
																								<td class="header-row-td" width="100%" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: ##478fca; margin: 0px; font-size: 18px; padding-bottom: 8px; padding-top: 0px;" valign="top" align="left">Connect With Us</td>
																							</tr>
																						</tbody>
																					</table>
																		
																					<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 100px; background-color: transparent;" width="100" bgcolor="transparent" cellspacing="0" cellpadding="0" border="0">
																						<tbody>
																							<tr>
																								<td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 100px; background-color: transparent;" width="100" bgcolor="transparent" align="left">&nbsp;</td>
																							</tr>
																						</tbody>
																					</table>
																					<div style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; text-align: center;">
																						<a href="##" style="color: ##ffffff; text-decoration: none; margin: 0px; text-align: center; vertical-align: baseline; border: 4px solid ##428bca; padding: 4px 9px; font-size: 14px; line-height: 19px; background-color: ##428bca;"> &nbsp;&nbsp; &nbsp; Facebook &nbsp; &nbsp;&nbsp; </a>
																						<br>
																						<br>
																						<a href="##" style="color: ##ffffff; text-decoration: none; margin: 0px; text-align: center; vertical-align: baseline; border: 4px solid ##6fb3e0; padding: 4px 9px; font-size: 14px; line-height: 19px; background-color: ##6fb3e0;"> &nbsp;&nbsp; &nbsp; &nbsp; Twitter &nbsp; &nbsp; &nbsp; &nbsp; </a>
																						<br>
																						<br>
																						<a href="##" style="color: ##ffffff; text-decoration: none; margin: 0px; text-align: center; vertical-align: baseline; border: 4px solid ##d15b47; padding: 4px 9px; font-size: 14px; line-height: 19px; background-color: ##d15b47;"> &nbsp; &nbsp; &nbsp; Google + &nbsp; &nbsp; &nbsp; </a>
																					</div>
																				</cfif>
																				<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 100px; background-color: transparent;" width="100" bgcolor="transparent" cellspacing="0" cellpadding="0" border="0">
																					<tbody>
																						<tr>
																							<td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 100px; background-color: transparent;" width="100" bgcolor="transparent" align="left">&nbsp;</td>
																						</tr>
																					</tbody>
																				</table>
																				<hr data-skipstyle="true" style="border-width: 0px; height: 1px; background-color: ##e8e8e8;">

																				<table class="header-row" width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
																					<tbody>
																						<tr>
																							<td class="header-row-td" width="100%" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: ##478fca; margin: 0px; font-size: 18px; padding-bottom: 8px; padding-top: 10px;" valign="top" align="left">Contact Info</td>
																						</tr>
																					</tbody>
																				</table>
																				<span style="font-family: Arial, sans-serif; line-height: 19px; color: ##31708f; font-size: 13px;">#local.qGetCompanyDetails.Company_Name#</span>
																					<br>
																					<span style="font-family: Arial, sans-serif; line-height: 19px; color: ##31708f; font-size: 13px;">#local.qGetCompanyDetails.Company_Address# #local.qGetCompanyDetails.Company_City# #local.qGetCompanyDetails.Company_State# #local.qGetCompanyDetails.Company_Postal#</span>
																					<a href="https://www.google.com/maps/place/#local.qGetCompanyDetails.Company_Address#,+#local.qGetCompanyDetails.Company_City#,+#local.qGetCompanyDetails.Company_State#,+#local.qGetCompanyDetails.Company_Postal#">Map</a>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>

								<table class="table-space" height="32" style="height: 32px; font-size: 0px; line-height: 0; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="32" style="height: 32px; width: 600px; padding-left: 18px; padding-right: 18px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" align="center">&nbsp;<table bgcolor="##E8E8E8" height="0" width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td bgcolor="##E8E8E8" height="1" width="100%" style="height: 1px; font-size:0;" valign="top" align="left">&nbsp;</td></tr></tbody></table></td></tr></tbody></table>

								<table class="table-row" width="600" bgcolor="##FFFFFF" style="table-layout: fixed; background-color: ##ffffff;" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
												<table class="table-col" align="left" width="528" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
													<tbody>
														<tr>
															<td class="table-col-td" width="528" style="font-family: Arial, sans-serif; line-height: 19px; color: ##444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
																<div style="font-family: Arial, sans-serif; line-height: 19px; color: ##777777; font-size: 14px; text-align: center;">PLEASE DO NOT REPLY TO THIS EMAIL.  THIS EMAIL ACCOUNT IT NOT MONITORED
																</div>
																<table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 528px; background-color: ##ffffff;" width="528" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 528px; background-color: ##ffffff;" width="528" bgcolor="##FFFFFF" align="left">&nbsp;</td></tr></tbody></table>

															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
								<table class="table-space" height="14" style="height: 14px; font-size: 0px; line-height: 0; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<td class="table-space-td" valign="middle" height="14" style="height: 14px; width: 600px; background-color: ##ffffff;" width="600" bgcolor="##FFFFFF" align="left">&nbsp;</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					 </table>
					</body>
					</html>	
				</cfoutput>
			</cfmail>
			<cfcatch type="any">
			</cfcatch>
		</cftry>
		<cfset var res = StructNew()>
		<cfset res['title'] = 'Appointment'>
		<cfset res['start'] = arguments.apptStartTime>
		<cfset res['end'] 	= arguments.apptEndTime>
		<cfset res['ID'] 	= qryResult.generatedkey>
		<cfreturn res />
	</cffunction>

	<cffunction name="listAppointments" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = {} >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qGetAppointments" datasource="#request.dsn#" >
					SELECT DISTINCT A.Appointment_ID
				      ,A.Start_Time
				      ,A.End_Time
				      ,A.Service_ID
				      ,A.Customer_ID
				      ,A.Date_Booked
				      ,P.Service_Name
				  	FROM [dbo].[Appointments] AS A
				  	
				  	INNER JOIN Professionals_Services AS PS
				  	on A.Service_ID = PS.Service_ID
				  	inner join Predefined_Services as  P
				  	on PS.Service_ID = P.Service_ID
				  	WHERE A.Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer">
				  	and A.Start_Time >= <cfqueryparam value="#dateformat(now(),'yyyy-mm-dd')#">	
				</cfquery>
				<cfif !structKeyExists(local.returnVal, "appointments")>
					<cfset structInsert(local.returnVal, "appointments", {}) />
				</cfif>	

				<cfif qGetAppointments.recordcount>
					<cfoutput>
						<cfloop query = "qGetAppointments">
							<cfif !structKeyExists(local.returnVal.appointments, dateformat(Start_Time,'yyyy-mm-dd'))>
								<cfset structInsert(local.returnVal.appointments, "#dateformat(Start_Time,'yyyy-mm-dd')#", []) />
							</cfif>

							<cfset arrayAppend(local.returnVal.appointments[dateformat(Start_Time,'yyyy-mm-dd')], { 
								Appointment_ID = Appointment_ID, 
								Start_Time = DateTimeFormat(Start_Time,'hh:nn tt'),
								End_Time = DateTimeFormat(End_Time,'hh:nn tt'),
								Service_ID = Service_ID , 
								Customer_ID = Customer_ID , 
								Date_Booked = Date_Booked ,
								booking_date ="#dateformat(Start_Time,'dd-mm-yyyy')#",
								Service_Name = Service_Name
							})/>
						</cfloop>
					</cfoutput>
				<cfelse>
					<cfset local.returnVal.status = 0 >
				</cfif>

			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction> 

	<cffunction name="checkServicesExist" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qcheckServices" datasource="#request.dsn#" >
					SELECT Service_ID
				  	FROM [dbo].[Professionals_Services] 
				  	WHERE Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer">
				</cfquery>
				
				<cfif qcheckServices.recordcount>
					<cfset local.returnVal.status = 1 >
				<cfelse>
					<cfset local.returnVal.status = 0 >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction> 

	<!---<cffunction name="addCustomer" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "First_Name,Last_Name,Company_ID,professional_id,location_id">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.fields = structNew() >
			<cfset local.fields.Company_ID = local.validateAPICall.data.company_id>
			<cfset local.fields.professional_id =  local.validateAPICall.data.professional_id>
			<cfset local.fields.location_id =  local.validateAPICall.data.location_id>
			<cfset local.fields.First_Name =  local.validateAPICall.data.First_Name>
			<cfset local.fields.Last_Name =  local.validateAPICall.data.Last_Name>
			<cfset local.respondsStatus = argumentsValidation( args = local.fields, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qcheckCustmerEmailExist" datasource="#request.dsn#" >
					SELECT Email_Address
				  	FROM [dbo].[Customers] 
				  	WHERE Email_Address = <cfqueryparam value="#local.validateAPICall.data.Email_Address#" cfsqltype="cf_sql_varchar">
				</cfquery>
				<cfif ! qcheckCustmerEmailExist.recordcount>
					<cfquery datasource="#request.dsn#" result="qryResult" name="qInsertCustomer">
						INSERT INTO
							customers (First_Name,Last_Name,Mobile_Phone,email_address,Password,Company_ID,Preferred_Professional_ID,Preferred_Location_ID)
						VALUES (
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#local.validateAPICall.data.First_Name#">,
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#local.validateAPICall.data.Last_Name#">,
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#local.validateAPICall.data.Mobile_Phone#">,
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#local.validateAPICall.data.email_address#">,
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(local.validateAPICall.data.Password,"SHA")#">,
							<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Company_ID#">,
							<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.professional_id#">,
							<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.location_id#">
							)
					</cfquery>
					<cfset local.returnVal.Customer_ID = qryResult.generatedkey >
					<cfset local.returnVal.status = 1 >
				<cfelse>
					<cfset local.returnVal.errorMsg = "Email address already exists" >
					<cfset local.returnVal.status = 0 >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction> --->
<cffunction name="addCustomer" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "First_Name,Last_Name,Company_ID,professional_id,location_id">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.fields = structNew() >
			<cfset local.fields.Company_ID = local.validateAPICall.data.company_id>
			<cfset local.fields.professional_id =  local.validateAPICall.data.professional_id>
			<cfset local.fields.location_id =  local.validateAPICall.data.location_id>
			<cfset local.fields.First_Name =  local.validateAPICall.data.First_Name>
			<cfset local.fields.Last_Name =  local.validateAPICall.data.Last_Name>
			<cfset local.respondsStatus = argumentsValidation( args = local.fields, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfif structKeyExists(local.validateAPICall, 'email_address') and len(local.validateAPICall.email_address)>
					
					<cfquery name="qcheckCustmerEmailExist" datasource="#request.dsn#" >
						SELECT Email_Address
					  	FROM [dbo].[Customers] 
					  	WHERE Email_Address = <cfqueryparam value="#local.validateAPICall.data.Email_Address#" cfsqltype="cf_sql_varchar">
					</cfquery>
					
					<cfif  qcheckCustmerEmailExist.recordcount>
						<cfset local.returnVal.errorMsg = "Email address already exists" >
						<cfset local.returnVal.status = 0 >
						<cfreturn serializeJSON(local.returnVal)>
					</cfif>
				</cfif>
				<cfquery datasource="#request.dsn#" result="qryResult" name="qInsertCustomer">
					INSERT INTO
						customers (First_Name,Last_Name,Mobile_Phone,email_address,Password,Company_ID,Preferred_Professional_ID,Preferred_Location_ID)
					VALUES (
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#local.validateAPICall.data.First_Name#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#local.validateAPICall.data.Last_Name#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#local.validateAPICall.data.Mobile_Phone#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#local.validateAPICall.data.email_address#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(local.validateAPICall.data.Password,"SHA")#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Company_ID#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.professional_id#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.location_id#">
						)
				</cfquery>
				<cfset local.returnVal.Customer_ID = qryResult.generatedkey >
				<cfset local.returnVal.status = 1 >
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction> 

	<cffunction name="listCustomer" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Company_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				
				<cfquery datasource="#request.dsn#" name="qListCustomers">
					SELECT DISTINCT 
						Customers.Customer_ID, Customers.First_Name, Customers.Last_Name,Customers.Email_Address
		            FROM  
						Customers
					WHERE 1 = 1
					AND Company_ID = <cfqueryparam value="#local.validateAPICall.data.company_id#" cfsqltype="cf_sql_integer">
		            ORDER BY 
						Customers.Last_Name, Customers.First_Name
				</cfquery>
				<cfset local.returnVal.list = [] />
				<cfif qListCustomers.RecordCount >
					<cfloop query= "qListCustomers" >
						<cfif qListCustomers.First_Name neq '' >
							<cfset arrayAppend(local.returnVal.list,{Customer_ID:Customer_ID ,Name:First_Name&' '&Last_Name}) />
						</cfif>
					</cfloop>
				<cfelse>
					<cfset local.returnVal.status = 0/>
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction> 

	<cffunction name="listServices" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Location_ID,Professional_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qListServicesByProfessionals" datasource="#request.dsn#">
					SELECT DISTINCT  Predefined_Services.Service_ID, Predefined_Services.Service_Name, Professionals_Services.Price, Professionals_Services.Service_Time
					FROM   Predefined_Services  
					INNER JOIN
                    	Professionals_Services 
                    ON Predefined_Services.Service_ID = Professionals_Services.Service_ID 
                    INNER JOIN
                    	Professionals 
                    ON Professionals_Services.Professional_ID = Professionals.Professional_ID
					WHERE Professionals.Location_ID = <cfqueryparam value="#local.validateAPICall.data.Location_ID#" cfsqltype="cf_sql_integer" />
					AND Professionals.Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer" /> 

		        </cfquery> 

				<cfset local.returnVal.list = [] />
				<cfif qListServicesByProfessionals.RecordCount >
					<cfloop query= "qListServicesByProfessionals" >
						<cfset arrayAppend(local.returnVal.list,{Service_ID:Service_ID ,Service_Name:Service_Name,Service_Time:Service_Time,Price :Price }) />
					</cfloop>
				<cfelse>
					<cfset local.returnVal.status = 0/>
					<cfset local.returnVal.errorMsg = "No service Exist"/>
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction> 

	<cffunction name="deleteAppointment" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Appointment_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qDelAppointment" datasource="#request.dsn#">
		            DELETE Appointments 
					WHERE Appointment_ID = <cfqueryparam value="#local.validateAPICall.data.Appointment_ID#" cfsqltype="cf_sql_integer" />
		        </cfquery> 
				<cfset local.returnVal.status = 1 >
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<cffunction name="deleteService" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,Service_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery datasource="#request.dsn#">
					DELETE FROM Professionals_Services 
					WHERE Professional_ID= <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer" />
					AND	Service_ID= <cfqueryparam value="#local.validateAPICall.data.Service_ID#" cfsqltype="cf_sql_integer" />
				</cfquery>
				<cfquery datasource="#request.dsn#">
					DELETE FROM Services 
					WHERE Service_ID= <cfqueryparam value="#local.validateAPICall.data.Service_ID#" cfsqltype="cf_sql_integer" />
				</cfquery> 
				<cfset local.returnVal.status = 1 >
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
	
	<cffunction name="addGalleryDetails" access="remote" output="false"returntype="string" returnformat="plain">
 		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList ="company_id,professional_id,gallery_description,image">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.fields = structNew() >
			<cfset local.fields.company_id = local.validateAPICall.data.company_id>
			<cfset local.fields.professional_id =  local.validateAPICall.data.professional_id>
			<cfset local.fields.gallery_description =  local.validateAPICall.data.gallery_description>
			<cfset local.fields.image =  local.validateAPICall.data.image>
			<cfset local.respondsStatus = argumentsValidation( args = local.fields, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfif structKeyExists(local.validateAPICall.data, 'image') and len(local.validateAPICall.data.image) >
					<cfset variables.destination = GetTempDirectory()&createuuid() >
					<cfif NOT DirectoryExists("#variables.destination#")>
						<cfdirectory action = "create" directory = "#variables.destination#" >
					</cfif>
					
					<cfquery name="qInsertProfilePhoto" datasource="#request.dsn#" result="myResult">
						INSERT INTO Gallery (Company_ID,Professional_ID,Description) 
						VALUES (
							<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.company_id#" >,
							<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.professional_id#" >,
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#local.validateAPICall.data.gallery_description#" >
							)
					</cfquery>
					<cfset variables.galleryId = myResult.IDENTITYCOL >
					<cfset variables.gallerryImage = imageReadBase64(#local.validateAPICall.data.image#)>
					<cfset variables.uploadedImageInfo = ImageInfo(#variables.gallerryImage#)>
					<cfset variables.FileExtention = "." & variables.gallerryImage.metadata.Compression.CompressionTypeName />
					<cfset variables.serverFileName = "#variables.galleryId##variables.FileExtention#" >
					<cfset variables.targetPath = expandpath('../../')&"admin/images/company/gallery/#local.validateAPICall.data.company_id#">
				
					<cfif NOT DirectoryExists("#variables.targetPath#")>
						<cfdirectory action = "create" directory = "#variables.targetPath#" >
					</cfif>
					<!--- <cfdump var="#variables.targetPath#/#variables.serverFileName#"><cfabort> --->
					<cfimage
						
						action = "write"
						destination = "#variables.targetPath#/#variables.serverFileName#"
						source = "#variables.gallerryImage#"
						overwrite="yes"
						> 
					
					<cfif variables.uploadedImageInfo.width gt 250 >		
						<cfset ImageScaleToFit(variables.gallerryImage,250,"","highestQuality")>
						<cfset variables.serverFileNameThumb = variables.galleryId&'_tn'&variables.FileExtention >
						<cfset variables.destDir = variables.targetPath & "/" & variables.serverFileNameThumb />
						<cfimage action = "write"  source = "#variables.gallerryImage#" destination="#destDir#" name = "#variables.serverFileNameThumb#"  overwrite="true">
						
					<cfelse>
						<cfset variables.serverFileNameThumb = variables.serverFileName  >
					</cfif>		
					<cfquery name="qInsertProfilePhoto" datasource="#request.dsn#" result="myResult">
						UPDATE Gallery 
						SET Image_Name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#variables.serverFileName#" >,
							Thumb_Name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#variables.serverFileNameThumb#" >
						WHERE Gallery_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#variables.galleryId#" >
					</cfquery> 
					
					
					<!--- <cfimage action="convert" source="#variables.targetPath#\#variables.galleryId##variables.FileExtention#" destination="#variables.targetPath#\#variables.galleryId#.jpg" overwrite="true" />

					<cfimage action="resize" source="#variables.FilePath#\#variables.Professional_ID#.jpg" destination="#variables.FilePath#\#variables.Professional_ID#.jpg" width="300" height="300" overwrite="true" /> --->

					<!--- <cffile action="delete" file="#variables.targetPath#\#variables.serverFileNameThumb#" /> --->
					<cfset local.returnVal.status = 1 >
					<cfset local.returnVal.galleryId = variables.galleryId >

				<cfelse>
					<cfset local.returnVal.status = 0 >
					<cfset local.returnVal.errorMsg = "No Image exists" >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
    </cffunction>

	<cffunction name="deleteGallery" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "gallery_id,company_id">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="getGallery" datasource="#request.dsn#">
					SELECT Image_Name,Thumb_Name
					FROM Gallery 
					WHERE Gallery_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.gallery_id#" >
				</cfquery>

				<cfif getGallery.recordcount >
					<cfoutput>
						<cfset variables.destination = ExpandPath("\")&"admin\images\company\gallery\#local.validateAPICall.data.company_id#\" >
						<cfset variables.deletePathOne = variables.destination&#getGallery.Image_Name# >
						<cfset variables.deletePathTwo = variables.destination&#getGallery.Thumb_Name# >
						<cftry>
							<cffile action = "delete"  file = "#variables.deletePathOne#">			
							<cffile action = "delete" file = "#variables.deletePathTwo#">
							<cfcatch type="any"><cfabort></cfcatch>
						</cftry>
						<!---<cffile action = "delete"  file = "#variables.deletePathOne#">
						<cffile action = "delete" file = "#variables.deletePathTwo#">--->
					</cfoutput>
					<cfquery name="deleteImage" datasource="#request.dsn#">
						DELETE FROM Gallery
						WHERE Gallery_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.gallery_id#" >
					</cfquery>
					<cfset local.returnVal.status = 1 >
				<cfelse>
					<cfset local.returnVal.status = 0 >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<cffunction name="addService" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,Service_ID,Service_Price,Service_Time">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="getServiceDetails" datasource="#request.dsn#" result="serviceDetails">
					SELECT Professional_ID,Service_ID
					FROM Professionals_Services
					WHERE Professional_ID=<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Professional_ID#">
					AND Service_ID=<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Service_ID#">
				</cfquery>

				<cfif serviceDetails.recordcount eq 0>
					<cfquery name="insertService" datasource="#request.dsn#" result="serviceData">
						INSERT INTO Professionals_Services(Service_ID,Professional_ID,Price,Service_Time)
						VALUES (
							<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Service_ID#">,
							<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Professional_ID#">,
							<cfqueryparam cfsqltype="cf_sql_money" value="#local.validateAPICall.data.Service_Price#">,
							<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Service_Time#">
							)
					</cfquery>
				<cfelse>
					<cfquery name="updateService" datasource="#request.dsn#" result="serviceData">
						UPDATE Professionals_Services
							SET 
							Price=<cfqueryparam cfsqltype="cf_sql_money" value="#local.validateAPICall.data.Service_Price#">,
							Service_Time=<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Service_Time#">
						   		
					  	WHERE Professional_ID=<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Professional_ID#">
					  	AND Service_ID=<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Service_ID#">
					 </cfquery>
				</cfif>
				<cfset local.returnVal.status = 1 >
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
    <cffunction name="getProfessionalAvailabilityNextId" access="private" output="false" returntype="numeric">
		<cfset var local = StructNew() />
 		
		<cfquery name="local.qryResults" datasource="#request.dsn#">
        	SELECT ISNULL(Max(Availability_ID), 0) as MaxID
            FROM Availability
		</cfquery>
        
        <cfset local.MaxID = local.qryResults.MaxID + 1 />

        <cfreturn local.MaxID />
    </cffunction>
	<cffunction name="createTimeformatCalender" access="remote" returnformat="plain" returntype="any">
    	<cfargument name="start_Date" type ="string" required="true">
    	<cfargument name="end_Date" type ="string" required="true">
    	<cfargument name="Start_Time" type ="string" required="true">
    	<cfargument name="End_Time" type ="string" required="true">
    	<cfargument name="isAppointment" type ="numeric" required="false">
    	<cfargument name="service_Time" type ="numeric" required="false">
		<cfset start_Date = arguments.start_Date />
		<cfset end_Date =  arguments.end_Date />
		<cfset start_tym =  arguments.Start_Time />
		<cfset end_tym =  arguments.End_Time />
		<cfif findNoCase("AM", start_tym) >
			<cfset start_tym = replace(start_tym,'AM',' ') />
			<cfif findNoCase(":", start_tym) >
				<cfset startAMhour = listFirst(start_tym,':') />
				<cfset startAMmin = listLast(start_tym,':') />
			<cfelse>
				<cfset startAMmin = 0 />
				<cfset startAMhour = start_tym />
			</cfif>
			<cfset dateTimeStart = createDateTime(year(start_Date), month(start_Date), day(start_Date), startAMhour, startAMmin, 0) >
		</cfif>
		<cfif findNoCase("AM", end_tym) >
			<cfset end_tym = replace(end_tym,'AM',' ') />
			<cfif findNoCase(":", end_tym) >
				<cfset endAMhour = listFirst(end_tym,':') />
				<cfset endAMmin = listLast(end_tym,':') />
			<cfelse>
				<cfset endAMmin = 0 />
				<cfset endAMhour = end_tym />
			</cfif>
			<cfset dateTimeEnd = createDateTime(year(end_Date), month(end_Date), day(end_Date), endAMhour, endAMmin, 0) >
			<cfif structKeyExists(arguments, 'isAppointment') >
				<cfset dateTimeEnd = dateAdd('n', service_Time, dateTimeEnd) />
			</cfif>
		</cfif>
		<cfif findNoCase("PM", start_tym) >
			<cfset start_tym = replace(start_tym,'PM',' ') />
			<cfif findNoCase(":", start_tym) >
				<cfset startPMhour = listFirst(start_tym,':') />
				<cfif not findNoCase("12",startPMhour)>
					<cfset startPMhour = startPMhour + 12 >
				</cfif>
					<cfset startPMmin = listLast(start_tym,':') />
				
			<cfelse>
				<cfset startPMmin = 0 >
				<cfset startPMhour = start_tym />
				<cfset startPMhour = startPMhour + 12 >
			</cfif>
			<cfset dateTimeStart = createDateTime(year(start_Date), month(start_Date), day(start_Date), startPMhour, startPMmin, 0) >
			
		</cfif>
		<cfif findNoCase("PM", end_tym) >
			<cfset end_tym = replace(end_tym,'PM',' ') />
			<cfif findNoCase(":", end_tym) >
				<cfset endPMhour = listFirst(end_tym,':') />
				<cfif not findNoCase("12",endPMhour)>
					<cfset endPMhour = endPMhour + 12 >
				</cfif>
				
				<cfset endPMmin = listLast(end_tym,':') />
			<cfelse>
				<cfset endPMhour = end_tym />
				<cfset endPMhour = endPMhour + 12 >
				<cfset endPMmin = 0 />
			</cfif>
			<cfset dateTimeEnd = createDateTime(year(end_Date), month(end_Date), day(end_Date), endPMhour, endPMmin, 0) >
			<cfif structKeyExists(arguments, 'isAppointment') >
				<cfset dateTimeEnd = dateAdd('n', service_Time, dateTimeEnd) />
			</cfif>
		</cfif>

		<cfset var res = StructNew()>
		<cfset res['dateTimeStart'] = dateTimeStart>
		<cfset res['dateTimeEnd'] = dateTimeEnd>
		<cfreturn res />
	</cffunction>
	
	<cffunction name="UpdateProfessionalDetails" access="remote" output="false"returntype="string" returnformat="plain">
 		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList ="Professional_ID,Location_ID,First_Name,Last_Name,Mobile_Phone,Email_Address">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.fields = StructNew() />
			<cfset local.fields.Professional_ID = local.validateAPICall.data.Professional_ID />
			<cfset local.fields.Location_ID = local.validateAPICall.data.Location_ID />
			<cfset local.fields.First_Name = local.validateAPICall.data.First_Name />
			<cfset local.fields.Mobile_Phone = local.validateAPICall.data.Mobile_Phone />
			<cfset local.fields.Email_Address = local.validateAPICall.data.Email_Address />
			<cfset local.fields.Last_Name = local.validateAPICall.data.Last_Name />
			
			<cfset local.respondsStatus = argumentsValidation( args = local.fields, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name = "qCheckProfessionalExist" datasource="#request.dsn#">
					select Professional_ID 
					from Professionals
					where Professional_ID = <cfqueryparam value = "#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer">
				</cfquery>
				<cfif qCheckProfessionalExist.recordcount >
					<cfset local.profileDetails = UpdateProfessional(Professional_ID = local.validateAPICall.data.Professional_ID,Location_ID = local.validateAPICall.data.Location_ID,First_Name = local.validateAPICall.data.First_Name,Last_Name=local.validateAPICall.data.Last_Name,Mobile_Phone=local.validateAPICall.data.Mobile_Phone,Email_Address=local.validateAPICall.data.Email_Address,Bio = local.validateAPICall.data.Bio,Appointment_Increment = local.validateAPICall.data.Appointment_Increment) />
					<cfset variables.Professional_ID = local.validateAPICall.data.Professional_ID />
					<cfif structKeyExists(local.validateAPICall.data, 'image') and len(local.validateAPICall.data.image) >
						<cfset image = imageReadBase64(#local.validateAPICall.data.image#)>
						
						<cfset variables.FilePath = expandPath("/images/staff") />
						<cfset variables.FileExtention = "." & image.metadata.Compression.CompressionTypeName />
						<cfset variables.FileName = '#variables.Professional_ID##variables.FileExtention#' />
						<cfimage
							
							action = "write"
							destination = "#variables.FilePath#\#variables.Professional_ID##variables.FileExtention#"
							source = "#image#"
							overwrite="true"
							> 
						
						<cfimage action="convert" source="#variables.FilePath#\#variables.Professional_ID##variables.FileExtention#" destination="#variables.FilePath#\#variables.Professional_ID#.jpg" overwrite="true" />

						<cfimage action="resize" source="#variables.FilePath#\#variables.Professional_ID#.jpg" destination="#variables.FilePath#\#variables.Professional_ID#.jpg" width="300" height="300" overwrite="true" />
						<cffile action="delete" file="#variables.FilePath#\#variables.FileName#" />
					</cfif>
					<cfset local.returnVal.status = 1 >
				<cfelse>
					<cfset local.returnVal.status = 0 >
					<cfset local.returnVal.errorMsg = "No Profile exists" >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
    </cffunction>
	<cffunction name="UpdateProfessional" access="remote" output="false" returntype="any">
		<!--- Make a cfargument for each column name --->
		<cfargument name="Professional_ID" type="numeric" required="true">
		<cfargument name="Location_ID" type="string" required="false">
		<cfargument name="First_Name" type="string" required="false">
		<cfargument name="Last_Name" type="string" required="false">
		<cfargument name="Mobile_Phone" type="string" required="false">
		<cfargument name="Email_Address" type="string" required="false" default="">
		<cfargument name="Bio" type="string" required="false" default="">
		<cfargument name="Active_Flag" type="string" required="false" default="1" >
		<cfargument name="Appointment_Increment" type="numeric" required="false" default="15">
		<cfargument name="passwordStatus" type="string" required="false">
		<cftry>
			<!--- if at least one argument exists (except the required Professional_ID) --->
			<cfquery name="getPassword" datasource="#request.dsn#" >
				SELECT Password 
				FROM Professionals
				WHERE Professional_ID=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.Professional_ID#">
			</cfquery>
			<cfif (structkeyexists(arguments,'Password') and len(arguments.Password))>
				<cfset pw = arguments.password />
			<cfelseif getPassword.recordcount and len(getPassword.Password)>
				<cfset pw = getPassword.Password >
			</cfif>
			<cfif StructCount(arguments) GT 1>
				<cfset var upd_cols = Duplicate(arguments)>
				<cfif structkeyexists(upd_cols, "Professional_ID")>
					<cfset StructDelete(upd_cols,"Professional_ID")>
				</cfif>
				<cfset var cnt = 0>

				<cfquery name="UpdateProfessional" datasource="#request.dsn#" result="updateresult">
					UPDATE Professionals
					SET 
					<cfif structKeyExists(arguments, "Location_ID")>
			   		 	Location_ID='#arguments.Location_ID#'
					</cfif>
					<cfif structKeyExists(arguments, "First_Name")>
			   			,First_Name='#arguments.First_Name#'
					</cfif>
					<cfif structKeyExists(arguments, "Last_Name")>
			   			,Last_Name='#arguments.Last_Name#'
					</cfif>
					<cfif structKeyExists(arguments, "Mobile_Phone")>
			   			,Mobile_Phone='#arguments.Mobile_Phone#'
					</cfif>
					<cfif structKeyExists(arguments, "Email_Address")>
			   			,Email_Address='#arguments.Email_Address#'
					</cfif>
					<cfif structKeyExists(arguments, "Password") and len(trim(arguments.Password)) and (len(#pw#))>
			   			,Password = <cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(pw,"SHA")#">
					</cfif>
					<cfif structKeyExists(arguments, "Bio")>
			   			,Bio='#arguments.Bio#'
					</cfif>
					<cfif structKeyExists(arguments, "Active_Flag")>
			   			,Active_Flag='#arguments.Active_Flag#'
					</cfif>
					<cfif structKeyExists(arguments, "Appointment_Increment")>
			   			,Appointment_Increment = '#arguments.Appointment_Increment#' 
					</cfif>
					WHERE Professional_ID=#arguments.Professional_ID#
				</cfquery>
			</cfif>
		 	<cfset local.data = {} >
		 	<cfset local.data.value = true >
		 	<cfset local.data.pw = pw >
		<cfcatch>
			<cfreturn cfcatch.message />
		</cfcatch>
		</cftry>
		<cfreturn local.data />
	</cffunction>
	<cffunction name="updateCompanyDetails" access="remote" output="false" returntype="string" returnformat="plain">
 		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Web_Address,Company_Name,Company_Address,Company_City,company_State,Company_Postal,company_id">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.fields = StructNew() />
			<cfset local.fields.Web_Address = local.validateAPICall.data.Web_Address />
			<cfset local.fields.Company_Name = local.validateAPICall.data.Company_Name />
			<cfset local.fields.Company_City = local.validateAPICall.data.Company_City />
			<cfset local.fields.Company_Address = local.validateAPICall.data.Company_Address />
			<cfset local.fields.Company_Postal = local.validateAPICall.data.Company_Postal />
			<cfset local.fields.company_id = local.validateAPICall.data.company_id />
			<cfset local.fields.Company_State = local.validateAPICall.data.Company_State />
			<cfset local.respondsStatus = argumentsValidation( args = local.fields, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name = "qCheckCompanyExist" datasource="#request.dsn#">
					select company_id 
					from Companies
					where company_id = <cfqueryparam value = "#local.validateAPICall.data.company_id#" cfsqltype="cf_sql_integer">
				</cfquery>
				<cfif qCheckCompanyExist.recordcount >
					<cfset local.availabilityDetails = UpdateCompany(Web_Address = local.validateAPICall.data.Web_Address,Company_Name = local.validateAPICall.data.Company_Name,Company_Address = local.validateAPICall.data.Company_Address,Company_Address2=local.validateAPICall.data.Company_Address2,Company_City=local.validateAPICall.data.Company_City,Company_State=local.validateAPICall.data.Company_State,Company_Postal=local.validateAPICall.data.Company_Postal,Company_Phone = local.validateAPICall.data.Company_Phone,Company_Email = local.validateAPICall.data.Company_Email,Company_Fax = local.validateAPICall.data.Company_Fax,company_id = local.validateAPICall.data.company_id,Company_Description = local.validateAPICall.data.Company_Description) />
					<cfset variables.Company_ID = local.validateAPICall.data.company_id />
					<cfif structKeyExists(local.validateAPICall.data, 'image') and len(local.validateAPICall.data.image) >
						<cftry>
							<cfset image = imageReadBase64(#local.validateAPICall.data.image#)>

							<cfset variables.FilePath = expandPath("/images/company/") />
							<cfset variables.FileExtention = "." & image.metadata.Compression.CompressionTypeName />
							<cfset variables.FileName = '#variables.Company_ID##variables.FileExtention#' />
							<cfimage
								
								action = "write"
								destination = "#variables.FilePath#\#variables.Company_ID##variables.FileExtention#"
								source = "#image#"
								overwrite="true"
								> 
							
							<cfimage action="convert" source="#variables.FilePath#\#variables.Company_ID##variables.FileExtention#" destination="#variables.FilePath#\#variables.Company_ID#.jpg" overwrite="true" />

							<cfimage action="resize" source="#variables.FilePath#\#variables.Company_ID#.jpg" destination="#variables.FilePath#\#variables.Company_ID#.jpg" width="300" height="300" overwrite="true" />
							<cffile action="delete" file="#variables.FilePath#\#variables.FileName#" />
						<cfcatch>
							
						</cfcatch>
						</cftry>
					</cfif>
					<cfset local.returnVal.status = 1 >
				<cfelse>
					<cfset local.returnVal.status = 0 >
					<cfset local.returnVal.errorMsg = "No company exists" >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
    </cffunction>
	<cffunction name = "updateCompany" access="remote" output="false" returntype="any">
		<cfargument name="Web_Address" type="string" required="false" default="">
		<cfargument name="Company_Name" type="string" required="false" default="">
		<cfargument name="Company_Address" type="string" required="false" default="">
		<cfargument name="Company_Address2" type="string" required="false" default="">
		<cfargument name="Company_City" type="string" required="false" default="">
		<cfargument name="Company_State" type="string" required="false" default="">
		<cfargument name="Company_Postal" type="string" required="false" default="">
		<cfargument name="Company_Phone" type="string" required="false" default="">
		<cfargument name="Company_Email" type="string" required="false" default="">
		<cfargument name="Company_Fax" type="string" required="false" default="">
		<cfargument name="company_id" type="string" required="true" default="">
		<cfargument name="Company_Description" type="string" required="false" default="">
		<cfquery name="UpdateCompany" datasource="#request.dsn#">
			UPDATE Companies
			SET
			<cfif structKeyExists(arguments, 'Web_Address') >
			   Web_Address='#arguments.Web_Address#'
			</cfif>
			<cfif structKeyExists(arguments, 'Company_Name') >
			   ,Company_Name='#reReplace(arguments.Company_Name,"(^[a-z]|\s+[a-z])","\U\1","ALL")#'
			</cfif>
			<cfif structKeyExists(arguments, 'Company_Address') >
			   ,Company_Address='#reReplace(arguments.Company_Address,"(^[a-z]|\s+[a-z])","\U\1","ALL")#'
			</cfif>
			<cfif structKeyExists(arguments, 'Company_Address2') >
			  ,Company_Address2='#reReplace(arguments.Company_Address2,"(^[a-z]|\s+[a-z])","\U\1","ALL")#'
			</cfif>
			<cfif structKeyExists(arguments, 'Company_City') >
			   ,Company_City='#reReplace(arguments.Company_City,"(^[a-z]|\s+[a-z])","\U\1","ALL")#'
			</cfif>
			<cfif structKeyExists(arguments, 'Company_State') >
			   ,Company_State='#reReplace(arguments.Company_State,"(^[a-z]|\s+[a-z])","\U\1","ALL")#'
			</cfif>
			<cfif structKeyExists(arguments, 'Company_Postal') >
			    ,Company_Postal='#arguments.Company_Postal#'
			</cfif>
			<cfif structKeyExists(arguments, 'Company_Phone') >
			   ,Company_Phone='#arguments.Company_Phone#'
			</cfif>
			<cfif structKeyExists(arguments, 'Company_Email') >
			    ,Company_Email='#arguments.Company_Email#'
			</cfif>
			<cfif structKeyExists(arguments, 'Company_Fax') >
			   ,Company_Fax='#arguments.Company_Fax#'
			</cfif>
			<cfif structKeyExists(arguments, 'Company_Description') >
				,Company_Description='#arguments.Company_Description#'
			</cfif>

		    
			  WHERE Company_ID=#arguments.company_id#
		</cfquery>
		<cfreturn true />
	</cffunction>

	<!--- location --->
	<cffunction name="updateLocationDetails" access="remote" output="false" returntype="string" returnformat="plain">
 		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Contact_Name,Location_ID,Company_ID,Location_Address,Location_City,Location_State,Location_Postal,Location_Phone ">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.fields = StructNew() />
			<cfset local.fields.Location_ID = local.validateAPICall.data.Location_ID />
			<cfset local.fields.Company_ID = local.validateAPICall.data.Company_ID />
			<cfset local.fields.Contact_Name = local.validateAPICall.data.Contact_Name />
			<cfset local.fields.Location_Address = local.validateAPICall.data.Location_Address />
			<cfset local.fields.Location_City = local.validateAPICall.data.Location_City />
			<cfset local.fields.Location_State = local.validateAPICall.data.Location_State />
			<cfset local.fields.Location_Postal = local.validateAPICall.data.Location_Postal />
			<cfset local.fields.Location_Phone = local.validateAPICall.data.Location_Phone />
			<cfset local.respondsStatus = argumentsValidation( args = local.fields, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name = "qCheckLocationExist" datasource="#request.dsn#">
					select Location_ID 
					from Locations
					where Location_ID = <cfqueryparam value = "#local.validateAPICall.data.Location_ID#" cfsqltype="cf_sql_integer">
				</cfquery>
				<cfif qCheckLocationExist.recordcount >
					<cfset local.availabilityDetails = UpdateLocation(Location_ID= local.validateAPICall.data.Location_ID,Company_ID = local.validateAPICall.data.Company_ID,Contact_Name = local.validateAPICall.data.Contact_Name,Contact_Phone=local.validateAPICall.data.Contact_Phone,Location_Name=local.validateAPICall.data.Location_Name,Location_Address=local.validateAPICall.data.Location_Address,Location_Address2=local.validateAPICall.data.Location_Address2,Location_City = local.validateAPICall.data.Location_City,Location_State = local.validateAPICall.data.Location_State,Location_Postal = local.validateAPICall.data.Location_Postal,Location_Phone = local.validateAPICall.data.Location_Phone,Location_Fax = local.validateAPICall.data.Location_Fax,Description=local.validateAPICall.data.Description,Time_Zone_ID=local.validateAPICall.data.Time_Zone_ID,Sunday_Hours=local.validateAPICall.data.Sunday_Hours,Monday_Hours=local.validateAPICall.data.Monday_Hours,Tuesday_Hours=local.validateAPICall.data.Tuesday_Hours,Wednesday_Hours=local.validateAPICall.data.Wednesday_Hours,Thursday_Hours=local.validateAPICall.data.Thursday_Hours,Friday_Hours=local.validateAPICall.data.Friday_Hours,Saturday_Hours=local.validateAPICall.data.Saturday_Hours,Payment_Methods_List=local.validateAPICall.data.Payment_Methods_List,Services_List=local.validateAPICall.data.Services_List,Parking_Fees=local.validateAPICall.data.Parking_Fees,Cancellation_Policy=local.validateAPICall.data.Cancellation_Policy,Languages=local.validateAPICall.data.Languages,Directions=local.validateAPICall.data.Directions) />
					<cfset local.returnVal.status = 1 >
				<cfelse>
					<cfset local.returnVal.status = 0 >
					<cfset local.returnVal.errorMsg = "No Location exists" >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
    </cffunction>
	<cffunction name="UpdateLocation" access="remote" output="false" returntype="any">
		<!--- Make a cfargument for each column name --->
        <cfargument name="Location_ID" type="string" required="false">
		<cfargument name="Company_ID" type="string" required="false">
		<cfargument name="Contact_Name" type="string" required="false" default="">
		<cfargument name="Contact_Phone" type="string" required="false" default="">
        <cfargument name="Location_Name" type="string" required="false" default="">
		<cfargument name="Location_Address" type="string" required="false" default="">
		<cfargument name="Location_Address2" type="string" required="false" default="">
		<cfargument name="Location_City" type="string" required="false" default="">
		<cfargument name="Location_State" type="string" required="false" default="">
		<cfargument name="Location_Postal" type="string" required="false" default="">
		<cfargument name="Location_Phone" type="string" required="false" default="">
		<cfargument name="Location_Fax" type="string" required="false" default="">
		<cfargument name="Description" type="string" required="false" default="">
		<cfargument name="Time_Zone_ID" type="string" required="false" default="">
		<cfargument name="Sunday_Hours" type="string" required="false" default="">
		<cfargument name="Monday_Hours" type="string" required="false" default="">
		<cfargument name="Tuesday_Hours" type="string" required="false" default="">
		<cfargument name="Wednesday_Hours" type="string" required="false" default="">
		<cfargument name="Thursday_Hours" type="string" required="false" default="">
		<cfargument name="Friday_Hours" type="string" required="false" default="">
		<cfargument name="Saturday_Hours" type="string" required="false" default="">
		<cfargument name="Payment_Methods_List" type="string" required="true" default="">
		<cfargument name="Services_List" type="string" required="true" default="">
		<cfargument name="Parking_Fees" type="string" required="false" default="">
		<cfargument name="Cancellation_Policy" type="string" required="false" default="">
		<cfargument name="Languages" type="string" required="false" default="">
		<cfargument name="Directions" type="string" required="false" default="">
		<!--- <cftry> --->
			<cfloop from="1" to="7" index="dayindex">
				<cfif structKeyExists(arguments,"Begin_#dayindex#") AND structKeyExists(arguments,"End_#dayindex#")>
					<cfif arguments['Begin_#dayindex#'] neq 'Closed' AND arguments['End_#dayindex#'] neq 'Closed'>
						<cfset arguments['#DayOfWeekAsString(dayindex)#_Hours'] = "#TimeFormat(arguments['Begin_#dayindex#'],'h:mm tt')# &mdash; #TimeFormat(arguments['End_#dayindex#'],'h:mm tt')#">
					<cfelse>
						<cfset arguments['#DayOfWeekAsString(dayindex)#_Hours'] = "Closed">
					</cfif>
				</cfif>
			</cfloop>

			<cfquery name="UpdateLocation" datasource="#request.dsn#">
				UPDATE Locations
					SET 
				   		Company_ID='#arguments.Company_ID#'
				   		,Contact_Name='#arguments.Contact_Name#'
				   		<cfif len(arguments.Contact_Phone)>
				   		,Contact_Phone='#arguments.Contact_Phone#'
				   		</cfif>
	                    ,Location_Name='#arguments.Location_Name#'
				   		,Location_Address='#reReplace(arguments.Location_Address,"(^[a-z]|\s+[a-z])","\U\1","ALL")#'
				   		,Location_Address2='#reReplace(arguments.Location_Address2,"(^[a-z]|\s+[a-z])","\U\1","ALL")#'
				   		,Location_City='#arguments.Location_City#'
				   		,Location_State='#arguments.Location_State#'
				   		,Location_Postal='#arguments.Location_Postal#'
				   		<cfif len(arguments.Location_Phone)>
				   		,Location_Phone='#arguments.Location_Phone#'
				   		</cfif>
				   		<cfif len(arguments.Location_Fax)>
				   		,Location_Fax='#arguments.Location_Fax#'
				   		</cfif>
				   		,Description='#arguments.Description#'
				   		,Directions='#arguments.Directions#'
				   		,Time_Zone_ID='#arguments.Time_Zone_ID#'
				   		,Sunday_Hours='#arguments.Sunday_Hours#'
				   		,Monday_Hours='#arguments.Monday_Hours#'
				   		,Tuesday_Hours='#arguments.Tuesday_Hours#'
				   		,Wednesday_Hours='#arguments.Wednesday_Hours#'
				   		,Thursday_Hours='#arguments.Thursday_Hours#'
				   		,Friday_Hours='#arguments.Friday_Hours#'
				   		,Saturday_Hours='#arguments.Saturday_Hours#'
				   		,Payment_Methods_List='#arguments.Payment_Methods_List#'
				   		,Services_List='#arguments.Services_List#' 
				   		,Parking_Fees='#arguments.Parking_Fees#'
				   		,Cancellation_Policy='#arguments.Cancellation_Policy#'
				   		,Languages='#arguments.Languages#'
			  	WHERE Location_ID=#arguments.Location_ID#
			</cfquery>
			<cfreturn true />
		<!--- <cfcatch>
			<cfreturn cfcatch.message />
		</cfcatch>
		</cftry> --->
		
	</cffunction>

	<cffunction name="updateProfilePassword" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "password,confirmPassword,Professional_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfif local.validateAPICall.data.password eq local.validateAPICall.data.confirmPassword>
					<cfquery name="UpdateProfessional" datasource="#request.dsn#" result="updateresult">
						UPDATE Professionals
						SET Password=<cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(local.validateAPICall.data.Password, "SHA")#">
						WHERE Professional_ID=<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Professional_ID#">
					</cfquery>
					<cfset local.returnVal.status = 1 >
				<cfelse>
					<cfset local.returnVal.errorMsg = "Password mismatch" />
					<cfset local.returnVal.status = 0 >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<cffunction name="UpdateCustomerDetails" access="remote" output="false"returntype="string" returnformat="plain">
 		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList ="First_Name,Last_Name,customerId">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.fields = structNew() >
			<cfset local.fields.First_Name  = local.validateAPICall.data.First_Name />
			<cfset local.fields.Last_Name  = local.validateAPICall.data.Last_Name />
			<cfset local.fields.customerId  = local.validateAPICall.data.customerId />
			<cfset local.respondsStatus = argumentsValidation( args = local.fields, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name = "qCheckCustomerExist" datasource="#request.dsn#">
					select Customer_ID 
					from customers
					where Customer_ID = <cfqueryparam value = "#local.validateAPICall.data.customerId#" cfsqltype="cf_sql_integer">
				</cfquery>
				<cfif qCheckCustomerExist.recordcount >
					<cfset local.profileDetails = updateCustomer(firstName = local.validateAPICall.data.First_Name,lastName = local.validateAPICall.data.Last_Name,mobileNumber = local.validateAPICall.data.Mobile_Phone,emailId=local.validateAPICall.data.email_address,customerId=local.validateAPICall.data.customerId) />

					<cfset local.returnVal.status = 1 >
				<cfelse>
					<cfset local.returnVal.status = 0 >
					<cfset local.returnVal.errorMsg = "No Customer exists" >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
    </cffunction>
	<cffunction name="updateCustomer" access="remote" output="false" returntype="string" returnformat="plain">
		<cfargument name="firstName" type="string" required="true">
		<cfargument name="lastName" type="string" required="true">
		<cfargument name="mobileNumber" type="string" required="false">
		<cfargument name="emailId" type="string" required="false">
		<cfargument name="customerId" type="numeric" required="false">

		<cfset variables.isEmailExist = false >
		<cfif len(trim(arguments.emailId))>
			<cfquery datasource="#request.dsn#" name="checkEmailExist">
				SELECT
					count(*) as total
				FROM
					customers
				WHERE
					Email_Address = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.emailId#">
				AND
					Customer_ID != <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.customerId#">
			</cfquery>
			<cfif checkEmailExist.total>
				<cfset variables.isEmailExist = true>
			</cfif>
		</cfif>
		<cfif not variables.isEmailExist>	
			<cfquery datasource="#request.dsn#">
				UPDATE
					customers
				SET 
					First_Name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.firstName#">,
					Last_Name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.lastName#">,
					Mobile_Phone = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.mobileNumber#">,
					Email_Address = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.emailId#">
				WHERE
					Customer_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.customerId#">
			</cfquery>
			<cfreturn 1>
		</cfif>
		<cfreturn 0>
	</cffunction>
	<cffunction name="updateCustomerPassword" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "password,confirmPassword,customerId">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfif local.validateAPICall.data.password eq local.validateAPICall.data.confirmPassword>
					<cfquery name="qChangeCustomerPassword" datasource="#request.dsn#">	
						UPDATE 
							customers
						SET 
							password = <cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(local.validateAPICall.data.password, "SHA")#">
						WHERE 
							Customer_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.customerId#">
					</cfquery>
					<cfset local.returnVal.status = 1 >
				<cfelse>
					<cfset local.returnVal.errorMsg = "Password mismatch" />
					<cfset local.returnVal.status = 0 >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<cffunction name="deleteCustomer" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Customer_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery datasource="#request.dsn#" name="qDeleteCustomer">
					DELETE FROM 
						customers 
					WHERE 
						Customer_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Customer_ID#">
				</cfquery>
				<cfset local.returnVal.status = 1 >
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<cffunction name="testpro" access="remote" output="false"returntype="string" returnformat="plain">
		<cfargument name="image" type="any"/>
		<cfargument name="data" type="any"/>
		<cfdump var="#arguments#"><cfabort>
		<cfset local.returnVal = structNew() >
		<cfset local.returnVal.status = 0 >
		<cfif structKeyExists(arguments, 'data') and isJSON(arguments.data)>
			<cfset local.data = deserializeJSON(arguments.data) />
			<cfquery name = "qCheckProfessionalExist" datasource="#request.dsn#">
				select Professional_ID 
				from Professionals
				where Professional_ID = <cfqueryparam value = "#local.data.Professional_ID#" cfsqltype="cf_sql_integer">
			</cfquery>
			<cfif qCheckProfessionalExist.recordcount >
				<cfset local.profileDetails = UpdateProfessional(Professional_ID = local.data.Professional_ID,Location_ID = local.data.Location_ID,First_Name = local.data.First_Name,Last_Name=local.data.Last_Name,Mobile_Phone=local.data.Mobile_Phone,Email_Address=local.data.Email_Address,Bio = local.data.Bio) />
				<cfset variables.Professional_ID = local.data.Professional_ID />
				<cfif structKeyExists(arguments, 'image') and len(arguments.image) >
					<cfset variables.FilePath = expandPath("/images/staff/") />
					<cffile action="upload" filefield="image" destination="#variables.FilePath#" nameConflict="Overwrite" accept="image/jpeg, image/jpg, image/gif, image/png" />
					<cfset variables.FileExtention = "." & cffile.clientFileExt />
					<cfset variables.FileName = cffile.clientFile />
					<cfimage action="convert" source="#variables.FilePath##cffile.clientFile#" destination="#variables.FilePath##variables.Professional_ID#.jpg" overwrite="true" />

					<cfimage action="resize" source="#variables.FilePath##variables.Professional_ID#.jpg" destination="#variables.FilePath##variables.Professional_ID#.jpg" width="300" height="300" overwrite="true" />
					<cffile action="delete" file="#variables.FilePath##variables.FileName#" />
				</cfif>
				<cfset local.returnVal.status = 1 >
			<cfelse>
				<cfset local.returnVal.status = 0 >
				<cfset local.returnVal.errorMsg = "No Profile exists" >
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.returnVal)>
    </cffunction>
    <cffunction name="listGallery" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Company_ID,Professional_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="getGallery" datasource="#request.dsn#">
					SELECT * 
					FROM Gallery
					WHERE 1=1
					<cfif isDefined('local.validateAPICall.data.Professional_ID')>
				  	AND Professional_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Professional_ID#">
					</cfif>
					<cfif isDefined('local.validateAPICall.data.Company_ID')>
				  	AND Company_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Company_ID#">
					</cfif>
					order by Gallery_ID desc
				</cfquery>
				<cfset local.returnVal.list = [] />
				<cfif getGallery.RecordCount >
					<cfloop query= "getGallery" >
						<cfset variables.imagePath = "">
						<cfset variables.imageThumbPath = "">
						<cfif len(getGallery.Image_Name)>
							<cfset variables.imagePath 		= getGallery.Image_Name>
						</cfif>
						<cfif len(getGallery.Thumb_Name)>
							<cfset variables.imageThumbPath = getGallery.Thumb_Name>
						</cfif>
						
						<cfset variables.imgLink = "" >
						<cfif  variables.imageThumbPath neq "" and  variables.imagePath neq "">
							<!--- <cfset variables.imgLink = expandpath("/../admin/images/company/gallery/#local.validateAPICall.data.company_id#/#variables.imageThumbPath#")/> --->
							<cfset img = "www.salonworks.com/admin/images/company/gallery/#local.validateAPICall.data.company_id#/#variables.imageThumbPath#" >
							<cfset arrayAppend(local.returnVal.list,{Gallery_ID:Gallery_ID ,Description:Description,img:img}) />
							<cfset local.returnVal.status = 1/>
						<cfelse>
							<cfset local.returnVal.status = 0/>
						</cfif>
						
					</cfloop>
					<cfset local.returnVal.status = 1/>
				<cfelse>
					<cfset local.returnVal.status = 0/>
				</cfif>
				<cfreturn serializeJSON(local.returnVal)>
			</cfif>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
    <cffunction name="getGallery" access="public" output="false" returntype="query" hint="Returns query of Gallery based on Company_ID">
		<cfargument name="Company_ID" type="numeric" required="false"> 
		<cfargument name="Professional_ID" type="numeric" required="false">
		
		<cfquery name="getGallery" datasource="#request.dsn#">
			SELECT * 
			FROM Gallery
			WHERE 1=1
			<cfif isDefined('arguments.Professional_ID')>
		  	AND Professional_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.Professional_ID#">
			</cfif>
			<cfif isDefined('arguments.Company_ID')>
		  	AND Company_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.Company_ID#">
			</cfif>
			order by Gallery_ID desc
		</cfquery>
		
		<cfreturn getGallery>
	</cffunction>

	<cffunction name="testcomp" access="remote" output="false"returntype="string" returnformat="plain">
		<cfargument name="image" type="any"/>
		<cfargument name="data" type="any"/>

		<cfset local.returnVal = structNew() >
		<cfset local.returnVal.status = 0 >
		<cfif structKeyExists(arguments, 'data') and isJSON(arguments.data)>
			<cfset local.data = deserializeJSON(arguments.data) />
			<cfquery name = "qCheckCompanyExist" datasource="#request.dsn#">
				select company_id 
				from Companies
				where company_id = <cfqueryparam value = "#local.data.company_id#" cfsqltype="cf_sql_integer">
			</cfquery>
			<cfif qCheckCompanyExist.recordcount >
				<cfset local.availabilityDetails = UpdateCompany(Web_Address = local.data.Web_Address,Company_Name = local.data.Company_Name,Company_Address = local.data.Company_Address,Company_Address2=local.data.Company_Address2,Company_City=local.data.Company_City,Company_State=local.data.Company_State,Company_Postal=local.data.Company_Postal,Company_Phone = local.data.Company_Phone,Company_Email = local.data.Company_Email,Company_Fax = local.data.Company_Fax,company_id = local.data.company_id,Company_Description = local.data.Company_Description) />
				<cfset variables.Company_ID = local.data.company_id />
				<cfif structKeyExists(arguments, 'image') and len(arguments.image) >
					<cfset variables.FilePath = expandPath("/images/company/") />
					<cffile action="upload" filefield="image" destination="#variables.FilePath#" nameConflict="Overwrite" accept="image/jpeg, image/jpg, image/gif, image/png" />
					<cfset variables.FileExtention = "." & cffile.clientFileExt />
					<cfset variables.FileName = cffile.clientFile />

					<cfimage action="convert" source="#variables.FilePath##cffile.clientFile#" destination="#variables.FilePath##variables.Company_ID#.jpg" overwrite="true" />

					<cfimage action="resize" source="#variables.FilePath##variables.Company_ID#.jpg" destination="#variables.FilePath##variables.Company_ID#.jpg" width="300" height="300" overwrite="true" />

					<cffile action="delete" file="#variables.FilePath##variables.FileName#" />
				</cfif>
				<cfset local.returnVal.status = 1 >
			<cfelse>
				<cfset local.returnVal.status = 0 >
				<cfset local.returnVal.errorMsg = "No company exists" >
			</cfif>
		</cfif>
		<cfreturn serializeJSON(local.returnVal)>
    </cffunction>
   

    <!--- Availability calendar--->
	<cffunction name="insertAvailabilityTypeNone" access="remote" output="false" returntype="struct">
 		<cfargument name="Professional_ID" type="numeric" required="true" />
        <cfargument name="AvailabilityID" type="any" required="false" />
        <cfargument name="StartDate" type="string" required="true" />
        <cfargument name="EndDate" type="string" required="true" />
        <cfsetting showdebugoutput="false">
		<cfset var local = StructNew() />
		<cfif arguments.AvailabilityID eq ''>
			<cfset arguments.AvailabilityID = getProfessionalAvailabilityNextId()>
		</cfif>
		<cfquery name="local.qryResults" datasource="#request.dsn#" result="qryRes">
			INSERT INTO Availability(
			Professional_ID, Availability_ID,Start_Date,End_Date,Recurrence_Type_ID)
			VALUES (<cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer" />,
					<cfqueryparam value="#arguments.AvailabilityID#" cfsqltype="cf_sql_integer" />,
					<cfqueryparam value="#arguments.StartDate#" cfsqltype="cf_sql_timestamp"/>,
					<cfqueryparam value="#arguments.EndDate#" cfsqltype="cf_sql_timestamp"/>,
					0)
		</cfquery>
		
		<cfset var res = StructNew()>
		<cfset res['title'] = 'Available'>
		<cfset res['start'] = arguments.StartDate>
		<cfset res['end'] 	= arguments.EndDate>
		<cfset res['ID'] 	= arguments.AvailabilityID>
		<cfreturn res />
    </cffunction>
    <cffunction name="addAvailabilityTypeNone" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,start_Date,end_Date,Start_Time,End_Time">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfset start_Date = local.validateAPICall.data.start_Date />
				<cfset end_Date =  local.validateAPICall.data.end_Date />
				<cfset start_tym =  local.validateAPICall.data.Start_Time />
				<cfset end_tym =  local.validateAPICall.data.End_Time />
				<cfset local.data = createTimeformatCalender(start_Date=start_Date,end_Date=end_Date,Start_Time=start_tym,End_Time=end_tym) >
				<cfset dateTimeStart = local.data.dateTimeStart />
				<cfset dateTimeEnd = local.data.dateTimeEnd />
				<cfset local.availabilityDetails = insertAvailabilityTypeNone(Professional_ID = local.validateAPICall.data.Professional_ID,StartDate = dateTimeStart,EndDate = dateTimeEnd) />
				<cfset local.returnVal.data = local.availabilityDetails />
				<cfset local.returnVal.status = 1 >
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
	<cffunction name="editAvailabilityTypeNone" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,Availability_ID,start_Date,end_Date,Start_Time,End_Time">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfset start_Date = local.validateAPICall.data.start_Date />
				<cfset end_Date =  local.validateAPICall.data.end_Date />
				<cfset start_tym =  local.validateAPICall.data.Start_Time />
				<cfset end_tym =  local.validateAPICall.data.End_Time />
				<cfset local.data = createTimeformatCalender(start_Date=start_Date,end_Date=end_Date,Start_Time=start_tym,End_Time=end_tym) >
				<cfset dateTimeStart = local.data.dateTimeStart />
				<cfset dateTimeEnd = local.data.dateTimeEnd />
				<cfset local.availabilityDetails = updateAvailabilityTypeNone(Professional_ID = local.validateAPICall.data.Professional_ID,StartDate = dateTimeStart,EndDate = dateTimeEnd,Availability_ID = local.validateAPICall.data.Availability_ID) />
				<!--- <cfset local.returnVal.data = local.availabilityDetails /> --->
				<cfset local.returnVal.status = 1 >
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
	<cffunction name="updateAvailabilityTypeNone" access="remote" output="false" returnFormat="json" returntype="struct">
    	<cfargument name="Professional_ID" type="numeric" required="true" />
    	<cfargument name="Availability_ID" type="numeric" required="true" />
		<cfargument name="startDate" type="string" required="true" />
		<cfargument name="endDate" type="string" required="true" />
		<cfsetting showdebugoutput="false">
		<!--- <cfdump var="#arguments#">
		<cfdump var="#this.RECURRENCE_TYPE_NONE#"><cfabort> --->
		<cfset var local = {} />
		<cfset local.Response = {
        					Success = true,
        					ErrMsg = ""} /> 
		
		<cftry>
			<cfquery name="local.qryResults" datasource="#request.dsn#" result="updateqryResult">
	            UPDATE 
					Availability
				SET 
					Start_Date = <cfqueryparam value="#arguments.StartDate#" cfsqltype="cf_sql_timestamp" />,
					End_Date =  <cfqueryparam value="#arguments.endDate#" cfsqltype="cf_sql_timestamp"/>				
										
				WHERE Availability_ID = <cfqueryparam value="#arguments.Availability_ID#" cfsqltype="cf_sql_integer" /> AND 
				Professional_ID = <cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer" /> AND 
				Recurrence_Type_ID = <cfqueryparam value="0" cfsqltype="cf_sql_integer">
	        </cfquery> 
	        
	    <cfcatch type="any">
			<cfset local.Response.Success = false />
			<cfset local.Response.ErrMsg = cfcatch.message />
		</cfcatch>
		</cftry>	
        <cfreturn  local.Response />
    </cffunction>

	<!--- dailyDays not completed--->
	<cffunction name="addAvailabilityTypeDailyDays" access="remote" output="false"returntype="string" returnformat="plain">
 		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,start_Date,end_Date,Start_Time,End_Time,RecurCountInterval,EndRecurrenceDate,EndAfterOcurrences,recurrenceRangeType">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>

			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfset start_Date = local.validateAPICall.data.start_Date />
				<cfset end_Date =  local.validateAPICall.data.end_Date />
				<cfset start_tym =  local.validateAPICall.data.Start_Time />
				<cfset end_tym =  local.validateAPICall.data.End_Time />
				<cfset local.data = createTimeformatCalender(start_Date=start_Date,end_Date=end_Date,Start_Time=start_tym,End_Time=end_tym) >
				<cfset dateTimeStart = local.data.dateTimeStart />
				<cfset dateTimeEnd = local.data.dateTimeEnd />
				<cfset local.availabilityDetails = insertAvailabilityTypeDailyDays(Professional_ID = local.validateAPICall.data.Professional_ID,StartDate = dateTimeStart,EndDate = dateTimeEnd,RecurCountInterval=local.validateAPICall.data.RecurCountInterval,EndRecurrenceDate=local.validateAPICall.data.EndRecurrenceDate,EndAfterOcurrences=local.validateAPICall.data.EndAfterOcurrences,recurrenceRangeType=local.validateAPICall.data.recurrenceRangeType) />
				<cfset local.returnVal.data = local.availabilityDetails />
				<cfset local.returnVal.status = 1 >
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
    </cffunction> 
	<cffunction name="insertAvailabilityTypeDailyDays" access="remote" output="false" returntype="struct">
    	<cfargument name="Professional_ID" type="numeric" required="true" />
        <cfargument name="AvailabilityID" type="numeric" required="false" />
        <cfargument name="StartDate" type="any" required="true" />
        <cfargument name="EndDate" type="any" required="true" />
        <cfargument name="RecurCountInterval" type="numeric" required="true" />
        <cfargument name="EndRecurrenceDate" type="any" required="false" />
        <cfargument name="EndAfterOcurrences" type="numeric" required="false" />
        <cfargument name="recurrenceRangeType" type="numeric" required="false" />
      <!---   <cfdump var="#arguments#"> --->
        <cfsetting showdebugoutput="false">
		<cfset var local = StructNew() />
		<cfif arguments.AvailabilityID eq "">
			<cfset arguments.AvailabilityID = getProfessionalAvailabilityNextId()>
		</cfif>
		<cfset timestart = dateTimeFormat(StartDate,'yyyy-mm-dd hh:nn:ss') >
		<cfset timeend = dateTimeFormat(EndDate,'yyyy-mm-dd hh:nn:ss') >
		<cfquery name="local.qryResults" datasource="#request.dsn#">
        	INSERT INTO Availability(
            Professional_ID, Availability_ID,Start_Date,End_Date,Recurrence_Type_ID,
            Recur_Count_Interval, End_Recurrence_Date, End_After_Ocurrences, Recurrence_Range_Type_ID)
            VALUES (<cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer" />,
            		<cfqueryparam value="#arguments.AvailabilityID#" cfsqltype="cf_sql_integer" />,
                    <cfqueryparam value="#timestart#" />,
					<cfqueryparam value="#timeend#" />,
                    1,
                    <cfqueryparam value="#arguments.RecurCountInterval#" cfsqltype="cf_sql_integer" />,
                    <cfif Len(arguments.EndRecurrenceDate)>#arguments.EndRecurrenceDate#<cfelse>NULL</cfif>,
                    <cfif arguments.EndAfterOcurrences GT 0><cfqueryparam value="#arguments.EndAfterOcurrences#" cfsqltype="cf_sql_integer" /><cfelse>NULL</cfif>,
                    <cfqueryparam value="#arguments.recurrenceRangeType#" cfsqltype="cf_sql_integer" />)
		</cfquery>
		
		<cfset var res = StructNew()>
		<cfset res['title'] = 'Available'>
		<cfset res['start'] = arguments.StartDate>
		<cfset res['end'] 	= arguments.EndDate>
		<cfset res['ID'] 	= arguments.AvailabilityID>
		<cfreturn res />
    </cffunction>
    <cffunction name="editAvailabilityTypeDailyDays" access="remote" output="false"returntype="string" returnformat="plain">
 		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,start_Date,end_Date,Start_Time,End_Time,RecurCountInterval,EndRecurrenceDate,EndAfterOcurrences,recurrenceRangeType,Availability_ID,updateType,ExceptionDate">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			
			<cfset start_Date = local.validateAPICall.data.start_Date />
			<cfset end_Date =  local.validateAPICall.data.end_Date />
			<cfset start_tym =  local.validateAPICall.data.Start_Time />
			<cfset end_tym =  local.validateAPICall.data.End_Time />
			<cfset local.data = createTimeformatCalender(start_Date=start_Date,end_Date=end_Date,Start_Time=start_tym,End_Time=end_tym) >
			<cfset dateTimeStart = local.data.dateTimeStart />
			<cfset dateTimeEnd = local.data.dateTimeEnd />
			<cfset local.availabilityDetails = updateAvailabilityTypeDailyDays(Professional_ID = local.validateAPICall.data.Professional_ID,StartDate = dateTimeStart,EndDate = dateTimeEnd,RecurCountInterval=local.validateAPICall.data.RecurCountInterval,EndRecurrenceDate=local.validateAPICall.data.EndRecurrenceDate,EndAfterOcurrences=local.validateAPICall.data.EndAfterOcurrences,recurrenceRangeType=local.validateAPICall.data.recurrenceRangeType,Availability_ID=local.validateAPICall.data.Availability_ID,updateType=local.validateAPICall.data.updateType,ExceptionDate = local.validateAPICall.data.ExceptionDate) />
			<!--- <cfset local.returnVal.data = local.availabilityDetails /> --->
			<cfset local.returnVal.status = 1 >
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
    </cffunction> 
    <cffunction name="updateAvailabilityTypeDailyDays" access="remote" output="false" returnFormat="json" returntype=	"struct">
        <cfargument name="Professional_ID" type="numeric" required="true" />
        <cfargument name="Availability_ID" type="numeric" required="true" />
        <cfargument name="StartDate" type="string" required="true" />
        <cfargument name="EndDate" type="string" required="true" />
        <cfargument name="RecurCountInterval" type="any" required="true" />
        <cfargument name="EndRecurrenceDate" type="string" required="false" />
        <cfargument name="EndAfterOcurrences" type="numeric" required="false" />
        <cfargument name="recurrenceRangeType" type="numeric" required="false" />
        <cfargument name="updateType" type="numeric" required="true" default="1"/>
        <cfargument name="ExceptionDate" type="string" required="false"/>
       
        <cfsetting showdebugoutput="false">
        <cfset var local = {} />
        <cfset local.Response = {
                            Success = true,
                            ErrMsg = ""} /> 
        
        <cftry>
        	<cfif arguments.updateType EQ 1>
        		<cfset local.insertException = updateRecurrenceAvailability(Professional_ID=arguments.Professional_ID,Availability_ID=arguments.Availability_ID,ExceptionDate=arguments.ExceptionDate)>
        		<cfset local.addAvailability = insertAvailabilityTypeDailyDays(Professional_ID = arguments.Professional_ID,StartDate = arguments.StartDate,EndDate = arguments.EndDate,RecurCountInterval=arguments.RecurCountInterval,EndRecurrenceDate=arguments.EndRecurrenceDate,EndAfterOcurrences=arguments.EndAfterOcurrences,recurrenceRangeType=arguments.recurrenceRangeType) />
        		
			<cfelseif arguments.updateType EQ 2 OR arguments.updateType EQ 0>
				<cfquery name="local.qryResults" datasource="#request.dsn#" result="updateqryResult">
	                UPDATE 
	                    Availability
	                SET 
	                    Start_Date = <cfqueryparam value="#arguments.StartDate#" cfsqltype="cf_sql_timestamp" />,
	                    End_Date =  <cfqueryparam value="#arguments.endDate#" cfsqltype="cf_sql_timestamp"/> ,
	                    Recur_Count_Interval = <cfqueryparam value="#arguments.RecurCountInterval#" cfsqltype="cf_sql_integer"/>,
	                    End_Recurrence_Date = <cfif Len(arguments.EndRecurrenceDate)><cfqueryparam value="#arguments.EndRecurrenceDate#" cfsqltype="cf_sql_date"/><cfelse>NULL</cfif>,
	                    End_After_Ocurrences = <cfif Len(arguments.EndAfterOcurrences)><cfqueryparam value="#arguments.EndAfterOcurrences#" cfsqltype="cf_sql_integer"/><cfelse>NULL</cfif>,
	                    Recurrence_Range_Type_ID = <cfif Len(arguments.recurrenceRangeType)><cfqueryparam value="#arguments.recurrenceRangeType#" cfsqltype="cf_sql_integer"/><cfelse>NULL</cfif>
	                WHERE 
	                    Availability_ID = <cfqueryparam value="#arguments.Availability_ID#" cfsqltype="cf_sql_integer" /> AND 
	                    Professional_ID = <cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer" /> 
	            </cfquery> 
        	</cfif>
           
        <cfcatch type="any">
            <cfset local.Response.Success = false />
            <cfset local.Response.ErrMsg = cfcatch.message />
        </cfcatch>
        </cftry>    
        <cfreturn  local.Response />
      </cffunction>
    <cffunction name="insertAvailabilityTypeWeekly" access="remote" output="false" returntype="struct">
    	<cfargument name="Professional_ID" type="numeric" required="true" />
        <cfargument name="Availability_ID" type="numeric" required="false" />
        <cfargument name="StartDate" type="string" required="true" />
        <cfargument name="EndDate" type="string" required="true" />
        <cfargument name="WeekDays" type="string" required="true" />
        <cfargument name="RecurCountInterval" type="numeric" required="true" />
        <cfargument name="EndRecurrenceDate" type="string" required="false" />
        <cfargument name="EndAfterOcurrences" type="numeric" required="false" />
        <cfargument name="recurrenceRangeType" type="numeric" required="false" /> 
        <cfsetting showdebugoutput="false">
		<cfset var local = StructNew() />
		<cfif arguments.Availability_ID eq "">
			<cfset arguments.Availability_ID = getProfessionalAvailabilityNextId()>
		</cfif>
		<cfset timestart = dateTimeFormat(arguments.StartDate,'yyyy-mm-dd hh:nn:ss') >
		<cfset timeend = dateTimeFormat(arguments.EndDate,'yyyy-mm-dd hh:nn:ss') >
		<cfquery name="local.qryResults" datasource="#request.dsn#">
        	INSERT INTO Availability(
            Professional_ID, Availability_ID,Start_Date,End_Date,Recurrence_Type_ID, Week_Days,
            Recur_Count_Interval, End_Recurrence_Date, End_After_Ocurrences, Recurrence_Range_Type_ID)
            VALUES (<cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer" />,
            		<cfqueryparam value="#arguments.Availability_ID#" cfsqltype="cf_sql_integer" />,
                    <cfqueryparam value="#timestart#" />,
					<cfqueryparam value="#timeend#" />,
					3,
                    <cfqueryparam value="#arguments.WeekDays#" cfsqltype="cf_sql_varchar" />,
                    <cfqueryparam value="#arguments.RecurCountInterval#" cfsqltype="cf_sql_integer" />,
                    <cfif Len(arguments.EndRecurrenceDate)>#arguments.EndRecurrenceDate#<cfelse>NULL</cfif>,
                    <cfif arguments.EndAfterOcurrences GT 0><cfqueryparam value="#arguments.EndAfterOcurrences#" cfsqltype="cf_sql_integer" /><cfelse>NULL</cfif>,
                    <cfqueryparam value="#arguments.recurrenceRangeType#" cfsqltype="cf_sql_integer" />)  
		</cfquery>
		
		<cfset var res = StructNew()>
		<cfset res['title'] = 'Available'>
		<cfset res['start'] = arguments.StartDate>
		<cfset res['end'] 	= arguments.EndDate>
		<cfset res['ID'] 	= arguments.Availability_ID>
		<cfreturn res />
    </cffunction> 
    <cffunction name="updateRecurrenceAvailability" output="false" returntype="struct" access="remote">
		<cfargument name="Professional_ID" type="numeric" required="true" />
        <cfargument name="Availability_ID" type="numeric" required="true" />
        <cfargument name="ExceptionDate" type="string" required="true" />
		
		<cfsetting showdebugoutput="false">
		<cfset var local = {} />
		<cfset local.Response = {
        					Success = true,
        					ErrMsg = ""} />
		<cftry>
		<cfquery name="local.qryResults" datasource="#request.dsn#">
			INSERT INTO Availability_Exceptions(Availability_ID, Professional_ID, Exception_Date)
			VALUES (<cfqueryparam value="#arguments.Availability_ID#" cfsqltype="cf_sql_integer" />,
					<cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer" />,
					'#arguments.ExceptionDate#')
		</cfquery>
		<cfcatch type="any">
			<cfset local.Response.Success = false />
			<cfset local.Response.ErrMsg = cfcatch.message />
		</cfcatch>
		</cftry>	
        <cfreturn  local.Response />		
	</cffunction>
    
    <cffunction name="addAvailabilityTypeWeekly" access="remote" output="false"returntype="string" returnformat="plain">
 		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,Availability_ID,start_Date,end_Date,Start_Time,End_Time,RecurCountInterval,EndRecurrenceDate,EndAfterOcurrences,recurrenceRangeType">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>

			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfset start_Date = local.validateAPICall.data.start_Date />
				<cfset end_Date =  local.validateAPICall.data.end_Date />
				<cfset start_tym =  local.validateAPICall.data.Start_Time />
				<cfset end_tym =  local.validateAPICall.data.End_Time />
				<cfset local.data = createTimeformatCalender(start_Date=start_Date,end_Date=end_Date,Start_Time=start_tym,End_Time=end_tym) >
				<cfset dateTimeStart = local.data.dateTimeStart />
				<cfset dateTimeEnd = local.data.dateTimeEnd />
				<cfset local.availabilityDetails = insertAvailabilityTypeWeekly(Professional_ID = local.validateAPICall.data.Professional_ID,StartDate = dateTimeStart,EndDate = dateTimeEnd,RecurCountInterval=local.validateAPICall.data.RecurCountInterval,EndRecurrenceDate=local.validateAPICall.data.EndRecurrenceDate,EndAfterOcurrences=local.validateAPICall.data.EndAfterOcurrences,recurrenceRangeType=local.validateAPICall.data.recurrenceRangeType,WeekDays =local.validateAPICall.data.WeekDays) />
				<cfset local.returnVal.data = local.availabilityDetails />
				<cfset local.returnVal.status = 1 >
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
    </cffunction> 
    <cffunction name="addAvailabilityTypeDailyWeekdays" access="remote" output="false"returntype="string" returnformat="plain">
 		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,start_Date,end_Date,Start_Time,End_Time,RecurCountInterval,EndRecurrenceDate,EndAfterOcurrences,recurrenceRangeType">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>

			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfset start_Date = local.validateAPICall.data.start_Date />
				<cfset end_Date =  local.validateAPICall.data.end_Date />
				<cfset start_tym =  local.validateAPICall.data.Start_Time />
				<cfset end_tym =  local.validateAPICall.data.End_Time />
				<cfset local.data = createTimeformatCalender(start_Date=start_Date,end_Date=end_Date,Start_Time=start_tym,End_Time=end_tym) >
				<cfset dateTimeStart = local.data.dateTimeStart />
				<cfset dateTimeEnd = local.data.dateTimeEnd />
				<cfset local.availabilityDetails = insertAvailabilityTypeDailyWeekdays(Professional_ID = local.validateAPICall.data.Professional_ID,StartDate = dateTimeStart,EndDate = dateTimeEnd,EndRecurrenceDate=local.validateAPICall.data.EndRecurrenceDate,EndAfterOcurrences=local.validateAPICall.data.EndAfterOcurrences,recurrenceRangeType=local.validateAPICall.data.recurrenceRangeType) />
				<cfset local.returnVal.data = local.availabilityDetails />
				<cfset local.returnVal.status = 1 >
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
    </cffunction> 
    <cffunction name="insertAvailabilityTypeDailyWeekdays" access="remote" output="false" returntype="struct">
    	<cfargument name="Professional_ID" type="numeric" required="true" />
        <cfargument name="Availability_ID" type="numeric" required="false" />
        <cfargument name="StartDate" type="string" required="true" />
        <cfargument name="EndDate" type="string" required="true" />
        <cfargument name="EndRecurrenceDate" type="string" required="false" />
        <cfargument name="EndAfterOcurrences" type="numeric" required="false" />
        <cfargument name="recurrenceRangeType" type="numeric" required="false" />
        <cfsetting showdebugoutput="false">     
		<cfset var local = StructNew() />
		<cfif arguments.Availability_ID eq "">
			<cfset arguments.Availability_ID = getProfessionalAvailabilityNextId()>
		</cfif>
		<cfset timestart = dateTimeFormat(arguments.StartDate,'yyyy-mm-dd hh:nn:ss') >
		<cfset timeend = dateTimeFormat(arguments.EndDate,'yyyy-mm-dd hh:nn:ss') >
		<cfset EndRecurrenceDate = dateTimeFormat(arguments.EndRecurrenceDate,'yyyy-mm-dd') >
		<cfquery name="local.qryResults" datasource="#request.dsn#">
        	INSERT INTO Availability(
            Professional_ID, Availability_ID,Start_Date,End_Date,Recurrence_Type_ID,
            End_Recurrence_Date, End_After_Ocurrences, Recurrence_Range_Type_ID)
            VALUES (<cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer" />,
            		<cfqueryparam value="#arguments.Availability_ID#" cfsqltype="cf_sql_integer" />,
                    <cfqueryparam value="#timestart#" />,
					<cfqueryparam value="#timeend#" />,
					2,
                    <cfif Len(arguments.EndRecurrenceDate)>#EndRecurrenceDate#<cfelse>NULL</cfif>,
                    <cfif arguments.EndAfterOcurrences GT 0><cfqueryparam value="#arguments.EndAfterOcurrences#" cfsqltype="cf_sql_integer" /><cfelse>NULL</cfif>,
                    <cfqueryparam value="#arguments.recurrenceRangeType#" cfsqltype="cf_sql_integer" />)                    
		</cfquery>
		
		<cfset var res = StructNew()>
		<cfset res['title'] = 'Available'>
		<cfset res['start'] = arguments.StartDate>
		<cfset res['end'] 	= arguments.EndDate>
		<cfset res['ID'] 	= arguments.Availability_ID>
		<cfreturn res />
    </cffunction> 
    <cffunction name="editAvailabilityTypeDailyWeekDays" access="remote" output="false"returntype="string" returnformat="plain">
 		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,start_Date,end_Date,Start_Time,End_Time,RecurCountInterval,EndRecurrenceDate,EndAfterOcurrences,recurrenceRangeType,Availability_ID,updateType,ExceptionDate">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			
			<cfset start_Date = local.validateAPICall.data.start_Date />
			<cfset end_Date =  local.validateAPICall.data.end_Date />
			<cfset start_tym =  local.validateAPICall.data.Start_Time />
			<cfset end_tym =  local.validateAPICall.data.End_Time />
			<cfset local.data = createTimeformatCalender(start_Date=start_Date,end_Date=end_Date,Start_Time=start_tym,End_Time=end_tym) >
			<cfset dateTimeStart = local.data.dateTimeStart />
			<cfset dateTimeEnd = local.data.dateTimeEnd />
			<cfset local.availabilityDetails = updateAvailabilityTypeDailyWeekdays(Professional_ID = local.validateAPICall.data.Professional_ID,StartDate = dateTimeStart,EndDate = dateTimeEnd,RecurCountInterval=local.validateAPICall.data.RecurCountInterval,EndRecurrenceDate=local.validateAPICall.data.EndRecurrenceDate,EndAfterOcurrences=local.validateAPICall.data.EndAfterOcurrences,recurrenceRangeType=local.validateAPICall.data.recurrenceRangeType,Availability_ID=local.validateAPICall.data.Availability_ID,updateType=local.validateAPICall.data.updateType,ExceptionDate=local.validateAPICall.data.ExceptionDate) />
			<!--- <cfset local.returnVal.data = local.availabilityDetails /> --->
			<cfset local.returnVal.status = 1 >
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
    </cffunction> 
    <cffunction name="updateAvailabilityTypeDailyWeekdays" access="remote" output="false" returnFormat="json" returntype="struct">
        <cfargument name="Professional_ID" type="numeric" required="true" />
        <cfargument name="Availability_ID" type="numeric" required="true" />
        <cfargument name="StartDate" type="string" required="true" />
        <cfargument name="EndDate" type="string" required="true" />
        <cfargument name="EndRecurrenceDate" type="string" required="false" />
        <cfargument name="EndAfterOcurrences" type="numeric" required="false" />
        <cfargument name="recurrenceRangeType" type="numeric" required="false" />
        <cfargument name="updateType" type="numeric" required="true" default="1"/>
        <cfargument name="ExceptionDate" type="string" required="false"/>
        <cfsetting showdebugoutput="false">

        <cfset var local = {} />
        <cfset local.Response = {
                            Success = true,
                            ErrMsg = ""} /> 
       
        <cftry>
        	<cfif arguments.updateType EQ 1>
        		<cfset local.insertException = updateRecurrenceAvailability(Professional_ID=arguments.Professional_ID,Availability_ID=arguments.Availability_ID,ExceptionDate=arguments.ExceptionDate)>
        		
        		<cfset local.addAvailability = insertAvailabilityTypeDailyWeekdays(Professional_ID = arguments.Professional_ID,StartDate = arguments.StartDate,EndDate = arguments.EndDate,EndRecurrenceDate=arguments.EndRecurrenceDate,EndAfterOcurrences=arguments.EndAfterOcurrences,recurrenceRangeType=arguments.recurrenceRangeType) /> />
			<cfelseif arguments.updateType EQ 2 OR arguments.updateType EQ 0>
				<cfquery name="local.qryResults" datasource="#request.dsn#" result="updateqryResult">
	                UPDATE 
	                    Availability
	                SET 
	                    Start_Date = <cfqueryparam value="#arguments.StartDate#" cfsqltype="cf_sql_timestamp" />,
	                    End_Date =  <cfqueryparam value="#arguments.endDate#" cfsqltype="cf_sql_timestamp"/> ,
	                    End_Recurrence_Date = <cfif Len(arguments.EndRecurrenceDate)><cfqueryparam value="#arguments.EndRecurrenceDate#" cfsqltype="cf_sql_date"/><cfelse>NULL</cfif>,
	                    End_After_Ocurrences = <cfif Len(arguments.EndAfterOcurrences)><cfqueryparam value="#arguments.EndAfterOcurrences#" cfsqltype="cf_sql_integer"/><cfelse>NULL</cfif>,
	                    Recurrence_Range_Type_ID = <cfif Len(arguments.recurrenceRangeType)><cfqueryparam value="#arguments.recurrenceRangeType#" cfsqltype="cf_sql_integer"/><cfelse>NULL</cfif>
	                 WHERE 
	                    Availability_ID = <cfqueryparam value="#arguments.Availability_ID#" cfsqltype="cf_sql_integer" /> AND 
	                    Professional_ID = <cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer" /> 
	            </cfquery> 
			</cfif>
           

        <cfcatch type="any">
            <cfset local.Response.Success = false />
            <cfset local.Response.ErrMsg = cfcatch.message />
        </cfcatch>
        </cftry>    
        <cfreturn  local.Response />
    </cffunction>
    <cffunction name="editAvailabilityTypeWeekly" access="remote" output="false"returntype="string" returnformat="plain">
 		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,start_Date,end_Date,Start_Time,End_Time,RecurCountInterval,EndRecurrenceDate,EndAfterOcurrences,recurrenceRangeType,weekDays,Availability_ID,updateType,ExceptionDate">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			
			<cfset start_Date = local.validateAPICall.data.start_Date />
			<cfset end_Date =  local.validateAPICall.data.end_Date />
			<cfset start_tym =  local.validateAPICall.data.Start_Time />
			<cfset end_tym =  local.validateAPICall.data.End_Time />
			<cfset local.data = createTimeformatCalender(start_Date=start_Date,end_Date=end_Date,Start_Time=start_tym,End_Time=end_tym) >
			<cfset dateTimeStart = local.data.dateTimeStart />
			<cfset dateTimeEnd = local.data.dateTimeEnd />
			<cfset local.availabilityDetails = updateAvailabilityTypeWeekly(Professional_ID = local.validateAPICall.data.Professional_ID,StartDate = dateTimeStart,EndDate = dateTimeEnd,RecurCountInterval=local.validateAPICall.data.RecurCountInterval,EndRecurrenceDate=local.validateAPICall.data.EndRecurrenceDate,EndAfterOcurrences=local.validateAPICall.data.EndAfterOcurrences,recurrenceRangeType=local.validateAPICall.data.recurrenceRangeType,Availability_ID=local.validateAPICall.data.Availability_ID,weekDays=local.validateAPICall.data.weekDays,updateType=local.validateAPICall.data.updateType,ExceptionDate =local.validateAPICall.data.ExceptionDate) />
			<cfif local.availabilityDetails.Success eq 'true'>
				<cfset local.returnVal.status = 1 >
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
    </cffunction> 
    <cffunction name="updateAvailabilityTypeWeekly" access="remote" output="false" returnFormat="json" returntype="struct">
        <cfargument name="Professional_ID" type="numeric" required="true" />
        <cfargument name="Availability_ID" type="numeric" required="true" />
        <cfargument name="StartDate" type="string" required="true" />
        <cfargument name="EndDate" type="string" required="true" />
        <cfargument name="weekDays" type="string" required="true" />
        <cfargument name="recurCountInterval" type="numeric" required="true" />
        <cfargument name="EndRecurrenceDate" type="string" required="false" />
        <cfargument name="EndAfterOcurrences" type="numeric" required="false" />
        <cfargument name="recurrenceRangeType" type="numeric" required="false" />
        <cfargument name="updateType" type="numeric" required="true" default="1"/>
        <cfargument name="ExceptionDate" type="string" required="false"/>
        <cfsetting showdebugoutput="false">
        <cfset var local = {} />
        <cfset local.Response = {
                            Success = true,
                            ErrMsg = ""} /> 
       
        <cftry>
        	<cfif arguments.updateType EQ 1>
        		<cfset local.insertException = updateRecurrenceAvailability(Professional_ID=arguments.Professional_ID,Availability_ID=arguments.Availability_ID,ExceptionDate=arguments.ExceptionDate)>
        		<cfset local.addAvailability = insertAvailabilityTypeWeekly(Professional_ID = arguments.Professional_ID,StartDate = arguments.StartDate,EndDate = arguments.EndDate,RecurCountInterval=arguments.RecurCountInterval,EndRecurrenceDate=arguments.EndRecurrenceDate,EndAfterOcurrences=arguments.EndAfterOcurrences,recurrenceRangeType=arguments.recurrenceRangeType,WeekDays =arguments.WeekDays)  /> />
        		
			<cfelseif arguments.updateType EQ 2 OR arguments.updateType EQ 0>
	            <cfquery name="local.qryResults" datasource="#request.dsn#" result="updateqryResult">
	                UPDATE 
	                    Availability
	                SET 
	                    Start_Date = <cfqueryparam value="#arguments.StartDate#" cfsqltype="cf_sql_timestamp" />,
	                    End_Date =  <cfqueryparam value="#arguments.endDate#" cfsqltype="cf_sql_timestamp"/> ,
	                    End_Recurrence_Date = <cfif Len(arguments.EndRecurrenceDate)><cfqueryparam value="#arguments.EndRecurrenceDate#" cfsqltype="cf_sql_date"/><cfelse>NULL</cfif>,
	                    End_After_Ocurrences = <cfif Len(arguments.EndAfterOcurrences)><cfqueryparam value="#arguments.EndAfterOcurrences#" cfsqltype="cf_sql_integer"/><cfelse>NULL</cfif>,
	                    Recurrence_Range_Type_ID = <cfif Len(arguments.recurrenceRangeType)><cfqueryparam value="#arguments.recurrenceRangeType#" cfsqltype="cf_sql_integer"/><cfelse>NULL</cfif>,
	                    Week_Days = <cfqueryparam value="#arguments.weekDays#" cfsqltype="cf_sql_varchar"/>,
	                    Recur_Count_Interval = <cfqueryparam value="#arguments.recurCountInterval#" cfsqltype="cf_sql_integer"/>
	                 WHERE 
	                    Availability_ID = <cfqueryparam value="#arguments.Availability_ID#" cfsqltype="cf_sql_integer" /> AND 
	                    Professional_ID = <cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer" /> 
	            </cfquery> 
	        </cfif>

        <cfcatch type="any">
            <cfset local.Response.Success = false />
            <cfset local.Response.ErrMsg = cfcatch.message />
        </cfcatch>
        </cftry>    
        <cfreturn  local.Response />
    </cffunction>
    <cffunction name="listAvailability" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = {} >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qGetAvailability" datasource="#request.dsn#" >
					SELECT DISTINCT Availability_ID
				      ,Start_Date
				      ,End_Date
				      ,Recurrence_Type_ID
				      ,Recur_Count_Interval
				      ,Recurrence_Range_Type_ID
				      ,End_Recurrence_Date
				      ,Week_Days
				    
				  	FROM [dbo].[Availability] 
				  	WHERE Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer">
				  	AND Availability_ID NOT IN(SELECT Availability_ID FROM Availability_Exceptions)
				</cfquery>
				
				<cfif !structKeyExists(local.returnVal, "availability")>
					<cfset structInsert(local.returnVal, "availability", {}) />
				</cfif>	

				<cfif qGetAvailability.recordcount>
					<cfoutput>
						<cfloop query = "qGetAvailability">
							<cfif !structKeyExists(local.returnVal.availability, dateformat(Start_Date,'yyyy-mm-dd'))>
								<cfset structInsert(local.returnVal.availability, "#dateformat(Start_Date,'yyyy-mm-dd')#", []) />
							</cfif>

							<cfset arrayAppend(local.returnVal.availability[dateformat(Start_Date,'yyyy-mm-dd')], { 
								Availability_ID = Availability_ID, 
								End_time = DateTimeFormat(End_Date,'hh:nn tt'),
								Start_time = DateTimeFormat(Start_Date,'hh:nn tt'),
								date = DateTimeFormat(Start_Date,'yyyy-mm-dd'),
								Recurrence_Type_ID = Recurrence_Type_ID,
								Recur_Count_Interval = Recur_Count_Interval , 
								Recurrence_Range_Type_ID = Recurrence_Range_Type_ID , 
								End_Recurrence_Date = End_Recurrence_Date ,
								Week_Days = Week_Days
							})/>
						</cfloop>
					</cfoutput>
					<cfset local.returnVal.status = 1 >
				<cfelse>
					<cfset local.returnVal.status = 0 >
				</cfif>

			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction> 
	<cffunction name="getAvailabilityById" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Availability_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="getAvailabilityById" datasource="#request.dsn#">
					SELECT
						   [Availability_ID]
					      ,[Professional_ID]
					      ,[Start_Date]
					      ,[End_Date]
					      ,[Recurrence_Type_ID]
					      ,[Recur_Count_Interval]
					      ,[Week_Days]
					      ,[Recurrence_Weekday_Interval_ID]
					      ,[Recurrence_Ordinal_Interval_ID]
					      ,[Yearly_Month]
					      ,[Day_Value]
					      ,[End_Recurrence_Date]
					      ,[End_After_Ocurrences]
					      ,[Recurrence_Range_Type_ID]
					FROM 
						[Availability]
					WHERE
					Availability_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Availability_ID#">
				</cfquery>
				<cfif getAvailabilityById.RecordCount >
					<cfset local.returnVal = {Availability_ID:getAvailabilityById.Availability_ID ,Professional_ID:getAvailabilityById.Professional_ID ,Start_time:dateTimeFormat(getAvailabilityById.Start_Date,'hh:nn tt'),Start_Date:dateTimeFormat(getAvailabilityById.Start_Date,'mm-dd-yyyy'),End_Time:dateTimeFormat(getAvailabilityById.End_Date,'hh:nn tt'),Recurrence_Type_ID:getAvailabilityById.Recurrence_Type_ID,Recur_Count_Interval:getAvailabilityById.Recur_Count_Interval,status:1} >
				<cfelse>
					<cfset local.returnVal.status = 0/>
				</cfif>
				
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
    
	<!---Edit customer by id  --->
    <cffunction name="getCustomerById" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Customer_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="getCustomerById" datasource="#request.dsn#">
					SELECT
						First_Name,Last_Name,Mobile_Phone,Email_Address
					FROM 
						customers
					WHERE
					Customer_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Customer_ID#">
				</cfquery>
				<cfif getCustomerById.RecordCount >
					<cfset local.returnVal = {First_Name:getCustomerById.First_Name ,Last_Name:getCustomerById.Last_Name ,Mobile_Phone:getCustomerById.Mobile_Phone,Email_Address:getCustomerById.Email_Address,status:1} >
				<cfelse>
					<cfset local.returnVal.status = 0/>
				</cfif>
				
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
	<cffunction name="updateAppointment" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Appointment_ID,Professional_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.fields = structNew() >
			<cfset local.fields.Appointment_ID = local.validateAPICall.data.Appointment_ID >
			
			<cfset local.fields.Professional_ID =  local.validateAPICall.data.Professional_ID>
			<cfset local.respondsStatus = argumentsValidation( args = local.fields, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="local.qGetServiceDetails" datasource="#request.dsn#" >
					SELECT Service_Time
					FROM Professionals_Services
					WHERE Service_ID = <cfqueryparam value="#local.validateAPICall.data.Service_ID#" cfsqltype="cf_sql_integer">
					and Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer">
				</cfquery>
				
				<cfset local.error = 0/>
				<cfif not len(local.validateAPICall.data.start_Date) >
					<cfset local.error = 1/>
				<cfelseif not len(local.validateAPICall.data.Start_Time) >
					<cfset local.error = 1/>
				</cfif>
				<cfif local.qGetServiceDetails.recordcount > 
					<cfset start_Date = local.validateAPICall.data.start_Date />
					<cfset end_Date =  local.validateAPICall.data.start_Date />
					<cfset start_tym =  local.validateAPICall.data.Start_Time />
					<cfset end_tym =  local.validateAPICall.data.Start_Time />
					<cfset isAppointment =  1 />
					<cfset service_Time =  local.qGetServiceDetails.Service_Time />

					<cfset local.data = createTimeformatCalender(start_Date=start_Date,end_Date=end_Date,Start_Time=start_tym,End_Time=end_tym,isAppointment = isAppointment,service_Time = service_Time) >
					<cfset dateTimeStart = local.data.dateTimeStart />
					<cfset dateTimeEnd = local.data.dateTimeEnd />
					<cfquery name="local.qryResults" datasource="#request.dsn#" result="updateqryResult">
			            UPDATE 
							Appointments 
						SET 
							 Start_Time = 
							 		
							 			<cfqueryparam value="#dateTimeStart#" cfsqltype="cf_sql_timestamp" />
							 		,
							End_Time = 
									
										<cfqueryparam value="#dateTimeEnd#" cfsqltype="cf_sql_timestamp" />
									,
											
							Service_ID = 
									
										<cfqueryparam value="#local.validateAPICall.data.Service_ID#" cfsqltype="cf_sql_integer" />
									,
											
							Customer_ID = 
									
										<cfqueryparam value="#local.validateAPICall.data.Customer_ID#" cfsqltype="cf_sql_integer" />
												
						WHERE Appointment_ID = <cfqueryparam value="#local.validateAPICall.data.Appointment_ID#" cfsqltype="cf_sql_integer" />
			        </cfquery>
					<cfset local.returnVal.status = 1 >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
	<cffunction name="getAppointmentById" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,AppointmentID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qGetAppointmentsDetails" datasource="#request.dsn#">
		        	SELECT 	A.Appointment_ID, A.Professional_ID, A.Start_Time, A.End_Time,A.Service_ID,A.Customer_ID, A.Date_Booked,C.First_Name,C.Last_Name,PS.Service_Name			
		            FROM  
						Appointments AS A
					LEFT JOIN customers AS C
					ON A.Customer_ID = C.Customer_ID
					/*LEFT JOIN Services AS S 
					ON A.Service_ID = S.Service_ID*/
					LEFT JOIN Professionals_Services AS P 
					ON A.Service_ID = P.Service_ID
					LEFT JOIN Predefined_Services AS PS
					ON P.Service_ID = PS.Service_ID
		            WHERE 	
						A.Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer" /> 
					AND
		            	Appointment_ID = <cfqueryparam value="#local.validateAPICall.data.AppointmentID#" cfsqltype="cf_sql_integer" />
				</cfquery>
				<cfif qGetAppointmentsDetails.RecordCount >
					<cfset local.returnVal = {
							Start_Time:dateTimeformat(qGetAppointmentsDetails.Start_Time,'hh:nn tt' )
							,End_Time:dateTimeformat(qGetAppointmentsDetails.End_Time,'hh:nn tt')
							,Start_Date:dateTimeformat(qGetAppointmentsDetails.End_Time,'mm-dd-yyyy')
							,Customer_ID:qGetAppointmentsDetails.Customer_ID
							,Professional_ID:qGetAppointmentsDetails.Professional_ID,
							Appointment_ID:qGetAppointmentsDetails.Appointment_ID
							,Service_ID:qGetAppointmentsDetails.Service_ID
							,Service_Name :qGetAppointmentsDetails.Service_Name
							,Customer_Name : qGetAppointmentsDetails.First_Name &''&qGetAppointmentsDetails.Last_Name
							,status:1} >
				<cfelse>
					<cfset local.returnVal.status = 0/>
				</cfif>
				
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
	<cffunction name="getServiceById" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Company_ID,Professional_ID,Service_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qGetServiceDetails" datasource="#request.dsn#">
		        	select Predefined_Services.Service_ID,Predefined_Services.Service_Type_ID,Predefined_Services.Service_Name,Cast(CONVERT(DECIMAL(10,2),Professionals_Services.Price) as nvarchar) as price ,Professionals_Services.Service_Time 
					from Professionals_Services 
					left join Predefined_Services on Professionals_Services.Service_ID = Predefined_Services.Service_ID 
					where Professional_ID='#local.validateAPICall.data.professional_id#'
				</cfquery>
				<cfif qGetServiceDetails.RecordCount >
					<cfset local.returnVal = {Service_Time:qGetServiceDetails.Service_Time ,Price:qGetServiceDetails.Price ,Service_ID:qGetServiceDetails.Service_ID,status:1} >
				<cfelse>
					<cfset local.returnVal.status = 0/>
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
	<cffunction name="updateService" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Service_ID,Professional_ID,Company_ID,Service_Name,Service_Time,Service_Description,Price">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery datasource="#request.dsn#" name="UpdateServiceQuery" result="UpdateServiceQueryresult">
					UPDATE Services
						SET 
					   		Service_Name='#local.validateAPICall.data.Service_Name#' 
					   		,Service_Description='#local.validateAPICall.data.Service_Description#' 
					   		<cfif NOT structKeyExists(local.validateAPICall.data, "Professional_ID")>
					   		,Price=#local.validateAPICall.data.Price#
					   		,Service_Time='#local.validateAPICall.data.Service_Time#'
					   		</cfif>
							,Company_ID=#local.validateAPICall.data.Company_ID#
				  	WHERE Service_ID=#local.validateAPICall.data.Service_ID#
				 </cfquery>
				<cfif structKeyExists(local.validateAPICall.data, "Professional_ID")>
				 	<cfset this.AssignService( argumentCollection = local.validateAPICall.data )>
				</cfif>
				<cfset local.returnVal.status = 1 />
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<cffunction name="AssignService" access="remote" output="false">
		<cfargument name="Professional_ID" type="numeric" required="true">
		<cfargument name="Service_ID" type="numeric" required="true">
		<cfargument name="service_time" type="numeric" required="true">
		<cfargument name="price" type="numeric" required="true">
		
		<cfquery name="checkIfServiceIsAssigned" datasource="#request.dsn#">
			SELECT * FROM Professionals_Services 
			WHERE Professional_ID=#arguments.Professional_ID#
			AND	Service_ID=#arguments.Service_ID#
		</cfquery>
		
		<cfif not checkIfServiceIsAssigned.recordcount>
			<cfquery datasource="#request.dsn#">
				INSERT INTO Professionals_Services
					(Professional_ID, Service_ID, service_time, price)
				VALUES
					(#arguments.Professional_ID#, #arguments.Service_ID#, #arguments.service_time#, #arguments.price#)
			</cfquery>
		<cfelse>

			<cfquery datasource="#request.dsn#">
				UPDATE 	Professionals_Services
				SET	
						service_time = #arguments.service_time#,
						price = #arguments.price#
				WHERE 	
						Professional_ID = #arguments.Professional_ID#
				AND 	Service_ID = #arguments.Service_ID#
			</cfquery>
		</cfif>
		<cfreturn 1 >
	</cffunction>

	<cffunction name="getServiceType" access="remote" returnformat="plain" returntype="String">

		<cfset local.returnVal = structNew() >
		<cfset local.returnVal.status = 0 >
		
		<cfquery name="getProfessionService" datasource="#request.dsn#">
		   SELECT pf.Profession_ID,pf.Profession_Name from Professions pf
		   INNER JOIN Predefined_Service_Types pst on pf.Profession_ID = pst.Profession_ID 
		   GROUP BY pf.Profession_ID,pf.Profession_Name 
		   -- SELECT Profession_ID,Profession_Name FROM Professions 
		</cfquery>
		<cfif getProfessionService.RecordCount >
			<cfset local.returnVal.list = [] />
			<cfloop query="getProfessionService" >
				<cfset arrayAppend(local.returnVal.list,{Profession_Name:getProfessionService.Profession_Name ,Profession_ID:getProfessionService.Profession_ID}) />
			</cfloop>
			<cfset local.returnVal.status = 1/>
		<cfelse>
			<cfset local.returnVal.status = 0/>
		</cfif>
		<cfreturn serializeJSON(local.returnVal)>
		</cffunction>

		<cffunction name="getServicesSub" access="remote" returnformat="plain" returntype="String">
	    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
			<cfset local.argsList = "Profession_ID">
			<cfif local.validateAPICall.status >
				<cfset local.returnVal = structNew() >
				<cfset local.returnVal.status = 0 >
				<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
				<cfset local.returnVal = local.respondsStatus >
				<cfif local.respondsStatus.status >
					<cfquery name="getServiceTypes" datasource="#request.dsn#">
						SELECT pst.Service_Type_ID,pst.Service_Type_Name from Predefined_Service_Types pst
						INNER JOIN Predefined_Services ps on pst.Service_Type_ID = ps.Service_Type_ID 
						WHERE Profession_ID= #local.validateAPICall.data.Profession_ID#
						GROUP BY pst.Service_Type_ID,pst.Service_Type_Name 
					</cfquery>

					<cfif getServiceTypes.RecordCount >
						<cfset local.returnVal.list = [] />
						<cfloop query="getServiceTypes" >
							<cfset arrayAppend(local.returnVal.list,{Service_Type_ID:getServiceTypes.Service_Type_ID ,Service_Type_Name:getServiceTypes.Service_Type_Name}) />
							<cfset local.returnVal.status = 1 />
						</cfloop>
					<cfelse>
						<cfset local.returnVal.status = 0/>
					</cfif>
					
				</cfif>
				<cfreturn serializeJSON(local.returnVal)>
			</cfif>
			<cfreturn serializeJSON(local.validateAPICall)>
		</cffunction>
		<!--- For getting the sublist of services first --->
		<cffunction name="getServicesSubDefault" access="remote" returnformat="plain" returntype="String">
	    	
				<cfset local.returnVal = structNew() >
				<cfset local.returnVal.status = 0 >
				
					<cfquery name="getServiceTypes" datasource="#request.dsn#">
						SELECT pst.Service_Type_ID,pst.Service_Type_Name from Predefined_Service_Types pst
						INNER JOIN Predefined_Services ps on pst.Service_Type_ID = ps.Service_Type_ID 
						WHERE Profession_ID= <cfqueryparam value="5" cfsqltype="cf_sql_integer" />
						GROUP BY pst.Service_Type_ID,pst.Service_Type_Name 
					</cfquery>

					<cfif getServiceTypes.RecordCount >
						<cfset local.returnVal.list = [] />
						<cfloop query="getServiceTypes" >
							<cfset arrayAppend(local.returnVal.list,{Service_Type_ID:getServiceTypes.Service_Type_ID ,Service_Type_Name:getServiceTypes.Service_Type_Name}) />
							<cfset local.returnVal.status = 1 />
						</cfloop>
					<cfelse>
						<cfset local.returnVal.status = 0/>
					</cfif>
				<cfreturn serializeJSON(local.returnVal)>
		</cffunction>
		<cffunction name="getServicesSubList" access="remote" returnformat="plain" returntype="String">
	    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
			<cfset local.argsList = "Professional_ID,Service_Type_ID">
			<cfif local.validateAPICall.status >
				<cfset local.returnVal = structNew() >
				<cfset local.returnVal.status = 0 >
				<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
				<cfset local.returnVal = local.respondsStatus >
				<cfif local.respondsStatus.status >
					<cfquery name="getServicesSublist" datasource="#request.dsn#">
						SELECT distinct *
						FROM 	Predefined_Services s
						LEFT JOIN Professionals_Services ps on s.Service_ID = ps.Service_ID and ps.Professional_ID = #local.validateAPICall.data.Professional_ID#
						WHERE 	s.Service_Type_ID = <cfqueryparam value="#local.validateAPICall.data.Service_Type_ID#" cfsqltype="cf_sql_integer" /> 
					</cfquery>
					
					<cfif getServicesSublist.RecordCount >
						<cfset local.returnVal.list = [] />
						<cfset local.notaddedlist = [] />
						<cfloop query="getServicesSublist" >
							<cfquery name="getServiceItem" datasource="#request.dsn#">
								SELECT Service_ID
								FROM 	Professionals_Services
								where Service_ID = #getServicesSublist.Service_ID#
								AND Professional_ID = #local.validateAPICall.data.Professional_ID#
							</cfquery>
							<cfif getServiceItem.recordcount>
								<cfset arrayAppend(local.returnVal.list,{Service_Name:getServicesSublist.Service_Name ,Price:getServicesSublist.Price,Service_ID:Service_ID,Service_Time:Service_Time,alreadyAdded = 1,Service_Type_ID:local.validateAPICall.data.Service_Type_ID}) />
							<cfelse>
								<cfset arrayAppend(local.notaddedlist,{Service_Name:getServicesSublist.Service_Name ,Price:getServicesSublist.Price,Service_ID:Service_ID,Service_Time:Service_Time,alreadyAdded = 0,Service_Type_ID:local.validateAPICall.data.Service_Type_ID}) />
							</cfif>
							
							<cfset local.returnVal.status = 1 />
						</cfloop>
						<cfset local.returnVal.list.addAll( local.notaddedlist )>
					<cfelse>
						<cfset local.returnVal.status = 0/>
					</cfif>
					
				</cfif>
				<cfreturn serializeJSON(local.returnVal)>
			</cfif>
			<cfreturn serializeJSON(local.validateAPICall)>
		</cffunction>
	<cffunction name="deleteAvailability" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,Availability_ID,DeleteType,ExceptionDate">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			
			<cfset local.data = removeAvailability(Professional_ID = local.validateAPICall.data.Professional_ID,AvailabilityID = local.validateAPICall.data.Availability_ID,DeleteType = local.validateAPICall.data.DeleteType,ExceptionDate = local.validateAPICall.data.ExceptionDate) />
			
			<cfif local.data.Success eq true >
				<cfset local.returnVal.status = 1 >
			</cfif>
		
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
	<cffunction name="removeAvailability" access="remote" output="false" returnFormat="json" returntype="struct">
    	<cfargument name="Professional_ID" type="numeric" required="true" />
        <cfargument name="AvailabilityID" type="numeric" required="true" />
        <cfargument name="DeleteType" type="numeric" required="true" />
        <cfargument name="ExceptionDate" type="string" required="false" />
		<cfsetting showdebugoutput="false">
		<cfset var local = {} />
		<cfset local.Response = {
        					Success = true,
        					ErrMsg = ""} /> 
        <cftry>					
		<!--- Delete is from a series type, but only want to remove a single occurance, hence insert into exceptions (to series) --->
        <cfif arguments.DeleteType EQ 1>
            <cfquery name="local.qryResults" datasource="#request.dsn#">
                INSERT INTO Availability_Exceptions(Availability_ID, Professional_ID, Exception_Date)
                VALUES (<cfqueryparam value="#arguments.AvailabilityID#" cfsqltype="cf_sql_integer" />,
                		<cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer" />,'#arguments.ExceptionDate#')
            </cfquery>        
        
        <cfelseif arguments.DeleteType EQ 2 OR arguments.DeleteType EQ 0>
            
            <cfquery name="local.qryResults" datasource="#request.dsn#">
                DELETE FROM Availability_Exceptions
                WHERE 	Professional_ID = <cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer" /> AND
                        Availability_ID = <cfqueryparam value="#arguments.AvailabilityID#" cfsqltype="cf_sql_integer" />
            </cfquery>            
            
            <cfquery name="local.qryResults" datasource="#request.dsn#">
                DELETE FROM Availability
                WHERE 	Professional_ID = <cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer" /> AND
                        Availability_ID = <cfqueryparam value="#arguments.AvailabilityID#" cfsqltype="cf_sql_integer" />
            </cfquery>
            <cfset local.Response.Success = true />
			<cfset local.Response.ErrMsg = "" />
        </cfif>
       <cfcatch type="any">
			<cfset local.Response.Success = false />
			<cfset local.Response.ErrMsg = cfcatch.message />
		</cfcatch>
		</cftry>	
        <cfreturn  local.Response />
        
    </cffunction>

    <cffunction name="weekBasedView" access="remote" output="false" returnFormat="plain" returntype="string">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,next,prev">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.returnVal.currentWeekStart = ""/>
			<cfset local.returnVal.currentWeekEnd = ""/>
			<!--- <cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status > --->
			<cfset  variables.currentWeekStart = #DateFormat(DateAdd("d", "-#DayOfWeek(Now()) - 1#", Now()), "yyyy-mm-dd")# />
			<!--- <cfdump var="#DateFormat(DateAdd("d", "-#DayOfWeek(Now()) - 1#", Now()), "yyyy-mm-dd")#"> --->
			<cfset  variables.currentWeekEnd = "#DateFormat(dateAdd('d', 6, variables.currentWeekStart), "yyyy-mm-dd")#" />
			<cfif structKeyExists(local.validateAPICall.data, 'next') and local.validateAPICall.data.next eq 'next' >
				<cfset variables.currentWeekStart = local.validateAPICall.data.currentWeekStart />
				<cfset variables.currentWeekEnd = local.validateAPICall.data.currentWeekEnd />
				
				<cfset variables.currentWeekStart = DateFormat(DateAdd('ww',1,variables.currentWeekStart),"yyyy-mm-dd") />
				<cfset variables.currentWeekEnd = DateFormat(DateAdd('d',6,variables.currentWeekStart),"yyyy-mm-dd") />
			<cfelseif structKeyExists(local.validateAPICall.data, 'prev') and local.validateAPICall.data.prev eq 'prev' >
				<cfset variables.currentWeekStart = local.validateAPICall.data.currentWeekStart />
				<cfset variables.currentWeekEnd = local.validateAPICall.data.currentWeekEnd />
				<cfset variables.currentWeekStart = DateFormat(DateAdd('ww',-1,variables.currentWeekStart),"yyyy-mm-dd") />
				<cfset variables.currentWeekEnd = DateFormat(DateAdd('d',6,variables.currentWeekStart),"yyyy-mm-dd") />
			</cfif>
			<cfset local.returnVal.currentWeekStart = "#variables.currentWeekStart#"/>
			<cfset local.returnVal.currentWeekEnd = "#variables.currentWeekEnd#"/>
			<cfquery name="qGetAvailability" datasource="#request.dsn#" >
				SELECT DISTINCT Availability_ID
			      ,Start_Date
			      ,End_Date
			      ,Recurrence_Type_ID
			      ,Recur_Count_Interval
			      ,Recurrence_Range_Type_ID
			      ,End_Recurrence_Date
			      ,Week_Days
			    
			  	FROM [dbo].[Availability] 
			  	WHERE Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer">
			  	AND Availability_ID NOT IN(SELECT Availability_ID FROM Availability_Exceptions)
			  	AND Start_Date BETWEEN '#variables.currentWeekStart#' and '#variables.currentWeekEnd#'
			</cfquery>

			<cfif !structKeyExists(local.returnVal, "availability")>
				<cfset structInsert(local.returnVal, "availability", []) />
			</cfif>	

			<cfif qGetAvailability.recordcount>
				<cfoutput>
					
					<cfloop query = "qGetAvailability">
						<!--- <cfif !structKeyExists(local.returnVal.availability, dateformat(Start_Date,'yyyy-mm-dd'))>
							<cfset structInsert(local.returnVal.availability, "#dateformat(Start_Date,'yyyy-mm-dd')#", []) />
						</cfif> --->

						<cfset arrayAppend(local.returnVal.availability, { 
							Availability_ID = Availability_ID, 
							Month = dateformat(Start_Date,'mmm'),
							Day = dateformat(Start_Date,'dd'),
							Date = Day = dateformat(Start_Date,'dd-mm-yyyy'),
							End_time = DateTimeFormat(End_Date,'hh:nn tt'),
							Start_time = DateTimeFormat(Start_Date,'hh:nn tt'),
							Recurrence_Type_ID = Recurrence_Type_ID,
							Recur_Count_Interval = Recur_Count_Interval , 
							Recurrence_Range_Type_ID = Recurrence_Range_Type_ID , 
							End_Recurrence_Date = End_Recurrence_Date ,
							Week_Days = Week_Days
							

						})/>
					</cfloop>
				</cfoutput>
				<cfset local.returnVal.status = 1 >
			<cfelse>
				<cfset local.returnVal.status = 0 >
			</cfif>
				<!--- <cfdump var="#qGetAvailability#"><cfabort> --->
			<cfreturn serializeJSON(local.returnVal)>
			
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
    </cffunction>
    <cffunction name="searchCustomer" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Customer_Name,Professional_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				
				<cfquery datasource="#request.dsn#" name="qListCustomers">
					SELECT DISTINCT 
						Customers.First_Name, Customers.Last_Name,Customers.Customer_ID
		            FROM  
						Customers
					WHERE 1 = 1
					AND First_Name like ('%#local.validateAPICall.data.Customer_Name#%')
					AND Preferred_Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer">
		            ORDER BY 
						Customers.Last_Name, Customers.First_Name
				</cfquery>
				<cfset local.returnVal.list = [] />
				<cfif qListCustomers.RecordCount >
					<cfloop query= "qListCustomers" >
						<cfif qListCustomers.First_Name neq '' >
							<cfset arrayAppend(local.returnVal.list,{Customer_ID:Customer_ID ,Name:First_Name&' '&Last_Name}) />
						</cfif>
					</cfloop>
					<cfset local.returnVal.status = 1/>
				<cfelse>
					<cfset local.returnVal.status = 0/>
					<cfset local.returnVal.errorMsg = "No matching customer name"/>
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction> 

	<cffunction
	    name="ISOToDateTime"
	    access="public"
	    returntype="string"
	    output="false"
	    hint="Converts an ISO 8601 date/time stamp with optional dashes to a ColdFusion date/time stamp.">

	    <!--- Define arguments. --->
	    <cfargument
	        name="Date"
	        type="string"
	        required="true"
	        hint="ISO 8601 date/time stamp."
	        />

	    <!---
	        When returning the converted date/time stamp,
	        allow for optional dashes.
	    --->
	    <cfreturn ARGUMENTS.Date.ReplaceFirst(
	        "^.*?(\d{4})-?(\d{2})-?(\d{2})T([\d:]+).*$",
	        "$1-$2-$3 $4"
	        ) />
	</cffunction>


	<!---  function for get payment options	 --->
	<cffunction  name="getCheckoutOptions" access="remote" returnformat="plain" returntype="String">
		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "professional_id,company_id">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew()>
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfset local.bankAccountList = variables.paymentObj.getBankAcc( professional_id = local.validateAPICall.data.professional_id, Company_ID = local.validateAPICall.data.company_id ) >
				<cfset local.getAllOptions = variables.paymentObj.getCheckoutMethods()>
				<cfset local.returnVal.status = 1 >
				<cfset local.returnVal.options = arrayNew(1) >
				<cfset local.loopIndex = 1>
				<cfloop query="local.getAllOptions">
					<cfset local.optionsList = structNew()>
					<cfif trim(local.getAllOptions.type) eq "Credit Card" and local.bankAccountList.recordcount>
						<cfset local.optionsList.id = local.getAllOptions.id>
						<cfset local.optionsList.type = local.getAllOptions.type>
					<cfelseif trim(local.getAllOptions.type) neq "Credit Card">
						<cfset local.optionsList.id = local.getAllOptions.id>
						<cfset local.optionsList.type = local.getAllOptions.type>
					</cfif>
					<cfif not StructIsEmpty(local.optionsList) >
						<cfset local.returnVal.options[local.loopIndex] = local.optionsList >
						<cfset local.loopIndex += 1>
					</cfif>
				</cfloop>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<!--- Function to get the service prices --->
	 <cffunction name="getProServicePrice" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Appointment_ID,Professional_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qGetId" datasource="#request.dsn#">
					SELECT 	Customer_ID,Service_ID
					FROM 	Appointments
					WHERE 	Appointment_ID = <cfqueryparam value="#local.validateAPICall.data.Appointment_ID#" cfsqltype="cf_sql_integer">
				</cfquery>
				<cfset local.serviceId = qGetId.Service_ID />
				<cfquery name="qGetPrice" datasource="#request.dsn#">
					SELECT 	Price
					FROM 	Professionals_Services
					WHERE 	Service_ID = <cfqueryparam value="#local.serviceId#" cfsqltype="cf_sql_integer">
					AND 	Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer">
				</cfquery>
				<cfset local.servicePrice = qGetPrice.Price />
				<cfif len(local.servicePrice) >
					<cfset local.returnVal.servicePrice = local.servicePrice />
					<cfset local.returnVal.status = 1 >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal) />
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

<!--- Checkout payments --->
	<cffunction  name="checkOutNonCreditCard" access="remote" returnformat="plain" returntype="String">
		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "amount,paymentMethod,currency,appointmentId,Professional_ID,company_id">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew()>
			<cfset local.returnVal.status = 0 > 
			<cfset local.fields = structNew() />
			<cfset local.fields.amount = local.validateAPICall.data.amount >
			<cfset local.fields.paymentMethod = local.validateAPICall.data.paymentMethod >
			<cfset local.fields.currency = local.validateAPICall.data.currency >
			<cfset local.fields.appointmentId = local.validateAPICall.data.appointmentId >
			<cfset local.fields.Professional_ID = local.validateAPICall.data.Professional_ID >
			<cfset local.fields.company_id = local.validateAPICall.data.company_id >
			<cfset local.checkGiftCardVal = local.validateAPICall.data.checkGiftCardVal > 

			<cfset local.respondsStatus = argumentsValidation( args = local.fields, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >

			<cfif local.respondsStatus.status >
				<cfif  local.checkGiftCardVal eq ""  and (local.validateAPICall.data.paymentMethod eq 'Gift Card')>
					<cfset local.returnVal.status = 0 >
					<cfset local.returnVal.errorMsg = "Please enter a valid gift card number" >
					<cfreturn serializeJSON(local.returnVal)>
				<cfelseif  local.checkGiftCardVal eq ""  and (local.validateAPICall.data.paymentMethod eq 'Check') >
					<cfset local.returnVal.status = 0 > 
					<cfset local.returnVal.errorMsg = "Please enter a valid check number" >
					<cfreturn serializeJSON(local.returnVal)>
				<cfelseif local.checkGiftCardVal eq ""  and (local.validateAPICall.data.paymentMethod eq 'Cash')>
					<cfset local.checkGiftCardVal = 0 />
				</cfif>
				<cfset local.response = deserializeJSON(variables.paymentObj.checkoutPayment(amount = local.validateAPICall.data.amount ,paymentMethod = local.validateAPICall.data.paymentMethod ,checkGiftCardVal =  local.checkGiftCardVal,currency = local.validateAPICall.data.currency,appointmentId = local.validateAPICall.data.appointmentId,Professional_ID = local.validateAPICall.data.Professional_ID,company_id = local.validateAPICall.data.company_id)) />
				<cfset local.returnVal.response = local.response/>
				<cfif local.response.status eq "succeeded">
					<cfset local.returnVal.status = 1/>
				<cfelse>
					<cfset local.returnVal.status = 0/>
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<!--- function for get company details and personal details for add bank --->
	<cffunction name="getDetailsForAddBank" access="remote" returnformat="plain" returntype="String">
		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "professional_id,company_id">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew()>
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfset local.professionalData =  variables.paymentObj.getProfessional( Professional_ID = local.validateAPICall.data.professional_id, Company_ID = local.validateAPICall.data.company_id )>
				<cfset local.profileDataForm = structNew()>
				<cfset local.profileDataForm.first_name = local.professionalData.First_Name>
				<cfset local.profileDataForm.last_name = local.professionalData.Last_Name>
				<cfset local.profileDataForm.phone = local.professionalData.Mobile_Phone>
				<cfset local.profileDataForm.address = local.professionalData.Home_Address>
				<cfset local.profileDataForm.address1 = local.professionalData.Home_Address2>
				<cfset local.profileDataForm.city = local.professionalData.Home_City>
				<cfset local.profileDataForm.state = local.professionalData.Home_State>
				<cfset local.profileDataForm.postal = local.professionalData.Home_Postal>
				<cfset local.profileDataForm.email_address = local.professionalData.email_address>
				<cfset local.returnVal.profileDataForm = local.profileDataForm >
				<cfset local.companyData =  variables.paymentObj.getCompany( Company_ID = local.validateAPICall.data.company_id )>
				<cfset local.companyDataForm = structNew()>
				<cfset local.companyDataForm.company_name =local.companyData.company_name>
				<cfset local.companyDataForm.company_address = local.companyData.Company_Address>
				<cfset local.companyDataForm.company_address1 = local.companyData.Company_Address2>
				<cfset local.companyDataForm.company_city = local.companyData.company_city>
				<cfset local.companyDataForm.company_postal_code = local.companyData.company_postal>
				<cfset local.companyDataForm.company_state = local.companyData.company_state>
				<cfset local.companyDataForm.company_web_address = local.companyData.web_address>
				<cfset local.companyDataForm.company_phone = local.companyData.company_phone>
				<cfset local.companyDataForm.company_email = local.companyData.company_email>
				<cfset local.returnVal.companyDataForm = local.companyDataForm >
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<cffunction name="createBankAccount" access="remote" returnformat="plain" returntype="String">
		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfif local.validateAPICall.status >
			<cfset local.argsList = "first_name,last_name,phone,address,city,state,postal,company_name,company_address,company_city,company_postal_code,company_state,company_web_address,dob,ssn,account_number,routing_number,tax_id_registrar,account_holder_name,professional_id,company_id">
			<cfset local.returnVal = structNew()>
			<cfset local.returnVal.status = 0 >
			<cfset local.argumentsdata = listToArray(local.argsList)>
			<cfloop array="#local.argumentsdata#" index="key">
				<cfif structKeyExists(local.validateAPICall.data, "#key#") and not len(trim(evaluate("local.validateAPICall.data.#key#"))) >
					<cfset local.returnVal.errorMsg = "Please enter a valid #key#">
					<cfreturn serializeJSON(local.returnVal)>
				<cfelseif  structKeyExists(local.validateAPICall.data, "phoneNumber")  and len(trim(evaluate("local.validateAPICall.data.phoneNumber"))) LE 12>
					<cfset local.returnVal.errorMsg = "Please enter a valid Phone number">
					<cfreturn serializeJSON(local.returnVal)>
				</cfif>
			</cfloop>
			<cfset local.professionalObj = createObject("component",".admin.professionals") />
			<cfset local.accountStatus = variables.paymentObj.checkStripeAccount( professional_id = local.validateAPICall.data.professional_id, Company_ID = local.validateAPICall.data.company_id )>
			<cfset local.account.tos_shown_and_accepted = true>
			<cfset local.account.legal_entity.maiden_name = "" >
			<cfset local.bankDetails.account_holder_type = "company">
			<cfset local.bankDetails.default_for_currency = false >

			<cfset local.profileDataForm.first_name = local.validateAPICall.data.first_name>
			<cfset local.profileDataForm.last_name = local.validateAPICall.data.last_name>
			<cfset local.profileDataForm.phone = local.validateAPICall.data.phone>
			<cfset local.profileDataForm.address = local.validateAPICall.data.address>
			<cfset local.profileDataForm.address1 = local.validateAPICall.data.address1>
			<cfset local.profileDataForm.city = local.validateAPICall.data.city>
			<cfset local.profileDataForm.state = local.validateAPICall.data.state>
			<cfset local.profileDataForm.postal = local.validateAPICall.data.postal>
			<cfset local.profileDataForm.professional_id = local.validateAPICall.data.professional_id>

			<cfset local.companyDataForm.company_name =local.validateAPICall.data.company_name>
			<cfset local.companyDataForm.company_address = local.validateAPICall.data.company_address>
			<cfset local.companyDataForm.company_address1 = local.validateAPICall.data.company_address1>
			<cfset local.companyDataForm.company_city = local.validateAPICall.data.company_city>
			<cfset local.companyDataForm.company_postal_code = local.validateAPICall.data.company_postal_code>
			<cfset local.companyDataForm.company_state = local.validateAPICall.data.company_state>
			<cfset local.companyDataForm.company_web_address = local.validateAPICall.data.company_web_address>
			<cfset local.companyDataForm.company_id = local.validateAPICall.data.company_id>
			<cfset local.account.legal_entity.dob = local.validateAPICall.data.dob >
			<cfset local.account.legal_entity.ssn = local.validateAPICall.data.ssn >
			<cfset local.account.legal_entity.last_name = local.validateAPICall.data.last_name >
			<cfset local.account.legal_entity.first_name = local.validateAPICall.data.first_name >
			<cfset local.bankDetails.account_number = local.validateAPICall.data.account_number>
			<cfset local.bankDetails.routing_number = local.validateAPICall.data.routing_number>
			<cfset local.account.tax_id_registrar = local.validateAPICall.data.tax_id_registrar>
			<cfset local.bankDetails.account_holder_name = local.validateAPICall.data.account_holder_name>
			<cfset local.account.legal_entity.employerNumber = local.validateAPICall.data.employerNumber >

			<cfset local.professionalDetails = local.professionalObj.getProfessional( Professional_ID = local.validateAPICall.data.professional_id, Company_ID = local.validateAPICall.data.company_id )> 
			<cfset local.account.legal_entity.email = local.professionalDetails.Email_Address >
			<cfset local.account.legal_entity.phone = local.professionalDetails.Mobile_Phone >
			<cfset local.account.professional_id = local.validateAPICall.data.professional_id >
			<cfset local.account.company_id = local.validateAPICall.data.company_id >
			<cfset local.account.companyDataForm = local.companyDataForm >
			<cfset local.account.profileDataForm = local.profileDataForm >

			<cfset local.bankDetails.country = "US">
			<cfset local.bankDetails.professional_id = local.validateAPICall.data.professional_id >
			<cfset local.bankDetails.company_id = local.validateAPICall.data.company_id >
			<cfset local.bankDetails.currency = "usd">

			<cfset local.accountCreateStatus = {}>
			<cfif not local.accountStatus.recordcount >
				<cfset local.accountCreateStatus = variables.paymentObj.createAccount( account = local.account ) >
				<cfif structKeyExists(local.accountCreateStatus, "error")>
					<cfset local.returnVal.errorMsg = local.accountCreateStatus.error.message >
					<cfreturn serializeJSON(local.returnVal)>
				</cfif>
			</cfif>
			<cfif not structKeyExists(local.accountCreateStatus, "error")>
				<cfset local.bankAccountStatus = variables.paymentObj.createBank( bankDetails = local.bankDetails ) >
				<cfif structKeyExists(local.bankAccountStatus, "error")>
					<cfset local.returnVal.errorMsg = local.bankAccountStatus.error.message >
					<cfreturn serializeJSON(local.returnVal)>
				<cfelse>
					<cfset local.returnVal.status = 1 >
					<cfset local.returnVal.message = "Bank added successfully!.." >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<cffunction name="makePayment" access="remote" returnformat="plain" returntype="String">
		<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "professional_id,company_id,appointmentId,amount,card_number,card_name,exp_month,exp_year,cvv">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew()>
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfset local.returnVal.responds = deserializeJSON(variables.paymentObj.makePayment( amount = local.validateAPICall.data.amount, nameOnCard = local.validateAPICall.data.card_name, cardNo = local.validateAPICall.data.card_number, cvv =  local.validateAPICall.data.cvv, expYear = local.validateAPICall.data.exp_year, expMonth = local.validateAPICall.data.exp_month, currency  = "usd", appointmentId = local.validateAPICall.data.appointmentId, Professional_ID = local.validateAPICall.data.professional_id, company_id = local.validateAPICall.data.company_id ))>
				<cfif not structKeyExists(local.returnVal.responds, "error")>
					<cfset local.returnVal.status = 1>
				<cfelse>
					<cfset local.returnVal.status = 0 >
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<!--- Function to add blog --->
	<cffunction name="addBlog" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,Company_ID,Title,Description">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery datasource="#request.dsn#" name="qAddBolg" result="result">
					INSERT INTO
						Blog_Post (Company_ID,Professional_ID,Title,Description,Created_Date,Created_By)			
					VALUES (
						<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Company_ID#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Professional_ID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#local.validateAPICall.data.Title#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#local.validateAPICall.data.Description#">,
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Now()#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Professional_ID#">
						)
				</cfquery>
				<cfset local.returnVal.blogId = result.generatedKey />
				<cfset local.returnVal.status = 1 />
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
	<!--- Function to get blog details to edit --->
	<cffunction name="getBlogDetailsById" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,blog_id">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qGetBlogPostEdit" datasource="#request.dsn#">
					SELECT title,description
					FROM Blog_Post
					WHERE 1 = 1
					AND 
					blog_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.blog_id#">
					AND Professional_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.Professional_ID#">
				</cfquery>
				<cfif qGetBlogPostEdit.recordcount >
					<cfset local.returnVal = {title : qGetBlogPostEdit.title , description:qGetBlogPostEdit.description ,blog_id : local.validateAPICall.data.blog_id} />
					<cfset local.returnVal.status = 1 />
				<cfelse>
					<cfset local.returnVal.errorMsg = "No blog exists" />
					<cfset local.returnVal.status = 0 />
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<cffunction name="listBlog" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,Company_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qListBlogPost" datasource="#request.dsn#">
					SELECT * 
					FROM Blog_Post
					where Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#">
					and Company_ID = <cfqueryparam value="#local.validateAPICall.data.Company_ID#">
					order by blog_id desc
				</cfquery> 
				<cfset local.returnVal.list = [] />
				<cfif qListBlogPost.RecordCount >
					<cfloop query= "qListBlogPost" >
						<cfset arrayAppend(local.returnVal.list,{blog_id:blog_id ,title:title,description:description}) />
					</cfloop>
					<cfset local.returnVal.status = 1 />
				<cfelse>
					<cfset local.returnVal.status = 0/>
					<cfset local.returnVal.errorMsg ="No blogs Exist"/>
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<!--- Delete blog post --->
	<cffunction name="deleteBlog" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "blog_id">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery datasource="#request.dsn#" name="qDeleteBlog">
					DELETE FROM 
						Blog_Post 
					WHERE 
						Blog_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.blog_id#">
				</cfquery>
				
				<cfset local.returnVal.status = 1 />
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<!--- Update blog post --->
	<cffunction name="updateBlog" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "blog_id,Title,Description">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery datasource="#request.dsn#" name="qUpdateBolg">
					UPDATE
						Blog_Post
					SET 
						Title = <cfqueryparam cfsqltype="cf_sql_varchar" value="#local.validateAPICall.data.Title#">,
						Description = <cfqueryparam cfsqltype="cf_sql_varchar" value="#local.validateAPICall.data.Description#">
					WHERE
						blog_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#local.validateAPICall.data.blog_id#">
				</cfquery>
				<cfset local.returnVal.status = 1 />
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<!--- Email customer - send email to all --->
	<cffunction name="sendEmailToAll" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,emailSubject,emailContent">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="getAllcustomerEmail" datasource="#request.dsn#">
					select *
					from Customers
					where Preferred_Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_numeric" > 
				</cfquery>
				
				<cfif getAllcustomerEmail.recordcount >
					<cfloop query="getAllcustomerEmail">
					<!---<cfsavecontent variable="mailBody">
						<cfoutput>
							Dear #getAllcustomerEmail.FIRST_NAME# #getAllcustomerEmail.LAST_NAME#,
								<br>
								<cfoutput>#local.validateAPICall.data.emailContent#</cfoutput>
								<br>
								Regards,<br>
								SalonWorks Customer Support<br>
								salonworks@salonworks.com 
						</cfoutput>
					</cfsavecontent>
					<cfset local.mailCustomer = variables.objMailgun.sendMailViaMailgun(mailTo="#getAllcustomerEmail.Email_Address#",mailFrom="salonworks@salonworks.com",mailSubject="#local.validateAPICall.data.emailSubject#",mailHtml = "#mailBody#" ) />--->
						<cfoutput>
						<cfif len(getAllcustomerEmail.Email_Address) and isValid("email",getAllcustomerEmail.Email_Address) >
							<cfmail 
								from="salonworks@salonworks.com"
								subject="#local.validateAPICall.data.emailSubject#"
								type="HTML"
								to="#getAllcustomerEmail.Email_Address#"
								server="smtp-relay.sendinblue.com" port="587" username="ciredrofdarb@gmail.com" password="2xf5ZLbMdyDr0VSv" usetls="true" >
								Dear #getAllcustomerEmail.FIRST_NAME# #getAllcustomerEmail.LAST_NAME#,
								<br>
								<cfoutput>#local.validateAPICall.data.emailContent#</cfoutput>
								<br>
								Regards,<br>
								SalonWorks Customer Support<br>
								salonworks@salonworks.com
							</cfmail>
						</cfif>
						</cfoutput>
					</cfloop>
					<cfset local.returnVal.status = 1 />
				<cfelse>
					<cfset local.returnVal.errorMsg = "No customers exist" />
					<cfset local.returnVal.status = 0 />
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<!--- Email customer - end mail to selected --->
	<cffunction name="sendEmailToSelectedCustomers" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Professional_ID,emailSubject,emailContent">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.fields = structNew() />
			<cfset local.fields.Professional_ID = local.validateAPICall.data.Professional_ID/>
			<cfset local.fields.emailSubject =  local.validateAPICall.data.emailSubject/>
			<cfset local.fields.emailContent =  local.validateAPICall.data.emailContent/>

			<cfset local.respondsStatus = argumentsValidation( args = local.fields , argsList = local.argsList )>
			<cfif !ListLen(local.validateAPICall.data.Services) >
				<cfset local.returnVal.status = 0 >
				<cfset local.returnVal.errorMsg = "Services is empty." >
				<cfreturn serializeJSON(local.returnVal)>
			</cfif>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="getAllcustomerEmail" datasource="#request.dsn#">
					select *
					from Customers
					where Preferred_Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_numeric" > 
				</cfquery>
				<cfif getAllcustomerEmail.recordcount >
					<cfloop list="#local.validateAPICall.data.services#" index="item">
						<cfquery name="getmail" datasource="#request.dsn#">
							select distinct(EMAIL_ADDRESS) as EMAIL_ADDRESS,FIRST_NAME,LAST_NAME
							from Appointments a 
							inner join Customers c on a.Customer_ID = c.Customer_ID
							where a.Professional_ID = <cfqueryparam value="#local.validateAPICall.data.professional_id#" cfsqltype="cf_sql_numeric" > 
							AND a.Service_ID = <cfqueryparam value="#item#" cfsqltype="cf_sql_numeric" > 
						</cfquery>

						<cfloop query="getmail">
								<!---<cfsavecontent variable="mailBody">
								<cfoutput>
								Dear <cfoutput>#getmail.FIRST_NAME# #getmail.LAST_NAME#</cfoutput>,
								<br>
								#local.validateAPICall.data.emailContent#
								<br>
								Regards,<br>
								SalonWorks Customer Support<br>
								salonworks@salonworks.com
								</cfoutput>
							</cfsavecontent>
							<cfset local.mailCustomer = variables.objMailgun.sendMailViaMailgun(mailTo="#getmail.EMAIL_ADDRESS#",mailFrom="salonworks@salonworks.com",mailSubject="#local.validateAPICall.data.emailSubject#",mailHtml = "#mailBody#" ) />--->
							<cfif len(getmail.EMAIL_ADDRESS) and isValid("email",getmail.EMAIL_ADDRESS) >
							<cfmail 					
								from="salonworks@salonworks.com"
								subject="#local.validateAPICall.data.emailSubject#"
								type="HTML"
								to="#getmail.EMAIL_ADDRESS#"
								<!--- to="#getmail.EMAIL_ADDRESS#" --->
								server="smtp-relay.sendinblue.com" port="587" username="ciredrofdarb@gmail.com" password="2xf5ZLbMdyDr0VSv" usetls="true">
								Dear <cfoutput>#getmail.FIRST_NAME# #getmail.LAST_NAME#</cfoutput>,
								<br>
								#local.validateAPICall.data.emailContent#
								<br>
								Regards,<br>
								SalonWorks Customer Support<br>
								salonworks@salonworks.com
							</cfmail>
							</cfif>
						</cfloop>
					</cfloop>
					<cfset local.returnVal.status = 1 />
				<cfelse>
					<cfset local.returnVal.errorMsg = "No customers exist" />
					<cfset local.returnVal.status = 0 />
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<!--- Survey --->
	<cffunction name="addQuestionDetails" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "SurveyID,SurveyQuestion,IsActive">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qInsertQuestion" datasource="#request.dsn#" result="queryResult">
					INSERT INTO SurveyQuestions(
						SurveyID
						,SurveyQuestion
						,IsActive
						)
					 VALUES(
					 	<cfqueryparam value="#local.validateAPICall.SurveyID#" cfsqltype="cf_sql_integer">
					 	,<cfqueryparam value="#local.validateAPICall.SurveyQuestion#" cfsqltype="cf_sql_nvarchar">
					 	,<cfqueryparam value="#local.validateAPICall.data.IsActive#" cfsqltype="cf_sql_nvarchar">
					 	)
				</cfquery>
				<cfset local.returnVal.surveyQnId = queryResult.generatedKey />
				<cfset local.returnVal.status = 1 />
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
	<cffunction name="addSurvey" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "SurveyName,SurveyTitle,IsActive,UniqueKey,CompanyID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="local.qryInsSurvey" datasource="#request.dsn#" result="queryResult">
					INSERT INTO Surveys
					(
						SurveyName,
						SurveyTitle,
						IsActive,
						UniqueKey,
						CompanyID
					) 
					VALUES
					(
						<cfqueryparam value="#local.validateAPICall.data.SurveyName#" cfsqltype="cf_sql_varchar"/>,
						<cfqueryparam value="#local.validateAPICall.data.SurveyTitle#" cfsqltype="cf_sql_varchar"/>,
						<cfqueryparam value="#local.validateAPICall.data.IsActive#" cfsqltype="cf_sql_varchar"/>,
						<cfqueryparam value="#createUUID()#" cfsqltype="cf_sql_varchar"/>,
						<cfqueryparam value="#local.validateAPICall.data.CompanyID#" cfsqltype="cf_sql_integer"/>
					)
				</cfquery>
				<cfset local.returnVal.surveyQnId = queryResult.generatedKey />
				<cfset local.returnVal.status = 1 />
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<!--- <cffunction name="addSurveyDetails" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "SurveyName,SurveyTitle,IsActive,UniqueKey,CompanyID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qryInsSurveyData" datasource="#request.dsn#">
					<cfloop from="1" to="#count#" index="i">
						INSERT INTO SurveyData
						(
							SurveyID,
							SurveyQuesID,
							OptionID,
							Other
							
						)
						VALUES
						(	
							<cfqueryparam value="#arguments.survey.surveyid#" cfsqltype="cf_sql_integer">,
							<cfqueryparam value="#evaluate("arguments.survey.question_#i#")#" cfsqltype="cf_sql_integer">,
							<cfqueryparam value="#evaluate("arguments.survey.option_#i#")#" cfsqltype="cf_sql_integer">,
							<cfqueryparam value="#evaluate("arguments.survey.textboxother_#i#")#" cfsqltype="cf_sql_nvarchar">
						)
					</cfloop>
				</cfquery>
				<cfset local.returnVal.surveyQnId = queryResult.generatedKey />
				<cfset local.returnVal.status = 1 />
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>
 --->

 <!--- Update blog post --->
	<cffunction name="getInqueries" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Location_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qryInquiries" datasource="#request.dsn#">
					SELECT *
					FROM Inquiries
					WHERE Location_ID = <cfqueryparam value="#local.validateAPICall.data.Location_ID#" cfsqltype="cf_sql_integer" />  
					order by Inquiry_ID desc
				</cfquery>
				<cfset local.returnVal.list = [] />
				<cfif qryInquiries.RecordCount >
					<cfloop query= "qryInquiries" >
						<cfset arrayAppend(local.returnVal.list,{Inquiry_ID:Inquiry_ID ,Sender_Name:Sender_Name,Sender_Message:Sender_Message,Sender_Email:Sender_Email,Sender_Phone:Sender_Phone}) />
					</cfloop>
					<cfset local.returnVal.status = 1 />
				<cfelse>
					<cfset local.returnVal.status = 0/>
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<cffunction name="deleteInqueries" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Inquiry_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qryInquiries" datasource="#request.dsn#" result="queryResult">
					DELETE FROM Inquiries
					WHERE 
					<cfif structKeyExists(local.validateAPICall.data,"Inquiry_ID")>
						Inquiry_ID = <cfqueryparam value="#local.validateAPICall.data.Inquiry_ID#" cfsqltype="cf_sql_integer" /> 
					</cfif>
					<cfif structKeyExists(local.validateAPICall.data,"Inquiry_List") and len(trim(local.validateAPICall.data.Inquiry_List))>
						Inquiry_ID IN(<cfqueryparam value="#local.validateAPICall.data.Inquiry_List#" list="yes" cfsqltype="cf_sql_integer" />) 
					</cfif>
				</cfquery>
				<cfset local.returnVal.status = 1 />
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

	<!--- Get professional details --->
	<cffunction name="getProfessionalDetails" access="remote" returnformat="plain" returntype="String">
    	<cfset local.validateAPICall = validateAPICall( httpData = getHttpRequestData() )>
		<cfset local.argsList = "Location_ID,Company_ID,Professional_ID">
		<cfif local.validateAPICall.status >
			<cfset local.returnVal = structNew() >
			<cfset local.returnVal.status = 0 >
			<cfset local.respondsStatus = argumentsValidation( args = local.validateAPICall.data, argsList = local.argsList )>
			<cfset local.returnVal = local.respondsStatus >
			<cfif local.respondsStatus.status >
				<cfquery name="qGetDetails" datasource="#request.dsn#">
					SELECT p.first_name,p.last_name,p.Mobile_Phone,p.Email_Address,l.Location_Name,l.Contact_Name,l.Contact_Phone,l.Location_Address2,l.Description,l.Directions,l.Location_Fax,l.Location_Address,l.Location_City,l.Location_State,l.Location_Postal,l.Location_Phone,c.Web_Address,c.Company_Name,c.Company_Address,c.Company_City,c.Company_State,c.Company_Postal,c.Company_Phone,c.Company_Email,c.Company_Description,c.Company_Fax,c.Company_Address2,p.Bio,p.Appointment_Increment
					FROM Companies as c
					inner join Locations as l on c.company_id = l.company_id
					inner join professionals as p on l.Location_ID = p.Location_ID
					WHERE l.Location_ID = <cfqueryparam value="#local.validateAPICall.data.Location_ID#" cfsqltype="cf_sql_integer" />  
					and c.company_id = <cfqueryparam value="#local.validateAPICall.data.Company_ID#" cfsqltype="cf_sql_integer" />  
					and p.Professional_ID = <cfqueryparam value="#local.validateAPICall.data.Professional_ID#" cfsqltype="cf_sql_integer" /> 
				</cfquery>
				<!--- check company image exist on folder --->
				<cfset variables.FilePath = expandPath("/images/company/") />				
				<cfset variables.imagePath = "#variables.FilePath##local.validateAPICall.data.Company_ID#.jpg" />
				<cfset variables.company_image = ''>
				<cfif fileExists(variables.imagePath)>
					<cfset variables.company_image = "www.salonworks.com/admin/images/company/#local.validateAPICall.data.Company_ID#.jpg"> 
				</cfif>
				<!--- check professional image exist on folder --->
				<cfset variables.professionalImageLocation = expandPath("/images/staff/") />				
				<cfset variables.professionalImagePath = "#variables.professionalImageLocation##local.validateAPICall.data.Professional_ID#.jpg" />
				<cfset variables.professional_image = ''>
				<cfif fileExists(variables.professionalImagePath)>
					<cfset variables.professional_image = "www.salonworks.com/admin/images/staff/#local.validateAPICall.data.Professional_ID#.jpg"> 
				</cfif>
				<cfset local.returnVal.list = [] />
				<cfif qGetDetails.RecordCount >
					<cfloop query= "qGetDetails" >
						<cfset arrayAppend(local.returnVal.list,
							{
								first_name_professional:first_name
								,last_name_professional:last_name
								,Email_Address_professional:Email_Address
								,Mobile_Phone_professional:Mobile_Phone
								,Location_Name_professional:Location_Name
								,Contact_Name_location:Contact_Name
								,Location_Address_location:Location_Address
								,Location_City_location:Location_City
								,Location_State_location:Location_State
								,Location_Postal:Location_Postal
								,Location_Phone:Location_Phone
								,Location_Contact_Phone:Contact_Phone
								,Location_Address2:Location_Address2
								,Location_Directions:Directions
								,Location_Description:Description
								,Location_Fax:Location_Fax
								,Web_Address:Web_Address
								,Company_Name:Company_Name
								,Company_Address:Company_Address
								,Company_City:Company_City
								,Company_State:Company_State
								,Company_Postal:Company_Postal
								,Company_Phone:Company_Phone
								,Company_Email:Company_Email
								,Company_Description:Company_Description
								,Company_Fax:Company_Fax
								,Company_Address2:Company_Address2
								,Bio:Bio
								,Appointment_Increment:Appointment_Increment
								,company_image:variables.company_image
								,professional_image:variables.professional_image
							}
							) />
					</cfloop>
					<cfset local.returnVal.status = 1 />
				<cfelse>
					<cfset local.returnVal.status = 0/>
				</cfif>
			</cfif>
			<cfreturn serializeJSON(local.returnVal)>
		</cfif>
		<cfreturn serializeJSON(local.validateAPICall)>
	</cffunction>

</cfcomponent>


																				