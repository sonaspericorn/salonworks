<cfinclude template="site_header.cfm">
<cfinclude template="schedule_demo.cfm">
<div style="padding-top: 111px;">
<section class="top-grid" id="features">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1>Salon & Spa Management Software</h1>
                </div>
                <div class="otr-dflx">
                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div class="card text-center">
                            <i><svg class="icon"><use xlink:href="img/svg-sprite.svg#grid_1"/></svg></i>
                            <div class="card-body">
                                <h5 class="card-title">Text and Email Reminders</h5>
                                <div class="content"><p class="card-text">Whether a client books their appointment in person, online or by phone, they can receive automated text messages<span id="dots1">...</span><span id="more1" style="display:none;"> and/or emails as a courtesy reminder 24 hours before their appointment allowing them time to reschedule or cancel if needed. Message all your customers with bulk messages to let them know about new offers or special holiday hours.</span></p></div>
                                <div style="padding-top: 12px;">
                                	<button class="btn btn-primary" onclick="myFunction(1)" id="myBtn1">READ MORE</button>
                           		</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div class="card text-center">
                            <i><img src="salonnewhome/img/grid-icon-02.png" alt=""></i>
                            <div class="card-body">
                                <h5 class="card-title">Online Booking</h5>
                                <div class="content"><p class="card-text">Think of your new website as your personal receptionist! There will be no need to interrupt your day to take appointments. Just enter your availability in  <span id="dots2">...</span><span id="more2" style="display:none;">your own password protected administration site, and your clients will be able to book their own appointments online on their computer, smartphone, or mobile device.</span></p></div>
                                <div>
                                	<button class="btn btn-primary" onclick="myFunction(2)" id="myBtn2">READ MORE</button>
                            	</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div class="card text-center">
                            <i><svg class="icon"><use xlink:href="img/svg-sprite.svg#grid_3"/></svg></i>
                            <div class="card-body">
                                <h5 class="card-title">Custom Website</h5>
                                <div class="content"><p class="card-text">With your new website, you can customize your layout, display photos, and publish your very own blog. Salon Works provides an amazing opportunity <span id="dots3">...</span><span id="more3" style="display:none;">for you to strengthen your online reputation for your business. Whether you decide to continue after your free 30-day trial of our advanced features, you can keep your custom website for free. Manage your business from your desktop, tablet, and smartphone with SalonWorks!</span></p></div>
                                <button class="btn btn-primary" onclick="myFunction(3)" id="myBtn3">READ MORE</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div class="card text-center">
                            <i><img src="salonnewhome/img/grid-icon-04.png" alt=""></i>
                            <div class="card-body">
                                <h5 class="card-title">Manage Schedule Online</h5>
                                <div class="content"><p class="card-text">Experience more flexibility with our online calendar. Managing your schedule online appointments while you are out and about, make edits or take notes on customers <span id="dots4">...</span> <span id="more4" style="display:none;">or bookings.Your Salon Works calendar shows your schedule which allows your clients to see availabilities, book upcoming appointments, even reschedule. Your Salon Works calendar includes Color-coded services to make it easy to see what's coming up for you and your clients. </span></p></div>
                                <button  class="btn btn-primary" onclick="myFunction(4)" id="myBtn4">READ MORE</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="otr-dflx" style="padding-top: 10px;">
                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div class="card text-center">
                            <i><img src="salonnewhome/img/grid-icon-04.png" alt=""></i>
                            <div class="card-body">
                                <h5 class="card-title">Photo Gallery</h5>
                                <div class="content"><p class="card-text">Showcase your work by including images in your photo gallery. </p></div>
                                <button class="btn btn-primary">READ MORE</button>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div class="card text-center">
                            <i><img src="salonnewhome/img/grid-icon-04.png" alt=""></i>
                            <div class="card-body">
                                <h5 class="card-title">Client Management</h5>
                                <div class="content"><p class="card-text">Use your SalonWorks account to keep notes and track all details about your clients. Right at your fingertips you will have a history .<span id="dots5">...</span><span id="more5" style="display:none;">of each clients passed services and preferences.</span></p></div>
                                <button  class="btn btn-primary" onclick="myFunction(5)" id="myBtn5">READ MORE</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div class="card text-center">
                            <i><img src="salonnewhome/img/grid-icon-04.png" alt=""></i>
                            <div class="card-body">
                                <h5 class="card-title">Accept Credit Cards</h5>
                                <div class="content"><p class="card-text">Give your clients the option of paying by credit card and have the funds deposited into your account the next day making it <span id="dots6">...</span><span id="more6" style="display:none;"> convenient for both parties.Your transactions are fully secure and clients can also purchase gift certificates and products from your site using their card.  Even keep your client's cards on file to make it even easier each visit.</span></p></div>
                                <button  class="btn btn-primary" onclick="myFunction(6)" id="myBtn6">READ MORE</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div class="card text-center">
                            <i><img src="salonnewhome/img/grid-icon-04.png" alt=""></i>
                            <div class="card-body">
                                <h5 class="card-title">Next Day Bank Deposits</h5>
                                <div class="content"><p class="card-text">All of your earnings will be deposited into your bank account the following business day.  Just process your client's credit<span id="dots7">...</span><span id="more7" style="display:none;"> card upon checkout and see the funds the next day</span></p></div>
                                <button  class="btn btn-primary" onclick="myFunction(7)" id="myBtn7">READ MORE</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="otr-dflx" style="padding-top: 10px;">
                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div class="card text-center">
                            <i><img src="salonnewhome/img/grid-icon-04.png" alt=""></i>
                            <div class="card-body">
                                <h5 class="card-title">Charge for No-Show and Late Cancellations</h5>
                                <div class="content"><p class="card-text">Configure your services to require a credit card to book, and enact  your no-show / latecancellation policy.  Client's credit  <span id="dots8">...</span><span id="more8" style="display:none;">cards can automatically be charged if they miss their appointment without sufficient notice.</p></div>
                                <button  class="btn btn-primary" onclick="myFunction(8)" id="myBtn8">READ MORE</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div class="card text-center">
                            <i><img src="salonnewhome/img/grid-icon-04.png" alt=""></i>
                            <div class="card-body">
                                <h5 class="card-title" style="padding-bottom: 23px;">Reports</h5>
                                <div class="content"><p class="card-text">Allow your clients to view availability and schedule accordingly from your computer or smart phone</p></div>
                                <button class="btn btn-primary">READ MORE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    function myFunction(i) {
        
  var dots = document.getElementById("dots"+i);
  var moreText = document.getElementById("more"+i);
  var btnText = document.getElementById("myBtn"+i);

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "READ MORE"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "READ LESS"; 
    moreText.style.display = "inline";
  }
}
</script>

<cfinclude template="site_footer.cfm">