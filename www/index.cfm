<cftry>
<cfset variables.objMailgun =  createObject("component","cfc.mailgun") />
    <cfset variables.subdomain = "www">
    <cfif structKeyExists(url,"subDomain") and len(url.subDomain)>
        <cfset variables.subdomain = url.subDomain>
    </cfif>
    <!--- <cfset variables.subdomain = ListgetAt(cgi.server_name,1,'.')> --->
    <cfinvoke component="admin.company" method="getCompany" returnvariable="qCompany">
        <cfinvokeargument name="Web_Address" value="#variables.subdomain#">
    </cfinvoke>
    <cfif qCompany.recordcount gt 0 AND !ListFindNoCase("www,salonworks",variables.subdomain)>
        <cfset variables.Company_ID=qCompany.Company_ID>
    <cfelse>
        <cfset variables.Company_ID=0>
    </cfif>

    <cfif variables.Company_ID eq 0>
        <cfset variables.feature_text="Meet your New Personal Receptionist!!">
        <cfparam name="variables.company_id" default="0">
        <cfparam name="variables.location_id" default="0">
        <cfparam name="variables.professional_id" default="0">
        <cfset variables.companyCFC = createObject("component","admin.company") />
        <cfset variables.qrySocialMedia = variables.companyCFC.getCompanySocialMediaPlus(variables.company_id) />
        <!--- Query to get the payment methods --->
        <cfquery name="getPaymentMethods" datasource="#request.dsn#">
            SELECT Payment_Method_ID, Payment_Method From Payment_Methods Order By Order_By
        </cfquery>

        <!--- Query to get the time zones --->
        <cfquery name="getTimeZones" datasource="#request.dsn#">
            SELECT Time_Zone_ID, Timezone_Location FROM Time_Zones WHERE enabled = 1
        </cfquery>

        <cfinvoke component="admin.company" method="getCompany" returnvariable="qCompany">
            <cfinvokeargument name="Company_ID" value="#variables.company_id#">
        </cfinvoke>

        <cfif structKeyExists(form, 'personolized_demo_btn')>
            <cfinvoke component="admin.company" method="setPersonalDemo" returnvariable="demo_id">
                <cfinvokeargument name="First_name" value="#form.first_name_demo#">
                <cfinvokeargument name="Last_name" value="#form.last_name_demo#">
                <cfinvokeargument name="Email" value="#form.email_id_demo#">
                <cfinvokeargument name="Mobile" value="#form.mobile_phone_demo#">
            </cfinvoke>
        </cfif>
      
        <cfinclude template="site_header.cfm">
        <cfinclude template="schedule_demo.cfm">
        <div class="banner">
            <div class="banner-slide">
                <div class="banner-bg-1" style="background:#2a272e url(salonnewhome/img/banner-bg-02.jpg) no-repeat">
                    <div class="container">
                        <div class="row flex-row-reverse">
                            <div class="col-xl-6">
                                <div class="bnr-caption-otr">
                                    <div class="bnr-txt">
                                        <h2>WEBSITES AND ONLINE<br>SCHEDULING</h2>
                                        <p class="light">For the modern salon</p>
                                    </div>
                                   <!---  <button type="button" class="btn btn-primary mrg-rgt signupform"><a href="#signupform" style="text-decoration: none;">sign up for free</a> </button> --->
                                   <a href="#signupform" class="btn btn-primary mrg-rgt signupform" style="text-decoration: none;" >sign up for free</a>
                                    <button type="button" class="btn btn-secondary" onclick="window.open('http://salonworks.com/demo');">View demo site</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="banner-bg-2" style="background:#2a272e url(salonnewhome//img/banner-bg-03.jpg) no-repeat">
                    <div class="container">
                        <div class="row flex-row-reverse">
                            <div class="col-xl-6">
                                <div class="bnr-caption-otr">
                                    <div class="bnr-txt">
                                        <h2>APPOINTMENT REMINDERS<br> BY TEXT/EMAIL</h2>
                                        <p>Reduce Missed Appointments! Whether A Client Booked Their Appointment Online Or By Telephone, They Will Receive An Automated Text Message And/Or Email As A Courtesy Reminder 24 Hours Before Their Appointment.</p>
                                    </div>
                                    <!--- <button type="button" class="btn btn-primary mrg-rgt"><a href="#signupform" style="text-decoration: none;">sign up for free</a> </button> --->
                                    <a href="#signupform" class="btn btn-primary mrg-rgt signupform" style="text-decoration: none;" >sign up for free</a>
                                    <button type="button" class="btn btn-secondary" onclick="window.open('http://salonworks.com/demo');">View demo site</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="banner-bg-3" style="background:#2a272e url(salonnewhome//img/banner-bg-04.jpg) no-repeat">
                    <div class="container">
                        <div class="row flex-row-reverse">
                            <div class="col-xl-6">
                                <div class="bnr-caption-otr">
                                    <div class="bnr-txt">
                                        <h2>24/7 ONLINE CLIENT<br> BOOKING</h2>
                                        <p>Think Of Your New Web Site As Your Personal Receptionist! There Will Be No Need To Interrupt Your Day To Take Appointments. Just Enter Your Availability Using Your Own Password Protected Administration Site, And Your Clients Will Be Able To Book Their Own Appointments Online On Their Computer, Smartphone, Or Mobile Device</p>
                                    </div>
                                    <!--- <button type="button" class="btn btn-primary mrg-rgt"><a href="#signupform" style="text-decoration: none;">sign up for free</a> </button> --->
                                    <a href="#signupform" class="btn btn-primary mrg-rgt signupform" style="text-decoration: none;" >sign up for free</a>
                                    <button type="button" class="btn btn-secondary" onclick="window.open('http://salonworks.com/demo');">View demo site</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="banner-bg-4" style="background:#2a272e url(salonnewhome//img/banner-bg-01.jpg) no-repeat">
                    <div class="container">
                        <div class="row flex-row-reverse">
                            <div class="col-xl-6">
                                <div class="bnr-caption-otr">
                                    <div class="bnr-txt">
                                        <h2>FREE<br> CUSTOM WEBSITE</h2>
                                        <p>Whether You Decide To Continue Your 30 Day Trial Of Our Advanced Features, You Can Keep Your Custom Website For Free. With Your Site You Can Customize Your Layout, Publish Pictures, And Even Manage Your Own Blog.</p>
                                    </div>
                                    <!--- <button type="button" class="btn btn-primary mrg-rgt"><a href="#signupform" style="text-decoration: none;">sign up for free</a> </button> --->
                                    <a href="#signupform" class="btn btn-primary mrg-rgt signupform" style="text-decoration: none;" >sign up for free</a>
                                    <button type="button" class="btn btn-secondary" onclick="window.open('http://salonworks.com/demo');">View demo site</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="top-grid" id="features">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1>Salon & Spa Management Software</h1>
                    </div>
                    <div class="otr-dflx">
                        <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                            <div class="card text-center">
                                <i><svg class="icon"><use xlink:href="img/svg-sprite.svg#grid_1"/></svg></i>
                                <div class="card-body">
                                    <h5 class="card-title">Email & Text Notifications</h5>
                                    <div class="content"><p class="card-text">Decrease no shows and cancellations with our automated courtesy appointment reminder system</p></div>
                                    <a href="#" class="btn btn-primary">READ MORE</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                            <div class="card text-center">
                                <i><img src="salonnewhome/img/grid-icon-02.png" alt=""></i>
                                <div class="card-body">
                                    <h5 class="card-title">Online Booking</h5>
                                    <div class="content"><p class="card-text">User friendly appointment management for yourself and your clients</p></div>
                                    <a href="#" class="btn btn-primary">READ MORE</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                            <div class="card text-center">
                                <i><svg class="icon"><use xlink:href="img/svg-sprite.svg#grid_3"/></svg></i>
                                <div class="card-body">
                                    <h5 class="card-title">Custom Website</h5>
                                    <div class="content"><p class="card-text">Increase online presence with a fully customizable website; included at no additional charge.</p></div>
                                    <a href="#" class="btn btn-primary">READ MORE</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                            <div class="card text-center">
                                <i><img src="salonnewhome/img/grid-icon-04.png" alt=""></i>
                                <div class="card-body">
                                    <h5 class="card-title">Manage Your Calendar</h5>
                                    <div class="content"><p class="card-text">Allow your clients to view availability and schedule accordingly from your computer or smart phone</p></div>
                                    <a href="#" class="btn btn-primary">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="container-fluid">
            <div class="row">
                <div class="video-mang-scnt ">
                    <div class="col-xl-6 col-md-12 d-flex align-items-stretch">
                        <div class="video-bx">
                            <a class="popup-youtube" href="https://www.youtube.com/watch?v=Jz-W4mLjkwA"><img src="salonnewhome/img/video-bg.jpg" alt="">
                                <div class="ply-btn"></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-12 d-flex align-items-stretch">
                        <div class="content-box">
                            <article>
                                <h2>Manage Appointments Online</h2>
                                <strong>WITH YOUR FREE WEBSITE & ONLINE SCHEDULING SOFTWARE FOR THE MODERN SALON
                                </strong>
                                <div class="content">
                                    <p>Think of your new website as your personal receptionist!  There will be no need to interrupt your day to
                                        take appointments.  Just enter your availability your own password protected administration site, and your
                                    clients will be able to book their own appointments online on their computer, smartphone, or mobile device. </p>
                                    <p>By indicating your availability, clients can book their appointments online at any time based on your schedule,
                                        day or night. You will receive notifications of the appointments and can view and manage them from your
                                    calendar interface.</p>
                                </div>
                                <a href="http://salonworks.com/demo" class="btn btn-primary" target="new">VIEW DEMO</a>
                                <a href="pricing.cfm" class="btn btn-primary btn-clr-rd">See pricing</a>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="sctin-csmt ">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h3>Save time with SalonWorks</h3>
                            <strong>
                            SAVE TIME BY HAVING EVERYTHING YOU NEED IN ONE PLACE
                            </strong>
                        </div>
                    </div>
                    <div class="mid-grid otr-dflx ">
                        <div class="col-lg-4 col-md-12 d-flex align-items-stretch">
                            <div class="card text-center">
                                
                                <div class="card-body">
                                    <h5 class="card-title">Get More Appointments</h5>
                                    <div class="content"><p class="card-text">Increase you number of appointments by
                                        allowing customers to conveniently check your
                                    calendar, book online & even reschedule.</p></div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 d-flex align-items-stretch">
                            <div class="card text-center">
                                
                                <div class="card-body">
                                    <h5 class="card-title">Plan Your Day</h5>
                                    <div class="content"><p class="card-text">Our color coded calendar will allow you to manage
                                        appointments, openings and cancellations all in one
                                    easy to manage place.</p></div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 d-flex align-items-stretch" id="save_time">
                            <div class="card text-center">
                                
                                <div class="card-body">
                                    <h5 class="card-title">Client Retention</h5>
                                    <div class="content"><p class="card-text">Automatic text & e-mail confirmations will
                                        remind your clients of upcoming appointments
                                        and when it's time for their next visit.
                                    </p></div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="forms-otr" id="demo_close">
            <div class="container">
                <div class="row">
                    <cfif not structKeyExists(url, "success")>
                        <form action="index.cfm?success=true#save_time" name="personolized_demo_form" id="personolized_demo_form" method="POST" class="erroron_form">
                            <div class="col-md-12">
                                <h4>Schedule A Free Demo</h4>
                                <div class="form-list">
                                    <ul class="forms">
                                        <li>
                                            <div class="input-holder">
                                                <input type="text" class="form-control" placeholder="First Name" required="required" name="first_name_demo">
                                            </div>
                                        </li>
                                        <li>
                                            <div class="input-holder">
                                                <input type="text" placeholder="Last Name" required="required" name="last_name_demo">
                                            </div>
                                        </li>
                                        <li>
                                            <div class="input-holder">
                                                <input type="text" placeholder="Email" required="required" name="email_id_demo">
                                            </div>
                                        </li>
                                        <li>
                                            <div class="input-holder">
                                                <input type="text" placeholder="Mobile Phone" required="required" name="mobile_phone_demo" class="phone_us">
                                            </div>
                                        </li>
                                        <li>
                                            <div class="submt-btn">
                                                <button type="submit" class="sbmt btn btn-block btn-primary"  name="personolized_demo_btn">Schedule Demo</button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    <cfelse>
                        <div class="col-md-12">
                            <div class="text-xs-center">
                              <h1 style="text-align: center;">Thank you for your interest in SalonWorks!</h1>
                              <p class="lead" style="text-align: center;"><strong>An Integration Specialist will be in contact with you within 24 hours to schedule your free demo.</strong></p>
                            </div>
                        </div> 
                    </cfif>
                </div>
            </div>
        </div>
        <section class="sctn-btm" >
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- <i class="icon-check-1"></i>
                        <h4>Over <span>1,500</span> appointments booked online using SalonWorks platform.</h4> -->
                    </div>
                </div>
            </div>
        </section>
        <div class="faq-sign-otr" >
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="faq-content">
                            <h4>Read FAQ's</h4>
                            <strong>Find the answers to your questions about our salon booking software services.</strong>
                            <div class="accordion" id="accordionExample">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Is there a limit to the number of services or providers I can have in the system?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">
                                            No there is no limit to the number of services or providers that you can add to your profile.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Does SalonWorks allow me to accept payments online?
                                        </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                        <div class="card-body">
                                            Yes, by simply configuring your bank account information in our secure environment you will be able to accept credit cards.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
                                        Do you have an app and is it mobile responsive?
                                        </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                        <div class="card-body">
                                            Yes, we have an app for professionals and our user sites are mobile friendly.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFive">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseTwo">
                                        Can I sync my agenda with google calendar?
                                        </button>
                                        </h5>
                                    </div>
                                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                                        <div class="card-body">
                                            Yes, you can sync your SalonWorks calendar with your Google calendar.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingSix">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseTwo">
                                        What hardware is required?
                                        </button>
                                        </h5>
                                    </div>
                                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                                        <div class="card-body">
                                            You can manage your entire business from any computer, tablet, or mobile device. There are now hardware restrictions. All of your data is stored in the cloud and can be accessed from any device, anywhere.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingSeven">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseTwo">
                                        Do you offer training?
                                        </button>
                                        </h5>
                                    </div>
                                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                                        <div class="card-body">
                                            Yes, upon creating your account an onboarding specialist will contact you to show you how to use the software in the best way for your individual situation to get the most out of your experience
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 form-web-otr" id="signupform">
                        <form role="form" action="admin/process_registration.cfm" method="POST" id="register_form" enctype="multipart/form-data" class="erroron_form">
                            <div class="sign-up-hm">
                                <h5>Sign up to start your <span>FREE</span> 30 Day Trial!</h5>
                                <p>Take the steps TODAY to fill your appointment book by completing the form below.
                                    Try out all features with no credit card required, and watch your business grow while simplifying
                                    your appointments. Just fill out the form below!
                                </p>
                                <div class="forms-hm-otr">
                                    <div class="forms">
                                        <input type="hidden" id="Web_Address" name="web_address" value="" >
                                        <div class="col-md-6">
                                            <div class="input-holder">
                                                <input type="text" placeholder="First Name" class="form-control"  name="First_Name" required="required" value="" maxlength="30">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-holder">
                                                <input type="text" placeholder="Last Name" class="form-control" 
                                           name="Last_Name" required="required" value="" maxlength="30">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-holder">
                                                <input type="text" placeholder="Email" class="form-control" required="required" name="Email_Address" id="Email_Address" value="">
                                            </div>
                                        </div>
                                        <!--- changed 17/01/19 --->
                                        <div class="col-md-6">
                                            <div class="input-holder">
                                                <input type="password" placeholder="Password" class="form-control" required="required" name="Password" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-holder">
                                                <input type="text" placeholder="Mobile Phone" class="form-control phone_us" name="Mobile_Phone" value="" required="required">
                                            </div>
                                        </div>
                                            <cfoutput>
                                        <!--- <div class="col-md-12">
                                            <label class="check-box">Yes, contact me to schedule a live demo.
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div> --->
                                            <div class="col-md-6">
                                                <div class="g-recaptcha" data-sitekey="#application.sitekey#"></div>
                                                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                                            </div>
                                        </cfoutput>
                                        <div class="col-md-12">
                                            <div class="submt-btn">
                                                <div class="input-holder">
                                                    <input type="hidden" value="firstsignup" id="firstsignup" name="firstsignup">
                                                    <button type="button" class="sbmt finishBtn" value="Submit" style="text-transform: none;width:203px;box-shadow: 0 2px 4px 0 rgba(0,0,0,.25);" onclick="return fnCheckEmailAddress(isSignup = 1);">Sign-up with Email</button>
                                                    <input type="submit" id="finishBtn" style="display: none;">
                                                </div>
                                            </div>
                                        </div>
                                         <div class="container-fluid" style="margin-top:10px;">
                                            <div class="row">
                                                <div class="col-sm-4"><hr></div>
                                                <div class="col-sm-4" style="text-align:center;">OR</div>
                                                <div class="col-sm-4"><hr></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="margin-top:10px;">
                                            <div class="row" >
                                                <div class="col-md-6 col-sm-6 col-xs-12" >
                                                   <div id="facebookBtn" onclick="facebooklogin();">Sign-up with Facebook</div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12" >
                                                    <button class="customGoogleBtn" id="googleBtn">Sign-up with Google</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearFix" ></div>
        <cfinclude template="site_footer.cfm">
    <cfelse>
        <cfinclude template="./customer_sites/index.cfm">
        <!--- Customer Homepage --->
    </cfif>
    <cfcatch>
        <cfdump var="#cfcatch#" abort="true">
    </cfcatch>
</cftry>


