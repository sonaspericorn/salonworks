<cfif structKeyExists(form, 'personolized_demo_button')>
    <cfinvoke component="admin.company" method="setPersonalDemo" returnvariable="local.demo_id">
        <cfinvokeargument name="First_name" value="#form.first_name_demo#">
        <cfinvokeargument name="Last_name" value="#form.last_name_demo#">
        <cfinvokeargument name="Email" value="#form.email_id_demo#">
        <cfinvokeargument name="Mobile" value="#form.mobile_phone_demo#">
    </cfinvoke>
    <cfif structKeyExists(local, 'demo_id') and local.demo_id neq 0>
        <script>
            $(document).ready(function(){
                $('.demo_id_sch').val('<cfoutput>#local.demo_id#</cfoutput>');
                $('.freeDemoScheduleModal').modal('show');
            });
        </script>
    <cfelse>   
        <script>
            $(document).ready(function(){
                alert('Some error occurred, Please try again.');
            });
        </script>
    </cfif>
</cfif>
<cfif structKeyExists(form, 'schTime')>
    <cfquery name="updId" datasource="#request.dsn#" result="updIdResult">
        UPDATE personolized_demo
        SET TimeSch = <cfqueryparam value="#form.schtime#" cfsqltype="cf_sql_varchar">,
        Selected_Date = <cfqueryparam value="#form.date#" cfsqltype="cf_sql_timestamp">,
        mailSent = <cfqueryparam value="1" cfsqltype="cf_sql_integer">
        WHERE Demo_ID = <cfqueryparam value="#form.demo_id#" cfsqltype="cf_sql_integer">
    </cfquery>
    <cfif updIdResult.recordcount>
        <script>
            $(document).ready(function(){
                alert('Your time has been scheduled');
                location.href = location.href;
            });
        </script>
    </cfif>
</cfif>
<html class="no-js" lang="en">
    <head>       
        <link rel="stylesheet" href="pricing/css/bootstrap/bootstrap.css">
        <link rel="stylesheet" href="pricing/css/main.css">
        <link rel="stylesheet" href="pricing/css/slick.css">
        <link rel="stylesheet" href="pricing/css/fontello.css">
        <link rel="stylesheet" href="pricing/css/magnific-popup.css">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />
        <script src="js/indexscript.js"></script>
    </head>
    <body>
        <!--- side button for schedule a free demo --->    
        <a data-toggle="modal" href="#freeDemoModal">
            <div class="free-demo-btn">     
               <span>Schedule A Free Demo</span>
            </div>
        </a>
        <!--free demo Modal -->
        <div class="modal fade freeDemoModal" id="freeDemoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Schedule A Free Demo</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="index.cfm" name="personolized_demo_forms" id="personolized_demo_forms" method="POST" class="erroron_form">
                            <div class="forms-otr">           
                                <ul class="forms">
                                    <li>
                                        <div class="input-holder">
                                            <input type="text" placeholder="First Name" required="required" name="first_name_demo">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="input-holder">
                                            <input type="text" placeholder="Last Name" required="required" name="last_name_demo">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="input-holder">
                                            <input type="text" placeholder="Email" required="required" name="email_id_demo">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="input-holder">
                                            <input type="text" placeholder="Mobile Phone" required="required" name="mobile_phone_demo" class="phone_us">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="submt-btn">
                                            <button type="submit" class="sbmt" value="Submit" name="personolized_demo_button" >Schedule Demo</button>
                                        </div>
                                    </li>
                                </ul>
                             </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .carousel-control-prev {
              display: none;
            }
        </style>
        <!--free demo Modal -->
        <div class="modal fade freeDemoScheduleModal" id="DemoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Schedule A Free Demo</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <cfquery name="Demo" datasource="#request.dsn#">
                        select Time from demo_TimeSlot;
                    </cfquery>
                    <!--- <cfset local.Demo=structNew()> --->
                    <div class="modal-body">
                        <form action="index.cfm" name="demoTime_forms" id="demoTime_forms" method="POST" class="erroron_form">
                            <input type="hidden" name="demo_id" class="demo_id_sch" value="">
                            <div class="forms-otr">          
                                <div class="container">
                                    <cfoutput>
                                        <div class="container">
                                            <div id="schSlider" class="carousel slide" data-ride="carousel">
                                                <!-- The slideshow -->
                                                <div class="carousel-inner">
                                                    <cfset dtDemo = Fix( Now() ) />
                                                    <cfloop index="intOffset" from="1" to="10" step="1">
                                                        <cfset dtDay = DateAdd( "w", intOffset, dtDemo ) />
                                                        <div class="carousel-item <cfif intoffset eq 1>active</cfif>">
                                                            <div class="col-cstm" >
                                                                <h5 class="weekstyle dateValue" data-demoDate="#DateFormat( dtDay, "mm/dd/yyyy" )#">
                                                                    <cfoutput>#DateFormat( dtDay, "full" )#</cfoutput></h5>
                                                                <input type="hidden" name="date" class="dateHiddenValue" value="" >
                                                                <cfloop query="#Demo#">
                                                                    <input class="btn-cstm btn-style" onclick="return steDemodate(this)" type="submit" name="schTime" value="#Demo.Time#">
                                                                </cfloop>           
                                                            </div> 
                                                        </div> 
                                                    </cfloop>                       
                                                </div>
                                                <a class="carousel-control-prev" href="##schSlider" data-slide="prev" >
                                                    <span class="carousel-control-prev-icon" ></span>
                                                </a>
                                                <a class="carousel-control-next" href="##schSlider" data-slide="next" >
                                                    <span class="carousel-control-next-icon"></span>
                                                </a>
                                            </div>
                                        </div>
                                    </cfoutput>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--- <script>
                        $(document).ready(function(){
                            $('#schSlider').carousel({
                              interval: false
                            });  
                            $('#schSlider').on('slid.bs.carousel', checkitem);
                            function checkitem(){
                                var $this = $('#schSlider');
                                if ($('.carousel-inner .carousel-item:first').hasClass('active')) {
                                    $this.children('.carousel-control-prev').hide();
                                } else if ($('.carousel-inner .carousel-item:last').hasClass('active')) {
                                    $this.children('.carousel-control-next').hide();
                                } else {
                                    $this.children('.carousel-control-prev').css('display','flex');
                                    $this.children('.carousel-control-next').css('display','flex');
                                }
                            }
                        });                              
                    </script> --->
                </div>
            </div>
        </div>
        <!--free demo Modal -->
    </body>
</html>