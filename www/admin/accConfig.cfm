<cfsetting requesttimeout="99999999">
<cfset variables.page_title = "Accept Credit Cards">
<cfinclude template="header.cfm">
<style type="text/css">
	.validation_msg {
		color: red;
	}
	.addBtn {
		padding: 15px;
	}
	hr {
	    margin: 15px 0 !important;
	}
	.mt-1 {
		margin-top : 10px !important;
	}
	.legentClass {
		width:inherit; / Or auto /
		padding:0 10px; / To give a bit of padding on the left and right /
		border: none !important;
		margin-bottom : 0px !important;
	}
	fieldset{
		margin-bottom : 25px;
	}
	.modal-dialog {
		width: 50% !important;
	}
	.labl{
		font-size: 12px;
		margin-top: 15px;
	}

</style>
<!--- <style>
	#back2Top {
    width: 40px;
    line-height: 40px;
    overflow: hidden;
    z-index: 999;
    display: none;
    cursor: pointer;
    -moz-transform: rotate(270deg);
    -webkit-transform: rotate(270deg);
    -o-transform: rotate(270deg);
    -ms-transform: rotate(270deg);
    transform: rotate(270deg);
    position: fixed;
    bottom: 50px;
    right: 0;
    background-color: #DDD;
    color: #555;
    text-align: center;
    font-size: 30px;
    text-decoration: none;
}
#back2Top:hover {
    background-color: #DDF;
    color: #000;
}
</style> --->
<cfoutput>
	<cfparam name="variables.company_id" default=#session.company_id#>
	<cfparam name="variables.professional_id" default=#session.professional_id#>

	<cfif isDefined('session.professional_id')>
		<cfset variables.professional_id 	= session.Professional_ID>
	</cfif>
	<cfif isDefined('session.Company_ID')>
		<cfset variables.Company_ID			= session.Company_ID>
	</cfif>
	
	<cfset local.paymentObj = createObject("component","cfc.payments") />
	<cfset local.professionalObj = createObject("component","professionals") />
	<cfset local.accountStatus = local.paymentObj.checkStripeAccount( professional_id = variables.professional_id, Company_ID = variables.Company_ID )>
	<cfset local.accountData = structNew()>
	<cfset local.accountDetails = structNew()>
	<cfif local.accountStatus.recordCount>
		<cfset local.accountData.accountId = local.accountStatus.stripeAccId>
		<cfset local.accountDetails = local.paymentObj.getAccountDetailsById( accountData = local.accountData )>
	</cfif>
	<cfset local.errorMsg = "">
	<cfset local.errorMsgAcc = "">

	<cfif structKeyExists(form,"createBankAcc") >
		<cfset local.account = structNew()>
		<cfset local.companyDataForm = structNew()>
		<cfset local.profileDataForm = structNew()>
		<cfset local.bankDetails = structNew()>
		<cfset local.account.tos_shown_and_accepted = true>
		<cfset local.account.legal_entity.maiden_name = "" >
		<cfset local.bankDetails.account_holder_type = "company">
		<cfset local.bankDetails.default_for_currency = false >
		
		<cfset local.companyData = local.paymentObj.getCompany( Company_ID = variables.Company_ID )>
		<cfset local.professionalDeta = local.paymentObj.getProfessional( Professional_ID = variables.professional_id, Company_ID = variables.Company_ID )>	

		<cfset local.profileDataForm.first_name = local.professionalDeta.First_Name>
		<cfset local.profileDataForm.last_name = local.professionalDeta.Last_Name>
		<cfset local.profileDataForm.phone = local.professionalDeta.Mobile_Phone>
		<cfset local.profileDataForm.address = local.professionalDeta.Home_Address>
		<cfset local.profileDataForm.address1 = local.professionalDeta.Home_Address2>
		<cfset local.profileDataForm.city = local.professionalDeta.Home_City>
		<cfset local.profileDataForm.state = local.professionalDeta.Home_State>
		<cfset local.profileDataForm.postal = local.professionalDeta.Home_Postal>
		<cfset local.profileDataForm.professional_id = variables.professional_id>

		<cfset local.companyDataForm.company_name =local.companyData.company_name>
		<cfset local.companyDataForm.company_address = local.companyData.Company_Address>
		<cfset local.companyDataForm.company_address1 = local.companyData.Company_Address2>
		<cfset local.companyDataForm.company_city = local.companyData.company_city>
		<cfset local.companyDataForm.company_postal_code = local.companyData.company_postal>
		<cfset local.companyDataForm.company_state = local.companyData.company_state>
		<cfset local.companyDataForm.company_web_address = local.companyData.web_address>
		<cfset local.companyDataForm.company_id = variables.Company_ID>
		<!--- 	Bank end Validation start --->
		



		<!--- 	Bank end Validation End --->

		<!--- <cfset local.paymentObj.updateCompanyData( companyDataForm = local.companyDataForm )> --->
		<!--- <cfset local.paymentObj.updateProfileData( profileDataForm = local.profileDataForm )> --->

		<cfif len(local.errorMsgAcc) eq 0 >
			<cfset local.professionalDetails = local.professionalObj.getProfessional( Professional_ID = variables.professional_id, Company_ID = variables.Company_ID )> 
			<cfset local.account.legal_entity.email = local.professionalDetails.Email_Address >
			<cfset local.account.legal_entity.phone = local.professionalDetails.Mobile_Phone >
			<cfset local.account.professional_id = variables.professional_id >
			<cfset local.account.company_id = variables.Company_ID >
			<cfset local.account.companyDataForm = local.companyDataForm >
			<cfset local.account.profileDataForm = local.profileDataForm >

			<cfset local.bankDetails.country = "US">
			<cfset local.bankDetails.professional_id = variables.professional_id >
			<cfset local.bankDetails.company_id = variables.Company_ID >

			<cfset local.accountCreateStatus = {}>
			<cfif not local.accountStatus.recordcount >
				<cfset local.accountCreateStatus = local.paymentObj.createAccount( account = local.account ) >
				<cfif structKeyExists(local.accountCreateStatus, "error")>
					<cfset local.errorMsg = local.accountCreateStatus.error.message >
				</cfif>
			</cfif>
			<cfif not structKeyExists(local.accountCreateStatus, "error")>
				<cfset local.bankAccountStatus = local.paymentObj.createBank( bankDetails = local.bankDetails ) >
				<cfif structKeyExists(local.bankAccountStatus, "error")>
					<cfset local.errorMsg = local.bankAccountStatus.error.message >
				<cfelse>
					<cflocation  url="/admin/accConfig.cfm" addtoken="no">
				</cfif>
			</cfif>
		</cfif>
	</cfif>
	<cfif structKeyExists(form,"edit_bank_id") and len(form.edit_bank_id)>
		<cfset local.editData = structNew()>
		<cfset local.editData.account_holder_name = "" >
		<cfset local.editData.bankId = form.edit_bank_id >
		<cfset local.editData.professional_id = variables.professional_id >
		<cfset local.editData.company_id = variables.Company_ID >
		<cfif structKeyExists(form,"edit_holder_name") and len(form.edit_holder_name)>
			<cfset local.editData.account_holder_name = form.edit_holder_name >
		</cfif>
		<cfset local.editData.default_for_currency = false >
		<cfif structKeyExists(form,"edit_bankDefault") and form.edit_bankDefault eq "on" >
			<cfset local.editData.default_for_currency = true>
		</cfif>
		<cfset local.updateBank = local.paymentObj.updateBank( bankDetails = local.editData ) >
		<cfif structKeyExists(local.updateBank, "error")>
			<cfset local.errorMsg = local.updateBank.error.message >
		<cfelse>
			<cflocation  url="/admin/accConfig.cfm" addtoken="no">
		</cfif>
	</cfif>

	<cfif structKeyExists(form,"delete_bank_id") and len(form.delete_bank_id)>
		<cfset local.deleteBank = structNew()>
		<cfset local.deleteBank.bankId = form.delete_bank_id >
		<cfset local.deleteBank.professional_id = variables.professional_id >
		<cfset local.deleteBank.company_id = variables.Company_ID >
		<cfset local.deleteBankStatus = local.paymentObj.deleteStripeAcc( bankDetails = local.deleteBank ) >
		<cfif structKeyExists(local.deleteBankStatus, "error")>
			<cfset local.errorMsg = local.deleteBankStatus.error.message >
		<cfelse>
			<cflocation  url="/admin/accConfig.cfm" addtoken="no">
		</cfif>
	</cfif>

	<cfset local.companyData = local.paymentObj.getCompany( Company_ID = variables.Company_ID )>
	<cfset local.professionalDeta = local.paymentObj.getProfessional( Professional_ID = variables.professional_id, Company_ID = variables.Company_ID )>	
	<cfset local.bankAccountList = local.paymentObj.getBankAcc( professional_id = variables.professional_id, Company_ID = variables.Company_ID ) >
	<div class="row">
		<!--- <cfif len(local.errorMsgAcc) >
			<div class="col-md-12">
				<div class="alert alert-danger">
				  <strong>Warning!</strong> #local.errorMsgAcc#
				</div>
			</div>
		</cfif>
		<cfif len(local.errorMsg) >
			<div class="col-md-12">
				<div class="alert alert-danger">
				  <strong>Error!</strong> #local.errorMsg#
				</div>
			</div>
		</cfif> --->
		<!--- <cfif not local.bankAccountList.recordcount>
			<div class="col-md-12 addBtn">
				<button type="button" class="btn btn-info btn-lg addBankBtn" data-toggle="modal" data-target="##paymentModal">Add Bank Account</button>
			</div>
		</cfif>
 --->
		<!--- </cfif> --->
		

		<div class="table-responsive col-sm-12">
			<div class="alert alert-info">
			  	In order to accept credit cards, simply add the bank account where you want your funds deposited. Once set up, you can charge your clients' credit cards right from you computer or mobile device. Your funds will be deposited in 2 business days.
			</div>
			<cfif  local.bankAccountList.recordcount>
				<table id="table_customers" class="table table-striped table-bordered table-hover text-center table_customers">
					<thead>
						<tr>
							<th class="col-sm-2 text-center">Name</th>
							<th class="col-sm-2 text-center">Routing Number</th>
							<th class="col-sm-2 text-center">Account Number</th>
							<th class="col-sm-2 text-center">Status</th>
							<th class="col-sm-2 text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						<!--- <cfif local.bankAccountList.recordcount> --->
							<cfloop query="local.bankAccountList">
								<tr>
									<td align="center">#local.bankAccountList.acc_Name#</td>
									<td align="center">#local.bankAccountList.routing_number#</td>
									<td align="center">#local.bankAccountList.acc_number#</td>
									<td align="center">
										<cfset local.accStatus = "pending">
										<cfif structKeyExists(local.accountDetails, "legal_entity") and structKeyExists(local.accountDetails.legal_entity,"verification") and structKeyExists(local.accountDetails.legal_entity.verification,"status")>
											<cfset local.accStatus = local.accountDetails.legal_entity.verification.status>
										</cfif>
										#local.accStatus#
									</td>
									<td align="center">
										<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
											<a href="##" data-toggle="modal" data-target="##editModel" data-type="edit" data-id="#local.bankAccountList.id#" class="edit-customer actionBtn" >
												<i class="icon-edit"></i
											</a>
											<a href="##" data-toggle="modal" data-target="##deleteModel" data-type="delete" data-id="#local.bankAccountList.id#" class="delete-custumer actionBtn">
												<font color="red"><i class="icon-trash"></i></font>
											</a>
										</div>
									</td>
								</tr>
							</cfloop>
						<!--- <cfelse>
							<tr>
								<td align="center" colspan="5">No Bank Found</td>
							</tr> --->
						<!--- </cfif> --->
					</tbody>
				</table>
			</cfif>
		</div>
	</div>

<!--- <script>
	<cfif len(local.errorMsg) or len(local.errorMsgAcc)>
		$(document).ready(function(){
			$('##paymentModal').modal('show');
		});
	</cfif>

</script>
 --->
	<cfif not local.bankAccountList.recordcount>
		<div class="col-md-12 addBtn">
			<button type="button" class="btn btn-info btn-lg addBankBtn" data-toggle="modal" data-target="##paymentModal">Add Bank Account</button>
		</div>
	</cfif>
	<div class="modal fade paymt_modal" id="paymentModal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content mdl_content">
				<div class="modal-header text-center">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title mdl_header">Payments Setup</h4>
				</div>
				<!--- <cfif len(local.errorMsgAcc) >
			<div class="col-md-12">
				<div class="alert alert-danger">
				  <strong>Warning!</strong> #local.errorMsgAcc#
				</div>
			</div>
		</cfif>
		<cfif len(local.errorMsg) >
			<div class="col-md-12">
				<div class="alert alert-danger">
				  <strong>Error!</strong> #local.errorMsg#
				</div>
			</div>
		</cfif> --->
				<div class="col-md-12 hide stripeErrorSec">
					<div class="alert alert-danger">
					  <strong>Error!</strong><span id="stripeError"></span>
					</div>
				</div>
				<form name="bank-form" action="" id="paymentModalForm" method="POST">
					<div class="modal-body">
						<fieldset>
							<legend class="legentClass" style="border: none">Company</legend>
							<div class="row">
								<div class="col-xs-6">
									<label class="label_text labl">Company Name*</label>
									<input type="text" id="company_name" name="company_name" value="#local.companyData.company_name#" class="input_text form-control company borderStyle" placeholder="Company Name"  required="true">
									<span class="validation_msg companyError" style="display: none;">Company Name Required</span>  
								</div>
								<div class="col-xs-6 input_div">
									<label class="label_text labl">Company Web Address*</label>
									<input type="text" id="company_Web_Address" name="company_Web_Address" value="#cgi.SERVER_NAME#/#local.companyData.web_address#" class="input_text form-control company borderStyle" placeholder="Company Web Address"  required="true">
									<span class="validation_msg companyError" style="display: none;">Company Web Address Required</span>  
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<label class="label_text labl">Company Address*</label>
									<input type="text" id="company_Address" name="company_Address" value="#local.companyData.Company_Address#" class="input_text form-control company borderStyle" placeholder="Company Address" required="true">
									<span class="validation_msg companyError" style="display: none;">Company Address Required</span>  
								</div>
								<div class="col-xs-6 input_div">
									<label class="label_text labl">Company Address2</label>
									<input type="text" id="company_Address1" name="company_Address1" value="#local.companyData.Company_Address2#" class="input_text form-control borderStyle" placeholder="Company Address" >
								</div>
							</div>
					
							<div class="row">
								<div class="col-xs-6">
									<label class="label_text labl">Company City*</label>
									<input type="text" id="company_city" name="company_city" value="#local.companyData.company_city#" class="input_text form-control company" placeholder="Company City" required="true">
									<span class="validation_msg companyError" style="display: none;">Company City Required</span>    
								</div>
								<div class="col-xs-6 input_div">
									<label class="label_text labl">Company State*</label>
									<input type="text" id="company_state" name="company_state" value="#local.companyData.company_state#" class="input_text form-control company" placeholder="Company State" required="true">
									<span class="validation_msg companyError" style="display: none;">Company State Required</span> 
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 input_div">
									<label class="label_text labl">Company Postal Code*</label>
									<input type="text" id="company_postal_code" name="company_postal_code" value="#local.companyData.company_postal#" class="input_text form-control company" placeholder="Company Postal Code"  required="true">
									<span class="validation_msg companyError" style="display: none;">Company Postal Code Required</span>   
								</div>
								<div class="col-xs-6 input_div">
									<label class="label_text labl">Tax Id*</label>
									<input type="text" id="tax_id_registrar" name="tax_id_registrar" class="input_text form-control company" placeholder="Tax Id" required="true">
									<span class="validation_msg" style="display: none;">Tax ID is Required</span> 
								</div>
							</div>
						</fieldset>
						<fieldset>
								<legend class="legentClass" style="border: none">Bank</legend>
							<div class="row">
								<div class="col-xs-6">
									<label class="label_text labl">Account holder's name*</label>
									<input type="text" id="holder_name" name="holder_name"  class="input_text form-control company" placeholder="Holder Name" required="true">	
									<span class="validation_msg" style="display: none;">Account Holder Name Required</span> 								
								</div>
								<div class="col-xs-6 input_div">
									<label class="label_text labl">Routing number*</label>
									<input type="text" id="routing_number"  name="routing_number" class="input_text form-control company" placeholder="Routing Number" required="true">
									<span class="validation_msg" style="display: none;">Routing Number Required</span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<label class="label_text labl">Account number*</label>
									<input type="text" id="account_number"  name="account_number" class="input_text form-control companyAcc company" placeholder="Account Number" required="true">
									<span class="validation_msg" style="display: none;">Account Number Required</span>
								</div>
								<div class="col-xs-6 input_div">
									<label class="label_text labl">Re-enter account number</label>
									<input type="text" id="re_account_name"  class="input_text form-control companyAccRe company" placeholder="Re-enter account number" required="true">
									<span class="matchingError validation_msg" style="display: none;">Please re-enter account number</span> 
								</div>
							</div>
						</fieldset>
						<fieldset>
							<legend class="legentClass" style="border: none">Profile</legend>
							<div class="row">
								<div class="col-xs-6">
									<label class="label_text labl">First Name*</label>
									<input type="text" id="first_name"  name="first_name" value="#local.professionalDeta.first_name#" class="input_text form-control company" placeholder="First Name" required="true">
									<span class="validation_msg" style="display: none;">First Name Required</span>
								</div>
								<div class="col-xs-6 input_div">
									<label class="label_text labl">Last Name</label>
									<input type="text" id="last_name"  name="last_name" value="#local.professionalDeta.last_name#" class="input_text form-control company" placeholder="Last Name" required="true">
									<span class="validation_msg" style="display: none;">Last Name Required</span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<label class="label_text labl">Profile Address*</label>
									<input type="text" id="profile_address"  name="profile_address" value="#local.professionalDeta.home_address#" class="input_text form-control company" placeholder="Profile Address" required="true">
									<span class="validation_msg" style="display: none;">Profile Address Required</span>
								</div>
								<div class="col-xs-6 input_div">
									<label class="label_text labl">Profile Address2</label>
									<input type="text" id="profile_address1" name="profile_address1" value="#local.professionalDeta.Home_Address2#" class="input_text form-control" placeholder="Profile Address">
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<label class="label_text labl">Profile City*</label>
									<input type="text" id="profile_city" name="profile_city" value="#local.professionalDeta.home_city#" class="input_text form-control company" placeholder="Profile City"  required="true">
									<span class="validation_msg" style="display: none;">Profile City Required</span>
								</div>
								<div class="col-xs-6 input_div">
									<label class="label_text labl">Profile State*</label>
									<input type="text" id="profile_state" name="profile_state" value="#local.professionalDeta.home_state#" class="input_text form-control company" placeholder="Profile State"  required="true">
									<span class="validation_msg" style="display: none;">Profile State Required</span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<label class="label_text labl">Profile Postal Code</label>
									<input type="text" id="profile_postal_code" name="profile_postal_code" value="#local.professionalDeta.home_postal#" class="input_text form-control company" placeholder="Profile Postal Code" required="true">
									<span class="validation_msg" style="display: none;">Profile Postal Code Required</span>
								</div>
								<div class="col-xs-6 input_div">
									<label class="label_text labl">Profile Phone*</label>
									<input type="text" id="profile_phone" name="profile_phone" value="#local.professionalDeta.mobile_phone#" class="input_text form-control company"  placeholder="Profile Phone" required="true">
									<span class="validation_msg" style="display: none;">Profile Phone Required</span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<label class="label_text labl">Last 4 digits of ssn*</label>
									<input type="text" name="ssn" id="ssn" class="input_text form-control company" placeholder="XXXX"  required="true">
									<span class="validation_msg" style="display: none;">SSN Required</span> 
								</div>
								<div class="col-xs-6 input_div">
									<label class="label_text labl">Date of Birth*</label>
									<input type="text" name="dob" id="dob" class="input_text form-control company" placeholder="MM/DD/YYYY" required="true">
									<span class="validation_msg" style="display: none;">Date of Birth Required</span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 EinStyle">
									<label class="label_text">Have EIN?</label> 
								</div>
								<div class="col-xs-6 input_div EinStyle">
									<input type="checkbox" id="employerNumberCheck" name="employerNumberCheck" class="form-check-input">
								</div>
							</div>
							<div class="row" id="employerNumberSec">
								<style>.EinStyle{padding: 18px;} .mg{margin-top: -22px;margin-left: 2px;font-size: 13px;}</style>
								<div class="col-xs-6 input_div">
									<label class="label_text labl mg">EIN</label> 
									<!--- <span class="validation_msg" style="display: none;">Employer identification number Required</span> --->
									<input type="text" name="employerNumber" id="employerNumber" class="input_text form-control" style="height: 42px;margin-top: -4px;"> 
									<span class="validation_msg" style="display: none;">Employer identification number Required</span>
								</div>
								<!--- <div class="col-xs-6 input_div">
									<input type="text" name="employerNumber" id="employerNumber" class="input_text form-control">
								</div> --->
							</div>
						</fieldset>
						<!--- <div id="loading" style="margin-left:251px;"></div> --->
					</div>
					<div class="modal-footer" style="text-align: center;">
						<div class="row text-center">
							<input type="hidden" name="createBankAcc" value="true">
							<input type="hidden" name="professional_id" value="#variables.professional_id#">
							<input type="hidden" name="company_ID" value="#variables.Company_ID#">
							<button type="button" class="btn btn-primary" id="createBankAccBtn" name="createBankAccBtn">SUBMIT</button>
						</div>
					</div>
							
				</form>
			</div>
		</div>
	</div>
	<style>
		##loading .modal-content {
			background: none;
			border: none;
		}
		##loading .modal-body {
			/*height: 100vh;
			display: flex;
			justify-content: center;
			align-content: center;*/
			color: ##fff;
		}
		##loading.modal {
			text-align: center;
			padding: 0!important;
		}

		##loading.modal:before {
			content: '';
			display: inline-block;
			height: 100%;
			vertical-align: middle;
			margin-right: -4px;
		}

		##loading .modal-dialog {
			display: inline-block;
			text-align: left;
			vertical-align: middle;
		}
	</style>
<div class="modal fade" id="loading" role="dialog">
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="modal-body" >
					<div class="text-center"><img src="assets/images/Loading-Loop-1.gif" height="42" width="42"> loading...</div>
			</div>
		</div>
	</div>
</div>

	<div class="modal fade paymt_modal" id="editModel" role="dialog">
	    <div class="modal-dialog">
	      <!-- Modal content-->
	        <div class="modal-content mdl_content">
	            <div class="modal-header">
	                <h4 class="modal-title mdl_header">Payments Setup</h4>
	            </div>
            	<form name="bank-form" action="" method="POST">
		            <div class="modal-body">
		                <div class="row">
	                        <div class="col-xs-6">
	                            <label class="label_text">Account holder's name</label>
	                            <span class="validation_msg" style="display: none;">Account Holder Name Required</span>  
	                        </div>
	                        <div class="col-xs-6 input_div">
	                            <input type="hidden" id="edit_bank_id" name="edit_bank_id" value="">
	                            <input type="text" id="edit_holder_name" class="input_text form-control" name="edit_holder_name" placeholder="Holder Name" required="true">
	                        </div>
	                    </div>
	                    <hr>
						<div class="row">
	                        <div class="col-xs-6">
	                            <label class="label_text">Default bank account?</label> 
	                        </div>
	                        <div class="col-xs-6 input_div">
	                            <input type="checkbox" id="edit_bankDefault" name="edit_bankDefault" class="form-check-input">
	                        </div>
	                    </div>
		            </div>
		            <div class="modal-footer" style="text-align: center;">
		                <div class="row text-center">
		                    <button type="submit" class="btn btn-primary" id="edit_btn" name="edit_btn">SUBMIT</button>
		                </div>
		            </div>
	            </form>
	        </div>
	    </div>
	</div>
	<div class="modal fade paymt_modal" id="deleteModel" role="dialog">
	    <div class="modal-dialog">
	      <!-- Modal content-->
	        <div class="modal-content mdl_content">
	            <div class="modal-header">
	                <h4 class="modal-title mdl_header">Payments Setup</h4>
	            </div>
            	<form name="bank-form" action="" method="POST">
		            <div class="modal-body">
		                <div class="row">
	                        <div class="col-xs-6">
	                        	<input type="hidden" id="delete_bank_id" name="delete_bank_id" value="">
	                            <label class="label_text">Are you sure want to remove?</label>  
	                        </div>
	                    </div>
		            </div>
		            <div class="modal-footer" style="text-align: right;">
		                <div class="row text-center">
		                    <button type="submit" class="btn btn-primary" id="deleteBtn" name="deleteBtn">Yes</button>
		                </div>
		            </div>
	            </form>
	        </div>
	    </div>
	</div>
</cfoutput>

<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

<script>
	$('#profile_phone').inputmask("(999) 999-9999");
	$(".actionBtn").click(function(){
		var id = parseInt($(this).attr("data-id"));
		var type = $(this).attr("data-type");
		var professional_id = parseInt("<cfoutput>#variables.professional_id#</cfoutput>");
		var Company_ID = parseInt("<cfoutput>#variables.Company_ID#</cfoutput>");
		$.ajax({
			url: "/cfc/payments.cfc?method=getBankAccDetails",
			type: "post",
			data: {
				professional_id : professional_id,
				Company_ID : Company_ID,
				id : id
			},
			success: function(data){
				var obj = JSON.parse(data);
				if(type == "edit"){
					if(obj.DATA.length){
						$("#edit_holder_name").val(obj.DATA[0][1]);
						$("#edit_bank_id").val(obj.DATA[0][9]);
						if(obj.DATA[0][8]){
							$("#edit_bankDefault").attr("checked","true");
						}
					}
				} else {
					$("#delete_bank_id").val(obj.DATA[0][9]);
				}
			}, 
			error: function(data){
				console.log(data);
			}
		});
	});
	$("#employerNumberSec").addClass("hide");
	$("#employerNumberCheck").click(function(){
		$("#employerNumberSec").addClass("hide");
		$("#employerNumber").removeClass("company");
		if($(this).attr('checked')) {
			$("#employerNumberSec").removeClass("hide");
			$("#employerNumber").addClass("company");
		}
	});
	$(".addBankBtn").click(function(){
		$(".validation_msg").css("display","none");
		$(".table_customers").css("display","block");
	});

    $("#createBankAccBtn").click(function(){
		var error = false;
		// $("#paymentModalForm").focus();
		// $('input').on('focus', function() {
  //   		document.body.scrollTop = $(this).offset().top;
		// });
		if (! $(".stripeErrorSec").hasClass("hide")) {
			$(".stripeErrorSec").addClass("hide");
		}
		$(".validation_msg").css("display","none");
    	$(".company").each(function(){
			if( $(this).val() == "" ){
				$(this).parent().find(".validation_msg").css("display","block");
				error = true;
				$(this).focus();
				$('html, body').animate({
				    scrollTop: ($(this).offset().top)
				},500);
				return false;
			}
		});
		var companyAcc = $(".companyAcc").val();
		var companyAccRe = $(".companyAccRe").val();
		/*if( companyAcc == "" ){
			$(".companyAcc").parent().find(".validation_msg").css("display","block");
			error = true;
		}
		if( companyAcc != "" && companyAccRe == "" ){
			$(".companyAccRe").parent().find(".validation_msg").css("display","block");
			error = true;
		}*/
		if( companyAcc != companyAccRe ){
			$(".companyAccRe").parent().find(".validation_msg").css("display","block");
			error = true;
			$(".companyAccRe").focus();
			$('html, body').animate({
			    scrollTop: ($(".companyAccRe").offset().top)
			},500);
			return false;
		}
		console.log(error);
		if(!error){
			// $('#loading').html('<img src="assets/img/loading.gif" height="42" width="42"> loading...');
			$('#loading').modal('show');
			$("#stripeError").html("");
			// $(this).prop('disabled', true);
			$.ajax({ 
	            type: "POST", 
	            url: "stripeProcess.cfm", 
	            data: $("#paymentModalForm").serialize(), 
	            success: function(data) { 
	            	if($.trim(data) == "Success") {
	            		location.href = "/admin/accConfig.cfm";
	            	}
	            	else{
	            		$('#loading').modal('hide');
	        			$("#paymentModal").scrollTop(0);
		            	$(".stripeErrorSec").removeClass("hide");
		            	$("#stripeError").html(" "+data);
	            		// $(window).scrollTop($('#slider-anchor').offset().top);
	            	}
	            }     
	        });
			//$("#paymentModalForm").submit();
		}
		return false;
    });
</script>
<!--- <script>
	$( document ).ready(function() {
   			$('#createBankAccBtn').on('hidden.bs.modal', function (e) {
			$("#stripeError").html("");
			$(".stripeErrorSec").addClass("hide");
})
	});

	
</script> --->

<cfinclude template="footer.cfm">