
<cfset variables.page_title = "Cancel subscription">
<cfinclude template="header.cfm" >
<cfoutput>
	<div>
		<cfquery name="getsuscriptionid" datasource="#request.dsn#">
		   SELECT 
		   subscriptionId
		   FROM
		   Companies
		   WHERE
		   Company_ID=
		   <cfqueryparam value="#session.Company_ID#" cfsqltype="cf_sql_integer" />
		</cfquery>
		<cfif getsuscriptionid.recordcount and len(getsuscriptionid.subscriptionId) gt 1 >
			<!--- <h2>Cancel Subcription</h2> --->
			<h4>By cancelling your subscription,can lead to lost of access in the existing features of your website ! </h4>
			<form action="process_payment.cfm" method="post" id="calcel_subscription_form"> 
				<input type="hidden" name="subscriptionId" id="subscriptionId" value="#getsuscriptionid.subscriptionId#">
				<input type="submit" class="btn btn-lg btn-primary" style="margin-top: 14px;"name ="cancelSubscription" value="Cancel subscription" id="cancelSubscription">
			
			</form>
		</cfif>
		
	</div>
</cfoutput>
<cfinclude template="footer.cfm">