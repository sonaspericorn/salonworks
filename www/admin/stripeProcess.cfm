<cfset local.errorMsg = "">
<cfset local.errorMsgAcc = "">
<cfset local.paymentObj = createObject("component","cfc.payments") />
<cfset local.professionalObj = createObject("component","professionals") />
<cfif structKeyExists(form,"createBankAcc") >
	<cfset local.account = structNew()>
	<cfset local.companyDataForm = structNew()>
	<cfset local.profileDataForm = structNew()>
	<cfset local.bankDetails = structNew()>
	<cfset variables.Company_ID = form.company_ID>
	<cfset variables.professional_id = form.professional_id>
	<cfset local.accountStatus = local.paymentObj.checkStripeAccount( professional_id = variables.professional_id, Company_ID = variables.Company_ID )>
	<cfset local.account.tos_shown_and_accepted = true>
	<cfset local.account.legal_entity.maiden_name = "" >
	<cfset local.bankDetails.account_holder_type = "company">
	<cfset local.bankDetails.default_for_currency = false >
	
	<cfset local.companyData = local.paymentObj.getCompany( Company_ID = variables.Company_ID )>
	<cfset local.professionalDeta = local.paymentObj.getProfessional( Professional_ID = variables.professional_id, Company_ID = variables.Company_ID )>	

	<cfset local.profileDataForm.first_name = local.professionalDeta.First_Name>
	<cfset local.profileDataForm.last_name = local.professionalDeta.Last_Name>
	<cfset local.profileDataForm.phone = local.professionalDeta.Mobile_Phone>
	<cfset local.profileDataForm.address = local.professionalDeta.Home_Address>
	<cfset local.profileDataForm.address1 = local.professionalDeta.Home_Address2>
	<cfset local.profileDataForm.city = local.professionalDeta.Home_City>
	<cfset local.profileDataForm.state = local.professionalDeta.Home_State>
	<cfset local.profileDataForm.postal = local.professionalDeta.Home_Postal>
	<cfset local.profileDataForm.professional_id = variables.professional_id>

	<cfset local.companyDataForm.company_name =local.companyData.company_name>
	<cfset local.companyDataForm.company_address = local.companyData.Company_Address>
	<cfset local.companyDataForm.company_address1 = local.companyData.Company_Address2>
	<cfset local.companyDataForm.company_city = local.companyData.company_city>
	<cfset local.companyDataForm.company_postal_code = local.companyData.company_postal>
	<cfset local.companyDataForm.company_state = local.companyData.company_state>
	<cfset local.companyDataForm.company_web_address = local.companyData.web_address>
	<cfset local.companyDataForm.company_id = variables.Company_ID>
	<cfset local.bankDetails.account_holder_name ="">
	<!--- 	Bank end Validation start --->
	<cfif structKeyExists(form,"profile_phone") and len(form.profile_phone)>
		<cfset local.profileDataForm.phone = form.profile_phone>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Profile Phone Required <br>">
	</cfif>
	<cfif structKeyExists(form,"profile_address") and len(form.profile_address)>
		<cfset local.profileDataForm.address = form.profile_address>
		<cfset local.profileDataForm.address1 = form.profile_address1>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Profile Address Required <br>">
	</cfif>
	<cfif structKeyExists(form,"profile_city") and len(form.profile_city)>
		<cfset local.profileDataForm.city = form.profile_city>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Profile City Required <br>">
	</cfif>
	<cfif structKeyExists(form,"profile_state") and len(form.profile_state)>
		<cfset local.profileDataForm.state = form.profile_state>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Profile State Required <br>">
	</cfif>
	<cfif structKeyExists(form,"profile_postal_code") and len(form.profile_postal_code)>
		<cfset local.profileDataForm.postal = form.profile_postal_code>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Profile Postal Code Required <br>">
	</cfif>
	<cfif structKeyExists(form,"company_name") and len(form.company_name)>
		<cfset local.companyDataForm.company_name = form.company_name>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Company Name Required <br>">
	</cfif>
	<cfif structKeyExists(form,"company_address") and len(form.company_address)>
		<cfset local.companyDataForm.company_address = form.company_address>
		<cfset local.companyDataForm.company_address1 = form.company_address1>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Company Address Required <br>">
	</cfif>
	<cfif structKeyExists(form,"company_city") and len(form.company_city)>
		<cfset local.companyDataForm.company_city = form.company_city>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Company City Required <br>">
	</cfif>
	<cfif structKeyExists(form,"company_postal_code") and len(form.company_postal_code)>
		<cfset local.companyDataForm.company_postal_code = form.company_postal_code>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Company Postal Code Required <br>">
	</cfif>
	<cfif structKeyExists(form,"company_state") and len(form.company_state)>
		<cfset local.companyDataForm.company_state = form.company_state>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Company State Required <br>">
	</cfif>
	<cfif structKeyExists(form,"company_web_address") and len(form.company_web_address)>
		<cfset local.companyDataForm.company_web_address = form.company_web_address>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Company Web Address Required <br>">
	</cfif>
	<cfif structKeyExists(form,"holder_name") and len(form.holder_name)>
		<cfset local.bankDetails.account_holder_name = form.holder_name>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Account Holder Name Required <br>">
	</cfif>
	<cfif structKeyExists(form,"tax_id_registrar") and len(form.tax_id_registrar)>
		<cfset local.account.tax_id_registrar = form.tax_id_registrar>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Tax Id Required <br>">
	</cfif>
	<cfif structKeyExists(form,"routing_number") and len(form.routing_number)>
		<cfset local.bankDetails.routing_number = form.routing_number>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Bank Routing Number Required <br>">
	</cfif>
	<cfif structKeyExists(form,"account_number") and len(form.account_number)>
		<cfset local.bankDetails.account_number = form.account_number>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Bank Account Number Required <br>">
	</cfif>
	<cfif structKeyExists(form,"first_name") and len(form.first_name)>
		<cfset local.account.legal_entity.first_name = form.first_name >
		<cfset local.profileDataForm.first_name = form.first_name>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Profile First Name Required <br>">
	</cfif>
	<cfif structKeyExists(form,"last_name") and len(form.last_name)>
		<cfset local.account.legal_entity.last_name = form.last_name >
		<cfset local.profileDataForm.last_name = form.last_name>
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Profile Last Name Required <br>">
	</cfif>
	<cfif structKeyExists(form,"ssn") and len(form.ssn)>
		<cfset local.account.legal_entity.ssn = form.ssn >
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Profile SSN Required <br>">
	</cfif>
	<cfif structKeyExists(form,"dob") and len(form.dob)>
		<cfset local.account.legal_entity.dob = form.dob >
	<cfelse>
		<cfset local.errorMsgAcc = local.errorMsgAcc & "Profile DOB Required <br>">
	</cfif>
	<cfif structKeyExists(form,"employerNumber") and len(form.employerNumber)>
		<cfset local.account.legal_entity.employerNumber = form.employerNumber >
	</cfif>
	<cfset local.bankDetails.currency = "usd">
	<!--- 	Bank end Validation End --->

	<!--- <cfset local.paymentObj.updateCompanyData( companyDataForm = local.companyDataForm )> --->
	<!--- <cfset local.paymentObj.updateProfileData( profileDataForm = local.profileDataForm )> --->

	<cfif len(local.errorMsgAcc) eq 0 >
		<cfset local.professionalDetails = local.professionalObj.getProfessional( Professional_ID = variables.professional_id, Company_ID = variables.Company_ID )> 
		<cfset local.account.legal_entity.email = local.professionalDetails.Email_Address >
		<cfset local.account.legal_entity.phone = local.professionalDetails.Mobile_Phone >
		<cfset local.account.professional_id = variables.professional_id >
		<cfset local.account.company_id = variables.Company_ID >
		<cfset local.account.companyDataForm = local.companyDataForm >
		<cfset local.account.profileDataForm = local.profileDataForm >

		<cfset local.bankDetails.country = "US">
		<cfset local.bankDetails.professional_id = variables.professional_id >
		<cfset local.bankDetails.company_id = variables.Company_ID >

		<cfset local.accountCreateStatus = {}>
		<cfif not local.accountStatus.recordcount >
			<cfset local.accountCreateStatus = local.paymentObj.createAccount( account = local.account ) >
			<cfif structKeyExists(local.accountCreateStatus, "error")>
				<cfset local.errorMsg = local.accountCreateStatus.error.message >
			</cfif>
		</cfif>
		<cfif not structKeyExists(local.accountCreateStatus, "error")>
			<cfset local.bankAccountStatus = local.paymentObj.createBank( bankDetails = local.bankDetails ) >
			<cfif structKeyExists(local.bankAccountStatus, "error")>
				<cfset local.errorMsg = local.bankAccountStatus.error.message >
			</cfif>
		</cfif>
	</cfif>
</cfif>
<cfif len(local.errorMsg)>
	<cfoutput>#local.errorMsg#</cfoutput>
<cfelse>
	Success
</cfif>