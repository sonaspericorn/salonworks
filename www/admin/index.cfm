<cfset variables.page_title="Dashboard">
<cfset local.showcompanyupdatemodal = false/>
<cfquery name="checkValues" datasource="#request.dsn#">
	SELECT 1 
	FROM Companies c 
	INNER JOIN 
	Locations l 
	ON c.Company_ID=l.Company_ID 
	WHERE '' IN(
		Company_Name
		,Company_Address
		,Company_City
		,Company_State
		,Company_Postal
		,Company_Phone
		,Contact_Name
		,Location_Address
		,Location_City
		,Location_State
		,Location_Postal
		,Location_Phone
		,Sunday_Hours
		,Monday_Hours
		,Tuesday_Hours
		,Wednesday_Hours
		,Thursday_Hours
		,Friday_Hours
		,Saturday_Hours) 
	and c.Company_ID=#session.company_id# 
</cfquery>
<cfquery  name="checkNullValues" datasource="#request.dsn#">
	SELECT 1
	FROM Companies c  
	INNER JOIN 
	Locations l 
	ON c.Company_ID=l.Company_ID 
	WHERE c.Company_Name is null and c.Company_Address is null and c.Company_City is null and c.Company_State is null and c.Company_Postal is null and c.Company_Phone is null and l.Contact_Name is null and l.Location_Address is null and l.Location_City is null and l.Location_State is null and l.Location_Postal is null and l.Location_Phone is null and l.Sunday_Hours is null and l.Monday_Hours is null and l.Tuesday_Hours is null and l.Wednesday_Hours is null and l.Thursday_Hours is null and l.Friday_Hours is null and l.Saturday_Hours is null and c.Company_ID=#session.company_id#
</cfquery>
<!--- dateFormat(Now(),"yyyy-mm-dd") & " " & timeFormat(now(), "HH:mm:ss.SSS") --->
<cfquery name="getUpcomingAppoinments" datasource="#request.dsn#">
	SELECT TOP 3 p.Professional_ID,p.first_name,l.Location_Name,a.Appointment_ID,a.Start_Time,a.Date_Booked,a.Service_ID,s.Service_Name
	FROM Appointments a
	INNER JOIN Professionals p on p.Professional_ID = a.Professional_ID
	INNER JOIN Services s on s.Service_ID = a.Service_ID
	INNER JOIN Locations l on l.Location_ID = p.Location_ID
	WHERE a.Professional_ID = <cfqueryparam value="#session.professional_id#" cfsqltype="cf_sql_integer">
	AND a.Start_Time >= <cfqueryparam value="#dateFormat(Now(),"yyyy-mm-dd") & " " & timeFormat(now(), "HH:mm:ss.SSS")#" cfsqltype="cf_sql_timestamp">
	ORDER BY Start_Time ASC
</cfquery>


<cfquery name="getCustomerWebaddress" datasource="#request.dsn#">
	SELECT Company_ID, company_name, web_address
	FROM Companies 
	WHERE Company_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.company_id#">
</cfquery>


<cfif checkValues.recordcount>
	<cfset local.showcompanyupdatemodal = true/>
</cfif>
<cfif checkNullValues.recordcount>
	<cfset local.showcompanyupdatemodal = true/>
</cfif>
<cfinclude template="header.cfm">

<!---  --->
<cfparam name="URL.msg" default="" />
<div class="row">
<cfoutput>#URL.msg#


<!--- 
<a href="http://#qCompany.Web_Address#.salonworks.com" target="_blank">View your web site</a>
<p>
To enable online booking:<br>
1. <a id="serviceClickhere" class="clickhere" href="##">Click here</a> to add the services that you offer<br>
2. <a id="availabilityClickhere" class="clickhere" href="##">Click here</a> to go to the Calendar and add your availability<br>
</p> --->



<div class="col-md-12 col-sm-12 col-xs-12 OtrIcons">
	<div class="row">
		<div class="headingDash">
		<!--- 	<h2>Welcome To SalonWorks</h2> --->
		</div>
		<div class="IcnOtr">
			<div class="col-md-4 col-sm-4 col-xs-6 bgIcnOtr" id="dashCompanyProfile">
				<div class="bgIcn col-md-12">
					<i class="fa fa-id-card-o" aria-hidden="true"></i>
					<p>Edit Profile</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6 bgIcnOtr" id="dashCustomers">
				<div class="bgIcn col-md-12">
					<i class="fa fa-users" aria-hidden="true"></i>
					<p>Client List</p>
			   </div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6 bgIcnOtr" id="dashCalendar" onclick="calendarAppointments();">
				<div class="bgIcn col-md-12">
					<i class="fa fa-calendar" aria-hidden="true"></i>
					<p>Calender</p>
			 </div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6 bgIcnOtr" id="dashSocialMedia">
				<div class="bgIcn col-md-12">
					<i class="fa fa-comments-o" aria-hidden="true"></i>
					<p>Social Media</p>
			 	</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6 bgIcnOtr" id="dashEditWeb">
				<div class="bgIcn col-md-12">
					<i class="fa fa-desktop" aria-hidden="true"></i>
					<p>Edit Website</p>
			 	</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6 bgIcnOtr admin-support" >
				<div class="bgIcn col-md-12">
					<i class="fa fa-handshake-o" aria-hidden="true"></i>
					<p>Support</p>
			 </div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 boxOtr d-flex">
	<div class="col-md-6 col-sm-12 col-xs-12 boxbgOtr">
		<h3 class="hed">Notices</h3>
		<cfif getCustomerWebaddress.recordcount>
			<cfif structKeyExists(getCustomerWebaddress, "web_address") and len(trim(getCustomerWebaddress.web_address))>
				<div class="col-md-12 noticeDtl">
					<a id="sitename" href="https://#cgi.SERVER_NAME#/#getCustomerWebaddress.web_address#">View your custom web site</a>
				</div>
			</cfif>
		</cfif>
		<div class="col-md-12 noticeDtl">
			<a onclick="calendarAvailability();">Add your availability</a>
		</div>
		<div class="col-md-12 noticeDtl">
			<a onclick="calendarAppointments();">Add appointments</a>
		</div>
		<div class="col-md-12 noticeDtl">
			<a href="accConfig.cfm">Add a bank account for your credit card deposits</a>
		</div>
		<div class="col-md-12 noticeDtl">
			<a href="social_media.cfm">Connect to your social media</a>
		</div>
		<div class="col-md-12 noticeDtl">
			<a href="gallery.cfm">Upload photos to your gallery</a>
		</div>
	</div>

	<div class="col-md-6 col-sm-12 col-xs-12 boxbgOtr bdtLeft">
		<h3 class="hed">Upcoming Appointments</h3>
		<h4>What is on your list?</h4>	
		<cfif getUpcomingAppoinments.recordcount>
			<cfloop query="getUpcomingAppoinments">
				<cfoutput>
					<div class="col-md-12 contentDtl">
						#getUpcomingAppoinments.SERVICE_NAME# with #getUpcomingAppoinments.first_name#<br>
						#timeFormat(getUpcomingAppoinments.Start_Time, "HH:mm tt")# #getUpcomingAppoinments.Location_Name#
					</div>
				</cfoutput>
			</cfloop>
		<cfelse>
			<div class="col-md-12 contentDtl">
				Book appointments now. Manage your availability by clicking <a onclick="calendarAvailability()" class="cursor">here</a>. Enter new appointments to your calendar by clicking <a onclick="calendarAppointments()" class="cursor">here</a>
			</div>
		</cfif>
	</div>
</div>

</cfoutput>
</div>

 
<cfif local.showcompanyupdatemodal >
	<script type="text/javascript">
		function showcompanyupdatemodal() {
			$('#modalfirstlog').modal("show");

		}
		showcompanyupdatemodal();

	</script>
</cfif>	
<cfinclude template="footer.cfm">
