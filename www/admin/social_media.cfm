<!--- variable declaration begins --->
<cfset variables.page_title = "Social Media">
<cfparam name="variables.company_id" default="#session.company_id#">
<cfset variables.companyCFC = createObject("component","company") />
<cfset variables.socialIdList = "">
<cfset variables.Social_Media_ID = "">
<cfset variables.qrySocialMedia = variables.companyCFC.getCompanySocialMediaPlus(variables.company_id) />
<!--- variable declaration ends --->

<!--- control flow begins --->
<cfif isDefined('submitFrmDataCompany')>
	<cfloop query="variables.qrySocialMedia">
		<cfif Len(Evaluate("Form.URL_" & Social_Media_ID))>
			<cfset variables.Social_Media_ID = ListAppend(variables.Social_Media_ID,Evaluate("Form.URL_" & Social_Media_ID)) >
			<cfset variables.socialIdList = ListAppend(variables.socialIdList,Social_Media_ID) />
		</cfif>
	</cfloop>
	<cfif ListLen(variables.socialIdList)>
		<cfset variables.socialsubmit =variables.companyCFC.saveSocialMediaForm(session.company_id, variables)>	
		<cflocation url="social_media.cfm" addtoken="false">
	</cfif>
<cfelse>
	<cfinvoke component="company" method="getCompany" returnvariable="qCompany">
		<cfinvokeargument name="Company_ID" value="#variables.company_id#">
	</cfinvoke>	
</cfif>
<!--- control flow ends --->

<!--- html begins --->
<cfinclude template="header.cfm">
<cfoutput>
	<!--- <h5 style="padding-left: 60px;color:blue;">Enter the web address of each of your social media pages</h5> --->
	<form action="social_media.cfm" method="post" class="form-horizontal" role="form" id="register_company" name="register_company" enctype="multipart/form-data">
		<cfloop query="variables.qrySocialMedia">
		 	<div class="form-group">
				<label for="x" class="col-sm-3 control-label">#variables.qrySocialMedia.Site_Name#<img src="../images/#variables.qrySocialMedia.Logo_File#" border="0" width="25"/></label>
				<div class="col-sm-9">
					<input type="hidden" name="social_website_#variables.qrySocialMedia.Social_Media_ID#" value="#variables.qrySocialMedia.Web_Site#">
		            <input type="text" style="width: 323px;" class="form-control" id="socialMediaURLurl_#variables.qrySocialMedia.Social_Media_ID#" name="url_#variables.qrySocialMedia.Social_Media_ID#"
		            value="#variables.qrySocialMedia.url#" placeholder="#variables.qrySocialMedia.Site_Name#" data-social_url_id = "#variables.qrySocialMedia.Social_Media_ID#"/>
				</div>
			</div>
		</cfloop>
		<div class="col-sm-8">
			<div class="form-group">
				<label class="col-sm-3 control-label">&nbsp;</label>
				<div class="col-sm-9">
					<input type="submit" name="submitFrmDataCompany" class="social_url btn btn-info btn-sm">
				</div>
			</div>
		</div>
	</form>
</cfoutput>
<!--- html ends --->


<cfinclude template="footer.cfm">