<cfoutput>
	<cfset variables.developmentserver = false >
	<cfif variables.developmentserver >
			<cfset variables.url_CIM_ARB = "https://apitest.authorize.net/xml/v1/request.api">
			<cfset variables.login = "7xG5qVRm65k">
			<cfset variables.transactionkey = "97p5B2MD2v4v7tA3">
	<cfelse>
			<cfset variables.url_CIM_ARB = "https://api.authorize.net/xml/v1/request.api">
			<cfset variables.login = "3wqGHT3Vw48w">
			<cfset variables.transactionkey = "68R2Jujw26By29y5">
	</cfif>
	<cfset local.response = getHttpRequestData()>
	<cfquery name="insertTransaction" datasource="#request.dsn#">
				INSERT INTO 
					Transactions
				( webhook_status)
				VALUES
				(
					<cfqueryparam value="#serializeJSON(local.response.content)#" cfsqltype="cf_sql_nvarchar" />,
					
					)
				SELECT @@IDENTITY AS 'Invoice_ID';
			</cfquery>
	<cfdocument format="PDF" filename="webhookresponse_1.PDF" overwrite="true">
		<cfdump var="#local.response.content#">
	</cfdocument>

		<cfif structKeyExists(local.response.content, 'notifications')  and #len(local.response.content.notifications.notificationId)# and local.response.content.notifications.eventType eq 'net.authorize.payment.authcapture.created' >
			<cfsavecontent variable="myXml"><?xml version="1.0" encoding="utf-8"?>
				<getTransactionDetailsRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
				    <merchantAuthentication>
				        <name>7xG5qVRm65k</name>
				        <transactionKey>97p5B2MD2v4v7tA3</transactionKey>
				    </merchantAuthentication>
				    <transId>#local.response.content.payload.id#</transId>
				</getTransactionDetailsRequest>
			</cfsavecontent>
			<cfset response.XmlRequest = xmlParse(myXml)>

			<cfhttp url="#variables.url_CIM_ARB#" method="POST">
			<cfhttpparam type="XML" name="xml" value="#myXml#">
			</cfhttp>
			<cfdump var="#cfhttp#">
			<cfset response.XmlResponse = REReplace( cfhttp.FileContent, "^[^<]*", "", "all" ) />

			<cfset response.XmlResponse = xmlParse(response.XmlResponse)>
			<cfdump var="#response#">

			<CFIF isDefined('response.XmlResponse.getTransactionDetailsResponse.messages.message')>
				<CFIF response.XmlResponse.getTransactionDetailsResponse.messages.resultCode is "Ok">
			<!--- <cfset response.refId = response.XmlResponse.getTransactionDetailsResponse.refId.XmlText> --->
					<cfset response.subscriptionId = response.XmlResponse.getTransactionDetailsResponse.subscription.id>
					<cfset response.transactionid = response.XmlResponse.getTransactionDetailsResponse.transaction.id>
					<cfset response.authAmount = response.XmlResponse.getTransactionDetailsResponse.transaction.authAmount>
					<cfset response.Transaction_Time = response.XmlResponse.getTransactionDetailsResponse.transaction.submitTimeUTC>
				</CFIF>

			<cfquery name="insertTransaction" datasource="#request.dsn#">
				INSERT INTO 
					Transactions
				(Company_ID, subscription_id ,webhook_status,Transaction_ID,Transaction_Time,Amount_Paid)
				VALUES
				(<cfqueryparam value="#session.Company_ID#" cfsqltype="cf_sql_integer" />,
					<cfqueryparam value="#response.subscriptionId#" cfsqltype="cf_sql_numeric" />,
					<cfqueryparam value="#local.response.content#" cfsqltype="cf_sql_nvarchar" />,
					<cfqueryparam value="#response.transactionid#" cfsqltype="cf_sql_numeric" />,
					<cfqueryparam value="#response.Transaction_Time#" cfsqltype="cf_sql_date" />,
					<cfqueryparam value="#response.authAmount#" cfsqltype="cf_sql_money" />,
					)
				SELECT @@IDENTITY AS 'Invoice_ID';
			</cfquery>
	
		</cfif>
	
</cfoutput>