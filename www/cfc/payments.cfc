`<cfcomponent displayname="payment" extends="Stripe">
	<!--- <cfset variables.ApiKey = "sk_test_1VrdPvsoLLEvqvSejQmFFJcV"> --->
	<!--- <cfset variables.ApiKey = "sk_test_usnImcKLeHCyJPXHziiYWlOT"> --->
	<cfset variables.ApiKey = "sk_live_nRm2J0cSQ7nMzfcup3CT1F5v">
	<cfset variables.publishableKey = "pk_live_LAAX1NEAcYLeyKhToPC1ku3Q">
	<!--- get checkout Methods --->
	<cffunction name="getCheckoutMethods" access="public" returntype="Query">
		<cfquery name="local.qGetCheckoutMethods" datasource="#request.dsn#">
			SELECT  *
			FROM    Checkout_method
		</cfquery>
		<cfreturn local.qGetCheckoutMethods>
	</cffunction>

	<!--- function for get payment type --->
	<cffunction  name="getPaymentType" access="remote" returntype="String" returnformat="plain">
		<cfargument name="appointmentId" type="numeric">
		<cfset local.returnVal = structNew()>
		<cfset local.returnVal.paindBy = "Credit Card">
		<cfquery name="local.qGetPaymentType" datasource="#request.dsn#">
			SELECT  cm.*
			FROM    Checkout_method cm
			INNER JOIN stripeTransactions st ON st.stripId = cm.type
			WHERE st.Appointment_ID = <cfqueryparam value="#arguments.appointmentId#" cfsqltype="cf_sql_integer">
		</cfquery>
		<cfif local.qGetPaymentType.recordcount>
			<cfset local.returnVal.paindBy = local.qGetPaymentType.type>
		</cfif>
		<cfreturn serializeJSON(local.returnVal)>
	</cffunction>

	<!--- function for check account exists in stripe --->
	<cffunction name="checkStripeAccount" access="public" returntype="Query">
		<cfargument name="professional_id" type="numeric">
		<cfargument name="Company_ID" type="numeric">
		<cfquery name="local.qGetAccount" datasource="#request.dsn#">
			SELECT 	*
			FROM 	stripeAccount
			WHERE 	Professional_ID =  <cfqueryparam value="#arguments.professional_id#" cfsqltype="cf_sql_integer">
			AND 	Company_ID = <cfqueryparam value="#arguments.Company_ID#" cfsqltype="cf_sql_integer">
			AND  	isActive = <cfqueryparam value="1" cfsqltype="cf_sql_integer">
		</cfquery>
		<cfreturn local.qGetAccount>	
	</cffunction>

	<!--- function for check exists bank account --->
	<cffunction name="checkBankExists" access="public" returntype="Struct">
		<cfargument name="bankDetails" type="struct">
	</cffunction>

	<!--- create stripe external bank account --->
	<cffunction name="createBank" access="public" returntype="struct">
		<cfargument name="bankDetails" type="struct">
		<cfset local.errorMsg = {"error": {"code": "dbError","message": "Bank already added!"}}>
		<cfset local.returnVal = local.errorMsg>
		<cfset local.bankExists = getBankAcc( professional_id = arguments.bankDetails.professional_id, Company_ID = arguments.bankDetails.company_id, routing_number = arguments.bankDetails.routing_number )>
		<cfif not local.bankExists.recordcount >
			<cfset init( stripeApiKey = variables.ApiKey )>
			<cfset local.tokenStatus = createBankToken( bankDetails = arguments.bankDetails )>
			<cfset local.returnVal = local.tokenStatus>
			<cfif not structKeyExists(local.tokenStatus, "error")>
				<cfset local.accountId = checkStripeAccount( professional_id = arguments.bankDetails.professional_id, company_id = arguments.bankDetails.company_id).stripeAccId>
				<cfset local.errorMsg = {"error": {"code": "accountError","message": "Account not created"}}>
				<cfset local.returnVal = local.errorMsg>
				<cfif len(local.accountId)>
					<cfset local.errorMsg = {"error": {"code": "dbError","message": "Something wend wrong, Please contact administator!"}}>
					<cfset local.bankAccExists = getBankAcc( professional_id = arguments.bankDetails.professional_id, Company_ID = arguments.bankDetails.company_id)>
					<cfset local.default_for_currency = arguments.bankDetails.default_for_currency>
					<cfif !local.bankAccExists.recordcount >
						<cfset local.default_for_currency = "">
					</cfif>
					<cfset local.createBankStatus = createBankAccount( accountId = local.accountId, bankToken = local.tokenStatus.id , default_for_currency = local.default_for_currency) >
					<cfset local.returnVal = local.createBankStatus>
					<cfif not structKeyExists(local.createBankStatus, "error")>
						<cfset local.bankDetails = arguments.bankDetails>
						<cfset local.bankDetails.responds = local.createBankStatus>
						<cfset local.bankDetails.tokenResponds = local.tokenStatus>
						<cfset local.bankDetails.bankId = local.createBankStatus.id>
						<cfif len(local.default_for_currency) and local.default_for_currency >
							<cfset local.bankDetails = structNew()>
							<cfset local.bankDetails.accountId = local.accountId>
							<cfset local.bankDetails.bankId = local.createBankStatus.id>
							<cfset local.bankDetails.account_holder_name = arguments.bankDetails.account_holder_name>
							<cfset local.bankDetails.default_for_currency = local.default_for_currency>
							<cfset local.returnValNew = updateBankAcc( bankDetails = local.bankDetails ) >
						</cfif>
						<!--- <cfloop array="#local.createBankStatus.external_accounts.data#" index="externalBank">
							<cfif externalBank.routing_number eq arguments.bankDetails.routing_number>
								<cfset local.bankDetails.bankId = externalBank.id>
							</cfif>
						</cfloop> --->
						<cfset local.bankDetails.bankToken = local.tokenStatus.id>
						<cfset local.bankStatus = saveBankDetails( bankDetails = local.bankDetails )>
						<cfif not local.bankStatus>
							<cfset local.returnVal = local.errorMsg>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
		<cfreturn local.returnVal>
	</cffunction>

	<!--- function for save Bank details --->
	<cffunction name="saveBankDetails" access="public" returntype="Numeric">
		<cfargument name="bankDetails" type="struct">
		<cftry>
			<cfset local.accCount = len(arguments.bankDetails.account_number)>
			<cfset local.accTemp = local.accCount-4>
			<cfset local.accNum = "">
			<cfloop from="1" to="#local.accTemp#" index="count">
				<cfset local.accNum = local.accNum&"*">
			</cfloop>
			<cfset local.accNum = local.accNum&right(arguments.bankDetails.account_number,4)>
			<cfquery name="qSaveBankData" datasource="#request.dsn#">
				INSERT INTO bankAccount(acc_Name,professional_id,Company_ID,routing_number,acc_number,currency,country,default_acc,bankId,bankToken,responds_string,tokenResponds)
				VALUES	(<cfqueryparam value="#arguments.bankDetails.account_holder_name#" cfsqltype="cf_sql_varchar">,
						 <cfqueryparam value="#arguments.bankDetails.Professional_ID#" cfsqltype="cf_sql_integer">,
						 <cfqueryparam value="#arguments.bankDetails.company_id#" cfsqltype="cf_sql_integer">,
						 <cfqueryparam value="#arguments.bankDetails.routing_number#" cfsqltype="cf_sql_varchar">,
						 <cfqueryparam value="#local.accNum#" cfsqltype="cf_sql_varchar">,
						 <cfqueryparam value="#arguments.bankDetails.currency#" cfsqltype="cf_sql_varchar">,
						 <cfqueryparam value="#arguments.bankDetails.country#" cfsqltype="cf_sql_varchar">,
						 <cfqueryparam value="#arguments.bankDetails.default_for_currency#" cfsqltype="cf_sql_bit">,
						 <cfqueryparam value="#arguments.bankDetails.bankId#" cfsqltype="cf_sql_varchar">,
						 <cfqueryparam value="#arguments.bankDetails.bankToken#" cfsqltype="cf_sql_varchar">,
						 <cfqueryparam value="#serializeJSON(arguments.bankDetails.responds)#" cfsqltype="cf_sql_varchar">,
						 <cfqueryparam value="#serializeJSON(arguments.bankDetails.tokenResponds)#" cfsqltype="cf_sql_varchar">)
			</cfquery>
			<cfreturn 1>
			<cfcatch>
				<cfreturn 0>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---  Function for update Company Details	 --->
	<cffunction  name="updateCompanyData" access="public">
		<cfargument  name="companyDataForm" type="struct">
		<cfquery name="local.updateCompany" datasource="#request.dsn#">
			UPDATE 	Companies
			SET 	Web_Address = <cfqueryparam value="#trim(arguments.companyDataForm.company_web_address)#" cfsqltype="cf_sql_varchar">
					,Company_Name = <cfqueryparam value="#trim(arguments.companyDataForm.company_name)#" cfsqltype="cf_sql_varchar">
					,Company_Address = <cfqueryparam value="#trim(arguments.companyDataForm.company_address)#" cfsqltype="cf_sql_varchar">
					,Company_Address2 = <cfqueryparam value="#trim(arguments.companyDataForm.company_address1)#" cfsqltype="cf_sql_varchar">
					,Company_City = <cfqueryparam value="#trim(arguments.companyDataForm.company_city)#" cfsqltype="cf_sql_varchar">
					,Company_State = <cfqueryparam value="#trim(arguments.companyDataForm.company_state)#" cfsqltype="cf_sql_varchar">
					,Company_Postal = <cfqueryparam value="#trim(arguments.companyDataForm.company_postal_code)#" cfsqltype="cf_sql_varchar">
			WHERE 	Company_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#trim(arguments.companyDataForm.company_id)#">
		</cfquery>
	</cffunction>

	<!---  Function for Get Company Details	 --->
	<cffunction name="getCompany" access="public" output="false" returntype="query" hint="Returns query of Company based on Company_ID">
		<cfargument name="Company_ID" type="numeric" required="false" default="0">
		<cfquery name="local.getCompany" datasource="#request.dsn#">
			SELECT
		      	Web_Address
		      	,Company_Name
		      	,Company_Address
		      	,Company_Address2
		      	,Company_City
		      	,Company_State
		      	,Company_Postal
		      	,Company_Phone
		      	,Company_Email
		  FROM 	Companies
		  WHERE Company_ID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.Company_ID#">
		</cfquery>
		<cfreturn local.getCompany>
	</cffunction>

	<!---  Function for update Professionals and Location Details	 --->
	<cffunction  name="updateProfileData" access="public">
		<cfargument  name="profileDataForm" type="struct">
		<cfquery name="local.updateProfessional" datasource="#request.dsn#">
			UPDATE  pls
			SET 	pls.First_Name = <cfqueryparam value="#trim(arguments.profileDataForm.first_name)#" cfsqltype="cf_sql_varchar">
			      	,pls.Last_Name = <cfqueryparam value="#trim(arguments.profileDataForm.last_name)#" cfsqltype="cf_sql_varchar">
			FROM 	Professionals pls
			LEFT JOIN Locations ls on ls.Location_ID = pls.Location_ID 
			WHERE  	pls.Professional_ID = <cfqueryparam value="#trim(arguments.profileDataForm.professional_id)#" cfsqltype="cf_sql_integer">
		</cfquery>
		<cfquery name="local.updateProfessionalLocation" datasource="#request.dsn#">
			UPDATE  ls
			SET 	ls.Location_Phone = <cfqueryparam value="#trim(arguments.profileDataForm.phone)#" cfsqltype="cf_sql_varchar">
			      	,ls.Location_Address = <cfqueryparam value="#trim(arguments.profileDataForm.address)#" cfsqltype="cf_sql_varchar">
			      	,ls.Location_Address2 = <cfqueryparam value="#trim(arguments.profileDataForm.address1)#" cfsqltype="cf_sql_varchar">
			      	,ls.Location_City = <cfqueryparam value="#trim(arguments.profileDataForm.city)#" cfsqltype="cf_sql_varchar">
			      	,ls.Location_State = <cfqueryparam value="#trim(arguments.profileDataForm.state)#" cfsqltype="cf_sql_varchar">
					,ls.Location_Postal = <cfqueryparam value="#trim(arguments.profileDataForm.postal)#" cfsqltype="cf_sql_varchar">
			FROM 	Locations ls
			LEFT JOIN Professionals pls on ls.Location_ID = pls.Location_ID 
			WHERE  	pls.Professional_ID = <cfqueryparam value="#trim(arguments.profileDataForm.professional_id)#" cfsqltype="cf_sql_integer">
		</cfquery>
	</cffunction>

	<!---  Function for get Professionals Details --->
	<cffunction name="getProfessional" access="public" output="false" returntype="query" hint="Returns query of Location based on Location_ID">
		<cfargument name="Professional_ID" type="numeric" required="false" default="0">
		<cfargument name="Company_ID" type="numeric" required="false" default="0">
		<cfquery name="local.getProfessional" datasource="#request.dsn#">
			SELECT 	pls.First_Name
			      	,pls.Last_Name
			      	,ls.Location_Phone as Mobile_Phone
			      	,ls.Location_Address as Home_Address
			      	,ls.Location_Address2 as Home_Address2
			      	,ls.Location_City as Home_City
			      	,ls.Location_State as Home_State
			      	,ls.Location_Postal as Home_Postal
			      	,pls.Email_Address
			FROM 	Professionals pls
			LEFT JOIN Locations ls on ls.Location_ID = pls.Location_ID 
			WHERE  	pls.Professional_ID = <cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer">
		</cfquery>
		<cfreturn local.getProfessional>
	</cffunction>

	<!--- 
		<cffunction name="createAccount" access="public" returntype="struct">
			<cfargument name="account" type="struct">
			<cfset local.errorMsg = {"error": {"code": "dbError","message": "Something wend wrong, Please contact administator!"}}>
			<cfset init( stripeApiKey = variables.ApiKey )>
			<!--- <cfset local.tokenStatus = createAccToken( account = arguments.account )> --->
			<!--- <cfset local.returnVal = local.tokenStatus> --->
			<!--- <cfif not structKeyExists(local.tokenStatus, "error")> --->
			<cfset local.companyData = getCompany( Company_ID = arguments.account.company_id )>
			<cfset local.accountDetails = arguments.account>
			<cfif local.companyData.recordcount>
				<cfset local.accountDetails.legal_entity.address.city = local.companyData.Company_City>
				<cfset local.accountDetails.legal_entity.address.country = "us">
				<cfset local.accountDetails.legal_entity.address.line1 = local.companyData.Company_Address>
				<cfset local.accountDetails.legal_entity.address.line2 = local.companyData.Company_Address2>
				<cfset local.accountDetails.legal_entity.address.postal_code = local.companyData.Company_Postal>
				<cfset local.accountDetails.legal_entity.address.state = local.companyData.Company_State>
				<cfset local.accountDetails.legal_entity.business_name = local.companyData.Company_Name>
				<cfset local.accountDetails.business_url = "https://www.salonworks.com/"&local.companyData.Web_Address>
				<cfset local.accountDetails.statement_descriptor_prefix = local.companyData.Company_Name>
			</cfif>
			<cfset local.dob = arguments.account.legal_entity.dob.split("/")>
			<cfif not arrayLen(local.dob)>
				<cfset local.dob = arguments.account.legal_entity.dob.split("-")>
			</cfif>
			<cfset local.professionalDeta = getProfessional( Professional_ID = arguments.account.professional_id, Company_ID = arguments.account.company_id )>
			<cfif local.professionalDeta.recordcount>
				<cfset local.accountDetails.legal_entity.personal_address.address.city = local.professionalDeta.Home_City>
				<cfset local.accountDetails.legal_entity.personal_address.address.country = "us">
				<cfset local.accountDetails.legal_entity.personal_address.address.line1 = local.professionalDeta.Home_Address>
				<cfset local.accountDetails.legal_entity.personal_address.address.line2 = local.professionalDeta.Home_Address2>
				<cfset local.accountDetails.legal_entity.personal_address.address.postal_code = local.professionalDeta.Home_Postal>
				<cfset local.accountDetails.legal_entity.personal_address.address.state = local.professionalDeta.Home_State>
				<cfif arrayLen(local.dob) eq 3>
					<cfset local.accountDetails.legal_entity.personal_address.dob = structNew()>
					<cfset local.accountDetails.legal_entity.personal_address.dob.day = local.dob[1]>
					<cfset local.accountDetails.legal_entity.personal_address.dob.month = local.dob[2]>
					<cfset local.accountDetails.legal_entity.personal_address.dob.year = local.dob[3]>
				</cfif>
				<cfset local.accountDetails.legal_entity.first_name = local.professionalDeta.First_Name>
				<cfset local.accountDetails.legal_entity.last_name = local.professionalDeta.Last_Name>
				<cfset local.accountDetails.legal_entity.phone = val(replace(replace(replace(replace(arguments.account.legal_entity.phone,"(",""),")","")," ",""),"-",""))>
				<cfset local.accountDetails.legal_entity.ssn_last_4 = val(arguments.account.legal_entity.ssn)>
				<cfset local.accountDetails.legal_entity.tax_id_registrar = arguments.account.tax_id_registrar>
				<cfset local.accountDetails.legal_entity.email = arguments.account.legal_entity.email>
				<cfset local.accountDetails.legal_entity.type = "company">
			</cfif>

			<cfif structKeyExists(arguments.account.legal_entity,"employerNumber") and not len(trim(local.accountDetails.legal_entity.ssn_last_4))>
				<cfset local.accountDetails.legal_entity.personal_id_number = arguments.account.legal_entity.employerNumber>
			</cfif>
			
			<cfset local.accountDetails.tos_acceptance.date = LEFT(GetTickCount(),10)>
			<cfset local.accountDetails.tos_acceptance.ip = CGI.REMOTE_ADDR>
			<cfset local.accountDetails.type = "custom">
			<cfif local.companyData.recordcount and local.professionalDeta.recordcount>
				<cfset local.saveAccount = arguments.account >
				<cfset local.createConnectedAccountStatus = createStripAccount( accountData = local.accountDetails ) >
				<cfset local.returnVal = local.createConnectedAccountStatus>
				<cfif not structKeyExists(local.createConnectedAccountStatus, "error") >
					<cfset local.saveAccount.responds = local.createConnectedAccountStatus >
					<cfset local.saveAccount = saveAccountData( account = local.saveAccount )>
					<cfif not local.saveAccount>
						<cfset local.returnVal = local.errorMsg>
					</cfif>
				</cfif>
			</cfif>

			<cfreturn local.returnVal>
		</cffunction>
	--->

	<!--- create stripe Account --->
	<cffunction name="createAccount" access="public" returntype="struct">
		<cfargument name="account" type="struct">
		<cfset local.errorMsg = {"error": {"code": "dbError","message": "Something wend wrong, Please contact administator!"}}>
		<cfset init( stripeApiKey = variables.ApiKey )>
		<!--- <cfset local.tokenStatus = createAccToken( account = arguments.account )> --->
		<!--- <cfset local.returnVal = local.tokenStatus> --->
		<!--- <cfif not structKeyExists(local.tokenStatus, "error")> --->

		<cfset local.accountDetails = arguments.account>
		<cfset local.accountDetails.legal_entity.address.city = arguments.account.companydataform.company_city>
		<cfset local.accountDetails.legal_entity.address.country = "us">
		<cfset local.accountDetails.legal_entity.address.line1 = arguments.account.companydataform.company_address>
		<cfset local.accountDetails.legal_entity.address.line2 = arguments.account.companydataform.company_address1>
		<cfset local.accountDetails.legal_entity.address.postal_code = arguments.account.companydataform.company_postal_code>
		<cfset local.accountDetails.legal_entity.address.state = arguments.account.companydataform.company_state>
		<cfset local.accountDetails.legal_entity.business_name = arguments.account.companydataform.company_name>
		<cfset local.accountDetails.business_url = "https://www.salonworks.com/"&arguments.account.companydataform.company_web_address>
		<cfset local.accountDetails.statement_descriptor_prefix = arguments.account.companydataform.company_name>
		
		<cfset local.dob = arguments.account.legal_entity.dob.split("/")>
		<cfif not arrayLen(local.dob)>
			<cfset local.dob = arguments.account.legal_entity.dob.split("-")>
		</cfif>

		<cfset local.accountDetails.legal_entity.personal_address.address.city = arguments.account.profiledataform.city>
		<cfset local.accountDetails.legal_entity.personal_address.address.country = "us">
		<cfset local.accountDetails.legal_entity.personal_address.address.line1 = arguments.account.profiledataform.address>
		<cfset local.accountDetails.legal_entity.personal_address.address.line2 = arguments.account.profiledataform.address1>
		<cfset local.accountDetails.legal_entity.personal_address.address.postal_code = arguments.account.profiledataform.postal>
		<cfset local.accountDetails.legal_entity.personal_address.address.state = arguments.account.profiledataform.state>
		<cfif arrayLen(local.dob) eq 3>
			<cfset local.accountDetails.legal_entity.personal_address.dob = structNew()>
			<cfset local.accountDetails.legal_entity.personal_address.dob.day = local.dob[2]>
			<cfset local.accountDetails.legal_entity.personal_address.dob.month = local.dob[1]>
			<cfset local.accountDetails.legal_entity.personal_address.dob.year = local.dob[3]>
		</cfif>
		<cfset local.accountDetails.legal_entity.first_name = arguments.account.profiledataform.first_name>
		<cfset local.accountDetails.legal_entity.last_name = arguments.account.profiledataform.last_name>
		<cfset local.accountDetails.legal_entity.phone = val(replace(replace(replace(replace(arguments.account.profiledataform.phone,"(",""),")","")," ",""),"-",""))>
		<cfset local.accountDetails.legal_entity.ssn_last_4 = val(arguments.account.legal_entity.ssn)>
		<cfset local.accountDetails.legal_entity.tax_id_registrar = arguments.account.tax_id_registrar>
		<cfset local.accountDetails.legal_entity.email = arguments.account.legal_entity.email>
		<cfset local.accountDetails.legal_entity.type = "company">

		<cfif structKeyExists(arguments.account.legal_entity,"employerNumber") and not len(trim(local.accountDetails.legal_entity.ssn_last_4))>
			<cfset local.accountDetails.legal_entity.personal_id_number = arguments.account.legal_entity.employerNumber>
		</cfif>
		<cfset local.accountDetails.tos_acceptance.date = LEFT(GetTickCount(),10)>
		<cfset local.accountDetails.tos_acceptance.ip = CGI.REMOTE_ADDR>
		<cfset local.accountDetails.type = "custom">
		
		<cfset local.saveAccount = arguments.account >
		<cfset local.createConnectedAccountStatus = createStripAccount( accountData = local.accountDetails ) >
		<cfset local.returnVal = local.createConnectedAccountStatus>
		<cfif not structKeyExists(local.createConnectedAccountStatus, "error") >
			<cfset local.saveAccount.responds = local.createConnectedAccountStatus >
			<cfset local.saveAccount = saveAccountData( account = local.saveAccount )>
			<cfif not local.saveAccount>
				<cfset local.returnVal = local.errorMsg>
			</cfif>
		</cfif>

		<cfreturn local.returnVal>
	</cffunction>

	<cffunction name="getAccountDetailsById" returntype="Struct" access="public">
		<cfargument name="accountData" type="struct">
		<cfset init( stripeApiKey = variables.ApiKey )>
		<cfset local.status = getAccountDetails( accountData = arguments.accountData )>
		<cfreturn local.status>
	</cffunction>

	<cffunction name="updateAccount" access="public" returntype="Struct">
		<cfset init( stripeApiKey = variables.ApiKey )>
		<cfset local.accountDetails.accountId = "acct_1E3ao8FWF2xzTIUR">
		<cfset local.accountDetails.tos_acceptance = structNew()>
		<cfset local.accountDetails.tos_acceptance.date = LEFT(GetTickCount(),10)>
		<cfset local.accountDetails.tos_acceptance.ip = CGI.REMOTE_ADDR>
		<cfset local.status  = updateAcc( accountData = local.accountDetails )>
	</cffunction>

	<!--- save Account Data --->
	<cffunction name="saveAccountData" access="public" returntype="Numeric">
		<cfargument name="account" type="struct">
		<cftry>
			<cfquery name="qSaveAccountDetails" datasource="#request.dsn#">
				INSERT INTO  stripeAccount(Professional_ID,Company_ID,stripeAccId,first_name,last_name,ssn,dob,respondsString,isActive)
				VALUES (<cfqueryparam value="#arguments.account.professional_id#" cfsqltype="cf_sql_integer">,
						<cfqueryparam value="#arguments.account.company_id#" cfsqltype="cf_sql_integer">,
						<cfqueryparam value="#arguments.account.responds.id#" cfsqltype="cf_sql_varchar">,
						<cfqueryparam value="#arguments.account.legal_entity.first_name#" cfsqltype="cf_sql_varchar">,
						<cfqueryparam value="#arguments.account.legal_entity.last_name#" cfsqltype="cf_sql_varchar">,
						<cfqueryparam value="#arguments.account.legal_entity.ssn#" cfsqltype="cf_sql_integer">,
						<cfqueryparam value="#arguments.account.legal_entity.dob#" cfsqltype="cf_sql_varchar">,
						<cfqueryparam value="#serializeJSON(arguments.account.responds)#" cfsqltype="cf_sql_varchar">,
						<cfqueryparam value="1" cfsqltype="cf_sql_integer">)
			</cfquery>
			<cfreturn 1 >
			<cfcatch>
				<cfreturn 0 >
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- function for save payment --->
	<cffunction  name="checkoutPayment" access="remote" returntype="String" returnformat="plain">
		<cfargument name="amount" type="string">
		<cfargument name="paymentMethod" type="string">
		<cfargument name="checkGiftCardVal" type="string">
		<cfargument name="currency" type="string">
		<cfargument name="appointmentId" type="numeric">
		<cfargument name="Professional_ID" type="numeric">
		<cfargument name="company_id" type="numeric">
		<cfset local.returnVal = structNew()>
		<cfset local.returnVal.status = false>
		<cfset local.paymentData = structNew()>
		<cfset local.paymentData.id = arguments.paymentMethod>
		<cfset local.paymentData.source = {}>
		<cfset local.paymentData.refunds = {}>
		<cfset local.paymentData.paid = "YES">
		<cfset local.status = savePaymentData( paymentString = serializeJSON(local.paymentData), appointmentId = arguments.appointmentId, Professional_ID = arguments.Professional_ID )>
		<cfif local.status >
			<cfset local.returnVal.status = "succeeded">
		</cfif>
		<cfreturn serializeJSON(local.returnVal)>
	</cffunction>

	<!--- make payment via stripe --->
	<cffunction name="makePayment" access="remote" returntype="String" returnformat="plain">
		<cfargument name="amount" type="string">
		<cfargument name="nameOnCard" type="string">
		<cfargument name="cardNo" type="string">
		<cfargument name="cvv" type="string">
		<cfargument name="expYear" type="string">
		<cfargument name="expMonth" type="string">
		<cfargument name="currency" type="string">
		<cfargument name="appointmentId" type="numeric">
		<cfargument name="Professional_ID" type="numeric">
		<cfargument name="company_id" type="numeric">

		<cfset init( stripeApiKey = variables.ApiKey, currency = arguments.currency )>
		<cfset local.card = structNew()>
		<cfset local.card.number = arguments.cardNo>
		<cfset local.card.name = arguments.nameOnCard>
		<cfset local.card.exp_month = arguments.expMonth>
		<cfset local.card.exp_year = arguments.expYear>
		<cfset local.card.cvc = arguments.cvv>
		<cfset local.amount = arguments.amount>
		<cfset local.payment = 0>
		<cfset local.errorMsg = {"error": {"code": "dbError","message": "Account not added"}}> 
		<cfset local.returnVal = local.errorMsg> 
		<cfset local.accountStatus = checkStripeAccount( professional_id = arguments.Professional_ID, Company_ID = arguments.company_id )>
		<cfif local.accountStatus.recordcount>
			<cfset local.accountData = structNew()>
			<cfset local.accountData.accountId = local.accountStatus.stripeAccId>
			<cfset local.accountDetails = getAccountDetailsById( accountData = local.accountData )>
			<cfif structKeyExists(local.accountDetails, "legal_entity") and structKeyExists(local.accountDetails.legal_entity,"verification") and structKeyExists(local.accountDetails.legal_entity.verification,"status") and local.accountDetails.legal_entity.verification.status neq "verified">
				<cfset local.errorMsg = {"error": {"code": "dbError","message": "Your Account is in "&#local.accountDetails.legal_entity.verification.status#&" status, Please contact administrator!"}}> 
				<cfreturn serializeJSON(local.errorMsg)>
			</cfif>
			<cfset local.errorMsg = {"error": {"code": "dbError","message": "Please add Bank Account"}}> 
			<cfset local.tokenStatus = createToken(card=local.card,amount=local.amount)>
			<cfif Not structKeyExists(local.tokenStatus, "error")>
				<cfset local.destination = "">
				<cfset local.destinationRec = checkStripeAccount( professional_id = arguments.Professional_ID, company_id = arguments.company_id ) >
				<cfif local.destinationRec.recordcount>
					<cfset local.destination = local.destinationRec.stripeAccId>
					<cfset local.companyData = getCompany( Company_ID = arguments.company_id )>
					<cfset local.statement_descriptor = local.companyData.Company_Name>
					<cfset local.ChargeStatus = createCharge( amount=local.amount, customer=local.tokenStatus.id, destination = local.destination, statement_descriptor = local.statement_descriptor)>
					<cfset local.errorMsg = {"error": {"code": "dbError","message": "Something went wrong, Please contact administator!"}}>
					<cfif Not structKeyExists(local.ChargeStatus, "error")>
						<cfset local.payment = savePaymentData( paymentString = serializeJSON(local.ChargeStatus), appointmentId = arguments.appointmentId, Professional_ID = arguments.Professional_ID )>
					</cfif>
					<cfset local.returnVal = local.errorMsg> 
					<cfif local.payment>
						<cfset local.returnVal = local.ChargeStatus>
					</cfif>
				<cfelse>
					<cfset local.returnVal = local.errorMsg>
				</cfif>
			<cfelse>
				<cfset local.returnVal = local.tokenStatus>
			</cfif>
		</cfif>
		<cfreturn serializeJSON(local.returnVal)>
	</cffunction>

	<!--- saving payment details --->
	<cffunction name="savePaymentData" access="public" returntype="Any">
		<cfargument name="paymentString" type="string">
		<cfargument name="appointmentId" type="numeric">
		<cfargument name="Professional_ID" type="numeric">
		<cfset local.payment = deserializeJSON(arguments.paymentString) >
		<cfset local.customerId = getCustomerId( Appointment_ID = arguments.appointmentId ).Customer_ID>
		<cfif not local.customerId>
			<cfreturn 0>
		</cfif>
		<cftry>
			<cfquery name="qsavePayment" datasource="#request.dsn#">
				INSERT INTO stripeTransactions (Appointment_ID,Professional_ID,Customer_ID,stripId,refundString,sourceString,respondsString,paymentStatus,createdDate)
				VALUES	(<cfqueryparam value="#arguments.appointmentId#" cfsqltype="cf_sql_integer">,
						 <cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer">,
						 <cfqueryparam value="#local.customerId#" cfsqltype="cf_sql_integer">,
						 <cfqueryparam value="#local.payment.id#" cfsqltype="cf_sql_varchar">,
						 <cfqueryparam value="#serializeJSON(local.payment.refunds)#" cfsqltype="cf_sql_varchar">,
						 <cfqueryparam value="#serializeJSON(local.payment.source)#" cfsqltype="cf_sql_varchar">,
						 <cfqueryparam value="#serializeJSON(local.payment)#" cfsqltype="cf_sql_varchar">,
						 <cfqueryparam value="#local.payment.paid#" cfsqltype="cf_sql_varchar">,
						 <cfqueryparam value="#now()#" cfsqltype="cf_sql_timestamp">)
			</cfquery>
			<cfreturn 1>
		<cfcatch>
			<cfreturn 0>
		</cfcatch>
		</cftry>
	</cffunction>

	<!--- get CustomerId --->
	<cffunction name="getCustomerId" access="public" returntype="Query">
		<cfargument name="Appointment_ID" type="numeric">
		<cfquery name="qGetId" datasource="#request.dsn#">
			SELECT 	Customer_ID,Service_ID
			FROM 	Appointments
			WHERE 	Appointment_ID = <cfqueryparam value="#arguments.Appointment_ID#" cfsqltype="cf_sql_integer">
		</cfquery>
		<cfreturn qGetId>
	</cffunction>

	<!--- function for get service price --->
	<cffunction name="getServicePrice" access="remote" returntype="String" returnformat="plain">
		<cfargument name="Appointment_ID" type="numeric">
		<cfargument name="Professional_ID" type="numeric">
		<cfset local.serviceId = getCustomerId( Appointment_ID = arguments.Appointment_ID ).Service_ID>
		<cfquery name="qGetPrice" datasource="#request.dsn#">
			SELECT 	Price
			FROM 	Professionals_Services
			WHERE 	Service_ID = <cfqueryparam value="#local.serviceId#" cfsqltype="cf_sql_integer">
			AND 	Professional_ID = <cfqueryparam value="#arguments.Professional_ID#" cfsqltype="cf_sql_integer">
		</cfquery>
		<cfreturn serializeJSON(qGetPrice)>
	</cffunction>

	<!--- function for get bank account details --->
	<cffunction name="getBankAcc" access="public" returntype="Query">
		<cfargument name="professional_id" type="numeric">
		<cfargument name="Company_ID" type="numeric">
		<cfargument name="id" type="numeric" required="false" default="0">
		<cfargument name="routing_number" type="string" required="false" default="0">
		<cfquery name="local.qGentBank" datasource="#request.dsn#">
			SELECT 	*
			FROM 	bankAccount
			WHERE 	professional_id =  <cfqueryparam value="#arguments.professional_id#" cfsqltype="cf_sql_integer">
			AND 	Company_ID = <cfqueryparam value="#arguments.Company_ID#" cfsqltype="cf_sql_integer">
			AND 	isDelete = <cfqueryparam value="0" cfsqltype="cf_sql_bit">
			<cfif structKeyExists(arguments,"id") and arguments.id neq 0>
				AND 	id = <cfqueryparam value="#arguments.id#" cfsqltype="cf_sql_integer">
			</cfif>
			<cfif structKeyExists(arguments,"routing_number") and arguments.routing_number neq 0>
				AND 	routing_number = <cfqueryparam value="#arguments.routing_number#" cfsqltype="cf_sql_varchar">
			</cfif>
		</cfquery>
		<cfreturn local.qGentBank>
	</cffunction>

	<!--- function for update bank table --->
	<cffunction name="updateBankAccTable" access="public" returntype="Any">
		<cfargument name="bankDetails" type="struct">
		<cfquery name="qBankDetailsUpdate" datasource="#request.dsn#">
			UPDATE 	bankAccount
			SET 	acc_Name = <cfqueryparam value="#arguments.bankDetails.account_holder_name#" cfsqltype="cf_sql_varchar">,
					default_acc = <cfqueryparam value="#arguments.bankDetails.default_for_currency#" cfsqltype="cf_sql_bit">
			WHERE 	bankId = <cfqueryparam value="#arguments.bankDetails.bankId#" cfsqltype="cf_sql_varchar">
		</cfquery>
	</cffunction>

	<!--- function for delete bank from table --->
	<cffunction name="deleteStripeDetails" access="public" returntype="Any">
		<cfargument name="bankDetails" type="struct">
		<cfquery name="qBankDetailsUpdate" datasource="#request.dsn#">
			UPDATE 	bankAccount
			SET 	isDelete = <cfqueryparam value="1" cfsqltype="cf_sql_bit">
			WHERE 	bankId = <cfqueryparam value="#arguments.bankDetails.bankId#" cfsqltype="cf_sql_varchar">
		</cfquery>
		<cfquery name="local.qAccountUpdate" datasource="#request.dsn#">
			UPDATE 	stripeAccount
			SET 	isActive = <cfqueryparam value="0" cfsqltype="cf_sql_bit">
			WHERE 	Professional_ID =  <cfqueryparam value="#arguments.bankDetails.professional_id#" cfsqltype="cf_sql_integer">
			AND 	Company_ID = <cfqueryparam value="#arguments.bankDetails.company_id#" cfsqltype="cf_sql_integer">
			AND 	stripeAccId = <cfqueryparam value="#arguments.bankDetails.accountId#" cfsqltype="cf_sql_varchar">
		</cfquery>
	</cffunction>

	<!--- function for get account detail --->
	<cffunction name="getBankAccDetails" access="remote" returnformat="plain" returntype="String">
		<cfargument name="professional_id" type="numeric">
		<cfargument name="Company_ID" type="numeric">
		<cfargument name="id" type="numeric" required="false" default="0">
		<cfreturn serializeJSON(getBankAcc( professional_id = arguments.professional_id, Company_ID = arguments.Company_ID, id = arguments.id ))>
	</cffunction>

	<!--- function for update bank details  --->
	<cffunction name="updateBank" access="public" returntype="Struct">
		<cfargument name="bankDetails" type="struct">
		<cfset init( stripeApiKey = variables.ApiKey )>
		<cfset local.accountId = checkStripeAccount( professional_id = arguments.bankDetails.professional_id,Company_ID = arguments.bankDetails.company_id ).stripeAccId >
		<cfset local.bankDetails = arguments.bankDetails >
		<cfset local.bankDetails.accountId = local.accountId >
		<cfset local.returnVal = updateBankAcc( bankDetails = local.bankDetails ) >
		<cfif not structKeyExists(local.returnVal,"error")>
			<cfset updateBankAccTable( bankDetails = arguments.bankDetails )>
		</cfif>
		<cfreturn local.returnVal >
	</cffunction>

	<!--- function for detele bank  --->
	<cffunction name="deleteStripeAcc" access="public" returntype="Struct">
		<cfargument name="bankDetails" type="struct">
		<cfset init( stripeApiKey = variables.ApiKey )>
		<cfset local.accountId = checkStripeAccount( professional_id = arguments.bankDetails.professional_id,Company_ID = arguments.bankDetails.company_id ).stripeAccId >
		<cfset local.bankDetails = arguments.bankDetails >
		<cfset local.bankDetails.accountId = local.accountId >
		<cfset local.returnVal = deleteStripeAccount( bankDetails = local.bankDetails ) >
		<cfif not structKeyExists(local.returnVal,"error")>
			<cfset deleteStripeDetails( bankDetails = arguments.bankDetails )>
		</cfif>
		<cfreturn local.returnVal >
	</cffunction>

</cfcomponent>