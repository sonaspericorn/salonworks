<cfcomponent displayname="socialSignin" hint="">
	
	<cffunction name="isEmailExisting" access="remote" returntype="any" returnformat="json" output="false">
		<cfargument name="EmailAddress" type="string" required="no" default=""/>
		<cfargument name="firstName" type="string" required="no" />
		<cfargument name="lastname" type="string" required="no" />
		<cfargument name="socialId" type="string" required="no" default="0"/>
		<cfargument name="loginType" type="string" required="no" default="0"/>
		<cfset local.Response = 0>
	    <cfquery name="local.qryResults" datasource="#request.dsn#">
			SELECT TOP 1 1 FROM Professionals
			WHERE  socialId = <cfqueryparam value="#arguments.socialId#" cfsqltype="cf_sql_varchar" />			
	    </cfquery>
		<cfif not local.qryResults.RecordCount>
			<cfif len(trim(arguments.EmailAddress))>
				<cfquery name="local.qryResultsEmail" datasource="#request.dsn#">
					SELECT TOP 1 1 FROM Professionals
					WHERE Email_Address = <cfqueryparam value="#arguments.EmailAddress#" cfsqltype="cf_sql_varchar" />			
	        	</cfquery>
	        	<cfif not local.qryResultsEmail.RecordCount>
	        		<cfset variables.Company_ID = InsertCompany()>
					<cfset variables.Location_ID = InsertLocation( Company_ID = variables.Company_ID)>
					<cfset variables.Professional_ID = InsertProfessional( Location_ID = variables.Location_ID,First_Name = arguments.firstName,Last_Name = arguments.lastname,Email_Address = arguments.EmailAddress,socialId = arguments.socialId,loginType=arguments.loginType)>
					<cfset session.company_id = variables.Company_ID>
					<cfset local.response = 1>
				<cfelse>
					<cfquery name="local.qryResultsEmail" datasource="#request.dsn#">
						UPDATE Professionals
						SET socialId = <cfqueryparam value="#arguments.socialId#" cfsqltype="cf_sql_varchar" />,
						loginType = <cfqueryparam value="#arguments.loginType#" cfsqltype="cf_sql_varchar" />
						WHERE Email_Address = <cfqueryparam value="#arguments.EmailAddress#" cfsqltype="cf_sql_varchar" />
	            	</cfquery>
	            	<cfset local.response = 1>
	        	</cfif>
	    	<cfelse>
	    		<cfset variables.Company_ID = InsertCompany()>
				<cfset variables.Location_ID = InsertLocation( Company_ID = variables.Company_ID)>
				<cfset variables.Professional_ID = InsertProfessional( Location_ID = variables.Location_ID,First_Name = arguments.firstName,Last_Name = arguments.lastname,Email_Address = arguments.EmailAddress,socialId = arguments.socialId,loginType=arguments.loginType )>
				<cfset session.company_id = variables.Company_ID>
				<cfset local.response = 1>
			</cfif>
		<cfelse>
			<cfset local.Response = 1>
		</cfif>
	    <cfreturn local.Response/>
	</cffunction>

	<cffunction name="InsertCompany" access="public" output="false" returntype="numeric">
		<cfset Trial_Expiration = DateFormat(DateAdd("m",1,Now()),'dd-mmm-yyyy')>
		<cfset local.appointment_count=GetTickCount()>
		<cfset local.appointment_code=Right(local.appointment_count, 8)>
		<cftransaction isolation="READ_COMMITTED">
			<cfquery name="InsertCompany" datasource="#request.dsn#" result="rInsertCompany">
				INSERT INTO Companies(
					Trial_Expiration,
					appointment_code
				) VALUES (
					'#Trial_Expiration#',
					'#local.appointment_code#'
				)
			</cfquery>
			<cfquery name="getTrial" datasource="#request.dsn#">
				INSERT INTO Company_Prices
				(Company_ID, Company_Service_Plan_ID, Price)
					VALUES
				(#rInsertCompany.generatedkey#,2,49.99)
			</cfquery>
			<cfset newCompany_ID =  rInsertCompany.generatedkey>
		</cftransaction>
		<cfreturn newCompany_ID>
	</cffunction>	

	<cffunction name="InsertLocation" access="public" output="false" returntype="numeric" hint="">
		<cfargument name="Company_ID" type="string" required="false" default="">
		<cftransaction isolation="READ_COMMITTED">
			<cfquery name="InsertLocation" datasource="#request.dsn#" result="rInsertLocation">
				INSERT INTO Locations
				(Company_ID) VALUES ('#arguments.Company_ID#')
			</cfquery>
			<cfset newLocation_ID =  rInsertLocation.generatedkey>
		</cftransaction>
		<cfreturn newLocation_ID>
	</cffunction> 

	<cffunction name="InsertProfessional" access="public" output="false" returntype="numeric" hint="">
		<cfargument name="Location_ID" type="string" required="false" default="">
		<cfargument name="First_Name" type="string" required="false" default="">
		<cfargument name="Last_Name" type="string" required="false" default="">
		<cfargument name="Email_Address" type="string" required="false" default="">
		<cfargument name="socialId" type="string" required="false" default="">
		<cfargument name="loginType" type="string" required="false" default="">
		<cftransaction isolation="READ_COMMITTED">
			<cfquery name="InsertProfessional" datasource="#request.dsn#" result="rInsertProfessional">
				INSERT INTO Professionals(
					Location_ID,First_Name,Last_Name,Email_Address,socialId,loginType,Active_Flag
				) VALUES (
					'#arguments.Location_ID#'
					,'#arguments.First_Name#'
					,'#arguments.Last_Name#'
					,'#arguments.Email_Address#'
					,'#arguments.socialId#'
					,'#arguments.loginType#'
					, 1
				)
			</cfquery>
			<cfset newProfessional_ID =  rInsertProfessional.generatedkey>
		</cftransaction>
		<cfreturn newProfessional_ID>
	</cffunction> 

	<!--- Login function --->

	<cffunction name="SocialSignInLogin" access="remote" output="false" returntype="boolean" hint="Returns query of Location based on Location_ID">
	    <cfargument name="socialId" type="string" required="true" />
			<cfquery name="qLogin" datasource="#request.dsn#">
				SELECT     
				Professionals.Professional_ID
				, Professionals.First_Name
				, Professionals.Last_Name
				, Professionals.socialId
				, Professionals.loginType
				, Companies.Company_ID AS Company_Admin
				, Locations.Location_ID
				, Companies_1.Company_ID AS Company_ID
				FROM         Locations INNER JOIN
				                      Professionals ON Locations.Location_ID = Professionals.Location_ID INNER JOIN
				                      Companies AS Companies_1 ON Locations.Company_ID = Companies_1.Company_ID LEFT OUTER JOIN
				                      Companies ON Professionals.Professional_ID = Companies.Professional_Admin_ID
				WHERE Professionals.socialId='#arguments.socialId#' 
			</cfquery>
			<cfif qLogin.RecordCount GT 0>
		        <cfset session.Professional_ID  =  qLogin.Professional_ID />
		        <cfset session.Location_ID = qLogin.Location_ID>
		        <cfset session.First_Name = qLogin.First_Name>
		        <cfset session.Last_Name = qLogin.Last_Name>
		        <cfset session.Company_ID = qLogin.Company_ID>
		        <cfset session.signInId = qLogin.socialId>
		        <cfset session.loginType = qLogin.loginType>
		        <cfset session.isUserLoggedIn = true>
	       		<cfreturn true>
	    	</cfif>
		<cfreturn false>
	</cffunction>

	<!--- Customer site --->

	<cffunction name="iscustomerEmailExisting" access="remote" returntype="any" returnformat="json" output="false">
		<cfargument name="EmailAddress" type="string" required="no" />
		<cfargument name="firstName" type="string" required="no" />
		<cfargument name="lastname" type="string" required="no" />
		<cfargument name="socialId" type="string" required="no" />
		<cfargument name="companyId" type="string" required="no" />
		<cfargument name="loginType" type="string" required="no" />
			<cfset local.Response = 0>
			<cfquery name="local.custqryResults" datasource="#request.dsn#">
				SELECT TOP 1 * FROM customers
				WHERE  socialId = <cfqueryparam value="#arguments.socialId#" cfsqltype="cf_sql_varchar" />		
				AND Company_ID = <cfqueryparam value="#arguments.companyId#" cfsqltype="cf_sql_varchar" />		
		    </cfquery>
			<cfif not local.custqryResults.RecordCount>
				<cfif len(trim(arguments.EmailAddress))>
					<cfquery name="local.custqryResultsEmail" datasource="#request.dsn#">
						SELECT TOP 1 * FROM customers
						WHERE Email_Address = <cfqueryparam value="#arguments.EmailAddress#" cfsqltype="cf_sql_varchar" />	
						AND Company_ID = <cfqueryparam value="#arguments.companyId#" cfsqltype="cf_sql_varchar" />			
		        	</cfquery>
		        	<cfif not local.custqryResultsEmail.RecordCount>
		        		<cfset local.customerResult = InsertCustomersFromSocialSites(First_Name =  arguments.firstName,Last_Name = arguments.lastname,socialId = arguments.socialId,Email_Address = arguments.EmailAddress,companyId = arguments.companyId,loginType=arguments.loginType)>
		        		<cfset session.customerId = local.customerResult>
		        		<cfset local.response = 1>
		        	<cfelse>
		        		<cfquery name="local.qryResultsEmail" datasource="#request.dsn#">
							UPDATE customers
							SET socialId = <cfqueryparam value="#arguments.socialId#" cfsqltype="cf_sql_varchar" />,
							loginType = <cfqueryparam value="#arguments.loginType#" cfsqltype="cf_sql_varchar" />
							WHERE Email_Address = <cfqueryparam value="#arguments.EmailAddress#" cfsqltype="cf_sql_varchar" />
		            	</cfquery>
		            	<cfset session.customerId = local.custqryResultsEmail.customer_ID>
						<cfset local.Response = 1>
		        	</cfif>
		        <cfelse>
		        	<cfset local.customerResult = InsertCustomersFromSocialSites(First_Name =  arguments.firstName,Last_Name = arguments.lastname,socialId = arguments.socialId,Email_Address = arguments.EmailAddress,companyId = arguments.companyId,loginType=arguments.loginType)>
					<cfset session.customerId = local.customerResult>
				</cfif>
				<cfset session.name = arguments.firstName & arguments.lastname >
				<cfset session.loginType = arguments.loginType >	
			<cfelse>
				<cfset session.customerId = local.custqryResults.customer_ID>
				<cfset session.name = arguments.firstName & arguments.lastname >
				<cfset session.loginType = arguments.loginType >
				<cfset local.Response = 1>
			</cfif>
	    <cfreturn local.Response/>
	</cffunction>


	<cffunction name="InsertCustomersFromSocialSites" access="public" output="false" returntype="any" hint="">
		<cfargument name="First_Name" type="string" required="false" default="">
		<cfargument name="Last_Name" type="string" required="false" default="">
		<cfargument name="socialId" type="string" required="false" default="">
		<cfargument name="Email_Address" type="string" required="false" default="">
		<cfargument name="companyId" type="string" required="false" default="">
		<cfargument name="loginType" type="string" required="false" default="">
		<cftransaction isolation="READ_COMMITTED">
			<cfquery name="qcustomerInsert" datasource="#request.dsn#" result="rInsertcustomer">
				INSERT INTO Customers ( First_Name, Last_Name, socialId, Email_Address, Company_ID, loginType)
				VALUES(
					<cfqueryparam value="#arguments.First_Name#" cfsqltype="cf_sql_varchar" />,
					<cfqueryparam value="#arguments.Last_Name#" cfsqltype="cf_sql_varchar" />,
					<cfqueryparam value="#arguments.socialId#" cfsqltype="cf_sql_varchar" />,
					<cfqueryparam value="#arguments.Email_Address#" cfsqltype="cf_sql_varchar" />,
					<cfqueryparam value="#arguments.companyId#" cfsqltype="cf_sql_varchar" />,
					<cfqueryparam value="#arguments.loginType#" cfsqltype="cf_sql_varchar" />
				)
	        </cfquery>
			<cfset rInsertcustomer = rInsertcustomer.generatedkey>
		</cftransaction>
		<cfreturn rInsertcustomer>
	</cffunction> 
	
</cfcomponent>


