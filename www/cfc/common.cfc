<cfcomponent displayname="common" extends="Stripe">>
    <!--- Get time by zone --->
	<cffunction name="getTimeByZone" access="public" returntype="string" output="false"> 
		<cfargument name="timezone" type="any" required="true" />
		<cfset pstTimezone = createObject( "java", "java.util.TimeZone" ).getTimeZone(javaCast( "string", arguments.timezone ))/>
		<cfset pstCalendar = createObject( "java", "java.util.GregorianCalendar" ).init(pstTimezone) />
		<cfset pstCalendar.setTimeInMillis(pstCalendar.getTimeInMillis()) />
		<!---<cfset dtNow = DateAdd("s",-GetTimeZoneInfo().UTCTotalOffset,calendarToString( pstCalendar )) />--->
		<cfreturn calendarToString( pstCalendar ) >
	</cffunction>
	
	<!--- calendar To string --->
	<cffunction name="calendarToString" access="public" returntype="string" output="false">  
		<cfargument name="calendar" type="any" required="true" />  
		
		<!--- Define the local scope. --->
		<cfset var local = {} />  
		<!--- Create a simple date formatter that will take our calendar date and output a nice date string. --->
		<cfset local.formatter = createObject( "java", "java.text.SimpleDateFormat" ).init( javaCast( "string", "yyyy-MM-dd  HH:mm:ss" ) ) />  
		<!--- By default, the date formatter uses the date in the default timezone. However, since we are working with given calendar,we want to set the timezone of the formatter to be that of the calendar. --->
		<cfset local.formatter.setTimeZone( arguments.calendar.getTimeZone() ) />  
		<!--- Return the formatted date in the given timezone. --->
		<cfreturn local.formatter.format( arguments.calendar.getTime() ) />
	</cffunction>
</cfcomponent>