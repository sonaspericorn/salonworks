component name="Stripe" output=false accessors=true description="ColdFusion Wrapper for Stripe.com API" {
	
	property string stripeApiKey; // Stripe Secret Key
	property string baseUrl;
	property string currency; // Stripe currently has only 'usd' support
	
	public stripe function init(required string stripeApiKey, string baseUrl='https://api.stripe.com/v1/', string currency='usd') {
		
		setStripeApiKey(arguments.stripeApiKey);
		setBaseUrl(arguments.baseUrl);
		setCurrency(arguments.currency);
		
		return this;
	}
	
	
	/* CHARGES */
	public struct function createCharge(required numeric amount, string currency=getCurrency(), string customer='', any card, string description='', string destination='',string statement_descriptor) {
	
		local.HTTPService = createHTTPService('POST');

		local.HTTPService.setUrl(getBaseUrl() & 'charges');
		local.amount = amountToCents(arguments.amount); // convert amount to cents for Stripe
		local.HTTPService.addParam(type='formfield',name='amount',value=local.amount);
		local.HTTPService.addParam(type='formfield',name='currency',value=arguments.currency);
		local.HTTPService.addParam(type='formfield',name='statement_descriptor',value=arguments.statement_descriptor);
		if (Len(Trim(arguments.customer))) {
			local.HTTPService.addParam(type='formfield',name='source',value=Trim(arguments.customer));
		}
		if (Len(Trim(arguments.destination))) {
			local.HTTPService.addParam(type='formfield',name='destination',value=Trim(arguments.destination));
		}
		if (StructKeyExists(arguments,'card') AND isStruct(arguments.card)) {
			local.HTTPService.addParam(type='formfield',name='card[number]',value=arguments.card.number);
			local.HTTPService.addParam(type='formfield',name='card[exp_month]',value=arguments.card.exp_month);
			local.HTTPService.addParam(type='formfield',name='card[exp_year]',value=arguments.card.exp_year);
			if (StructKeyExists(arguments,'card.cvc')) {
				local.HTTPService.addParam(type='formfield',name='card[cvc]',value=arguments.card.cvc);
			}
			if (StructKeyExists(arguments,'card.name') AND Len(Trim(arguments.card.name))) {
				local.HTTPService.addParam(type='formfield',name='card[name]',value=arguments.card.name);
			}
			if (StructKeyExists(arguments,'card.address_line1') AND Len(Trim(arguments.card.address_line1))) {
				local.HTTPService.addParam(type='formfield',name='card[address_line1]',value=arguments.card.address_line1);
			}
			if (StructKeyExists(arguments,'card.address_line2') AND Len(Trim(arguments.card.address_line2))) {
				local.HTTPService.addParam(type='formfield',name='card[address_line2]',value=arguments.card.address_line2);
			}
			if (StructKeyExists(arguments,'card.address_zip') AND Len(Trim(arguments.card.address_zip))) {
				local.HTTPService.addParam(type='formfield',name='card[address_zip]',value=arguments.card.address_zip);
			}
			if (StructKeyExists(arguments,'card.address_state') AND Len(Trim(arguments.card.address_state))) {
				local.HTTPService.addParam(type='formfield',name='card[address_state]',value=arguments.card.address_state);
			}
			if (StructKeyExists(arguments,'card.address_country') AND Len(Trim(arguments.card.address_country))) {
				local.HTTPService.addParam(type='formfield',name='card[address_country]',value=arguments.card.address_country);
			}
		} else if (StructKeyExists(arguments,'card')) {
			local.HTTPService.addParam(type='formfield',name='card',value=Trim(arguments.card));
		}
		if (Len(Trim(arguments.description))) {
			local.HTTPService.addParam(type='formfield',name='description',value=Trim(arguments.description));
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	} 
	
	// Can get X number of charges (0 to 100, 10 is default), a specific charge or all charges for a specific customer
	public struct function readCharge(string chargeid='', string customerid='', numeric count, numeric offset) {
	
		local.HTTPService = createHTTPService('GET');

		local.url = getBaseUrl() & 'charges';
		if (Len(Trim(arguments.chargeid))) {
			local.url = local.url & '/' & arguments.chargeid;
		} else if (Len(Trim(arguments.customerid))) {
			local.url = local.url & '?customer=' & arguments.customerid;
		}
		if (StructKeyExists(arguments,'count') AND (arguments.count GT 0 AND arguments.count LTE 100)) {
			// Regex to find ?
			if (Find('?',local.url) GT 0) {
				local.url = local.url & '&count=' & arguments.count;
			} else {
				local.url = local.url & '?count=' & arguments.count;
		   	}
		}
		if (StructKeyExists(arguments,'offset') AND (arguments.offset GT 0)) {
			// Regex to find ?
		   if (Find('?',local.url) GT 0) {
		 		local.url = local.url & '&offset=' & arguments.offset;
		   } else {
				local.url = local.url & '?offset=' & arguments.offset;
		   }
		}
		local.HTTPService.setUrl(local.url);
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}  
	
	public struct function refundCharge(required string chargeid, numeric amount) {
	
		local.HTTPService = createHTTPService('POST');

		local.HTTPService.setUrl(getBaseUrl() & 'charges/' & arguments.chargeid & '/refund');
		if (StructKeyExists(arguments,'amount') AND arguments.amount GT 0) {
			local.HTTPService.addParam(type='formfield',name='amount',value=arguments.amount);
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}

	
	/* CUSTOMERS */
	public struct function createCustomer(any card, string coupon='', string email='', string description='', string plan='', timestamp trial_end) {
	
		local.HTTPService = createHTTPService('POST');

		local.HTTPService.setUrl(getBaseUrl() & 'customers');
		if (StructKeyExists(arguments,'card') AND isStruct(arguments.card)) {
			local.HTTPService.addParam(type='formfield',name='card[number]',value=arguments.card.number);
			local.HTTPService.addParam(type='formfield',name='card[exp_month]',value=arguments.card.exp_month);
			local.HTTPService.addParam(type='formfield',name='card[exp_year]',value=arguments.card.exp_year);
			if (StructKeyExists(arguments,'card.cvc')) {
				local.HTTPService.addParam(type='formfield',name='card[cvc]',value=arguments.card.cvc);
			}
			if (StructKeyExists(arguments,'card.name') AND Len(Trim(arguments.card.name))) {
				local.HTTPService.addParam(type='formfield',name='card[name]',value=arguments.card.name);
			}
			if (StructKeyExists(arguments,'card.address_line1') AND Len(Trim(arguments.card.address_line1))) {
				local.HTTPService.addParam(type='formfield',name='card[address_line1]',value=Trim(arguments.card.address_line1));
			}
			if (StructKeyExists(arguments,'card.address_line2') AND Len(Trim(arguments.card.address_line2))) {
				local.HTTPService.addParam(type='formfield',name='card[address_line2]',value=Trim(arguments.card.address_line2));
			}
			if (StructKeyExists(arguments,'card.address_zip') AND Len(Trim(arguments.card.address_zip))) {
				local.HTTPService.addParam(type='formfield',name='card[address_zip]',value=Trim(arguments.card.address_zip));
			}
			if (StructKeyExists(arguments,'card.address_state') AND Len(Trim(arguments.card.address_state))) {
				local.HTTPService.addParam(type='formfield',name='card[address_state]',value=Trim(arguments.card.address_state));
			}
			if (StructKeyExists(arguments,'card.address_country') AND Len(Trim(arguments.card.address_country))) {
				local.HTTPService.addParam(type='formfield',name='card[address_country]',value=Trim(arguments.card.address_country));
			}
		} else if (StructKeyExists(arguments,'card')) {
			local.HTTPService.addParam(type='formfield',name='card',value=Trim(arguments.card));
		}
		if (Len(Trim(arguments.coupon))) {
			local.HTTPService.addParam(type='formfield',name='coupon',value=Trim(arguments.coupon));
		}
		if (Len(Trim(arguments.email))) {
			local.HTTPService.addParam(type='formfield',name='email',value=Trim(arguments.email));
		}
		if (Len(Trim(arguments.description))) {
			local.HTTPService.addParam(type='formfield',name='description',value=Trim(arguments.description));
		}
		if (Len(Trim(arguments.plan))) {
			local.HTTPService.addParam(type='formfield',name='plan',value=Trim(arguments.plan));
		} 
		if (StructKeyExists(arguments,'trial_end') AND IsDate(arguments.trial_end)) {
			local.intUTCDate = timeToUTCInt(arguments.trial_end);
			local.HTTPService.addParam(type='formfield',name='trial_end',value=local.intUTCDate);
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	public struct function readCustomer(string customerid='', numeric count, numeric offset) {
	
		local.HTTPService = createHTTPService('GET');

		local.url = getBaseUrl() & 'customers';
		if (Len(Trim(arguments.customerid))) {
			local.HTTPService.setUrl(local.url & '/' & arguments.customerid);
		} else if (StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?count=' & arguments.count & '&offset=' & arguments.offset);
		} else if (StructKeyExists(arguments,'count') AND NOT StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?count=' & arguments.count);
		} else if (NOT StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?offset=' & arguments.offset);
		} else {
			local.HTTPService.setUrl(local.url);
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	public struct function updateCustomer(required string customerid, any card, string coupon='',string description='',string email='') {
	
		local.HTTPService = createHTTPService('POST');

		local.HTTPService.setUrl(getBaseUrl() & 'customers/' & arguments.customerid);
		if (StructKeyExists(arguments,'card') AND isStruct(arguments.card)) {
			local.HTTPService.addParam(type='formfield',name='card[number]',value=arguments.card.number);
			local.HTTPService.addParam(type='formfield',name='card[exp_month]',value=arguments.card.exp_month);
			local.HTTPService.addParam(type='formfield',name='card[exp_year]',value=arguments.card.exp_year);
			if (StructKeyExists(arguments,'card.cvc')) {
				local.HTTPService.addParam(type='formfield',name='card[cvc]',value=arguments.card.cvc);
			}
			if (StructKeyExists(arguments,'card.name') AND Len(Trim(arguments.card.name))) {
				local.HTTPService.addParam(type='formfield',name='card[name]',value=arguments.card.name);
			}
			if (StructKeyExists(arguments,'card.address_line1') AND Len(Trim(arguments.card.address_line1))) {
				local.HTTPService.addParam(type='formfield',name='card[address_line1]',value=Trim(arguments.card.address_line1));
			}
			if (StructKeyExists(arguments,'card.address_line2') AND Len(Trim(arguments.card.address_line2))) {
				local.HTTPService.addParam(type='formfield',name='card[address_line2]',value=Trim(arguments.card.address_line2));
			}
			if (StructKeyExists(arguments,'card.address_zip') AND Len(Trim(arguments.card.address_zip))) {
				local.HTTPService.addParam(type='formfield',name='card[address_zip]',value=Trim(arguments.card.address_zip));
			}
			if (StructKeyExists(arguments,'card.address_state') AND Len(Trim(arguments.card.address_state))) {
				local.HTTPService.addParam(type='formfield',name='card[address_state]',value=Trim(arguments.card.address_state));
			}
			if (StructKeyExists(arguments,'card.address_country') AND Len(Trim(arguments.card.address_country))) {
				local.HTTPService.addParam(type='formfield',name='card[address_country]',value=Trim(arguments.card.address_country));
			}
		} else if (StructKeyExists(arguments,'card')) {
			local.HTTPService.addParam(type='formfield',name='card',value=Trim(arguments.card));
		}
		if (Len(Trim(arguments.coupon))) {
			local.HTTPService.addParam(type='formfield',name='coupon',value=Trim(arguments.coupon));
		}
		if (Len(Trim(arguments.description))) {
			local.HTTPService.addParam(type='formfield',name='description',value=Trim(arguments.description));
		}
		if (Len(Trim(arguments.email))) {
			local.HTTPService.addParam(type='formfield',name='email',value=Trim(arguments.email));
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	public struct function deleteCustomer(required string customerid) {
	
		local.HTTPService = createHTTPService('DELETE');

		local.HTTPService.setUrl(getBaseUrl() & 'customers/' & arguments.customerid);
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}

	/* ACCOUNT TOKEN */
	public struct function createAccToken(required struct account) {
		local.HTTPService = createHTTPService('POST');
		local.HTTPService.setUrl(getBaseUrl() & 'tokens');
		if (StructKeyExists(arguments.account,'legal_entity.email')) {
			local.HTTPService.addParam(type='formfield',name='account[legal_entity][email]',value=arguments.account.legal_entity.email);
		}
		if (StructKeyExists(arguments.account,'legal_entity.first_name')) {
			local.HTTPService.addParam(type='formfield',name='account[legal_entity][first_name]',value=arguments.account.legal_entity.first_name);
		}
		if (StructKeyExists(arguments.account,'legal_entity.last_name')) {
			local.HTTPService.addParam(type='formfield',name='account[legal_entity][last_name]',value=arguments.account.legal_entity.last_name);
		}
		if (StructKeyExists(arguments.account,'legal_entity.maiden_name')) {
			local.HTTPService.addParam(type='formfield',name='account[legal_entity][maiden_name]',value=arguments.account.legal_entity.maiden_name);
		}
		if (StructKeyExists(arguments.account,'legal_entity.phone')) {
			local.HTTPService.addParam(type='formfield',name='account[legal_entity][phone]',value=arguments.account.legal_entity.phone);
		}
		if (StructKeyExists(arguments.account,'tos_shown_and_accepted')) {
			local.HTTPService.addParam(type='formfield',name='account[tos_shown_and_accepted]',value=arguments.account.tos_shown_and_accepted);
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		return deserializeJSON(local.HTTPResult.filecontent);
	}

	/* BANK TOKEN */
	public struct function createBankToken(required struct bankDetails) {
		local.HTTPService = createHTTPService('POST');
		local.HTTPService.setUrl(getBaseUrl() & 'tokens');
		if (StructKeyExists(arguments.bankDetails,'country')) {
			local.HTTPService.addParam(type='formfield',name='bank_account[country]',value=arguments.bankDetails.country);
		}
		if (StructKeyExists(arguments.bankDetails,'currency')) {
			local.HTTPService.addParam(type='formfield',name='bank_account[currency]',value=arguments.bankDetails.currency);
		}
		if (StructKeyExists(arguments.bankDetails,'account_holder_name')) {
			local.HTTPService.addParam(type='formfield',name='bank_account[account_holder_name]',value=arguments.bankDetails.account_holder_name);
		}
		if (StructKeyExists(arguments.bankDetails,'account_holder_type')) {
			local.HTTPService.addParam(type='formfield',name='bank_account[account_holder_type]',value=arguments.bankDetails.account_holder_type);
		}
		if (StructKeyExists(arguments.bankDetails,'routing_number')) {
			local.HTTPService.addParam(type='formfield',name='bank_account[routing_number]',value=arguments.bankDetails.routing_number);
		}
		if (StructKeyExists(arguments.bankDetails,'account_number')) {
			local.HTTPService.addParam(type='formfield',name='bank_account[account_number]',value=arguments.bankDetails.account_number);
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		return deserializeJSON(local.HTTPResult.filecontent);
	}

	/* ACCOUNT CREATE */
	public struct function createStripAccount( required struct accountData) {
		local.HTTPService = createHTTPService('POST');
		local.HTTPService.setUrl(getBaseUrl() & 'accounts');

		local.HTTPService.addParam(type='formfield',name='type',value=arguments.accountData.type);
		local.HTTPService.addParam(type='formfield',name='country',value="us");
		if( StructKeyExists(arguments.accountData,"legal_entity") ){
			if (StructKeyExists(arguments.accountData.legal_entity,'business_name') and len(arguments.accountData.legal_entity.business_name)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[business_name]',value=arguments.accountData.legal_entity.business_name);
			}

			if( StructKeyExists(arguments.accountData.legal_entity,"personal_address") ){
				if( StructKeyExists(arguments.accountData.legal_entity.personal_address,"address") ){
					if (StructKeyExists(arguments.accountData.legal_entity,'address') and StructKeyExists(arguments.accountData.legal_entity.personal_address.address,'city') and len(arguments.accountData.legal_entity.personal_address.address.city)) {
						local.HTTPService.addParam(type='formfield',name='legal_entity[personal_address][city]',value=arguments.accountData.legal_entity.personal_address.address.city);
					}
					if (StructKeyExists(arguments.accountData.legal_entity,'address') and StructKeyExists(arguments.accountData.legal_entity.personal_address.address,'country') and len(arguments.accountData.legal_entity.personal_address.address.country)) {
						local.HTTPService.addParam(type='formfield',name='legal_entity[personal_address][country]',value=arguments.accountData.legal_entity.personal_address.address.country);
					}
					if (StructKeyExists(arguments.accountData.legal_entity,'address') and StructKeyExists(arguments.accountData.legal_entity.personal_address.address,'line1') and len(arguments.accountData.legal_entity.personal_address.address.line1)) {
						local.HTTPService.addParam(type='formfield',name='legal_entity[personal_address][line1]',value=arguments.accountData.legal_entity.personal_address.address.line1);
					}
					if (StructKeyExists(arguments.accountData.legal_entity,'address') and StructKeyExists(arguments.accountData.legal_entity.personal_address.address,'line2') and len(arguments.accountData.legal_entity.personal_address.address.line2)) {
						local.HTTPService.addParam(type='formfield',name='legal_entity[personal_address][line2]',value=arguments.accountData.legal_entity.personal_address.address.line2);
					}
					if (StructKeyExists(arguments.accountData.legal_entity,'address') and StructKeyExists(arguments.accountData.legal_entity.personal_address.address,'postal_code') and len(arguments.accountData.legal_entity.personal_address.address.postal_code)) {
						local.HTTPService.addParam(type='formfield',name='legal_entity[personal_address][postal_code]',value=val(arguments.accountData.legal_entity.personal_address.address.postal_code));
					}
					if (StructKeyExists(arguments.accountData.legal_entity,'address') and StructKeyExists(arguments.accountData.legal_entity.personal_address.address,'state') and len(arguments.accountData.legal_entity.personal_address.address.state)) {
						local.HTTPService.addParam(type='formfield',name='legal_entity[personal_address][state]',value=arguments.accountData.legal_entity.personal_address.address.state);
					}
				}

				if( StructKeyExists(arguments.accountData.legal_entity.personal_address,"dob") ){
					if( StructKeyExists(arguments.accountData.legal_entity.personal_address.dob,"day") and len(arguments.accountData.legal_entity.personal_address.dob.day) ){
						local.HTTPService.addParam(type='formfield',name='legal_entity[dob][day]',value=val(arguments.accountData.legal_entity.personal_address.dob.day));
					}
					if( StructKeyExists(arguments.accountData.legal_entity.personal_address.dob,"month") and len(arguments.accountData.legal_entity.personal_address.dob.month) ){
						local.HTTPService.addParam(type='formfield',name='legal_entity[dob][month]',value=val(arguments.accountData.legal_entity.personal_address.dob.month));
					}
					if( StructKeyExists(arguments.accountData.legal_entity.personal_address.dob,"year") and len(arguments.accountData.legal_entity.personal_address.dob.year) ){
						local.HTTPService.addParam(type='formfield',name='legal_entity[dob][year]',value=val(arguments.accountData.legal_entity.personal_address.dob.year));
					}
				}
			}

			if (StructKeyExists(arguments.accountData.legal_entity,'first_name') and len(arguments.accountData.legal_entity.first_name)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[first_name]',value=arguments.accountData.legal_entity.first_name);
			}
			if (StructKeyExists(arguments.accountData.legal_entity,'last_name') and len(arguments.accountData.legal_entity.last_name)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[last_name]',value=arguments.accountData.legal_entity.last_name);
			}
			if (StructKeyExists(arguments.accountData.legal_entity,'personal_id_number') and len(arguments.accountData.legal_entity.personal_id_number)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[personal_id_number]',value=arguments.accountData.legal_entity.personal_id_number);
			}
			if (StructKeyExists(arguments.accountData.legal_entity,'phone') and len(arguments.accountData.legal_entity.phone)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[phone_number]',value=val(arguments.accountData.legal_entity.phone));
			}
			if (StructKeyExists(arguments.accountData.legal_entity,'ssn_last_4') and len(arguments.accountData.legal_entity.ssn_last_4)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[ssn_last_4]',value=val(arguments.accountData.legal_entity.ssn_last_4));
			}
			if (StructKeyExists(arguments.accountData.legal_entity,'tax_id_registrar') and len(arguments.accountData.legal_entity.tax_id_registrar)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[business_tax_id]',value=arguments.accountData.legal_entity.tax_id_registrar);
			}

			if( StructKeyExists(arguments.accountData.legal_entity,"address") ){
				if (StructKeyExists(arguments.accountData.legal_entity.address,'city') and len(arguments.accountData.legal_entity.address.city)) {
					local.HTTPService.addParam(type='formfield',name='legal_entity[address][city]',value=arguments.accountData.legal_entity.address.city);
				}
				local.HTTPService.addParam(type='formfield',name='legal_entity[address][country]',value="us");
				if (StructKeyExists(arguments.accountData.legal_entity.address,'line1') and len(arguments.accountData.legal_entity.address.line1)) {
					local.HTTPService.addParam(type='formfield',name='legal_entity[address][line1]',value=arguments.accountData.legal_entity.address.line1);
				}
				if (StructKeyExists(arguments.accountData.legal_entity.address,'line2') and len(arguments.accountData.legal_entity.address.line2)) {
					local.HTTPService.addParam(type='formfield',name='legal_entity[address][line2]',value=arguments.accountData.legal_entity.address.line2);
				}
				if (StructKeyExists(arguments.accountData.legal_entity.address,'postal_code') and len(arguments.accountData.legal_entity.address.postal_code)) {
					local.HTTPService.addParam(type='formfield',name='legal_entity[address][postal_code]',value=val(arguments.accountData.legal_entity.address.postal_code));
				}
				if (StructKeyExists(arguments.accountData.legal_entity.address,'state') and len(arguments.accountData.legal_entity.address.state)) {
					local.HTTPService.addParam(type='formfield',name='legal_entity[address][state]',value=arguments.accountData.legal_entity.address.state);
				}
			}
			if (StructKeyExists(arguments.accountData.legal_entity,'type') and len(arguments.accountData.legal_entity.type)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[type]',value=arguments.accountData.legal_entity.type);
			}
		}
		if (StructKeyExists(arguments.accountData,'business_url') and len(arguments.accountData.business_url)) {
			local.HTTPService.addParam(type='formfield',name='business_url',value=arguments.accountData.business_url);
		}
		// if (StructKeyExists(arguments.accountData,'statement_descriptor_prefix') and len(arguments.accountData.statement_descriptor_prefix)) {
		// 	local.HTTPService.addParam(type='formfield',name='settings[card_payments][statement_descriptor_prefix]',value=arguments.accountData.statement_descriptor_prefix);
		// }
		
		if( StructKeyExists(arguments.accountData,'tos_acceptance') ){
			if (StructKeyExists(arguments.accountData.tos_acceptance,'date') and len(arguments.accountData.tos_acceptance.date)) {
				local.HTTPService.addParam(type='formfield',name='tos_acceptance[date]',value=arguments.accountData.tos_acceptance.date);
			}
			if (StructKeyExists(arguments.accountData.tos_acceptance,'ip') and len(arguments.accountData.tos_acceptance.ip)) {
				local.HTTPService.addParam(type='formfield',name='tos_acceptance[ip]',value=arguments.accountData.tos_acceptance.ip);
			}
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		return deserializeJSON(local.HTTPResult.filecontent);
	}

	/* UPDATE ACCOUNT */
	public struct function updateAcc( required struct accountData ){
		local.HTTPService = createHTTPService('POST');
		local.HTTPService.setUrl(getBaseUrl() & 'accounts/'& arguments.accountData.accountId);

		if( StructKeyExists(arguments.accountData,"legal_entity") ){
			if (StructKeyExists(arguments.accountData.legal_entity,'business_name') and len(arguments.accountData.legal_entity.business_name)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[business_name]',value=arguments.accountData.legal_entity.business_name);
			}

			if( StructKeyExists(arguments.accountData.legal_entity,"personal_address") ){
				if( StructKeyExists(arguments.accountData.legal_entity.personal_address,"address") ){
					if (StructKeyExists(arguments.accountData.legal_entity,'address') and StructKeyExists(arguments.accountData.legal_entity.personal_address.address,'city') and len(arguments.accountData.legal_entity.personal_address.address.city)) {
						local.HTTPService.addParam(type='formfield',name='legal_entity[personal_address][city]',value=arguments.accountData.legal_entity.personal_address.address.city);
					}
					if (StructKeyExists(arguments.accountData.legal_entity,'address') and StructKeyExists(arguments.accountData.legal_entity.personal_address.address,'country') and len(arguments.accountData.legal_entity.personal_address.address.country)) {
						local.HTTPService.addParam(type='formfield',name='legal_entity[personal_address][country]',value=arguments.accountData.legal_entity.personal_address.address.country);
					}
					if (StructKeyExists(arguments.accountData.legal_entity,'address') and StructKeyExists(arguments.accountData.legal_entity.personal_address.address,'line1') and len(arguments.accountData.legal_entity.personal_address.address.line1)) {
						local.HTTPService.addParam(type='formfield',name='legal_entity[personal_address][line1]',value=arguments.accountData.legal_entity.personal_address.address.line1);
					}
					if (StructKeyExists(arguments.accountData.legal_entity,'address') and StructKeyExists(arguments.accountData.legal_entity.personal_address.address,'line2') and len(arguments.accountData.legal_entity.personal_address.address.line2)) {
						local.HTTPService.addParam(type='formfield',name='legal_entity[personal_address][line2]',value=arguments.accountData.legal_entity.personal_address.address.line2);
					}
					if (StructKeyExists(arguments.accountData.legal_entity,'address') and StructKeyExists(arguments.accountData.legal_entity.personal_address.address,'postal_code') and len(arguments.accountData.legal_entity.personal_address.address.postal_code)) {
						local.HTTPService.addParam(type='formfield',name='legal_entity[personal_address][postal_code]',value=val(arguments.accountData.legal_entity.personal_address.address.postal_code));
					}
					if (StructKeyExists(arguments.accountData.legal_entity,'address') and StructKeyExists(arguments.accountData.legal_entity.personal_address.address,'state') and len(arguments.accountData.legal_entity.personal_address.address.state)) {
						local.HTTPService.addParam(type='formfield',name='legal_entity[personal_address][state]',value=arguments.accountData.legal_entity.personal_address.address.state);
					}
				}

				if( StructKeyExists(arguments.accountData.legal_entity.personal_address,"dob") ){
					if( StructKeyExists(arguments.accountData.legal_entity.personal_address.dob,"day") and len(arguments.accountData.legal_entity.personal_address.dob.day) ){
						local.HTTPService.addParam(type='formfield',name='legal_entity[dob][day]',value=val(arguments.accountData.legal_entity.personal_address.dob.day));
					}
					if( StructKeyExists(arguments.accountData.legal_entity.personal_address.dob,"month") and len(arguments.accountData.legal_entity.personal_address.dob.month) ){
						local.HTTPService.addParam(type='formfield',name='legal_entity[dob][month]',value=val(arguments.accountData.legal_entity.personal_address.dob.month));
					}
					if( StructKeyExists(arguments.accountData.legal_entity.personal_address.dob,"year") and len(arguments.accountData.legal_entity.personal_address.dob.year) ){
						local.HTTPService.addParam(type='formfield',name='legal_entity[dob][year]',value=val(arguments.accountData.legal_entity.personal_address.dob.year));
					}
				}
			}

			if (StructKeyExists(arguments.accountData.legal_entity,'first_name') and len(arguments.accountData.legal_entity.first_name)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[first_name]',value=arguments.accountData.legal_entity.first_name);
			}
			if (StructKeyExists(arguments.accountData.legal_entity,'last_name') and len(arguments.accountData.legal_entity.last_name)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[last_name]',value=arguments.accountData.legal_entity.last_name);
			}
			if (StructKeyExists(arguments.accountData.legal_entity,'personal_id_number') and len(arguments.accountData.legal_entity.personal_id_number)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[personal_id_number]',value=arguments.accountData.legal_entity.personal_id_number);
			}
			if (StructKeyExists(arguments.accountData.legal_entity,'phone') and len(arguments.accountData.legal_entity.phone)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[phone_number]',value=val(arguments.accountData.legal_entity.phone));
			}
			if (StructKeyExists(arguments.accountData.legal_entity,'ssn_last_4') and len(arguments.accountData.legal_entity.ssn_last_4)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[ssn_last_4]',value=val(arguments.accountData.legal_entity.ssn_last_4));
			}
			if (StructKeyExists(arguments.accountData.legal_entity,'tax_id_registrar') and len(arguments.accountData.legal_entity.tax_id_registrar)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[business_tax_id]',value=arguments.accountData.legal_entity.tax_id_registrar);
			}

			if( StructKeyExists(arguments.accountData.legal_entity,"address") ){
				if (StructKeyExists(arguments.accountData.legal_entity.address,'city') and len(arguments.accountData.legal_entity.address.city)) {
					local.HTTPService.addParam(type='formfield',name='legal_entity[address][city]',value=arguments.accountData.legal_entity.address.city);
				}
				local.HTTPService.addParam(type='formfield',name='legal_entity[address][country]',value="us");
				if (StructKeyExists(arguments.accountData.legal_entity.address,'line1') and len(arguments.accountData.legal_entity.address.line1)) {
					local.HTTPService.addParam(type='formfield',name='legal_entity[address][line1]',value=arguments.accountData.legal_entity.address.line1);
				}
				if (StructKeyExists(arguments.accountData.legal_entity.address,'line2') and len(arguments.accountData.legal_entity.address.line2)) {
					local.HTTPService.addParam(type='formfield',name='legal_entity[address][line2]',value=arguments.accountData.legal_entity.address.line2);
				}
				if (StructKeyExists(arguments.accountData.legal_entity.address,'postal_code') and len(arguments.accountData.legal_entity.address.postal_code)) {
					local.HTTPService.addParam(type='formfield',name='legal_entity[address][postal_code]',value=val(arguments.accountData.legal_entity.address.postal_code));
				}
				if (StructKeyExists(arguments.accountData.legal_entity.address,'state') and len(arguments.accountData.legal_entity.address.state)) {
					local.HTTPService.addParam(type='formfield',name='legal_entity[address][state]',value=arguments.accountData.legal_entity.address.state);
				}
			}
			if (StructKeyExists(arguments.accountData.legal_entity,'type') and len(arguments.accountData.legal_entity.type)) {
				local.HTTPService.addParam(type='formfield',name='legal_entity[type]',value=arguments.accountData.legal_entity.type);
			}
		}
		if (StructKeyExists(arguments.accountData,'business_url') and len(arguments.accountData.business_url)) {
			local.HTTPService.addParam(type='formfield',name='business_url',value=arguments.accountData.business_url);
		}
		
		if( StructKeyExists(arguments.accountData,'tos_acceptance') ){
			if (StructKeyExists(arguments.accountData.tos_acceptance,'date') and len(arguments.accountData.tos_acceptance.date)) {
				local.HTTPService.addParam(type='formfield',name='tos_acceptance[date]',value=arguments.accountData.tos_acceptance.date);
			}
			if (StructKeyExists(arguments.accountData.tos_acceptance,'ip') and len(arguments.accountData.tos_acceptance.ip)) {
				local.HTTPService.addParam(type='formfield',name='tos_acceptance[ip]',value=arguments.accountData.tos_acceptance.ip);
			}
		}

		local.HTTPResult = local.HTTPService.send().getPrefix();
		return deserializeJSON(local.HTTPResult.filecontent);
	}

	/* GET ACCOUNT */ 
	public struct function getAccountDetails(required struct accountData) {
		local.HTTPService = createHTTPService('GET');
		local.HTTPService.setUrl(getBaseUrl() & 'accounts/'&arguments.accountData.accountId);		
		local.HTTPResult = local.HTTPService.send().getPrefix();
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	

	/* BANK ACCOUNT CREATE */
	public struct function createBankAccount( required string accountId, required string bankToken, string default_for_currency) {
		local.HTTPService = createHTTPService('POST');
		local.HTTPService.setUrl(getBaseUrl() & 'accounts/'& arguments.accountId &'/external_accounts');

		local.HTTPService.addParam(type='formfield',name='external_account',value=arguments.bankToken);
		// if (StructKeyExists(arguments,'default_for_currency') and len(arguments.default_for_currency)) {
			// local.HTTPService.addParam(type='formfield',name='default_for_currency',value=true;
		// }

		local.HTTPResult = local.HTTPService.send().getPrefix();
		// writeDump(local.HTTPResult);abort;
		return deserializeJSON(local.HTTPResult.filecontent);
	}

	/* BANK ACCOUNT UPDATE */
	public struct function updateBankAcc( required struct bankDetails ) {
		local.HTTPService = createHTTPService('POST');
		local.HTTPService.setUrl(getBaseUrl() & 'accounts/'&arguments.bankDetails.accountId&'/external_accounts/'&arguments.bankDetails.bankId);

		local.HTTPService.addParam(type='formfield',name='account_holder_name',value=arguments.bankDetails.account_holder_name);
		if( arguments.bankDetails.default_for_currency ){
			local.HTTPService.addParam(type='formfield',name='default_for_currency',value=arguments.bankDetails.default_for_currency);
		}

		local.HTTPResult = local.HTTPService.send().getPrefix();
		return deserializeJSON(local.HTTPResult.filecontent);
	}

	/* BANK ACCOUNT LIST */
	public struct function getBankAcc( required struct bankDetails ) {
		local.HTTPService = createHTTPService('GET');
		local.HTTPService.setUrl(getBaseUrl() & 'accounts/'&arguments.bankDetails.accountId&'/external_accounts');

		local.HTTPResult = local.HTTPService.send().getPrefix();
		return deserializeJSON(local.HTTPResult.filecontent);
	}

	/* DELETE BANK ACCOUNT */
	public struct function deleteStripeAccount( required struct bankDetails ) {
		local.HTTPService = createHTTPService('DELETE');
		local.HTTPService.setUrl(getBaseUrl() & 'accounts/'&arguments.bankDetails.accountId);

		local.HTTPResult = local.HTTPService.send().getPrefix();
		return deserializeJSON(local.HTTPResult.filecontent);
	}

   
	/* CARD TOKENS */	
	public struct function createToken(required struct card, numeric amount, string currency=getCurrency()) {
		
		local.HTTPService = createHTTPService('POST');

		local.HTTPService.setUrl(getBaseUrl() & 'tokens');
		
		if (StructKeyExists(arguments,'card') AND isStruct(arguments.card)) {
			local.HTTPService.addParam(type='formfield',name='card[number]',value=arguments.card.number);
			local.HTTPService.addParam(type='formfield',name='card[exp_month]',value=arguments.card.exp_month);
			local.HTTPService.addParam(type='formfield',name='card[exp_year]',value=arguments.card.exp_year);
			if (StructKeyExists(arguments,'card.cvc')) {
				local.HTTPService.addParam(type='formfield',name='card[cvc]',value=arguments.card.cvc);
			}
			if (StructKeyExists(arguments,'card.name') AND Len(Trim(arguments.card.name))) {
				local.HTTPService.addParam(type='formfield',name='card[name]',value=arguments.card.name);
			}
			if (StructKeyExists(arguments,'card.address_line1') AND Len(Trim(arguments.card.address_line1))) {
				local.HTTPService.addParam(type='formfield',name='card[address_line1]',value=Trim(arguments.card.address_line1));
			}
			if (StructKeyExists(arguments,'card.address_line2') AND Len(Trim(arguments.card.address_line2))) {
				local.HTTPService.addParam(type='formfield',name='card[address_line2]',value=Trim(arguments.card.address_line2));
			}
			if (StructKeyExists(arguments,'card.address_zip') AND Len(Trim(arguments.card.address_zip))) {
				local.HTTPService.addParam(type='formfield',name='card[address_zip]',value=Trim(arguments.card.address_zip));
			}
			if (StructKeyExists(arguments,'card.address_state') AND Len(Trim(arguments.card.address_state))) {
				local.HTTPService.addParam(type='formfield',name='card[address_state]',value=Trim(arguments.card.address_state));
			}
			if (StructKeyExists(arguments,'card.address_country') AND Len(Trim(arguments.card.address_country))) {
				local.HTTPService.addParam(type='formfield',name='card[address_country]',value=Trim(arguments.card.address_country));
			}
		}
		if (StructKeyExists(arguments,'amount')) {
			local.amount = amountToCents(arguments.amount); // convert amount to cents for Stripe
			local.HTTPService.addParam(type='formfield',name='amount',value=local.amount);
		}
		local.HTTPService.addParam(type='formfield',name='currency',value=arguments.currency);
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		// if (NOT isDefined("local.HTTPResult.statusCode")) {
		// 	throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		// } else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
		// 	throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		// }
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	public struct function readToken(required string tokenid) {
		
		local.HTTPService = createHTTPService('GET');

		local.HTTPService.setUrl(getBaseUrl() & 'tokens/' & arguments.tokenid);
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	
	/* PLANS */
	public struct function createPlan(required numeric amount, required string interval, required string name, string planid=CreateUUID(), string currency=getCurrency(), numeric trial_period_days) {
		
		local.HTTPService = createHTTPService('POST');

		local.HTTPService.setUrl(getBaseUrl() & 'plans');
		local.HTTPService.addParam(type='formfield',name='id',value=Trim(arguments.planid));
		local.amount = amountToCents(arguments.amount);
		local.HTTPService.addParam(type='formfield',name='amount',value=local.amount);
		local.HTTPService.addParam(type='formfield',name='currency',value=Trim(arguments.currency));
		local.HTTPService.addParam(type='formfield',name='interval',value=Trim(arguments.interval));
		local.HTTPService.addParam(type='formfield',name='name',value=Trim(arguments.name));
		if (StructKeyExists(arguments,'trial_period_days')) {
			local.HTTPService.addParam(type='formfield',name='trial_period_days',value=arguments.trial_period_days);
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	public struct function readPlan(string planid='', numeric count, numeric offset) {
		
		local.HTTPService = createHTTPService('GET');

		local.url = getBaseUrl() & 'plans';
		if (Len(Trim(arguments.planid))) {
			local.HTTPService.setUrl(local.url & '/' & arguments.planid);
		} else if (StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?count=' & arguments.count & '&offset=' & arguments.offset);
		} else if (StructKeyExists(arguments,'count') AND NOT StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?count=' & arguments.count);
		} else if (NOT StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?offset=' & arguments.offset);
		} else {
			local.HTTPService.setUrl(local.url);
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	public struct function updatePlan(required string planid, required string name) {
		
		local.HTTPService = createHTTPService('POST');

		local.HTTPService.setUrl(getBaseUrl() & 'plans/' & arguments.planid);
		local.HTTPService.addParam(type='formfield',name='name',value=Trim(arguments.name));
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	public struct function deletePlan(required string planid) {
	
		local.HTTPService = createHTTPService('DELETE');

		local.HTTPService.setUrl(getBaseUrl() & 'plans/' & arguments.planid);
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}


	/* SUBCRIPTIONS */	
	public struct function updateSubscription(required string customerid, required string planid, string coupon='', boolean prorate=true, timestamp trial_end, any card) {
		
		local.HTTPService = createHTTPService('POST');
		local.HTTPService.addParam(type='formfield',name='plan',value=arguments.planid);

		local.HTTPService.setUrl(getBaseUrl() & 'customers/' & arguments.customerid & '/subscription');
		if (Len(Trim(arguments.coupon))) {
			local.HTTPService.addParam(type='formfield',name='coupon',value=Trim(arguments.coupon));
		}
		local.HTTPService.addParam(type='formfield',name='prorate',value=arguments.prorate);
		if (StructKeyExists(arguments,'trial_end') AND IsDate(arguments.trial_end)) {
			local.intUTCDate = timeToUTCInt(arguments.trial_end);
			local.HTTPService.addParam(type='formfield',name='trial_end',value=local.intUTCDate);
		}
		if (StructKeyExists(arguments,'card') AND isStruct(arguments.card)) {
			local.HTTPService.addParam(type='formfield',name='card[number]',value=arguments.card.number);
			local.HTTPService.addParam(type='formfield',name='card[exp_month]',value=arguments.card.exp_month);
			local.HTTPService.addParam(type='formfield',name='card[exp_year]',value=arguments.card.exp_year);
			if (StructKeyExists(arguments,'card.cvc')) {
				local.HTTPService.addParam(type='formfield',name='card[cvc]',value=arguments.card.cvc);
			}
			if (StructKeyExists(arguments,'card.name') AND Len(Trim(arguments.card.name))) {
				local.HTTPService.addParam(type='formfield',name='card[name]',value=arguments.card.name);
			}
			if (StructKeyExists(arguments,'card.address_line1') AND Len(Trim(arguments.card.address_line1))) {
				local.HTTPService.addParam(type='formfield',name='card[address_line1]',value=Trim(arguments.card.address_line1));
			}
			if (StructKeyExists(arguments,'card.address_line2') AND Len(Trim(arguments.card.address_line2))) {
				local.HTTPService.addParam(type='formfield',name='card[address_line2]',value=Trim(arguments.card.address_line2));
			}
			if (StructKeyExists(arguments,'card.address_zip') AND Len(Trim(arguments.card.address_zip))) {
				local.HTTPService.addParam(type='formfield',name='card[address_zip]',value=Trim(arguments.card.address_zip));
			}
			if (StructKeyExists(arguments,'card.address_state') AND Len(Trim(arguments.card.address_state))) {
				local.HTTPService.addParam(type='formfield',name='card[address_state]',value=Trim(arguments.card.address_state));
			}
			if (StructKeyExists(arguments,'card.address_country') AND Len(Trim(arguments.card.address_country))) {
				local.HTTPService.addParam(type='formfield',name='card[address_country]',value=Trim(arguments.card.address_country));
			}
		} else if (StructKeyExists(arguments,'card')) {
			local.HTTPService.addParam(type='formfield',name='card',value=Trim(arguments.card));
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	public struct function deleteSubscription(required string customerid, boolean at_period_end=false) {
	
		local.HTTPService = createHTTPService('DELETE');

		local.HTTPService.setUrl(getBaseUrl() & 'customers/' & arguments.customerid & '/subscription');
		local.HTTPService.addParam(type='formfield',name='at_period_end',value=arguments.at_period_end);
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}


	/* INVOICE ITEMS */
	public struct function createInvoiceItem(required string customerid, required numeric amount, string currency=getCurrency(), string description='') {
		
		local.HTTPService = createHTTPService('POST');

		local.HTTPService.setUrl(getBaseUrl() & 'invoiceitems');
		local.HTTPService.addParam(type='formfield',name='customer',value=Trim(arguments.customerid));
		local.amount = amountToCents(arguments.amount); // convert amount to cents for Stripe
		local.HTTPService.addParam(type='formfield',name='amount',value=local.amount);
		local.HTTPService.addParam(type='formfield',name='currency',value=arguments.currency);	   
		if (Len(Trim(arguments.description))) {
			local.HTTPService.addParam(type='formfield',name='description',value=Trim(arguments.description));
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	public struct function readInvoiceItem(string invoiceitemid='', string customerid='', numeric count, numeric offset) {
		
		local.HTTPService = createHTTPService('GET');

		local.url = getBaseUrl() & 'invoiceitems';
		if (Len(Trim(arguments.invoiceitemid))) {
			local.HTTPService.setUrl(local.url & '/' & arguments.invoiceitemid);
		} else if (Len(Trim(arguments.customerid)) AND StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?customer=' & arguments.customer & 'count=' & arguments.count & '&offset=' & arguments.offset);
		} else if (Len(Trim(arguments.customerid)) AND StructKeyExists(arguments,'count') AND NOT StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?customer=' & arguments.customer & 'count=' & arguments.count);
		} else if (Len(Trim(arguments.customerid)) AND NOT StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?customer=' & arguments.customer & '&offset=' & arguments.offset);
		} else if (Len(Trim(arguments.customerid))) {
			local.HTTPService.setUrl(local.url & '?customer=' & arguments.customer);
		} else if (StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?count=' & arguments.count & '&offset=' & arguments.offset);
		} else if (StructKeyExists(arguments,'count') AND NOT StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?count=' & arguments.count);
		} else if (NOT StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?offset=' & arguments.offset);
		} else {
			local.HTTPService.setUrl(local.url);
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	public struct function updateInvoiceItem(required string invoiceitemid, required numeric amount, string currency=getCurrency(), string description='') {
		
		local.HTTPService = createHTTPService('POST');

		local.HTTPService.setUrl(getBaseUrl() & 'invoiceitems/' & arguments.invoiceitemid);
		local.amount = amountToCents(arguments.amount);
		local.HTTPService.addParam(type='formfield',name='amount',value=local.amount);
		local.HTTPService.addParam(type='formfield',name='currency',value=Trim(arguments.currency));
		if (Len(Trim(arguments.description))) {
			local.HTTPService.addParam(type='formfield',name='description',value=Trim(arguments.description));
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	public struct function deleteInvoiceItem(required string invoiceitemid) {
	
		local.HTTPService = createHTTPService('DELETE');

		local.HTTPService.setUrl(getBaseUrl() & 'invoiceitems/' & arguments.invoiceitemid);
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	
	/* INVOICES */	
	public struct function readInvoice(string invoiceid='', string customerid='', numeric count, numeric offset, boolean upcoming=false) {
		
		local.HTTPService = createHTTPService('GET');

		local.url = getBaseUrl() & 'invoices';
		if (Len(Trim(arguments.invoiceid))) {
			local.HTTPService.setUrl(local.url & '/' & arguments.invoiceid);
		} else if (Len(Trim(arguments.customerid)) AND StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?customer=' & arguments.customerid & 'count=' & arguments.count & '&offset=' & arguments.offset);
		} else if (Len(Trim(arguments.customerid)) AND StructKeyExists(arguments,'count') AND NOT StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?customer=' & arguments.customerid & 'count=' & arguments.count);
		} else if (Len(Trim(arguments.customerid)) AND NOT StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?customer=' & arguments.customerid & '&offset=' & arguments.offset);
		} else if (Len(Trim(arguments.customerid)) AND arguments.upcoming) {
			local.HTTPService.setUrl(local.url & '/upcoming/?customer=' & arguments.customerid);
		 } else if (Len(Trim(arguments.customerid)) AND NOT arguments.upcoming) {
			local.HTTPService.setUrl(local.url & '?customer=' & arguments.customerid);
		} else if (StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?count=' & arguments.count & '&offset=' & arguments.offset);
		} else if (StructKeyExists(arguments,'count') AND NOT StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?count=' & arguments.count);
		} else if (NOT StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?offset=' & arguments.offset);
		} else {
			local.HTTPService.setUrl(local.url);
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	
	/* COUPONS */
	public struct function createCoupon(required numeric percent_off, required string duration, string couponid='', numeric duration_in_months, numeric max_redemptions, timestamp redeem_by) {
	
		local.HTTPService = createHTTPService('POST');

		local.HTTPService.setUrl(getBaseUrl() & 'coupons');
		local.HTTPService.addParam(type='formfield',name='percent_off',value=arguments.percent_off);
		local.HTTPService.addParam(type='formfield',name='duration',value=arguments.duration);
		if (Len(Trim(arguments.couponid))) {
			local.HTTPService.addParam(type='formfield',name='id',value=arguments.couponid);
		}
		if (StructKeyExists(arguments,'duration_in_months')) {
			local.HTTPService.addParam(type='formfield',name='duration_in_months',value=arguments.duration_in_months);
		}
		if (StructKeyExists(arguments,'max_redemptions')) {
			local.HTTPService.addParam(type='formfield',name='max_redemptions',value=arguments.max_redemptions);
		}
		if (StructKeyExists(arguments,'redeem_by')) {
			local.redeem_by = timeToUTCInt(arguments.redeem_by);
			local.HTTPService.addParam(type='formfield',name='redeem_by',value=local.redeem_by);
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	public struct function readCoupon(string couponid='', numeric count, numeric offset) {
		
		local.HTTPService = createHTTPService('GET');

		local.url = getBaseUrl() & 'coupons';
		if (Len(Trim(arguments.couponid))) {
			local.HTTPService.setUrl(local.url & '/' & arguments.couponid);
		} else if (StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?count=' & arguments.count & '&offset=' & arguments.offset);
		} else if (StructKeyExists(arguments,'count') AND NOT StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?count=' & arguments.count);
		} else if (NOT StructKeyExists(arguments,'count') AND StructKeyExists(arguments,'offset')) {
			local.HTTPService.setUrl(local.url & '?offset=' & arguments.offset);
		} else {
			local.HTTPService.setUrl(local.url);
		}
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}
	
	public struct function deleteCoupon(required string couponid) {
	
		local.HTTPService = createHTTPService('DELETE');
		local.HTTPService.setUrl(getBaseUrl() & 'coupons/' & arguments.couponid);
		local.HTTPResult = local.HTTPService.send().getPrefix();
		
		if (NOT isDefined("local.HTTPResult.statusCode")) {
			throw(type='Stripe',errorcode="stripe_unresponsive", message="The Stripe server did not respond.", detail="The Stripe server did not respond.");
		} else if (left(local.HTTPResult.statusCode,3) NEQ "200") {
			throw(type='Stripe',errorcode=local.HTTPResult.statusCode, message=local.HTTPResult.statuscode, detail=local.HTTPResult.filecontent);
		}
		return deserializeJSON(local.HTTPResult.filecontent);
	}

	
	/* HELPER FUNCTIONS */	
	private HTTP function createHTTPService(string urlmethod='POST', numeric httptimeout=1000) {
		local.HTTPService = new HTTP();
		local.HTTPService.setUsername(getStripeApiKey());
		local.HTTPService.setPassword('');
		local.HTTPService.setMethod(arguments.urlmethod);
		local.HTTPService.setCharset('utf-8');
		local.HTTPService.setTimeout(arguments.httptimeout);

		return local.HTTPService;
	}
	
	private numeric function amountToCents(required numeric amount) {
		local.amount = arguments.amount * 100; // convert amount to cents for Stripe
		return local.amount;
	}
	
	public numeric function centsToAmount(required numeric cents) {
		local.amount = arguments.cents / 100; // convert cents to dollars
		return local.amount;
	}
	
	private numeric function timeToUTCInt(required timestamp date) {
		local.currUTCDate = DateConvert('local2utc',arguments.date);
		local.baseDate = CreateDateTime(1970,1,1,0,0,0);
		local.intUTCDate = DateDiff('s',local.baseDate,local.currUTCDate);
		return local.intUTCDate;
	}
	
	public datetime function utcIntToTime(required numeric intUtcTime) {
		local.baseDate = CreateDateTime(1970,1,1,0,0,0);
		return DateAdd('s',arguments.intUtcTime,DateConvert('utc2Local',local.baseDate));;
	}
	
}