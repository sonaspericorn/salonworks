<cfcomponent
	displayname="Application"
	output="true"
	hint="Handle the application.">

	<!--- Set up the application. --->
	<cfset THIS.SessionManagement = true />
	<cfset THIS.SetClientCookies = true />
	<cfif FindNoCase('salonworks.com',cgi.HTTP_HOST)>
		<cfset THIS.project_stage = "production" />
	<cfelseif FindNoCase('salonworks_staging.com',cgi.HTTP_HOST)>
		<cfset THIS.project_stage = "staging" />	
	<cfelse>
		<cfset THIS.project_stage = "development" />
	</cfif>
	<cfset this.datasources["salonworks"] = {
	  class: 'com.microsoft.sqlserver.jdbc.SQLServerDriver'
	, bundleName: 'mssqljdbc4'
	, bundleVersion: '4.0.2206.100'
	, connectionString: 'jdbc:sqlserver://salonworks.database.windows.net:1433;DATABASENAME=salonworks;sendStringParametersAsUnicode=true;SelectMethod=direct'
	, username: 'coldfusion'
	, password: "encrypted:40baf863d9fc3a38176b757a5aa9648f0e3c05fa4511a137ebdb33136cac422a"
	, connectionLimit:100
}>



	<!--- CF application definition --->
	<cfinclude template="environment/#this.project_stage#/app.cfm">
	<!--- Define the page request properties. --->
	<cfsetting requesttimeout="20" showdebugoutput="true" enablecfoutputonly="false" />

	<cffunction name="OnApplicationStart"
		access="public"
		returntype="boolean"
		output="false"
		hint="Fires when the application is first created.">

		<cfmodule template="environment/#this.project_stage#/config.cfm">
		<cfmodule template="environment/config.cfm">

		<cfset globalvars = attributes.globalvars />

		<!--- append to application scope --->
		<cflock scope="application" type="exclusive" timeout="3" throwontimeout="yes">
			<cfset StructAppend(application, globalvars, true)>
		</cflock>

		<!--- Return out. --->
		<cfreturn true />
	</cffunction>


	<cffunction name="OnSessionStart"
		access="public"
		returntype="void"
		output="false"
		hint="Fires when the session is first created.">

		<!--- Return out. --->
		<cfreturn />
	</cffunction>


	<cffunction name="OnRequestStart"
		access="public"
		returntype="boolean"
		output="false"
		hint="Fires at first part of page processing.">
		<!--- Define arguments. --->
		<cfargument
			name="TargetPage"
			type="string"
			required="true"
			/>
			
		<cftry>
		<cfif isDefined("url.resetApp")>
			<cfset OnApplicationStart() />
		</cfif>

 		<!--- <cfparam name="session.company_id" default="0">
 		<cfparam name="session.professional_id" default="0"> --->
 		<cfparam name="request.dsn" default="salonworks">
 		<cfset application.sitekey= '6LcPh48UAAAAAKGpWMSzzkhZbJMrmvBG1GIF66P-'>
 		<cfset application.secretkey='6LcPh48UAAAAAJpCntp20nngkah_0EwZejswGONC'>
 		<cfset variables.subdomain = "www">
		<cfset application.subdomain = "www">
		<cfif structKeyExists(url,"subDomain") and len(url.subDomain) >
			<cfset variables.subdomain = replace(replace(url.subDomain, "/", ""), "\", "")>
			<cfset application.subdomain = replace(replace(url.subDomain, "/", ""), "\", "")>
		</cfif>

		<cfinvoke component="admin.company" method="getCompany" returnvariable="qCompany">
			<cfinvokeargument name="Web_Address" value="#variables.subdomain#">
		</cfinvoke>

		<cfif variables.subdomain neq "www">
			<cfset local.query_string = replace(cgi.QUERY_STRING,"subDomain=#variables.subdomain#","")>
			<cfif qCompany.recordcount>
				<cfif cgi.SCRIPT_NAME eq "/index.cfm" and not structKeyExists(url,"customer")>
					<cflocation url="/#variables.subdomain##cgi.SCRIPT_NAME#?customer=true#local.query_string#" addtoken="no">
				</cfif>
			<cfelse>
				<cflocation url="/index.cfm" addtoken="no">
			</cfif>	
		</cfif>

		<!--- <cfset variables.subdomain = ListgetAt(cgi.server_name,1,'.')> --->
 		<cfif not findNoCase("www", cgi.request_url) and ((variables.subdomain eq 'www') or (variables.subdomain eq 'salonworks')) >
 			<cflocation url="http://www.SalonWorks.com#cgi.SCRIPT_NAME#" addtoken="no">
 		</cfif>
		<!--- <cfdump var="#variables.subdomain#" abort="true"> --->
		<!--- <cfset application.bugLogService=createObject("component","buglog.client.bugLogService").init(bugLogListener="buglog.listeners.bugLogListenerWS",bugEmailRecipients="jacinth@spericorn.com",bugEmailSender="aswathi.k@spericorn.com")> --->
 		<cfset application.devpath="../../">
 		<cfset application.filePath="/dev/templates/0001/">
 		<cfset application.projectpath = "/dev/">
 		<cfset application.objMailgun =  createObject("component","cfc.mailgun") />
		<!--- <cfif session.Company_ID gt 0>
			<cfset variables.Company_ID = session.Company_ID>
		</cfif> --->
		<cfinvoke component="admin.company" method="getCompany" returnvariable="qCompany">
			<cfinvokeargument name="Web_Address" value="#variables.subdomain#">
		</cfinvoke>
		<!--- <cfdump var="#qCompany#" abort="true"> --->
		<!--- <cfif session.Company_ID eq 0> --->
			<cfif qCompany.recordcount gt 0 AND !ListFindNoCase("www,SalonWorks",variables.subdomain)>
				<cfset variables.Company_ID=qCompany.Company_ID>
				<!--- <cfset variables.Template_ID=qCompany.Template_ID> --->
			<cfelse>
				<cfset variables.Company_ID=0>
				<!--- <cfset variables.Template_ID=0> --->
			</cfif>
		<!--- </cfif> --->
		<cfinvoke component="admin.location" method="getLocation" returnvariable="qLocation">
			<cfinvokeargument name="Company_ID" value="#variables.Company_ID#">
		</cfinvoke>

		<cfif qLocation.recordcount gt 0>
			<cfset variables.Location_ID=qLocation.Location_ID>
		<cfelse>
			<cfset variables.Location_ID=0>
		</cfif>
		<cfinvoke component="admin.professionals" method="getProfessional" returnvariable="qProfessional">
			<cfinvokeargument name="Location_ID" value="#variables.Location_ID#">
			<cfinvokeargument name="Company_ID" value="#variables.Company_ID#">
		</cfinvoke>
		<!--- Site Log  --->
		<cfif THIS.project_stage eq "production">
			<!--- <cfquery name="sitelog" datasource="salonworks">
				INSERT INTO Site_Log
				(Company_ID
				,Page
				,URL_Parameters
				,Referrer
				,IP_Address)
				Values
				(<cfqueryparam value="#variables.Company_ID#" cfsqltype="CF_SQL_INTEGER">
				,<cfqueryparam value="#cgi.PATH_INFO#" cfsqltype="CF_SQL_VARCHAR">
				,<cfqueryparam value="#cgi.QUERY_STRING#" cfsqltype="CF_SQL_VARCHAR">
				,<cfqueryparam value="#cgi.HTTP_REFERER#" cfsqltype="CF_SQL_VARCHAR">
				,<cfqueryparam value="#cgi.REMOTE_ADDR#" cfsqltype="CF_SQL_VARCHAR">
				)
			</cfquery> --->
		</cfif>
		<!--- Return out. --->
		<cfreturn true />
				<cfcatch>
					<cfdump var="#cfcatch#" abort="true">
				</cfcatch>
			</cftry>
	</cffunction>

	<cffunction name="OnRequest"
		access="public"
		returntype="void"
		output="true"
		hint="Fires after pre page processing is complete.">

		<!--- Define arguments. --->
		<cfargument
			name="TargetPage"
			type="string"
			required="true"
			/>
			<!--- <cfdump var="#url#">--->
			<!--- <cfdump var="#arguments#" abort="true">  --->
			<!--- Block public users from dev site --->
		<!--- <cfif not ListContains('127.0.0.1,99.103.70.43,107.194.73.65,75.103.109.211,173.172.44.148,89.205.33.15,220.227.196.145,13.58.245.95',cgi.REMOTE_ADDR) AND THIS.project_stage neq "production"> <cfabort></cfif> --->
		<!--- <table width="100%" bgcolor="red"><tr><td align="center"><span style="color:white;">Dev Site</span></td></tr></table> --->
		<!--- <cfif not findNoCase("https://www", cgi.request_url)>
 			<cflocation url="https://www.salonworks.com#cgi.SCRIPT_NAME#?#QUERY_STRING#" addtoken="no">
 		</cfif> --->

		<cfif isDefined('url.u')>
		<!--- So that people can't predict userIDs i'm putting the userid between 3 digits to the left and 2 digits to the right --->
			<cfset session.u=url.u>
			<cfset session.u=Left(session.u,8)>
			<cfset session.u=Right(session.u,5)>
		</cfif>
		<cfif isDefined('session.u')>
			<cfquery name="getCosmotrainingUser" datasource="preferredce">
				SELECT
					FirstName
					,LastName
					,Address
					,Address2
					,City
					,State
					,PostalCode
					,Email
					,Telephone
				FROM
					Students
				WHERE
					StudentID=<cfqueryparam value="#session.u#" cfsqltype="CF_SQL_INTEGER">
			</cfquery>
		</cfif>

		<!--- Include the requested page. --->
		<cfinclude template="#ARGUMENTS.TargetPage#" />

		<!--- Return out. --->
		<cfreturn />
	</cffunction>


	<cffunction name="OnRequestEnd"
		access="public"
		returntype="void"
		output="true"
		hint="Fires after the page processing is complete.">
		<!--- Return out. --->
		<cfreturn />
	</cffunction>


	<cffunction name="OnSessionEnd"
		access="public"
		returntype="void"
		output="false"
		hint="Fires when the session is terminated.">

		<!--- Define arguments. --->
		<cfargument
			name="SessionScope"
			type="struct"
			required="true"
			/>

		<cfargument
			name="ApplicationScope"
			type="struct"
			required="false"
			default="#StructNew()#"
			/>

		<!--- Return out. --->
		<cfreturn />
	</cffunction>


	<cffunction name="OnApplicationEnd"
		access="public"
		returntype="void"
		output="false"
		hint="Fires when the application is terminated.">

		<!--- Define arguments. --->
		<cfargument
			name="ApplicationScope"
			type="struct"
			required="false"
			default="#StructNew()#"
			/>

		<!--- Return out. --->
		<cfreturn />
	</cffunction>

 	
	<cffunction name="OnError"
		access="public"
		returntype="void"
		output="true"
		hint="Fires when an exception occures that is not caught by a try/catch.">

		<!--- Define arguments. --->
		<cfargument
			name="Exception"
			type="any"
			required="true"
			/>

		<cfargument
			name="EventName"
			type="string"
			required="false"
			default=""
			/> 
			<cfset errormail = application.objMailgun.sendMailViaMailgun(mailTo="reshma.r@spericorn.com",mailFrom="aswathi.k@spericorn.com",mailSubject="error on site",mailHtml = "#Exception#" ) />
			<!--- <cfmail server="smtp-relay.sendinblue.com" port="587" from="aswathi.k@spericorn.com" to="aswathi.k@spericorn.com" subject="error on site" type="HTML" 
			username="salonworks@salonworks.com" password="ZxhMVOAmav7FtIYR"  usetls="true" >
				<h3>Error occurred in SalonWorks.com Front end-live site #now()#</h3>
				<div>
					--------------------------------------------
					<cfdump var="#Exception#"/>
				</div>
			</cfmail> --->
			<cfquery name="bugLogEntry" datasource="salonworks_buglog">
				
				INSERT INTO [bl_Entry]
		           (
		           	[myDateTime]
		           ,[Message]
		           ,[ApplicationID]
		           ,[SourceID]
		           ,[SeverityID]
		           ,[HostID]
		           ,[ExceptionMessage]
		           ,[ExceptionDetails]
		           ,[UserAgent]
		           ,[TemplatePath]
		           ,[HTMLReport]
		           ,[createdOn])
		     VALUES
		           (
		           	getdate()
		           ,'#Exception.Message#'
		           ,6
		           ,1
		           ,1
		           ,1
		           ,'#Exception.Message#'
		           ,'#Exception.Detail#'
		            ,'#CGI.HTTP_USER_AGENT#'
		           ,'#CGI.CF_TEMPLATE_PATH#'
		           ,''
		           ,getdate())
 
			</cfquery> 
			<!--- <cfif structKeyExists(application,"bugLogService")>
				<cfdump var="#arguments#"> 
				<cfset application.bugLogService.notifyService("Error report from salonworks",#Exception#,'extrainfo')>
			</cfif> --->

		<!--- Return out. --->
		<cfreturn />
	</cffunction>
	
</cfcomponent>