<!doctype html>
<html class="no-js" lang="en">
<cfset strPath = "">
    <head>
         <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <title>Index</title>
        <link rel="icon" type="image/x-icon" href="" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<cfoutput>#strPath#</cfoutput>css/bootstrap.css">
      <link rel="stylesheet" href="<cfoutput>#strPath#</cfoutput>css/main.css">
      <link rel="stylesheet" href="<cfoutput>#strPath#</cfoutput>css/animations.css">
      <link rel="stylesheet" href="<cfoutput>#strPath#</cfoutput>css/animate.min.css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="salonnewhome/js/vendor/modernizr-2.8.3.min.js"></script>
        <script language="javascript" src="/dev/js/indexscript.js"></script>
		 <!--- Google Tag Manager --->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-K86HR56');</script>
		<!--- End Google Tag Manager --->
    </head>
    <body>
	<!--- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K86HR56"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) --->
    <div class="col-sm-12 p-0 nav-otr">
    <nav class="navbar navbar-expand-lg navbar-light bg-light mx-auto container-secn">
      <a class="navbar-brand" href="#"><img src="img/logo.png"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.cfm">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item feature">
            <a class="nav-link" href="index.cfm#feature">Features</a>
          </li>
          <!---<li class="nav-item pricing">
            <a class="nav-link" href="index.cfm#pricing">Pricing</a>
          </li>--->
           <li class="nav-item support">
            <a class="nav-link" href="" data-toggle="modal" data-target="#modalSupport">Support</a>
          </li>
          <li class="nav-item signup">
            <a class="nav-link log-clr" href="index.cfm#signup"><i class="far fa-user"></i> SignUp </a>
          </li>
           <li class="nav-item">
            <a class="nav-link log-dash ml-0" href="#">|</a>
          </li>
          <li class="nav-item">
            <a class="nav-link log-clr ml-0" href="" data-toggle="modal" data-target="#modalLogin"><i class="fas fa-lock"></i> Login</a>
          </li>
        </ul>
       </div>
    </nav>
</div>
<div class="col-md-12 bg-trms">
    <h2>Terms and Conditions</h2>
</div>   
<div class="col-md-12 p-0 sldr-otr">
   <div class="col-md-12 container-secn-trm mx-auto">
        <div class="row">
          <div class="col-md-12 trm-cnd">
            <h3>Website Terms and Conditions of Use</h3>
            <div class="col-md-12 top-trm p-0">
              <h4><span>1.</span> Terms</h4>
              <div class="col-md-12 cont-trm">
                By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trade mark law.
              </div>
            </div>
            <div class="col-md-12 top-trm p-0">
              <h4><span>2.</span> Use License</h4>
              <div class="col-md-12 cont-trm">
                  <ul>
                    <li>
                    <span>a.</span> Permission is granted to temporarily download one copy of the materials (information or software) on SalonWorks's web site for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:
                    <ul class="copy-ul">
                      <li><span>i</span> modify or copy the materials;</li>
                      <li><span>ii</span> use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li>
                      <li><span>iii</span> attempt to decompile or reverse engineer any software contained on SalonWorks's web site;</li>
                      <li><span>iv</span> remove any copyright or other proprietary notations from the materials; or</li>
                      <li><span>v</span> transfer the materials to another person or "mirror" the materials on any other server.</li>
                    </ul>
                  </li>
                  <li class="tpr-tp">
                     <span>b.</span> This license shall automatically terminate if you violate any of these restrictions and may be terminated by SalonWorks at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.
                  </li>
                  </ul>
              </div>
            </div>
            <div class="col-md-12 top-trm p-0">
              <h4><span>3.</span> Disclaimer</h4>
              <div class="col-md-12 cont-trm">
                  <ul>
                    <li>
                    <span>a.</span> The materials on SalonWorks's web site are provided "as is". SalonWorks makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, SalonWorks does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.
                   </li>
                  </ul>
              </div>
            </div>
            <div class="col-md-12 top-trm p-0">
              <h4><span>4.</span> Limitations</h4>
              <div class="col-md-12 cont-trm">
                In no event shall SalonWorks or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on SalonWorks's Internet site, even if SalonWorks or a SalonWorks authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.
              </div>
            </div>
            <div class="col-md-12 top-trm p-0">
              <h4><span>5.</span> Revisions and Errata</h4>
              <div class="col-md-12 cont-trm">
               The materials appearing on SalonWorks's web site could include technical, typographical, or photographic errors. SalonWorks does not warrant that any of the materials on its web site are accurate, complete, or current. SalonWorks may make changes to the materials contained on its web site at any time without notice. SalonWorks does not, however, make any commitment to update the materials.
              </div>
            </div>
            <div class="col-md-12 top-trm p-0">
              <h4><span>6.</span> Links</h4>
              <div class="col-md-12 cont-trm">
              SalonWorks has not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by SalonWorks of the site. Use of any such linked web site is at the user's own risk.
              </div>
            </div>
            <div class="col-md-12 top-trm p-0">
              <h4><span>7.</span> Refund Policy</h4>
              <div class="col-md-12 cont-trm">
              To request a refund, send an e-mail to refunds@salonworks.com.
              </div>
            </div>
            <div class="col-md-12 top-trm p-0">
              <h4><span>8.</span> Site Terms of Use Modifications</h4>
              <div class="col-md-12 cont-trm">
             SalonWorks may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use..
              </div>
            </div>
             <div class="col-md-12 top-trm p-0">
              <h4><span>9.</span> Governing Law</h4>
              <div class="col-md-12 cont-trm">
                Any claim relating to SalonWorks's web site shall be governed by the laws of the State of Texas without regard to its conflict of law provisions.
                General Terms and Conditions applicable to Use of a Web Site.
              </div>
            </div>
            <div class="col-md-12 top-trm p-0">
              <h4>Privacy Policy</h4>
              <div class="col-md-12 cont-trm">
               Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.
                  <ul>
                  </ul>
              </div>
            </div>
          </div>
        </div>
  </div>
</div> 
<footer class="footer">
  <div class="container-fluid container-secn featr-otr mx-auto">
    <div class="row">
      <div class="col-sm-4 ft-img">
        <img src="img/logo-f.png">
      </div>
      <div class="col-sm-2 ft-list">
        <ul class="footer_list">
          <li>
             <a  href="index.cfm">Home <span class="sr-only">(current)</span></a>
          </li>
          <li>
             <a  href="index.cfm#feature">Features</a>
          </li>
          <!---<li>
            <a  href="index.cfm#pricing">Pricing</a>
          </li>--->
          <li>
            <a href="" data-toggle="modal" data-target="#modalSupport">SUPPORT</a>
          </li>
          <li>
            <a  href="index.cfm#signup">SignUp for free </a>
          </li>
        </ul>
      </div>
      <div class="col-sm-3 sal-txt">
       <h3>SALONWORKS</h3>
       <div class="col-sm-12 p-0 sl-desc">
        Salon and spa management tool designed to bring simplicity to growing your clientele and operating your business in the digital age.
       </div>
      </div>
      <div class="col-sm-3 sal-txt plf">
         <h3>CONTACT US</h3>
         <div class="col-sm-12 p-0 sl-icn">
          <i class="fas fa-envelope"></i> <span>(978) 352-0235</span>

         </div>
         <div class="col-sm-12 p-0 sl-icn">
          <i class="fas fa-phone ph-sec"></i> <span>salonworks@salonworks.com</span>
         </div>
      </div>
    </div>
  </div>
  <div class="col-sm-12 p-0 ftr-pd">
     <div class="container-fluid container-secn featr-otr mx-auto">
      <div class="row">
      <div class="col-sm-6">
        <ul class="trm">
          <li><a href="terms.cfm">Terms and conditions</a> </li>
          <li>Copyright 2013 - 2018 ©SalonWorks</li>
        </ul>
      </div>
       <div class="col-sm-6 text-right sl-otr">
        <ul class="social">
          <li>Follow Us on</li>
          <li><i class="fab fa-facebook-f"></i></li>
        </ul>
      </div>
    </div>
  </div>
</div>
</footer>

<!-- Modal -->
<div class="modal fade video-learn" id="video-learn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <iframe width="100%" height="515" src="//www.youtube.com/embed/Jz-W4mLjkwA?autoplay=0" frameborder="0" allowfullscreen=""></iframe>
      </div>
     
    </div>
  </div>
</div>

<!-- The Modal login >> -->
<div class="modal fade loginModal" id="modalLogin">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal Header -->
         <!-- Modal body -->
         <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Log In</h4>
            <form action="/dev/admin/login.cfm" id="loginFrm" method="POST" class="erroron_form">
               <div class="form-group">
                  <input type="email" name="Email_Address_log" id="login-email" class="form-control" placeholder="Email">
               </div>
               <div class="form-group">
                  <input type="password" class="form-control" name="password" id="login-password" placeholder="Password">
               </div>
               <!-- <div class="form-group form-check">
                  <label class="form-check-label">
                    <input class="form-check-input" type="checkbox"> Remember me
                  </label>
                  </div> -->
               <button type="button" id="loginBtn" class="btn btn-block btn-primary">Log In</button>
               <div id="msgLogin" class="alert">&nbsp;</div>
            </form>
            <a class="float-right link-default" data-dismiss="modal" data-toggle="modal" href="#modalForgotPassword">Forgot password?</a>
         </div>
         <!-- Modal footer -->
         <div class="modal-footer">
            <p>
               Don't have an account? <br>
               <a href="index.cfm#signup">Sign up for your FREE 30 Day Trial now!</a>
            </p>
         </div>
      </div>
   </div>
</div>
<!-- The Modal login << -->
<!-- The Modal Forgot Password >> -->
<div class="modal fade loginModal" id="modalForgotPassword">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal Header -->
         <!-- Modal body -->
         <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Forgot Password</h4>
            <form action="#" id="forgotPassForm" method="POST" class="erroron_form">
               <div class="form-group">
                  <input type="email" class="form-control" name="Email_Address_forgot" id="forgotPassEmail" placeholder="Email" required="required">
               </div>
               <button type="button" class="btn btn-block btn-primary" id="forgotPassSubmit">Send Mail</button>
            </form>
         </div>
         <!-- Modal footer -->
         <div class="modal-footer">
            <p>
               We are ready to help you <br>
               <a href="#">Keep in touch with us and enjoy our services!</a>
            </p>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="emailsuccess" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Your password has been sent to your email address</h4>
        </div>
        <div class="modal-body">
           <div class="form-group btnEmailOkWrapper">
            <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnEmailOk">Ok
            </button>
          </div>
        </div>
      <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
  </div>
  <!-- /.modal --> 
</div>
<!-- The Modal  Forgot Password << -->
<!-- The Modal Support >> -->
<div class="modal fade loginModal modalSupport" id="modalSupport">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->

      <!-- Modal body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Need help? We're here to assist you!</h4>

        <form action="support.cfm" id="modalSupportform" method="POST" class="erroron_form">
          <div class="form-group">
            <input type="text" class="form-control"  placeholder="Name" name="Name" id="support-name" required>
          </div>
          <div class="form-group">
            <input type="email" class="form-control"  placeholder="Email" name="Email" id="support-email">
          </div>
          <div class="form-group">
            <input type="text" class="form-control"  placeholder="Subject" name="Subject" id="support-support">
          </div>
          <div class="form-group">
            <textarea class="form-control" placeholder="Enter Your Question or Message" name="Message"></textarea>
          </div>
          <button type="submit" class="btn btn-block btn-primary" name="submit">Submit</button>
        </form>

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <p>
           We are ready to help you <br>
            <a href="#">Keep in touch and enjoy our services!</a>
        </p>
    </div>
  </div>
</div>
</div>
<div class="modal fade modalsucces" id="modalsucces" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       Your password has been<br> send to your email address
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
       
      </div>
    </div>
  </div>
</div>
<!--- <!-- The Modal login >> -->
<div class="modal fade loginModal" id="modalLogin">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->

      <!-- Modal body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Log In</h4>

        <form action="#">
          <div class="form-group">
            <input type="email" class="form-control" placeholder="Email">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" id="pwd" placeholder="Password">
          </div>
          <!-- <div class="form-group form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="checkbox"> Remember me
            </label>
          </div> -->
          <button type="submit" class="btn btn-block btn-primary">Log In</button>
        </form>

       <a class="float-right link-default" data-dismiss="modal" data-toggle="modal" href="#modalForgotPassword">Forgot password?</a>


      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <p>
            Don't have an account? <br>
            <a href="#">Sign up for your FREE 30 Day Trial now!</a>
        </p>
    </div>
  </div>
</div>
</div>
<!-- The Modal login << -->

<!-- The Modal Forgot Password >> -->
<div class="modal fade loginModal" id="modalForgotPassword">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->

      <!-- Modal body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Forgot Password</h4>

        <form action="#">
          <div class="form-group">
            <input type="email" class="form-control" placeholder="Email">
          </div>
         
          <button type="submit" class="btn btn-block btn-primary">Submit</button>
        </form>

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <p>
           We are ready to help you <br>
            <a href="#">Keep in touch with us and enjoy our services!</a>
        </p>
    </div>
  </div>
</div>
</div>
<!-- The Modal  Forgot Password << --> --->

<!-- The Modal Support >> -->
<div class="modal fade loginModal modalSupport" id="modalSupport">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->

      <!-- Modal body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Need help? We're here to assist you!</h4>

        <form action="#">
          <div class="form-group">
            <input type="text" class="form-control"  placeholder="Name">
          </div>
          <div class="form-group">
            <input type="email" class="form-control" placeholder="Email">
          </div>
          <div class="form-group">
            <input type="text" class="form-control"  placeholder="Subject">
          </div>
          <div class="form-group">
            <textarea class="form-control" placeholder="Enter Your Question or Message"></textarea>
          </div>
          <button type="submit" class="btn btn-block btn-primary">Submit</button>
        </form>

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <p>
           We are ready to help you <br>
            <a href="#">Keep in touch and enjoy our services!</a>
        </p>
    </div>
  </div>
</div>
</div>
<!-- The Modal  Support << -->



        <script src="js/vendor/jquery-3.3.1.slim.min.js"></script>
        <script src="js/vendor/wow.min.js"></script>
        <script src="js/main.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.min.js"></script>
       
       
     
       <!---  <script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 

});
</script>
        <script type="text/javascript">
         $(function()  {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
       var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-success').addClass('btn-default');
            $item.addClass('btn-success');
            allWells.hide();
            $target.show();
            //$target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-success').trigger('click');
});
        </script> --->
        <script type="text/javascript">
    $(document).ready(function() {
        $('html,body').animate({

            scrollTop: $(window.location.hash).offset().top - 90
        }, 500);

    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.play').click(function(event) {
            event.preventDefault();
            //var url = $(this).html(); //this will not work  
            $(".js-video").append('<iframe width="100%" height="421" src="//www.youtube.com/embed/Jz-W4mLjkwA?rel=0&amp;autoplay=1&amp;html5=1" frameborder="0" allowfullscreen=""></iframe>');
            $(this).hide();
            //$('video-poster').css('z-index','-1');

        });
    });
</script>
<script type="text/javascript">
    // Check distance to top and display back-to-top.
    $(window).scroll(function() {
        if ($(this).scrollTop() > 800) {
            $('.iconWrapper').addClass('show-back-to-top');
        } else {
            $('.iconWrapper').removeClass('show-back-to-top');
        }
    });
    // Click event to scroll to top.
    $('.iconWrapper').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
</script>
<script>
    $(document).ready(function() {

        $(".feature").click(function() {
            $('html,body').stop(true, false).animate({

                scrollTop: $(".featr-otur").offset().top - 90
            }, 500);
            return false;
            $('html,body').animate({

                scrollTop: $(window.location.hash).offset().top - 90
            }, 500);

        });
        $(".pricing").click(function() {
            $('html,body').stop(true, false).animate({

                scrollTop: $(".pricing-secn").offset().top - 80
            }, 1000);
            return false;
            $('html,body').animate({

                scrollTop: $(window.location.hash).offset().top - 90
            }, 500);
        });

        $(".signup").click(function() {
            $('html,body').stop(true, false).animate({

                scrollTop: $(".form-web-otr").offset().top - 80
            }, 1000);
            return false;
            $('html,body').animate({

                scrollTop: $(window.location.hash).offset().top - 90
            }, 500);
        });
        $("#loginFrm").validate({
            rules: {
                Email_Address_log: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }

            },
            messages: {
                Email_Address_log: {
                    required: "Email is required"
                },
                password: "Password is required"
            }
        });
        $("#forgotPassForm").validate({
            rules: {
                Email_Address_forgot: {
                    required: true,
                    email: true
                },
            },
            messages: {
                Email_Address_forgot: {
                    required: "Email is required"
                },
            }
        });
        $("#modalSupportform").validate({
            rules: {
                Email: {
                    required: true,
                    email: true
                },
                Name: {
                    required: true
                },
                Subject: {
                    required: true
                },
                Message: {
                    required: true
                }
            },
            messages: {
                Email: {
                    required: "Email is required"
                },
                Name: "Name is required",
                Subject: "Subject is required",
                Message: "Message is required"
            }
        });
        $('#modalSupportform').validate({});
        if ($("#modalSupportform").valid()) {
            $('#modalSupportform').submit();
        } else {
            return false;
        }

    });
</script>
<script type="text/javascript">
    $(".signup-trial").click(function() {
        $('#modalLogin').modal('hide');
        $('html,body').stop(true, false).animate({

            scrollTop: $(".form-web-otr").offset().top - 80
        }, 1000);
        return false;
        $('html,body').animate({

            scrollTop: $(window.location.hash).offset().top - 90
        }, 500);


    });
</script>

<script>
    $(document).ready(function() {
        $('[data-toggle="popover"]').popover();
    });
</script>
<script type="text/javascript">
    $(function() {

        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function(e) {
            // e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-success').addClass('btn-default');
                $item.addClass('btn-success');
                allWells.hide();
                $target.show();
                //$target.find('input:eq(0)').focus();
            }

        });

        allNextBtn.click(function(e) {
            e.preventDefault();
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;
            // $('#register_form').validate({});
            // if ( !$( "#register_form" ).valid()) {

            //    isValid = false;
            // }
            if (curStepBtn == 'step-1') {
                if (!($('input[name=First_Name]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Last_Name]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Password]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Email_Address]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Mobile_Phone]').valid())) {
                    isValid = false;
                }
            } else if ((curStepBtn == 'step-2')) {

                if (!($('input[name=web_address]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Company_Name]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Company_Address]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Company_city]').valid())) {
                    isValid = false;
                }
                if (!($('#Company_State').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Company_Postal]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Company_Phone]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Company_Email]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Company_Fax]').valid())) {
                    isValid = false;
                }
                $('input[name=Contact_Name]').val($('input[name=First_Name]').val() + " " + $('input[name=Last_Name]').val());
                $('input[name=Location_Address]').val($('input[name=Company_Address]').val());
                $('input[name=Location_Address2]').val($('input[name=Company_Address2]').val());
                $('input[name=Location_City]').val($('input[name=Company_city]').val());
                $('#Location_State').val($('#Company_State').val());
                $('input[name=Location_Postal]').val($('input[name=Company_Postal]').val());
                $('input[name=Location_Phone]').val($('input[name=Company_Phone]').val());
                $('input[name=Location_Fax]').val($('input[name=Company_Fax]').val());

            } else {
                if (!($('input[name=Contact_Name]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Contact_Phone]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Location_Address]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Location_City]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Location_State]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Location_Postal]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Location_Phone]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=Location_Fax]').valid())) {
                    isValid = false;
                }
                if (!($('input[name=hiddenRecaptcha]').valid())) {
                    isValid = false;
                }
            }
            $('#bio').val($('#step-1').find('.input-holder').find('.Editor-container').find('.Editor-editor').html());
            $('#Company_Description').val($('#step-2').find('.input-holder').find('.Editor-container').find('.Editor-editor').html());
            $('#Description').val($('#step-3').find('.input-holder').find('.Editor-container').find('.Editor-editor').html());
            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');

            if ((curStepBtn == 'step-3') && isValid) {
                $('#register_form').submit();
            }
        });

        $('div.setup-panel div a.btn-success').trigger('click');

        fnCheckEmailAddress = function() {

            if ($('#Email_Address').val().length) {

                $.ajax({
                    type: "get",
                    url: "<cfoutput>#strPath#</cfoutput>admin/company.cfc",
                    data: {
                        method: "isExistingEmailAddress",
                        EmailAddress: $('#Email_Address').val(),
                        noCache: new Date().getTime()
                    },
                    dataType: "json",

                    // Define request handlers.
                    success: function(objResponse) {
                        // Check to see if request was successful.
                        if (objResponse.SUCCESS) {
                            if (objResponse.DATA) {
                                alert('The Email address, ' + $('#Email_Address').val() + ', entered already exist.  Please enter a different address.');
                                $('#Email_Address').val('');
                                $('#Email_Address').focus();
                            }

                        } else {
                            alert('Error: There is a problem with the system.  Please try again.  If problem presist, contact site help.');
                        }
                    },

                    error: function(objRequest, strError) {
                        alert('Error: There is a problem with the system.  Please try again.  If problem presist, contact site help. ' + strError);
                    }
                });
            }
        }
        //Bio note
        $("#bio").Editor({
                'block_quote': false,
                'undo': false,
                'redo': false,
                'insert_link': false,
                'unlink': false,
                'insert_img': false,
                'print': false,
                'strikeout': false,
                'togglescreen': false,
                'insert_table': false,
                'source': false,
                'hr_line': false,
                'splchars': false,
                'rm_format': false,

            }

        );
        $("#Company_Description").Editor({
                'block_quote': false,
                'undo': false,
                'redo': false,
                'insert_link': false,
                'unlink': false,
                'insert_img': false,
                'print': false,
                'strikeout': false,
                'togglescreen': false,
                'insert_table': false,
                'source': false,
                'hr_line': false,
                'splchars': false,
                'rm_format': false,

            }

        );
        $("#Description").Editor({
                'block_quote': false,
                'undo': false,
                'redo': false,
                'insert_link': false,
                'unlink': false,
                'insert_img': false,
                'print': false,
                'strikeout': false,
                'togglescreen': false,
                'insert_table': false,
                'source': false,
                'hr_line': false,
                'splchars': false,
                'rm_format': false,

            }

        );
        $('.close').click(function() {
            console.log(1);
            // $('#video-learn iframe').attr("src", jQuery("#video-learn iframe").attr("src"));
        });
    });
</script>

    </body>
</html>