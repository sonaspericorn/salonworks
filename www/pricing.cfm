<cfinclude template="site_header.cfm">
<!doctype html>
<html class="no-js" lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">
    <title>Home</title>
    <link rel="icon" type="image/x-icon" href="pricing/img/favicon.png" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="salonnewhome/css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="pricing/css/main.css">
    <link rel="stylesheet" href="salonnewhome/css/slick.css">
    <link rel="stylesheet" href="salonnewhome/css/fontello.css">
    <link rel="stylesheet" href="salonnewhome/css/magnific-popup.css">
    <link href="https://fonts.googleapis.com/css?family=Hind+Vadodara:300,400,500,600|Poppins:100,200,300,400,500,600,700,900" rel="stylesheet">
    <script src="salonnewhome/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  
<div class="free-demo-btn">
  <a data-toggle="modal" href="#freeDemoModal">
    <span>Schedule A Free Demo</span>
</a>
</div>
<section class="pricingSection">
  <div class="bannerPricing">
   <div class="container">
    <div class="pricingHeader">
        <h3>FREE LIFETIME WEBSITE & SETUP</h3>
        <h4>
            free 30 day trial and no credit card is needed and there is no obligation to continue!
        </h4>
    </div>

    <div class="col-md-9 d-flex ml-auto mr-auto pricingaMainOtr">
        <div class="col-md-6 pricingOtrSec">
            <div class="pricingOtr">
                <h3 class="priceHead">
                    FREE
                    <span class="perMnth">/Per Month</span>
                </h3>
                <h4 class="pricesub">
                    Starter Package
                </h4>
                <ul>
                    <li>Free Custom Site For Life</li>
                    <li>Photo Gallery</li>
                    <li>Blog</li>
                    <li class="btnsignup">
                        <a data-toggle="modal" href="#signupModal">
                            <button type="button" class="signupBtn">Sign-Up</button>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-6 pricingOtrSec">
            <div class="pricingOtr priceActive">
                <h3 class="priceHead">
                 <span class="dollr">$</span>
                 <span class="price">49.99</span>
                 <span class="perMnth">/Per Month</span>
             </h3>
             <h4 class="pricesub">
                Proffessional
            </h4>
            <ul>
                <li>Free Custom Site For Life</li>
                <li>Photo Gallery</li>
                <li>Blog</li>
                <li>Online Appointment Booking and Scheduling</li>
                <li>Text and Email Appointment Reminders<li>
                <li>Integrated Point-of-Sale and Credit Card Processing</li>
                <li>Reports</li>
                <li>Customer Management and Tracking</li>
                <li>Email Marketing</li>
                <li>Custom Surveys</li>
                <li class="btnsignup">
                    <a data-toggle="modal" href="#signupModal">
                        <button type="button" class="signupBtn">Sign-Up</button>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!--- <div class="col-md-4 pricingOtrSec">
        <div class="pricingOtr">
            <h3 class="priceHead">
                <span class="dollr">$</span>
                <span class="price">45.99</span>
                <span class="perMnth">/Per Month</span>
            </h3>
            <h4 class="pricesub">
                When paid annually
            </h4>
            <ul>
                <li>Custom Website</li>
                <li>Blog</li>
                <li>Photo Gallery</li>
                <li>Photo Gallery</li>
                <li>Photo Gallery</li>
                <li class="btnsignup">
                    <a data-toggle="modal" href="#signupModal">
                        <button type="button" class="signupBtn">Sign-Up</button>
                    </a>
                </li>
            </ul>
        </div>
    </div> --->
</div>
</div>
</div>
</section>




<!--signup Modal -->
<div class="modal fade freeDemoModal signupModal" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Sign Up</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    <form role="form" action="admin/process_registration.cfm" method="POST" id="register_form" enctype="multipart/form-data" class="erroron_form">
         <div class="forms-otr">           
            <ul class="forms">
                <input type="hidden" id="Web_Address" name="web_address" value="" >
                <li>
                    <div class="input-holder">
                        <input type="text" placeholder="First Name" name="First_Name" required="required" value="" maxlength="30">
                    </div>
                </li>
                <li>
                    <div class="input-holder">
                        <input type="text" placeholder="Last Name" name="Last_Name" required="required" value="" maxlength="30">
                    </div>
                </li>
                <li>
                    <div class="input-holder">
                        <input type="text" placeholder="Email" required="required" name="Email_Address" id="Email_Address" value="">
                    </div>
                </li>
                <li>
                    <div class="input-holder">
                        <input type="password" placeholder="Password" class="form-control" required="required" name="Password" value="">
                    </div>
                </li>
                <li>
                    <div class="input-holder">
                        <input type="text" placeholder="Mobile Phone" class="form-control phone_us" name="Mobile_Phone" value="" required="required">
                    </div>
                </li>
                <li style="padding-bottom: 10px;">
                    <cfoutput>
                        <div class="input-holder">
                            <div class="g-recaptcha" data-sitekey="#application.sitekey#"></div>
                            <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                        </div>
                    </cfoutput>
                </li>
                <!---  <li>
                    <div class="input-holder">
                        <textarea placeholder="Message"></textarea>
                    </div>
                </li> --->
                <!--- <li>
                    <label class="check-box">Yes, contact me to schedule a live demo.
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>
                </li> --->
                <li>
                    <div class="submt-btn">
                        <input type="hidden" value="firstsignup" id="firstsignup" name="firstsignup">
                        <button type="submit" class="sbmt finishBtn" value="Submit" onclick="return fnCheckEmailAddress(isSignup = 1);">SIGN-UP WITH EMAIL</button>
                        <input type="submit" id="finishBtn" style="display: none;">                 
                    </div>
                </li>
                <li>
                    <div class="row"  style="margin-top:20px;">
                        <div class="col-md-6 submt-btn">
                            <div id="facebookBtn" onclick="facebooklogin();">Sign-up with Facebook</div>
                        </div>
                        <div class="col-md-6 submt-btn" style="padding-left: 30px;">
                            <button class="customGoogleBtn" id="googleBtn">Sign-up with Google</button>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </form>
</div>

</div>
</div>
</div>
<!--free demo Modal -->

<!--free demo Modal -->
<div class="modal fade freeDemoModal" id="freeDemoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Schedule A Free Demo</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    <form action="pricing.cfm" name="personolized_demo_form" id="personolized_demo_form" method="POST" class="erroron_form">
         <div class="forms-otr">           
            <ul class="forms">
                <li>
                    <div class="input-holder">
                        <input type="text" placeholder="First Name" required="required" name="first_name_demo">
                    </div>
                </li>
                <li>
                    <div class="input-holder">
                        <input type="text" placeholder="Last Name" required="required" name="last_name_demo">
                    </div>
                </li>
                <li>
                    <div class="input-holder">
                        <input type="text" placeholder="Email" required="required" name="email_id_demo">
                    </div>
                </li>
                <li>
                    <div class="input-holder">
                        <input type="text" placeholder="Mobile Phone" required="required" name="mobile_phone_demo" class="phone_us">
                    </div>
                </li>
                <li>
                    <div class="submt-btn">
                        <button type="submit" class="sbmt" value="Submit" name="personolized_demo_button">Schedule Demo</button>
                    </div>
                </li>
            </ul>
        </div>
    </form>
   </div>

</div>
</div>
</div>
<cfif structKeyExists(form, 'personolized_demo_button')>
    <cfinvoke component="admin.company" method="setPersonalDemo" returnvariable="demo_id">
        <cfinvokeargument name="First_name" value="#form.first_name_demo#">
        <cfinvokeargument name="Last_name" value="#form.last_name_demo#">
        <cfinvokeargument name="Email" value="#form.email_id_demo#">
        <cfinvokeargument name="Mobile" value="#form.mobile_phone_demo#">
    </cfinvoke>
</cfif>
<!--free demo Modal -->

<script src="pricing/js/vendor/jquery-3.3.1.slim.min.js"></script>
<script src="pricing/js/vendor/popper.min.js"></script>
<script src="pricing/js/vendor/bootstrap.min.js"></script>
<script src="pricing/js/vendor/slick.min.js"></script>
<script src="pricing/js/vendor/jquery.magnific-popup.js"></script>
<script src="pricing/js/main.js"></script>
</body>
</html>
<cfinclude template="site_footer.cfm">