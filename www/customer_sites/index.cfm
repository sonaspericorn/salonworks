<cfinvoke component="admin.company" method="getCompany" returnvariable="qCompany">
	<cfinvokeargument name="Web_Address" value="#variables.subdomain#">
</cfinvoke>
<cfif structKeyExists(url,"template_id")>
	<cfset session.Template_ID = url.template_id>
<cfelse>
	<cfif qCompany.Template_ID eq "">
		<cfset variables.Template_ID = 1>
		<cfset session.Template_ID = variables.Template_ID>
	<cfelse>
		<cfset session.Template_ID = qCompany.Template_ID>
	</cfif>
</cfif>
<cfinclude template="customer_header.cfm">
<cfinclude template="/templates/#NumberFormat(session.Template_ID,'0000')#/index.cfm">