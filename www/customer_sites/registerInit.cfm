<cfset Session.LastPageReached = 1 /> 

<!--- Step 1. Professional Information --->
<cfset Session.Form.FirstName = "" />
<cfset Session.Form.LastName = "" />
<cfset Session.Form.LicenseNumber = "" />
<cfset Session.Form.LicenseExpirationMonth = "" />
<cfset Session.Form.LicenseExpirationYear = "" />
<cfset Session.Form.LicenseState = "" />
<cfset Session.Form.HomePhone = "" />
<cfset Session.Form.MobilePhone = "" />
<cfset Session.Form.HomeAddress = "" />
<cfset Session.Form.HomeAddress2 = "" />
<cfset Session.Form.HomeCity = "" />
<cfset Session.Form.HomePostal = "" />
<cfset Session.Form.HomeState = "" />
<cfset Session.Form.EmailAddress = "" />
<cfset Session.Form.Password = "" />
<cfset Session.Form.ServicesOffered = "" />
<cfset Session.Form.Accredidations = "" />
<cfset Session.Form.Bio = "" />


<!--- Step 2. Company Information --->
<cfset Session.Form.WebAddress = "" />
<cfset Session.Form.CompanyName = "" />
<cfset Session.Form.CompanyAddress = "" />
<cfset Session.Form.CompanyAddress2 = "" />
<cfset Session.Form.CompanyCity = "" />
<cfset Session.Form.CompanyState = "" />
<cfset Session.Form.CompanyPostal = "" />
<cfset Session.Form.CompanyPhone = "" />
<cfset Session.Form.CompanyEmail = "" />
<cfset Session.Form.CompanyFax = "" />
<cfset Session.Form.CompanyDescription = "" />

<!--- Step 3. Location Information --->
<cfset Session.Form.LocationName = "" />
<cfset Session.Form.ContactName = "" />
<cfset Session.Form.ContactPhone = "" />
<cfset Session.Form.LocationAddress = "" />
<cfset Session.Form.LocationAddress2 = "" />
<cfset Session.Form.LocationCity = "" />
<cfset Session.Form.LocationState = "" />
<cfset Session.Form.LocationPostal = "" />
<cfset Session.Form.LocationPhone = "" />
<cfset Session.Form.LocationFax = "" />
<cfset Session.Form.Description = "" />
<cfset Session.Form.Directions = "" />
<cfset Session.Form.TimeZoneID = "" />

<cfset Session.Form.SundayHoursFrom = "" />
<cfset Session.Form.MondayHoursFrom = "" />
<cfset Session.Form.TuesdayHoursFrom = "" />
<cfset Session.Form.WednesdayHoursFrom = "" />
<cfset Session.Form.ThursdayHoursFrom = "" />
<cfset Session.Form.FridayHoursFrom = "" />
<cfset Session.Form.SaturdayHoursFrom = "" />

<cfset Session.Form.SundayHoursTo = "" />
<cfset Session.Form.MondayHoursTo = "" />
<cfset Session.Form.TuesdayHoursTo = "" />
<cfset Session.Form.WednesdayHoursTo = "" />
<cfset Session.Form.ThursdayHoursTo = "" />
<cfset Session.Form.FridayHoursTo = "" />
<cfset Session.Form.SaturdayHoursTo = "" />

<cfset Session.Form.PaymentMethodsList = "" />
<cfset Session.Form.ServicesList = "" />
<cfset Session.Form.ParkingFees = "" />
<cfset Session.Form.CancellationPolicy = "" />
<cfset Session.Form.Languages = "" />


