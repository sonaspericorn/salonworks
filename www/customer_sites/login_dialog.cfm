
<html>
<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		.abcRioButton {
	        height: 29px !important;
	        width: 190px !important;
	        border: 1px solid #bbb !important;
	        border-radius: 5px !important;
	      }
	      .abcRioButtonSvgImageWithFallback.abcRioButtonIconImage.abcRioButtonIconImage18 {
	            margin-top: -3px !important;
	      }
	      span.abcRioButtonContents{
	            line-height: 27px !important;
	      }
	      .customGoogleBtn{
            border: 1px solid #eee;
		    height: 45px;
		    width: 350px;
		    border-radius: 4px;
		    padding:4px 26px 0px 20px;
		    font-size: 15px;
		    color: white;
		    box-shadow: 0 2px 4px 0 rgba(0,0,0,.25);
		    background-color: #fff;
		    cursor: pointer;
		    font-family: Helvetica, Arial, sans-serif;
		    letter-spacing: .25px;
		    overflow: hidden;
		    white-space: nowrap;
		    background: #dd4b39;
		    
		  }
		  .fbLoginDiv{
		  	text-align: center;
		  }

		  .customGoogleBtnDiv{
			text-align: center;
		  }

		  #loggingButton{
		  	border: 1px solid #eee;
		    height: 45px;
		    width: 350px;
		    border-radius: 4px;
		    padding:4px 26px 0px 20px;
		    font-size: 15px;
		    color: #fff;
		    box-shadow: 0 2px 4px 0 rgba(0,0,0,.25);
		    background-color: #3B5998;
		    cursor: pointer;
		    font-family: Helvetica, Arial, sans-serif;
		    letter-spacing: .25px;
		    overflow: hidden;
		    white-space: nowrap;
		    margin-bottom: 20px;
		  }

		  #signinModal .modal-content{
		  	    width: 70%;
    			left: 55px;
		  }
		  
		  form#frmSignin {
		    padding: 30px;
   		  }

   		  form#frmRegister {
		    padding: 30px;
   		  }

		.orDiv{
			margin: 32px 0 10px;
		}

		.orDiv-line{
			height: 1px;
			background-color: #ccc;
		}

		.orDiv-text{
			width: 20%;
		    font-size: 16px;
		    background-color: #fff;
		    position: relative;
		    top: -11px;
		    left: 40%;
		    text-align: center;
		}
		  .forgotPassLink{
		  	text-align: center;
		  	margin-top: 20px;
		  }

		.btnSignin{
			background: #3276b1;
		    color: #fff;
		    text-align: center;
		    padding: 10px;
		    font-size: 17px;
		    margin-top: 20px;
		}
		#btnRegister{
			background: #3276b1;
		    color: #fff;
		    text-align: center;
		    padding: 10px;
		    font-size: 17px;
		    margin-top: 20px;
		}
		.frmstyle{
			font-size: 15px;
			height: 45px;
		}

		.fa-facebook {
		  background: #3B5998;
		  color: white;
		  font-size: 20px;
		  float: left;
		}

		.fa-google{
	 		background: #dd4b39;
			color: white;
			font-size: 20px;
		  	float: left;
		}
		@media (max-width: 768px) {
			.a_register {
			   margin: 18px !important; } }
		@media (max-width: 768px) {
			.fieldsReset, .pwd {
				width: 100% !important;
				max-width: 400px !important;
			   margin: 0px auto !important; } }
		@media (max-width: 460px) {
			.customGoogleBtnDiv, .fbLoginDiv {
			    width:80% !important;
			    margin: 15px auto;
			    float: none !important; 
			    text-align: center !important;
		    } 
		    #loggingButton, .customGoogleBtn {
				width: 100% !important;
				padding: 0 !important;
				height: 35px !important;
		    }
		}
		@media (max-width: 370px) {
			.pwd {
			   width: 100%  !important; } }
		/*@media (max-width: 460px) {
			.fbLoginDiv {
			    width: 45% !important;
			    float:left !important; 
			    padding: 10px !important; } }*/
	</style>
</head>
<body>
<div class="modal fade" id="signinModal" tabindex="-1" role="dialog" aria-labelledby="Sign in" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<ul class="nav nav-tabs" id="tabContent">
			    <li class="active" style="width: 50%;text-align: center;font-size:17px;"><a href="#logIn" data-toggle="tab">Log In</a></li>
			    <li style="width: 50%;text-align: center;font-size:17px;"><a href="#signUp" data-toggle="tab">Sign Up<span class="close" data-dismiss="modal" aria-hidden="true">&times;</span></a></li>
			</ul>
			<div class="tab-content">
			    <div class="tab-pane active" id="logIn">
			    	<form name="frmSignin" id="frmSignin" role="form" class="form-horizontal">
			    		<cfoutput>
							<input type="hidden" id="companyid" name="companyid" value="#session.company_id#" />
						</cfoutput>
	                    <div class="fbLoginDiv">
	                       <button id="loggingButton" onclick="return facebooklogin();">
	                       	<a class="fa fa-facebook"></a>
	                       	Log in with Facebook</button>
	                    </div>
		                <div class="customGoogleBtnDiv">
	                      <button class="customGoogleBtn">
						  <a class="fa fa-google"></a>
	                      Log in with Google</button>
	                      	<div class="hide">
		                      	<div id="customBtn" class="customGPlusSignIn">
							    </div>
						    </div>
	                    </div>
	                    <div class="orDiv">
				          <div class="orDiv-line"></div>
				          <div class="orDiv-text">OR</div>
				        </div>
						<!--- <div class="col-sm-12"><hr></div> --->
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" style="height: 45px;font-size: 15px;" id="emailAddress" name="emailAddress" placeholder="Email" class="form-control" maxlength="100" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">	
								<input type="password" style="height: 45px;font-size: 15px;" id="pw" name="pw" class="form-control" placeholder="Password" maxlength="100" />
							</div>
						</div>
						<div id="signin-msg" class="alert"></div>
						<div class="forgotPassLink"><a href="" data-toggle="modal" data-target="#resetPassModal" data-dismiss="modal">Forgot my password?</a></div>
						<div class="btnSignin">Sign In</div>
			    	</form>
			    </div>
			    <div class="tab-pane" id="signUp">
					<form name="frmRegister" id="frmRegister" role="form" class="form-horizontal">
						<div class="fbLoginDiv">
	                       <button id="loggingButton" onclick="return facebooklogin();">
	                       <a class="fa fa-facebook"></a>
	                   	   Sign up with Facebook</button>
	                    </div>
		                <div class="customGoogleBtnDiv">
	                      <button class="customGoogleBtn">
						  <a class="fa fa-google"></a>
	                      Sign up with Google</button>
	                      	<div class="hide">
		                      	<div id="customBtn" class="customGPlusSignIn">
							    </div>
						    </div>
	                    </div>
	                    <div class="orDiv">
				          <div class="orDiv-line"></div>
				          <div class="orDiv-text">OR</div>
				        </div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" id="emailAddress" placeholder="Email" name="emailAddress" class="form-control required frmstyle" required maxlength="100" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" id="firstName" placeholder="First Name" name="firstName" class="form-control required frmstyle" required maxlength="50" />
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" id="lastName" placeholder="Last Name" name="lastName" class="form-control required frmstyle" required maxlength="50" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">	
								<input type="text" id="ph" name="ph" placeholder="Phone no:" class="form-control required phone_us frmstyle" required maxlength="12" /><br />
								<span style="padding-left: 2px;">Example: 512-753-0000</span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="password" id="pw" name="pw" placeholder="Choose a password" class="form-control required frmstyle" required maxlength="20" />
							</div>
						</div>
						<div id="register-msg" class="alert"></div>
						<div class="footer">
							<div id="btnRegister">Continue</div>
							<!--- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" id="btnRegister">Continue</button> --->
						</div>
					</form>
			    </div>
			</div>
		</div>
	</div>
</div>

<cfset resetPassKey = "" />
<cfif isDefined('url.reGenPass') >
	<cfset resetPassKey=url.reGenPass>
</cfif>

<cfif structKeyExists(url, "reGenPass")>
	<input type="hidden" value="1" id="reGenHid">
</cfif>
  <!-- Modal -->
  <div class="modal fade" id="resetPassModal" role="dialog">
    <div class="modal-dialog">
    	<style>
    	.hg{
    		height: 380px;
    	}
    	.ht{height:200px;} .btnForPass{margin: 40px;}
    	</style>
    
      <!-- Modal content-->
      <div class="modal-content hg">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Retrieve Password</h4>
        </div>
        <div class="modal-body ht">
            <p>Enter your email and to receive instructions</p>
            <form action="#" method="POST" id="forgot_form">
	            <div class="">
	          		<input type="email" class="form-control" name="Email_Address_forgot" id="forgotPassEmail" placeholder="Email">
	            </div>
	            <div class="btnForPass">
		            <button type="button" class="width-35 pull-right btn btn-sm btn-danger" id="forgotPassSubmit">
						<i class="icon-lightbulb"></i>
							Send Me!
					</button>
					<p class="error_box_forgot" style="color:red;"></p>
				</div>
			</form>
        </div>
        <div class="modal-footer"> 	
         <!---  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --->
        </div>
      </div>
      
    </div>
  </div>

  <!--- modat reset password --->
  <!-- Modal -->
<cfif structKeyExists(url, "reGenPass")>
	<cfquery name="getPasswordReset" datasource="#request.dsn#" result="rPasswordResetkey">
		SELECT pwdReset FROM customers 
		WHERE resetPassKey = <cfqueryparam value="#url.reGenPass#" cfsqltype="cf_sql_varchar">
	</cfquery>
	<div class="modal fade" id="resetPasswordModal" role="dialog">
		<div class="modal-dialog">
			<style>
			.hg{
			height: 380px;
			}
			.ht{height:200px;} .btnForPass{margin: 40px;} .pwd{width:300px;} .fieldsReset{margin-left: 100px;} .fields{padding: 5px;} .btn-reset{margin-right: 120px;}
			</style>

			<!-- Modal content-->
			<div class="modal-content hg">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Reset Password</h4>
				</div>
				<cfif getPasswordReset.RecordCount and getPasswordReset.pwdReset eq 1>
					<style>.linkExpired{position: absolute;left: 50px;top: 108px;}</style>
					<div class="linkExpired">The current url is no longer valid. Kindly contact web admin for any support</div>
				<cfelse>
					<div class="modal-body ht">
						<form action="#" method="POST" id="forgot_form">
						<div class="fieldsReset">
							<div class="fields">
								<input type="password" class="form-control pwd" name="password" id="password" placeholder="Password">
							</div>
							<div class="fields">
								<input type="password" class="form-control pwd" name="confirmPassword" id="confirmPassword" placeholder="Confirm Password">
								<input type="hidden" id="resetPassKey" name="resetPassKey" value="<cfoutput>#resetPassKey#</cfoutput>">
								<i class="icon-password"></i>
								<div id="passwordError"></div>
							</div>
							<div class="btnForPass">
								<button type="button" class="width-35 pull-right btn btn-sm btn-danger btn-reset" id="resetPasswordBtn" onclick="checkPassword()">
								<i class="icon-lightbulb"></i>
								Reset!
								</button>
								<style>.successMsg{display:none;}.errorMsg{display:none;color:red;}</style>
								<div class="successMsg" id="successMsg">Password reset successfully</div>
								<div class="errorMsg" id="errorMsg">Please fill all the fields</div>
								<p class="error_box_forgot" style="color:red;"></p>
							</div>
							<!--- <div><a href="index.cfm">Back to login</div> --->
							<!--- <a data-target="#signinModal" data-toggle="modal" class="MainNavText" id="MainNavHelp" 
							href="#signinModal">Back to login</a> --->
							</div>
						</form>
					</div>
				</cfif>
				<div class="modal-footer"> 	
				<!---  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --->
				</div>
			</div>
		</div>
	</div>
</cfif>
<!--- /modal reset password --->


<!--- 
<cfset variables.CustomerID = 0 />
<cfset variables.blnFailed = false />
<cfset variables.FailedMsg = ""/>

<div id="dlgTopLogin" title="Sign in">
	<div class="containerPadding">	
	 	<form id="frmTopLogIn" name="frmTopLogIn" action="#Replace(cgi.PATH_INFO,'/','')#" method="post">
			<cfif Len(variables.FailedMsg)>
			<div class="error">#variables.FailedMsg#</div>
			</cfif>
			<div><strong>Login</strong></div>
			<br />
			<div>
				<label>Your email address:</label><br />
				<input type="text" id="loginEmail" name="loginEmail" maxlength="100" />
			</div>
			<br />
			<div>
				<label>Password:</label><br />
				<input type="password" id="loginPassword" name="loginPassword" maxlength="100" />
			</div>
			<br />  
			<div>
				<button type="button" id="btnLogin" onclick="fnTopLogin()" style="width:200px">Login</button>
			</div>
		</form>		
	</div>
	 		 
	<br style="clear: left;" />
</div> --->
</body>
</html>
<script>
	function checkPassword() {
      var password = $('#password').val();
      var confirmPassword = $('#confirmPassword').val(); 
      if(password != "" && confirmPassword != "") {    
	      if(password == confirmPassword) {
				$.ajax({
					// console.log(1);
					type: "POST",
					url: "/cfc/customers.cfc",
					data: {
						method: "resetPassword",
						password: $('#password').val(),
						confirmPassword : $('#confirmPassword').val(),
						resetPassKey : $('#resetPassKey').val(),
						noCache: new Date().getTime()
						},
					dataType: "json",

					// Define request handlers.
					success: function( objResponse ){		

						// $("#successMsg").css("display","block");
						$("#signinModal").modal('show');
						$("#resetPasswordModal").modal('hide');
						// location.href = "login.cfm";
					}
				

					});
						
	      }
	      else {
	          $("#passwordStatus").val(1);    
	          $('#passwordError').empty();    
	          $('#password').css('border-color', 'red');
	          $('#confirmPassword').css('border-color', 'red');
	          $("#errorMsg").css("display","none");
	          $("#passwordError").append("Password does not match with confirm password ");
	      }
  		}else{
  			//alert('please enter password and confirm password.');
  			$("#errorMsg").css("display","block");
	  		return false;   
  		}
  }



	$('#forgotPassSubmit').on('click', function(){
		var email =  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
			$('.error_box_forgot').html("");
			$('.error_box_forgot').hide();
			var subdomain = "<cfoutput>#variables.subdomain#</cfoutput>";
	         var email_user=$.trim($('#forgotPassEmail').val());
	         console.log(email);
	          if(email_user.length ==0) {
	          	 $('.error_box_forgot').html("Email is required");
	          	 $('.error_box_forgot').show();
	          }else if(!email.test(email_user)) {
	          	$('.error_box_forgot').html("Email is not valid");
	          	$('.error_box_forgot').show();
	          }
	         else{
	         	$('.error_box_forgot').html("");
	         	$('.error_box_forgot').hide();
	            $.ajax({
	               url: "/cfc/customers.cfc?method=forgotPasswordCust&showtemplate=false",
	               type: 'POST',
	               data:{email:email_user,subdomain:subdomain},
	               success: function(data){
	                  console.log(data);
	                  if(data==1){
	                  	alert("Your password reset link has been send to your email address");
	                  	$('#forgot_form')[0].reset();
	                  	$('#resetPassModal').modal('hide');
	                  }
	                  else{

	                     alert("Invalid email address.Make sure this is your registered email address");
	                  }
	               },
	            });
	         }
		    });
		$(document).ready(function(){
			var restVal = $("#reGenHid").val();
			if(restVal == 1){
				$("#resetPasswordModal").modal('show');
			}

		});
		function onLoad() {
		    gapi.load('auth2', function() {
		        gapi.auth2.init();
		    });
		}
	</script>
	<script type="text/javascript">
    	var subDomain = "<cfoutput>#application.subdomain#</cfoutput>";
  	</script>

    </body>
    <script src="/js/socialLogin.js"></script>
    <script src="https://apis.google.com/js/api:client.js"></script>
